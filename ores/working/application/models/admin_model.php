<?php

class Admin_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function user_check($user,$pass,$role) {
        $sql = "SELECT * FROM user WHERE username LIKE ".$this->db->escape($user)." and password LIKE ".$this->db->escape($pass)."and role=".$role." and status=1"; 
        $query = $this->db->query($sql);
        $row = $query->result_array();
		
	
        if($row){
			return $row[0];
        }else{
            return false;
        }
    }
	public function add_user($data){
		$this->db->insert('user', $data); 
		$last_id = $this->db->insert_id();
		
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
	}
	/* 
		*	Function Get Latest User Registered 
		*  @author: Gurpreet 
	*/
	public function getLatestUsers(){		
		$this->db->select('id,first_name,last_name,username,email');
		$this->db->from('user');
		$this->db->where('role',1);
		$this->db->order_by("id", "desc"); 
		$this->db->limit(3, 0);		
		$query = $this->db->get();		
		$result = $query->result();	
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/* 
		*	Function Get Latest Drivers Registered 
		*  @author: Gurpreet 
	*/
	public function getLatestDrivers(){		
		$this->db->select('id,first_name,last_name,email');
		$this->db->from('driver');
		//$this->db->where('status',1);
		$this->db->order_by("id", "desc"); 
		$this->db->limit(3, 0);		
		$query = $this->db->get();		
		$result = $query->result();	
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/* 
		*	Function Get Latest Reservation completed
		*  @author: Gurpreet 
	*/
	public function getLatestreservations(){

		$this->db->select('reservation.id,user.first_name as u_fname, user.last_name as u_lname,driver.first_name,driver.last_name');
		$this->db->from('reservation');
		$this->db->where('reservation.status = 3');
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->join('driver', 'driver.id = reservation.driver_id');
		$this->db->order_by("id", "desc"); 
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
    public function get_all_user($limit, $start){
		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('role',1);
		$this->db->order_by("id", "desc"); 
		$this->db->limit($limit, $start);		
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	    public function count_all_user(){
		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('role',1);
		$this->db->order_by("id", "desc"); 
		$query = $this->db->get();
		$count = $query->num_rows();
		
		if($count){
			return $count;
		}else{
			return false;
		}
	}

	public function get_user($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		

		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
    public function update_user($data,$id){
		$this->db->where('id',$id);
		$return = $this->db->update('user',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	public function check_user_name($username,$id){
		$this->db->select('username');
		$this->db->from('user');
		$this->db->where('username',$username);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function check_user_email($email,$id){
		$this->db->select('email');
		$this->db->from('user');
		$this->db->where('email',$email);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function check_user_mobile_no($mobile_no,$id){
		$this->db->select('mobile_number');
		$this->db->from('user');
		$this->db->where('mobile_number',$mobile_no);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function check_user_other_no($mobile_no,$id){
		$this->db->select('other_number');
		$this->db->from('user');
		$this->db->where('other_number',$mobile_no);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function delete_user($id){
		$this->delete_user_billing_detail($id);
		$this->db->where('id',$id);
		$this->db->delete('user');
	}
	public function delete_user_billing_detail($id){
		$this->db->where('user_id',$id);
		$this->db->delete('billing_details');
	}
	public function password_check($oldpass,$session_name){		
		$this->db->select('*');
		$this->db->where('username', $session_name);
		$this->db->where('password', $oldpass);
		$query = $this->db->get('user');			
		return $query->result_array();	
	}
	public function update_password($session_name,$password){
		$this->db->where('username', $session_name);            
		//$this->db->where('user_type', 'admin');            
		$this->db->update('user',array('password'=>$password));	
	}
	
	/*
	*  This function is used to return all mobile_number
	*  based on sesrch criteria.
	*  Author: Devi Lal Verma
	*/
   public function get_all_mobile_number($mobile_number){
   	$this->db->select('mobile_number');
   	$this->db->like('mobile_number', $mobile_number);
   	return $this->db->get('user')->result();
   }
   
   /*
   *  This function is used to return all user
   *  based on search criteria.
   *  Author: Devi Lal Verma
   */
   public function search_user($search,$limit,$start){
   	$this->db->select('*');
   	$this->db->from('user');
   	$this->db->where('first_name',$search);
   	$this->db->or_where('last_name',$search);
   	$this->db->or_where('email',$search);
   	//$this->db->order_by("id", "desc");
   	$this->db->limit($limit, $start);
   	
   	
   	$query = $this->db->get();
   	
   	$result = $query->result();
   	
   	
	   	if($result){
	   		return $result;
	   	}
	   	else{
	   		return false;
	   	}
   	
   }
   /*
    * Counting the records based on the
    * user search term.
    * Author: Devi Lal Verma
    */
   public function count_search_user($search){
   
    $this->db->select('*');
   	$this->db->from('user');
   	$this->db->where('first_name',$search);
   	$this->db->or_where('last_name',$search);
   	$this->db->or_where('email',$search);
	
   	$query = $this->db->get();
   	$count = $query->num_rows();
   	
   	
	   	if($count){
	   		return $count;
	   	}
	   	else{
	   		return false;
	   	}
	} 
	/*
	 * The purpose of the method, 
	 * will be to check the various different states
	 * the $searchterm variable could be in.
	 * Author: Devi Lal Verma
	 */
	public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('searchterm', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('searchterm'))
		{
			$searchterm = $this->session->userdata('searchterm');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}
	public function get_username($mobile_number){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('mobile_number',$mobile_number);
		$query = $this->db->get();
		$result = $query->result();
		
		if($result){
			return $result;
		}
		else{
			return false;
		}
	}
	
	public function check_forgot_email($email,$role){
		$this->db->select('email,first_name');
		$this->db->from('user');
		$this->db->where('email',$email);
		$this->db->where('role',$role);
		$query = $this->db->get();
		$result = $query->result();
		
		if($result){ 
			return $result;
		}else{
			return false;
		}
	}
	public function reset_password($password,$email){
		$this->db->where('email', $email);
		$this->db->update('user',array('password'=>md5($password)));
	
	}

	/*
	 * This function is used to return user id,email address
	*  based on username.
	*  Author: Devi Lal Verma
	*/
	public function get_login_userId($username){
		$this->db->select('id,email');
		$this->db->from('user');
		$this->db->where('username',$username);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}
		else{
			return false;
		}
	
	}
	public function insert_key($ver_key , $email){
		$this->db->where('email',$email);
		$return = $this->db->update('user',array('key'=>$ver_key));
		if($return > 0){
			return true;
		}else{
			return false;
		}
}
		public function verify_key($ver_key){
		$this->db->select('email');
		$this->db->from('user');
		$this->db->where('key',$ver_key);
	
		$query = $this->db->get();
		$result = $query->result();

			if(!empty($result)){
				return true;
			}else{			
				return NULL;
			}
		}
		
		public function send_username($ver_key){
			$this->db->select('username,first_name,email');
			$this->db->from('user');
			$this->db->where('key',$ver_key);
		
			$query = $this->db->get();
			$result = $query->result();
		
			if(!empty($result)){
				return $result;
			}else{
				return NULL;
			}
		}
		
		public function update_key_password($ver_key ,$password){
		$this->db->where('key',$ver_key);
		$return = $this->db->update('user',array('password'=>$password));
		
		if($return > 0){
	
			return true;
			
		}else{
		
			return false;
		}
		}
		public function empty_key($ver_key ,$password){
		$this->db->where('password',$password);
		$return = $this->db->update('user',array('key'=>''));
		if($return > 0){
			return true;
		}else{
			return false;
		}
		}
		/*
		 *  This function is used to return all contact_name
		*  based on sesrch criteria.
		*  Author: Devi Lal Verma
		*/
		public function get_contact_name($referencia){
			$this->db->select('first_name,last_name');
			$this->db->like('first_name', $referencia);
			$this->db->like('last_name', $referencia);
			return $this->db->get('user')->result();
		}
		 public function update_userkey($data,$code){
		$this->db->where('key',$code);
		$return = $this->db->update('user',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	} 
	
	
	//count all notification 
	 public function count_notifications(){
		
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->where('notification',1);
		
		$query = $this->db->get();
		$count = $query->num_rows();
		
		if($count){
			return $count;
		}else{
			return false;
		}
	}

	
    public function notification_check($id) {
       $this->db->select('notification');
		$this->db->from('reservation');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result(); 
	
	$n_res = $result[0]->notification;
	if(!empty($n_res)){
			return $n_res;
        }else{
            return 0;
        }
    }

	
	  public function notification_update($id) {

		$this->db->where('id',$id);
		$return = $this->db->update('reservation',array('notification'=>0));

		if($return > 0){
			return true;
		}else{
			return false;
		}
    }
	//get role of session user
	public function get_role($userid){
		$this->db->select('role');
		$this->db->from('user');
		$this->db->where('id',$userid);
		$query = $this->db->get();
		$result = $query->result();
		
		if($result){
			return $result[0]->role;
		}
		else{
			return false;
		}
	}
	
	public function get_user_id($trip_id){
	$this->db->select('user_id');
	$this->db->from('reservation');
	$this->db->where('id',$trip_id);
	$query=$this->db->get();
	$result=$query->result();
	if($result){
		return $result;
	}
	else{
		return false;
	}
	}
	
}
?>
