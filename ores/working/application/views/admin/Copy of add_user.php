<div class="wrapper">

	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?> 
			</td>
  
			<td class="contentbox">
				<div id="mainContent">
					<div class="grayStripRight">Add User</div><br/>
					<div id="error">
							<?php 
								$errors = validation_errors();
								if(!empty($wrong_insert)){echo $wrong_insert;}
								//if(!empty($error)){echo $error;}
							?>
					</div>
					<?php echo form_open(); 
							
						if(form_error('first_name')) { ?>
							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>First Name</label>
							<input type="text" class="shadow" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
						</div>	
						<?php if(form_error('last_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Last Name</label>
							<input type="text" class="shadow" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>">
						</div>
						
						<?php if(form_error('username')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Username</label>
							<input type="text" class="shadow" name="username" id="username" value="<?php echo set_value('username'); ?>">
						</div>

						<?php if(form_error('email')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Email</label>
							<input type="text" class="shadow" id="email" name="email" value="<?php echo set_value('email'); ?>">
						</div>
						
						<?php if(form_error('password')) { ?>
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
						<label>Password</label>
						<input type="password" class="shadow" name="password" value="<?php echo set_value('password'); ?>">
						</div>
						
						<?php if(form_error('company_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Company Name</label>
							<input type="text" class="shadow" name="company_name" id="company_name" value="<?php echo set_value('company_name'); ?>">
						</div>
						
						<?php if(form_error('mobile_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Mobile Number</label>
							<input type="text" class="shadow" name="mobile_number" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>">
						</div>
						<?php if(form_error('other_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Other Number</label>
							<input type="text" class="shadow" name="other_number" id="other_number" value="<?php echo set_value('other_number'); ?>">
						</div>
						
						
						<?php if(form_error('address_title')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
						<label>Address Title</label>
						<?php $options = array(
						'' => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); ?>
						<?php echo form_dropdown('address_title', $options, '');?>
						</div>
						
						
						<?php if(form_error('address_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 1</label>
							<input type="text" class="shadow" name="address_1" id="address_1" value="<?php echo set_value('address_1'); ?>">
							
						</div>	
						<?php if(form_error('address_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 2</label>
							<input type="text" class="shadow" name="address_2" id="address_2" value="<?php echo set_value('address_2'); ?>">
						
						</div>	
						
						<?php if(form_error('city')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>City</label>
							<input type="text" class="shadow" name="city" id="city" value="<?php echo set_value('city'); ?>">
						</div>
						<?php if(form_error('state')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>State</label>
							<input type="text" class="shadow" name="state" id="state" value="<?php echo set_value('state'); ?>">
						</div>
						<?php if(form_error('country')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Country</label>
							<select id="country" class="shadow" name="country" id='country'>
							<option value="">-- please Select --</option>
							<?php
							$ctrb   = $this->db->get('country');
							foreach($ctrb->result() as $ctrb){
							echo "<option value='".$ctrb->CountryID."'>";
							echo $ctrb->CountryName;
							echo "</option>";
							}
							?>
							</select>
						</div>
						
						<?php if(form_error('zip_code')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Zip Code</label>
						<input type="text" class="shadow" name="zip_code" id="zip_code" value="<?php echo set_value('zip_code'); ?>">
						</div>
						<?php if(form_error('status')) { ?>
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Status</label>
						<?php $options = array(
						'' =>  '-- Please select --',
						'1' => 'Active',
						'2' => 'Inactive',
					
						); ?>
						<?php echo form_dropdown('status', $options, '');?>
						</div>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<input style=" margin-top:10px;" class="submit_c_ebook" type="submit" name="adduser" value="Submit">
						</div>
					
					<?php echo form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>

