<div class="wrapper">
	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?> 
			</td>
  
			<td class="contentbox">
				<div id="mainContent">
					<div class="grayStripRight">Edit User</div><br/>
					<div id="error">
							<?php 
								$errors = validation_errors();
								if(!empty($wrong_update)){echo $wrong_update;}
								//if(!empty($error)){echo $error;}
							?>
					</div>
					<?php echo form_open(''); 
						
						if(form_error('first_name')) { ?>
							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>First Name</label>
							<input type="text" class="shadow" name="first_name" id="first_name" value="<?php if(!empty($user->first_name)) echo $user->first_name; ?>">
						</div>	
						<?php if(form_error('last_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Last Name</label>
							<input type="text" class="shadow" name="last_name" id="last_name" value="<?php if(!empty($user->last_name)) echo $user->last_name; ?>">
						</div>
						
						<?php
						if(form_error('username')) { ?>						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							
							<label>Username</label>
							<input type="text" class="shadow" name="username" id="username" value="<?php if(!empty($user->username)) echo $user->username; ?>">
						</div>
						
						
						<?php if(form_error('email')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Email</label>
							<input type="text" class="shadow" id="email" name="email" value="<?php if(!empty($user->email)) echo $user->email; ?>">
						</div>
						<?php if(form_error('password')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Password</label>
							<input type="password" class="shadow" id="password" name="password" value="">
						</div>
						
						<?php if(form_error('company_name')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Company Name</label>
							<input type="text" class="shadow" name="company_name" id="company_name" value="<?php if(!empty($user->company_name)) echo $user->company_name; ?>">
						</div>
						
						<?php if(form_error('mobile_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Mobile Number</label>
							<input type="text" class="shadow" name="mobile_number" id="mobile_number" value="<?php if(!empty($user->mobile_number)) echo $user->mobile_number; ?>">
						</div>
						<?php if(form_error('other_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Other Number</label>
							<input type="text" class="shadow" name="other_number" id="other_number" value="<?php if(!empty($user->other_number)) echo $user->other_number; ?>">
						</div>
						<?php if(form_error('address_title')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Address Title</label>
						<?php $options = array(
						''  => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); ?>
						<?php echo form_dropdown('address_title', $options,$user->address_title);?>
						</div>
						
						<?php if(form_error('address_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 1</label>
							<input type="text" class="shadow" name="address_1" id="address_1" value="<?php if(!empty($user->address_1)) echo $user->address_1; ?>">
						</div>
							<?php if(form_error('address_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 2</label>
							<input type="text" class="shadow" name="address_2" id="address_2" value="<?php if(!empty($user->address_2)) echo $user->address_2; ?>">
						</div> 
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>City</label>
							<input type="text" class="shadow" name="city" id="city" value="<?php if(!empty($user->city)) echo $user->city; ?>">
						</div> 
						
						<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State</label>
							<input type="text" class="shadow" name="state" id="state" value="<?php if(!empty($user->state)) echo $user->state; ?>">
						</div> 
						
						<?php if(form_error('country')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Country</label>
							<select id="country" class="shadow" name="country" id='country'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('country');
						foreach($ctrb->result() as $ctrb){
							if($ctrb->CountryID==$user->country){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->CountryID."' $sel>". $ctrb->CountryName."</option>";
						}
						?>
						</select>
						</div> 
						
						<?php if(form_error('zip_code')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip Code</label>
							<input type="text" class="shadow" name="zip_code" id="zip_code" value="<?php if(!empty($user->zip_code)) echo $user->zip_code; ?>">
						</div> 
						<?php if(form_error('status')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Status</label>
						<?php $options = array(
						'' => '-- Please select --',
						'1' => 'Active',
						'2' => 'Inactive',
					
						); ?>
						<?php echo form_dropdown('status', $options, $user->status);?>
						</div>
						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<input style=" margin-top:10px;" class="submit_btn_update" type="submit" name="addartist" value="Update">
						</div>


					<?php form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>