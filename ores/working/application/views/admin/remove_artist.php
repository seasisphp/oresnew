<div class="wrapper">
	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?> 
			</td>
  
			<td class="contentbox">
				<div id="mainContent">
					<div class="grayStripRight">Remove Artist</div><br/>
					<div id="error">
							<?php 
								$errors = validation_errors();
								if(!empty($wrong_insert)){echo $wrong_insert;}
								//if(!empty($error)){echo $error;}
							?>
					</div>
					<?php echo form_open_multipart(''); ?>
						<div class="txtbox1" style="margin-left:24%"> 	
							<h2>Do you really want to remove this Artist (<a href="<?php echo base_url();?>admin/home/remove_artist/<?php  echo $artist_id; ?>/y">Yes</a> / <a href="<?php echo base_url();?>admin/home/remove_artist/<?php  echo $artist_id; ?>/n">No</a>)</h2>
						
						</div>


					<?php form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>