<div class="wrapper">
<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
<tr>
<td class="aside">
 <!-- sideBatr goes here --> 
<?php $this->load->view('template/admin_sidebar'); ?>
 
  </td>
  
  <td class="contentbox">
   <div id="mainContent">
    <div class="grayStripRight">Update <?php echo ucwords($data['title']); ?> Page</div><br/>
	<a style="border: 1px solid #EFEFEF;float: right;margin-right: 70px;padding: 4px;" href="<?php echo base_url().'home/'.$data['page']; ?>" target="_blank">View Page</a>
	<?php echo form_open_multipart();
	$suc = $this->session->flashdata('message');
	if(isset($suc)){echo $this->session->flashdata('message'); }
	?>

<div class="txtbox1"> 
<label>Title</label>
<input type="text" class="shadow" name="title" value="<?php echo $data['title']; ?>">
</div>
<?php if(form_error('title')) { ?>
<div class="txtbox1"> 
<label>&nbsp;</label>
<span class="validation" style="color:#FF0000;"><?php echo form_error('title'); ?></span>
</div> <?php } ?>


<div class="txtbox1"> 
<label>Meta Tags</label>
<input type="text" class="shadow" name="meta_tags" value="<?php echo $data['meta_tags']; ?>">
</div>
<?php if(form_error('meta_tags')) { ?>
<div class="txtbox1"> 
<label>&nbsp;</label>
<span class="validation" style="color:#FF0000;"><?php echo form_error('meta_tags'); ?></span>
</div> <?php } ?>


<div class="txtbox1"> 
<label>Meta Keywords</label>
<input type="text" class="shadow" name="meta_keyword" value="<?php echo $data['meta_keyword']; ?>">
</div>
<?php if(form_error('meta_keyword')) { ?>
<div class="txtbox1"> 
<label>&nbsp;</label>
<span class="validation" style="color:#FF0000;"><?php echo form_error('meta_keyword'); ?></span>
</div> <?php } ?>

<div class="txtbox1"> 
<label>Meta Description</label>

<textarea style="height:250px;width:500px"id="meta_description" class="txtarea" name="meta_description"><?php echo $data['meta_description']; ?></textarea>
</div>
<?php if(form_error('meta_description')) { ?>
<div class="txtbox1"> 
<label>&nbsp;</label>
<span class="validation" style="color:#FF0000;"><?php echo form_error('meta_description'); ?></span>
</div> <?php } ?>

<div class="txtbox1"> 
<label>Description</label>
<textarea style="height:250px;width:500px"id="descr" class="txtarea" name="page_description"><?php echo $data['page_description']; ?></textarea>
</div>
<?php if(form_error('page_description')) { ?>
<div class="txtbox1"> 
<label>&nbsp;</label>
<span  style="color:#FF0000;" class="validation"><?php echo form_error('page_description'); ?></span>
</div><?php } ?>

<div class="txtbox1"> 
<label>&nbsp;</label>


<div style="float:left;">
<div style="float:left; width:230px;">
<?php $page_id = $this->uri->segment(4);
	if($page_id == 2){?>
<input style=" margin-top:10px;" class="" type="file" name="image" id="image" value="" />
<?php } ?>
<br />
<?php  if(isset($error)){ ?>
<span  style="color:#FF0000; display:inline-block; margin-left:0;" class="validation"><?php echo $error['error']; ?></span>
<?php } ?> 
<input style=" margin-top:10px;"  class="submit_btn_update" type="submit" name="submit" value="Submit" /> <br />

</div>
<div style="height:100px;width:100px;float:left; margin-left:12px">
<img src="<?php echo base_url().'uploads/pages/shor_thumb/'.$data['product_short_thumb_image']?>" ></div><br clear="all" />

</div><br clear="all" />



</div>
<?php form_close() ; ?>
	 
    </div>
  </td>
  </tr>
 </table>
    </div>