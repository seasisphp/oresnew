 <script>
 function assignDriver(driverId,responceId){
		//alert(driverId+"=="+responceId);
		$.ajax({
					url : "<?php echo base_url() ?>admin/dispatchlog/driverpostAjax",
					method : "post",
					data: 'driver_id='+driverId+'&reservation_id='+responceId,		
						type : "post",
						success : function(data) {
						 // alert(data);
						  $('#assignResponce'+responceId).html('Driver Assigned');
						   $('#changeStatus'+responceId).html('Assigned');
						   $('#changeStatus'+responceId).addClass("alert alert-success");
						  
						}
				});
		//alert(driverId+"=="+responceId);
			/*var driverId = $("#assign_driver_id").val();

					$.ajax({
					url : "<?php echo base_url() ?>admin/dispatchlog/driverpostAjax",
					method : "post",
					data: 'driver_id='+driverId,		
						type : "post",
						success : function(data) {
						  //alert(data);
						  
						}
				});*/
	

 
 }
 </script>
 
 <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?>
			</div>
			 <div class="span9" id="content">

                    
                    <div class="row-fluid">
					<?php $msg = $this->session->flashdata('msg');?>
					<?php if($msg):?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
					</div>
					<?php endif;?>
					<!-- block -->
						<div style="clear:both;"></div>
                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Dispatch log</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
  									<table class="table">
						              <thead>
						                <tr>
						        <th class="ck_field" align="left">Id #</th>		
								<th align="left">Service Type</th>		
								<th align="left">Status</th>		
								<th align="left"> PU Date</th>		
								<th align="left">PU Time</th>		
								<th align="left">Duration</th>		
								<th align="left">Client Name</th>		
								
								<th align="left">Driver</th>	
								<th align="left">Requested Driver</th>	
								<th align="left">PU Location</th>
								<th align="left">D/O Location</th>
						                </tr>
						              </thead>
						              <tbody>
						               <?php  
									if(!empty($dlog)){
									$serial_no=1;
								foreach($dlog as $dispatch_log){ 	?>	
						
									<tr> 
										<td><?php  echo $serial_no; ?></td>
										
										<td>
										<?php if($dispatch_log->service_type==1){ ?>
											Personal Driver Service
											<?php } else if ($dispatch_log->service_type==2){ ?>
											Airport Transportation
											<?php } else if ($dispatch_log->service_type==3){ ?>
											Double driver Pick Up
											<?php } ?>
											</td>
										
										
									
										<td> <span id="changeStatus<?php echo $dispatch_log->reservation_id ?>"><?php if($dispatch_log->status==0){ ?>
											Unassigned
											<?php } else { ?>
											Assigned
											<?php } ?></span>
											</td>
										
										<td><?php  echo $dispatch_log->trip_date; ?></td>
										<td><?php  echo $dispatch_log->pickup_time; ?></td>
										<td><?php  echo $dispatch_log->approx_hours; ?></td>
										<td><?php  echo $dispatch_log->first_name; ?></td>
							
										<td> 
										<select id="assign_driver_id" class="shadow" name="assign_driver_id" id='assign_driver_id' onchange="assignDriver(this.value,'<?php echo $dispatch_log->reservation_id ?>')">
										<option value="">-- Assign Driver --</option>
										<?php 
										$ctrb   = $this->db->get('driver');
										foreach($ctrb->result() as $ctrb){
										if($ctrb->id==$dispatch_log->assign_driver_id){$sel='selected="selected"';}else{$sel='';}
										echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
										}
										?>
										</select>
										</td>
									
										<td>
										
										
										
										<?php  echo $dispatch_log->driverName; ?>
										
										
										</td>
										<td><?php  echo $dispatch_log->pickup_address; ?></td>
										<td><?php  echo $dispatch_log->dropoff_1; ?></td>
									
										<td>
										<span>
										<a href="<?php echo base_url(); ?>admin/home/edit_dlogs/<?php echo $dispatch_log->reservation_id; ?>"><img src="<?php echo base_url();?>images/icons/edit.png" title="Edit"></a>
										</span> &nbsp;
										<span>
										<a href="<?php echo base_url(); ?>admin/home/remove_dlogs/<?php echo $dispatch_log->reservation_id; ?>"><img src="<?php echo base_url();?>images/icons/delete.png" title="Delete"></a>
										</span>
										</td>
									</tr><?php 
									$serial_no++;
								} 
							}?>
						              </tbody>
						            </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
					<?php echo $this->pagination->create_links(); ?>
					
          <div class="clr"></div>
                    </div>

</div>			
