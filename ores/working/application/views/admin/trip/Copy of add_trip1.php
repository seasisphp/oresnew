<div class="wrapper">
	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?> 
			</td>
  
			<td class="contentbox">
				<div id="mainContent">
					<div class="grayStripRight">Add Trip Detail</div><br/>
					<div id="error">
							<?php 
								$errors = validation_errors();
								if(!empty($wrong_insert)){echo $wrong_insert;}
								//if(!empty($error)){echo $error;}
							?>
					</div>
					<?php echo form_open(); 
							
						if(form_error('service_type')) { ?>
							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('service_type'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Service Type</label>
							<input type="text" class="shadow" name="service_type" id="service_type" value="<?php echo set_value('service_type'); ?>">
						</div>	
						
						<?php if(form_error('trip_status')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_status'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Trip Status</label>
							<input type="text" class="shadow" name="trip_status" id="trip_status" value="<?php echo set_value('trip_status'); ?>">
						</div>

						<?php if(form_error('trip_date')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Pick Up Date</label>
							<input type="text" class="shadow" id="trip_date" name="trip_date" value="<?php echo set_value('trip_date'); ?>">
						</div>
						
						<?php if(form_error('pickup_time')) { ?>
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
						<label>Pick Up Time</label>
						<input type="pickup_time" class="shadow" name="pickup_time" value="<?php echo set_value('pickup_time'); ?>">
						</div>
						
						<?php if(form_error('pickup_address')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_address'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Pu Location</label>
							<input type="text" class="shadow" name="pickup_address" id="pickup_address" value="<?php echo set_value('pickup_address'); ?>">
						</div>
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>City</label>
							<input type="text" class="shadow" name="city" id="city" value="<?php echo set_value('city'); ?>">
						</div>	
						
							<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State</label>
							<input type="text" class="shadow" name="state" id="state" value="<?php echo set_value('state'); ?>">
						</div>	
						<?php if(form_error('zip')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip</label>
							<input type="text" class="shadow" name="zip" id="zip" value="<?php echo set_value('zip'); ?>">
						</div>	
				      	<?php if(form_error('airport')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airport'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Airport</label>
							<input type="text" class="shadow" name="airport" id="airport" value="<?php echo set_value('airport'); ?>">
						</div>
						<?php if(form_error('airline')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airline'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Airline</label>
							<input type="text" class="shadow" name="airline" id="airline" value="<?php echo set_value('airline'); ?>">
						</div>
						<?php if(form_error('flight_no')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('flight_no'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>flight_no</label>
							<input type="text" class="shadow" name="flight_no" id="flight_no" value="<?php echo set_value('flight_no'); ?>">
						</div>
						<?php if(form_error('dropoff_1')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('dropoff_1'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Drop Off Time</label>
							<input type="text" class="shadow" name="dropoff_1" id="dropoff_1" value="<?php echo set_value('dropoff_1'); ?>">
						</div>
						
						<?php if(form_error('dropoff_2')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('dropoff_2'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Dispatched Time</label>
							<input type="text" class="shadow" name="dropoff_2" id="dropoff_2" value="<?php echo set_value('dropoff_2'); ?>">
						</div>
						
						<?php if(form_error('approx_hours')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('approx_hours'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Duration</label>
							<input type="text" class="shadow" name="approx_hours" id="approx_hours" value="<?php echo set_value('approx_hours'); ?>">
						</div>
						<?php if(form_error('user_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('user_id'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Username</label>
							<input type="text" class="shadow" name="user_id" id="user_id" value="<?php echo set_value('user_id'); ?>">
						</div>
						
						
					
						
						
						
						
						
						<?php if(form_error('desination_1')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_1'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Drop-Off Location 1</label>
						<input type="text" class="shadow" name="desination_1" id="desination_1" value="<?php echo set_value('desination_1'); ?>">
						</div>
						
						<?php if(form_error('desination_2')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_2'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Drop-Off Location 2</label>
						<input type="text" class="shadow" name="desination_2" id="desination_2" value="<?php echo set_value('desination_2'); ?>">
						</div>
						
						
						<?php if(form_error('comments')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('comments'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Trip Notes</label>
						<input type="text" class="shadow" name="comments" id="comments" value="<?php echo set_value('comments'); ?>">
						</div>
						
						
						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<input style=" margin-top:10px;" class="submit_c_ebook" type="submit" name="addtrip" value="Submit">
						</div>
					
					<?php echo form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>

<script>
$(document).ready(function() {

    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
    $('input.datepicker').Zebra_DatePicker();

 });
 </script>
