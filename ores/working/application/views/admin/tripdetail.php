<?php 
$Tripdetail=$this->session->userdata('tripdetail');
?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); 
					$date =  explode(" ",$tripdetail->trip_date_from); 
					$fdate = date ("m/d/Y",strtotime($date[0]));
					$pudate =  $fdate;
					$putime = date("g:iA", strtotime($date[1]));
					$date1 =  explode(" ",$tripdetail->trip_date_to) ; 
					if($date1[0]=='0000-00-00'){
						$doffdate='';
					}
					else{
						$doffdate = date ("m/d/Y",strtotime($date1[0]));
					}
					$dofftime =  $date1[1];
					?> 
                </div>
                <!--/span-->
				
<div class="span9" id="content">
                      <!-- morris stacked chart -->
					  
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Trip Detail</div>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12">
										<div id="error"> 
										<?php 
											$errors = validation_errors();
											if(!empty($wrong_insert)){echo $wrong_insert;}
											//if(!empty($error)){echo $error;}
										?>
										<?php echo form_open(); 

										?>
						<div class="form-left">	
							<?php if(form_error('status')) { ?>
									
								<div class="txtbox1"> 
									<label>&nbsp;</label>
									<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
								</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Trip Status<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						''  => '-- Please select --',
						'0' => 'Pending',
						'1' => 'Waiting',
						'2' => 'Dispatched',
						'3' => 'Completed',
						'4' => 'Canceled',
						); ?>
						<?php 
						if(!empty($_POST['status'])){
							$index=$_POST['status'];
						}
						elseif(!empty($Tripdetail->status)){
							$index=$Tripdetail->status;
						}
						else{
							$index='';
						}
				
						$status_id = 'id="trip_status"';
						echo form_dropdown('status',$options,$index,$status_id);?>
						</div>
							
						</div>
						<div class="form-right">	
							<?php if(form_error('trip_date_from')) { 
							
						
							?>							
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_from'); ?></span>
							</div> <?php
		
							} ?>
							<div class="txtbox1"> 
								<label>Pick-Up Date<span class="validation" style="color:#FF0000;">*</span></label>
								<input type="text" readonly="readonly" class="shadow datetime" name="trip_date_from" id="trip_date_from" value="<?php if(!empty($_POST['trip_date_from'])) echo $_POST['trip_date_from']; elseif(!empty($Tripdetail->trip_date_from)) echo $pudate; ?>">
							</div>
						</div>
						<div class="form-left">	
						<?php
						if(form_error('pickup_time')) { ?>						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							
							<label>Pick-Up Time<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="pickup_time" id="pickup_time" value="<?php if(!empty($_POST['pickup_time'])) echo $_POST['pickup_time']; elseif(!empty($Tripdetail->pickup_time)) echo $putime ; ?>">
						</div>
						</div>
						<div class="form-right">	
							<?php if(form_error('trip_date_to')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_to'); ?></span>
							</div> <?php } ?>
					
						<div class="txtbox1"> 
							<label>Drop Off Date<span class="validation" style="color:#FF0000;"></span></label>
							<input type="text" readonly="readonly" class="shadow datetime" id="trip_date_todate" name="trip_date_todate" value="<?php if(!empty($_POST['trip_date_todate'])) echo $_POST['trip_date_todate']; elseif(!empty($Tripdetail->trip_date_to)) echo $doffdate; ?>">
						</div>
						</div>
					
						<!-- <div class="form-left">	
							<?php if(form_error('dropoff_time')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('dropoff_time'); ?></span>
							</div> <?php } ?>
				
							<div class="txtbox1"> 
								<label>Drop Off Time<span class="validation" style="color:#FF0000;"></span></label>
								<input type="text" placeholder="HH:MM" readonly="readonly" class="shadow datetime" id="dropoff_time" name="dropoff_time" value="<?php if(!empty($Tripdetail->trip_date_to)) echo  $dofftime; else echo set_value('dropoff_time')?>">
							</div>
						</div> -->
					
					<div class="form-left">					
						<div class="txtbox1"> 
						<label>Driver</label>
						 <select id="driver_id" class="shadow" name="driver_id" id='driver_id'>
										<option class="driver-sel-opt"value="">-- Assign Driver --</option>
										
										<?php 
									
										foreach($AdriverByCity[$tripdetail->id] as $dcity){
										if(isset($_POST['driver_id'])){
										if($_POST['driver_id']==$dcity->id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt' value='".$dcity->id."' $sel>". $dcity->first_name."</option>";
										}
										else{	
										if($dcity->id==$tripdetail->driver_id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt'  value='".$dcity->id."' $sel>". $dcity->first_name." ". $dcity->last_name."</option>";
										}
										}
										foreach($NACityDrivers[$tripdetail->id] as $dcity){
										if(isset($_POST['driver_id'])){
										if($_POST['driver_id']==$dcity->id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt-na' value='".$dcity->id."' $sel>". $dcity->first_name."</option>";
										}
										else{
										if($dcity->id==$tripdetail->driver_id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt-na'  value='".$dcity->id."' $sel>". $dcity->first_name." ". $dcity->last_name."</option>";
										}
										}
										foreach($AnonCityDrivers[$tripdetail->id] as $dcity){
										if(isset($_POST['driver_id'])){
										if($_POST['driver_id']==$dcity->id){$sel='selected="selected"';}else{$sel='';}
										echo "<option  class='driver-sel-opt' value='".$dcity->id."' $sel>". $dcity->first_name."</option>";
										}
										else{
										if($dcity->id==$tripdetail->driver_id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt'  value='".$dcity->id."' $sel>". $dcity->first_name." ". $dcity->last_name."</option>";
										}
										}
										foreach($NAnonCityDrivers[$tripdetail->id] as $dcity){
										if(isset($_POST['driver_id'])){
										if($_POST['driver_id']==$dcity->id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt-na' value='".$dcity->id."' $sel>". $dcity->first_name."</option>";
										}
										else{	
										if($dcity->id==$tripdetail->driver_id){$sel='selected="selected"';}else{$sel='';}
										echo "<option class='driver-sel-opt-na'  value='".$dcity->id."' $sel>". $dcity->first_name." ". $dcity->last_name."</option>";
										}
										}
										?>
										</select>
						
						</div> 
				</div>
			
				<div class="form-left">		
						<?php if(form_error('service_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('service_id'); ?></span>
						</div> <?php } ?>
							<div class="txtbox1"> 
							<label>Service Type<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="service_id" class="shadow" name="service_id" id='service_id'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('service_type');
						foreach($ctrb->result() as $ctrb){
							if(isset($_POST['service_id'])){
							if($_POST['service_id']==$ctrb->service_type_id){$sel='selected="selected"';}else{$sel='';}
							echo "<option value='".$ctrb->service_type_id."' $sel>". $ctrb->service_type_desc."</option>";
							}
							else{
							if($ctrb->service_type_id==$Tripdetail->service_id){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->service_type_id."' $sel>". $ctrb->service_type_desc."</option>";
							}
							
						}
						?>
						</select>
						</div> 
				</div>	

				<div class="form-left">	
						<?php if(form_error('user_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('user_id'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Client Name<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="user_id" class="shadow" name="user_id" id='user_id'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('user');
						foreach($ctrb->result() as $ctrb){
							if(isset($_POST['user_id'])){
								if($ctrb->id==$_POST['user_id']){
								$sel='selected="selected"';}else{$sel='';}
								echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
							else{
							if($ctrb->id==$Tripdetail->user_id){$sel='selected="selected"';}else{$sel='';}
							echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
						}
						?>
						</select>
						</div> 
				</div>

			 	<div class="form-left">	
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Pick Up City<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="city" id="city" maxlength= "20" value="<?php  if(!empty($_POST['city'])) echo $_POST['city']; elseif(!empty($Tripdetail->city)) echo $Tripdetail->city; ?>">
						</div> 
				</div>
				<div class="form-right">	
						<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="state" id="state" maxlength= "20" value="<?php  if(!empty($_POST['state'])) echo $_POST['state']; elseif(!empty($Tripdetail->state)) echo $Tripdetail->state; ?>">
						</div> 
				</div>
				<div class="form-left">	
						<?php if(form_error('zip')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="zip" id="zip" maxlength= "20" value="<?php  if(!empty($_POST['zip'])) echo $_POST['zip']; elseif(!empty($Tripdetail->zip)) echo $Tripdetail->zip; ?>">
						</div> 
				</div>
				
				<div class="form-right">	
						<?php if(form_error('address')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Pick Up Location<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text"  class="shadow" name="address" id="address" value="<?php if(!empty($_POST['address'])) echo $_POST['address'];  elseif(!empty($Tripdetail->address)) echo $Tripdetail->address; ?>">
						</div>
				</div>
				<div id="address2_div" class="form-right" style="display:none;">	
						<?php if(form_error('address_2')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Pick Up Location 2</label>
							<input type="text"  class="shadow" name="address_2" id="address_2" value="<?php if(!empty($_POST['address_2'])) echo $_POST['address_2'];  elseif(!empty($Tripdetail->address_2)) echo $Tripdetail->address_2;  ?>">
						</div>
				</div>
				<div id="airport_div" class="form-left" style="display:none;">	
						<?php if(form_error('airport')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airport'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Airport<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="airport" id="airport" maxlength= "20" value="<?php  if(!empty($_POST['airport'])) echo $_POST['airport']; elseif(!empty($Tripdetail->airport)) echo $Tripdetail->airport; ?>">
						</div> 
				</div>
				<div id="airline_div" class="form-right" style="display:none;">	
						<?php if(form_error('airline')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airline'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Airline<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="airline" id="airline" maxlength= "20" value="<?php  if(!empty($_POST['airline'])) echo $_POST['airline']; elseif(!empty($Tripdetail->airline)) echo $Tripdetail->airline; ?>">
						</div> 
				</div>
				<div id="flight_div" class="form-left" style="display:none;">	
						<?php if(form_error('flight_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('flight_number'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Flight No<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="flight_number" id="flight_number" maxlength= "20" value="<?php  if(!empty($_POST['flight_number'])) echo $_POST['flight_number']; elseif(!empty($Tripdetail->flight_number)) echo $Tripdetail->flight_number; ?>">
						</div> 
				</div>
				<div id="dropoff_div" style="display:none;">
				<div class="form-left">	
						<?php if(form_error('desination_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Drop Off location-1<span class="validation" style="color:#FF0000;">*</span></label>
							<textarea class="shadow" name="desination_1" maxlength= "20" id="desination_1"><?php if(!empty($_POST['desination_1'])) echo $_POST['desination_1'];  elseif(!empty($Tripdetail->desination_1)) echo $Tripdetail->desination_1; ?></textarea>
						</div> 
				</div>
					<div class="form-right">		
						<?php if(form_error('desination_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Drop Off location-2</label>
							<textarea class="shadow" name="desination_2" maxlength= "20"  id="desination_2"><?php if(!empty($_POST['desination_2'])) echo $_POST['desination_2']; elseif(!empty($Tripdetail->desination_2)) echo $Tripdetail->desination_2; ?></textarea>
						</div> 
					</div>
					</div>
					<div class="form-left">		
							<div class="txtbox1"> 
							<label>Trip Notes</label>
							<textarea class="txtarea" rows="5" cols="30" maxlength= "50"  name="comments"><?php if(isset($_POST['comments'])) echo $_POST['comments']; elseif(!empty($Tripdetail->comments)) echo $Tripdetail->comments; ?></textarea>
							</div> 
					
					</div>
					<div class="form-right clear">	
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span><input  class="update_record btn btn-primary btn-large" id="update_record"  type="submit" name="addartist" value="Update"></span>
							<span>
		
							<a class="btn btn-primary btn-large" href="<?php echo base_url();?>admin/dispatchlog/dlogs">View Dispatch log</a></span>
						
						</div>
					</div>


					<?php form_close() ; ?>
							
							
							
					
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {


	<?php if(!empty($Tripdetail)&& $Tripdetail->service_id==4){?>
		 document.getElementById("dropoff_div").style.display = "block";
    <?php }elseif(isset($_POST['service_id'])&& $_POST['service_id']==4){?>
		 document.getElementById("dropoff_div").style.display = "block";
    <?php }else{?>
	 document.getElementById("dropoff_div").style.display = "none";
	<?php }?>
	<?php if(!empty($Tripdetail)&& $Tripdetail->service_id==1){?>
	 	  document.getElementById("address2_div").style.display = "block";
	 <?php }elseif(isset($_POST['service_id'])&& $_POST['service_id']==1){?>
		  document.getElementById("address2_div").style.display = "block";
	<?php }else {?>
	 	  document.getElementById("address2_div").style.display = "none";
	<?php }?>

	<?php if(!empty($Tripdetail)&& $Tripdetail->service_id==2){?>
	     document.getElementById("airport_div").style.display = "block";
	     document.getElementById("airline_div").style.display = "block";
	     document.getElementById("flight_div").style.display = "block";
	<?php }elseif(isset($_POST['service_id'])&& $_POST['service_id']==2){?>
	 	 document.getElementById("airport_div").style.display = "block";
	     document.getElementById("airline_div").style.display = "block";
	     document.getElementById("flight_div").style.display = "block";
	<?php }else{?>
		 document.getElementById("airport_div").style.display = "none";
	     document.getElementById("airline_div").style.display = "none";
	     document.getElementById("flight_div").style.display = "none";
    <?php }?>

	<?php if(!empty($Tripdetail)&& $Tripdetail->service_id==3){?>
		  document.getElementById("airport_div").style.display = "block";
		  document.getElementById("airline_div").style.display = "block";
		
	<?php }elseif(isset($_POST['service_id'])&& $_POST['service_id']==3){?>
		  document.getElementById("airport_div").style.display = "block";
		  document.getElementById("airline_div").style.display = "block";
	<?php }elseif(!empty($Tripdetail)&& $Tripdetail->service_id==2||isset($_POST['service_id'])&& $_POST['service_id']==2){?>
	      document.getElementById("airport_div").style.display = "block";
		  document.getElementById("airline_div").style.display = "block";
	<?php }else{?>
		 document.getElementById("airport_div").style.display = "none";
		 document.getElementById("airline_div").style.display = "none";
	
	<?php }?>
    
	 $('#service_id').change( function(){
	 var e = document.getElementById("service_id");
     var service_id = e.options[e.selectedIndex].value;
	 if(service_id==4){
		 document.getElementById("dropoff_div").style.display = "block";
     }else {
		 document.getElementById("dropoff_div").style.display = "none";
     }
	 if(service_id==1){
		 document.getElementById("address2_div").style.display = "block";
     }else {
		 document.getElementById("address2_div").style.display = "none";
     }
	 if(service_id==2){
		 document.getElementById("airport_div").style.display = "block";
	     document.getElementById("airline_div").style.display = "block";
	     document.getElementById("flight_div").style.display = "block";
     }else {
		 document.getElementById("airport_div").style.display = "none";
	     document.getElementById("airline_div").style.display = "none";
	     document.getElementById("flight_div").style.display = "none";
     }
	 if(service_id==3||service_id==2){
		 document.getElementById("airport_div").style.display = "block";
	     document.getElementById("airline_div").style.display = "block";
     }else {
		 document.getElementById("airport_div").style.display = "none";
	     document.getElementById("airline_div").style.display = "none";
     }
     
	 });

    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
	var d = new Date();
	var year = d.getFullYear();
	d.setFullYear(year);    
   $('#trip_date_from').datepicker({dateFormat: 'mm/dd/yy', changeYear: true, changeMonth: true, yearRange: '1920:' + year + '',defaultDate:d}); 

	  jQuery('#pickup_time').timeEntry({ampmPrefix: ' '}).change(function() { 
		    var log = $('#log'); 
		    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	  });
	  jQuery('#dropoff_time').timeEntry({ampmPrefix: ' '}).change(function() { 
		    var log = $('#log'); 
		    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	  });
	

    $('#trip_date_todate').datepicker({dateFormat: 'mm/dd/yy', changeYear: true, changeMonth: true, yearRange: '1920:' + year + '',defaultDate:d});
	//$('#trip_date_to').timepicker();
 });
 
 </script>
 <script type="text/javascript">
$(function() {	
	 // To hide error message
	setInterval(function(){
		$('.alert-success').css('visibility', 'hidden');
	},10000);
	$(".update_record").click(function() {
		var val=$("#update_record").val();
		var r=confirm("Do you really want to update this record");
		if (r==true)
		  {
			$.ajax({
				url:'/ores/admin/dispatchlog/Tripdetail',
				data: 'id='+val,
				type:'POST',
				beforeSend: function() {
					// alert("loading")
				},
				success:function(data){
					$('.alert-success').css('visibility', 'visible');
					  $('#'+val).remove();
				}
			});
		  }
		else
		  {
			  return false;
		  }
	});

	$('#trip_status').change(function(){
		var val=$("#trip_status").val();
		var id = <?php echo $Tripdetail->id;?>;
		var service_id = $('#service_id').val();
		alert(service_id);
		var r=confirm("Do you really want to change trip status");
		if (r==true)
		  {
			if(val==2){  
			$.ajax({
				url:'<?php echo base_url();?>admin/dispatchlog/sendConfirmationMail',
				data: {id: id,service_id: service_id},
				type:'POST',
				beforeSend: function() {
					// alert("loading")
				},
				success:function(data){
					console.log(data);
				}
			});
			}
			else 
			{
				return false;	
		 	 }
		  }
		else
		  {
			  return false;
		  }
	
	});
});
</script>
 