<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<title>Admin Panel::ORES</title>
<head>

<link href="<?php echo base_url(); ?>css/admin/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>css/admin/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>css/admin/styles.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>css/admin/forgot_password_popup.css" rel="stylesheet" media="screen">
    
</head>
<body id="login">

<div class="navbar navbar-fixed-top">

            <div class="navbar-inner">
				
                <div class="container-fluid">
                    <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar"> <span class="icon-bar"></span>
						 <span class="icon-bar"></span>
						 <span class="icon-bar"></span>

                    </a>
                    <div class="nav-collapse collapse">
               <a href="#" class="brand">ORES - Admin Panel</a>
               
                       <div id="forgotpopup">
                       <div class="container">
   
    <form class="form-horizontal well" method="post" id="form" action="<?php echo base_url(); ?>admin/home/doforget">
        <fieldset>
          <legend>Reset password</legend>
         
            <div class="control-group">
               <div class="textbox forget_msg"><span  style="color: #FF0000;"></span>
               </div>
                <label for="forget_email"> Email</label>
                <input class="box" type="text" id="forget_email" name="email" />
            </div>
            <div class="form-actions">
                <input type="button" id="forget_submit" class="btn btn-primary" value="Reset" />
            </div>
            <?php if( isset($info)): ?>
                <div class="alert alert-success">
                    <?php echo($info) ?>
                </div>
                <?php endif; ?>
            <?php  if( isset($error)): ?>
                <div class="alert alert-error">
                    <?php echo($error) ?>
                </div>
            <?php endif; ?>
             
        </fieldset>
    </form>
</div> 
                        
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>

    <div class="container">

						<span style="color:#FF0000; margin-bottom:10px;"><?php  if(isset($msg) && $msg!='') echo $msg; ?></span>
	<?php if(isset($_GET['password']) && $_GET['password']=="reset"):  ?>
	  <div class="alert alert-success">
										<button data-dismiss="alert" class="close">x</button>
										<strong>Success!</strong> Password have been reset. Please check your email  
									</div>
       <?php 
	   endif;
	   $attributes = array('class' => 'form-signin');
	   echo form_open('admin',$attributes); ?>
        <h4 class="form-signin-heading">Admin Login</h4>
					<div id="login_er"></div>
						<div id="login_success"></div>
        <input type="text" name="ad_name"  id="ad_name" maxlength="10" class="input-block-level" placeholder="Username" >
        <input type="password" name="ad_pass"  id="ad_pass" maxlength="20" class="input-block-level" placeholder="Password">
        <!--<label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>-->
		<input type="button" class="btn btn-large btn-primary"  id="checklogin" value="Sign in">
		<a href="#" id='forgot_password_id' style="float:right;">Forgot Password</a>
		<?php echo form_close(); ?>

    </div> 
    <!-- /container -->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	 <script src="<?php echo base_url();?>js/1.9.1_jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jQuery.bPopup.js"></script>
	<script>
		$(document).ready(function(){
			var user; var pwd;
			user = $('#ad_name').val();
			if(user!=''){
				pwd = $('#ad_pass').focus();
			}else{
				$('#ad_name').focus();
			} 
			$("#ad_name").keyup(function(event){
				if(event.which == 13){ 
					$("#checklogin").click();
				}
			});	
			$("#ad_pass").keyup(function(event){
				if(event.which == 13){ 
					$("#checklogin").click();
				}
			});	
				
			$('#checklogin').click(function(){ 
				var username = $.trim($('#ad_name').val());
				var pass = $.trim($('#ad_pass').val());
				if(username !='' && pass !=''){
					
					//alert(username+' : '+pass);
					$.post("<?php echo base_url();?>admin/home/login_check",{"ad_name":username,"ad_pass":pass},function(data){	
						if(data == 'ok'){
							$('#login_success').html('Redirecting....');
							$('#login_er').css('display','none');
							window.location = '<?php echo base_url();?>admin/home';
						}else{
							$('#login_er').html('The username/password you have entered are not matched.');
							}
					}); 
				}else{
					$('#login_er').html('Please fill username/passwrod field(s).');
				}
			});	
			$('#forgot_password_id').click(function(e){
				e.preventDefault();
				$('.forget_msg>span').html("");
				$('#forget_email').val('');
				$('#forgotpopup').bPopup({
					
				});	
			});
 			$('#forget_submit').click(function(){
 				var email = $.trim($('#forget_email').val());
 				$.post("<?php echo base_url();?>admin/home/doforget",{"email":email},function(data){
 	 			     if(data=="true"){
  	 			    	$('.forget_msg>span').html("");
  	 			    	$('#login_er').html("Password has been reset and has been sent to email id:" + email);
  	 			    	window.location = '<?php echo base_url();?>admin/home?password=reset';
 	 			     }
 	 			     else{
 					$('.forget_msg>span').html("");
 					$('.forget_msg>span').html(data);
 	 			     }
 	 				
	 			});
	 			});
		});
	</script>
  </body>
 
</html>
