<script>
function changeURL(value){
 $(".inner_lt").empty().html('Loading.. <img src="<?php echo base_url(); ?>images/admin/ajax-loader.gif" />');
	var myURL = '<?php echo base_url(); ?>admin/driveravailability';
	document.location = myURL + "?did="+value;
}
</script>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
                <!--/span-->
<div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block" style="margin-top:30px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Manage Driver Availability</div>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12"> 
								
										<label>Select driver  </label><select id="driver" onchange="changeURL(this.value);">
														<option value="">--select one--</option>
														<?php 
															if(count($this->data['drivers'])>0):
																foreach($this->data['drivers'] as $drivers):
																	?>	
																	<option <?php if(isset($_GET['did']) && $_GET['did']==$drivers->id) echo "selected"; ?> value="<?php echo $drivers->id ?>"><?php echo $drivers->first_name ?> <?php echo $drivers->last_name ?></option>
																	<?php 
																
																endforeach;
															endif;

														?>
													</select> 
								<?php if(!empty($driverById)): ?>
								<span class="fr colorCal">
									<ul>
											<li>
												<div class="input-color">
													<span class="input">Available</span>
													<div class="color-box" style="background-color: #FFFF00;"></div>
												</div>
											</li>
											<li>
												<div class="input-color">
													<span class="input">Unavailable</span>
													<div class="color-box" style="background-color: #FF0000;"></div>
												</div>
											</li>
									</ul>								
								</span>
								<?php endif; ?>
								<div class="clear"></div>
								<div id='loading' style='display:none'>Loading..<img src="<?php echo base_url(); ?>images/admin/ajax-loader.gif" /></div>
								<div class="inner_lt" align="center">
								<?php if($this->input->get('did')!="") echo "<div id='calendar'></div>"; ?>
								</div>
									
									
								
								
								</div>
										
										
							
				</div>
			</td>
		</tr>
	</table>
</div>
<?php if(!empty($driverById)): ?>
<!--- Pop Up Manage Availablity ADD-->
<form name="availablityForm" id="availablityForm" method="post" action="">

<div class="modal fade" id="test_modal">

  <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Add Availablity</h3>
  </div>
  <span id="responce" style="margin-left:20px;"></span>
  <input type="hidden" name="adriver_id" id="adriver_id" value="<?php echo $driverById->id ?>" >

  <div class="modal-body">
	<table cellpadding="6">
		<tr>
			<td><label><strong>Driver Name: &nbsp;</strong></label></td> 
			<td> <input type="text" id="driver_name" name="driver_name" value="<?php echo $driverById->first_name."&nbsp;".$driverById->last_name  ?>"  disabled></td>
		</tr>
		<tr>
			<td><strong>Status:</strong> </td> <td>  <input type="radio" checked="checked" name="available" value="1">&nbsp; <span>Available</span> &nbsp;&nbsp;
				<input type="radio" name="available" value="0">&nbsp;<span>Unavailable</span>
			</td>
		</tr>
		<tr>
			<td><label><strong>Time Range From:&nbsp;</strong></strong></td>
			<td>
				<div class="input-append date from_datetime">
				<input  type="text"  name="time_range_from" id="time_range_from">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
			</td> 
		</tr>
		<tr>	
			<td><label><strong>Time Range To:</strong></label></td>
			<td><div class="input-append date to_datetime">
				<input  type="text"  name="time_range_to" id="time_range_to">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div></td> 
		</tr>
	</table>
  </div>
  <div class="modal-footer">
    <a href="javascript:void(0);" class="btn" data-dismiss="modal">Close</a>
    <a href="javascript:void(0);" id="sbmDriverAvailabilty" class="btn btn-primary">Save</a>
  </div>
</div>
</form>
<!--Edit Availablity -->

<form name="editavailablityForm" id="editavailablityForm" method="post" action="">

<div class="modal fade" id="edit_modal">

  <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Edit Availablity</h3>
  </div>
  <span id="responce" style="margin-left:20px;"></span>
  <input type="hidden" name="adriver_id" id="adriver_id" value="<?php echo $driverById->id ?>" >
      <input type="hidden" name="availablity_id" id="availablity_id" value="" >

  <div class="modal-body">
	<table cellpadding="6">
		<tr>
			<td><label><strong>Driver Name: &nbsp;</strong></label></td> 
			<td> <input type="text" id="edit_driver_name" name="edit_driver_name" value="<?php echo $driverById->first_name."&nbsp;".$driverById->last_name  ?>"  disabled></td>
		</tr>
		<tr>
			<td><strong>Status:</strong> </td> 
			<td>  <input type="radio" checked="checked" name="edit_available" id="edit_available_av" value="1">&nbsp; <span>Available</span> &nbsp;&nbsp;
				<input type="radio" name="edit_available" id="edit_available_nv" value="0">&nbsp;<span>Unavailable</span>
			</td>
		</tr>
		<tr>
			<td><label><strong>Time Range From:&nbsp;</strong></strong></td>
			<td>
				<div class="input-append date from_datetime">
				<input  type="text"  name="edit_time_range_from" id="edit_time_range_from">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
			</td> 
		</tr>
		<tr>	
			<td><label><strong>Time Range To:</strong></label></td>
			<td><div class="input-append date to_datetime">
				<input  type="text"  name="edit_time_range_to" id="edit_time_range_to">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div></td> 
		</tr>
	</table>
  </div>
  <div class="modal-footer">
    <a href="javascript:void(0);" class="btn" data-dismiss="modal">Close</a>
    <a href="javascript:void(0);" id="editDriverAvailabilty" class="btn btn-primary">Update</a>
  </div>
</div>
</form>


<?php endif; ?>
<script>	

$(document).ready(function() {

	$('#sbmDriverAvailabilty').click(function(){
		    var datastring = $("#availablityForm").serialize();
			$("#responce").empty().html('Loading.. <img src="<?php echo base_url(); ?>images/admin/ajax-loader.gif" />');
			$.ajax({
					url : "<?php echo base_url(); ?>admin/driveravailability/ajaxPostAvailability",
					method : "post",
					data: datastring,		
					type : "post",
					success : function(data) {
							$("#responce").html('<ul style="margin:0 0 0 21px;"><li style="color:green;list-style:none;margin:0px; padding:0px;" ><i class="icon-ok"></i>Driver availability saved</li></ul>');
							var myURL = '<?php echo base_url(); ?>admin/driveravailability';
							document.location = myURL + "?did=<?php echo $_GET['did'] ?>";
						}
				});
	});
	$('#editDriverAvailabilty').click(function(){
		    var datastring = $("#editavailablityForm").serialize();
			$("#responce").empty().html('Loading.. <img src="<?php echo base_url(); ?>images/admin/ajax-loader.gif" />');
			$.ajax({
					url : "<?php echo base_url(); ?>admin/driveravailability/ajaxEditAvailability",
					method : "post",
					data: datastring,		
					type : "post",
					success : function(data) {
							$("#responce").html('<ul style="margin:0 0 0 21px;"><li style="color:green;list-style:none;margin:0px; padding:0px;" ><i class="icon-ok"></i>Driver availability saved</li></ul>');
							var myURL = '<?php echo base_url(); ?>admin/driveravailability';
							document.location = myURL + "?did=<?php echo $_GET['did'] ?>";
						}
				});
	});

$('#test_modal').modal('hide');
	var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({	
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
				},
			timeFormat: 'hh:mm tt{ - hh:mm tt}' ,
				eventColor: '#FFFF00',
				textColor: '#000000',
				editable: true,	
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {
				//alert(new Date(y, m, d, 10, 30));
				//Start date functionality
				var fullStartDate,startDate,startMonth,startYear,startHours,startMinutes,startSeconds;
				startDate = start.getDate();
				if(startDate<10) startDate ="0"+startDate;
				startMonth = start.getMonth()+1;
				if(startMonth<9) startMonth ="0"+startMonth;
				startYear = start.getFullYear();
				startHours = start.getHours();
				if(startHours<10) startHours ="0"+startHours;
				startMinutes = start.getMinutes();
				if(startMinutes<10) startMinutes ="0"+startMinutes;
				startSeconds = start.getSeconds();
				if(startSeconds<10) startSeconds ="0"+startSeconds;
				fullStartDate = startYear+"-"+startMonth+"-"+startDate+" "+startHours+":"+startMinutes+":"+startSeconds;
				$("#time_range_from").val(fullStartDate);
				//alert(start+"=="+end);
			
				//enddatefunctionality
				var fullendDate,endDate,endMonth,endYear,endHours,endMinutes,endSeconds;
					endDate = end.getDate();
				if(endDate<10) endDate ="0"+endDate;
					endMonth = end.getMonth()+1;
				if(endMonth<9) endMonth ="0"+endMonth;
					endYear = end.getFullYear();
					endHours = end.getHours();
				if(endHours<10) endHours ="0"+endHours;
					endMinutes = end.getMinutes();
				if(endMinutes<10) endMinutes ="0"+endMinutes;
					endSeconds = end.getSeconds();
				if(endSeconds<10) endSeconds ="0"+endSeconds;
				fullendDate = endYear+"-"+endMonth+"-"+endDate+" "+endHours+":"+endMinutes+":"+endSeconds;
				$("#time_range_to").val(fullendDate);
					$("#responce").empty();
					$('#test_modal').modal('show');									
				},						
				events:{
					url:"<?php echo base_url(); ?>admin/driveravailability/getCalResponce",	
					data: 	{driver_id: '<?php echo $_GET['did'] ?>'}
				},
				
				loading: function(bool) {
					if (bool) $('#loading').show();
					else $('#loading').hide();
				},
				eventClick: function(event) {
						$.ajax({
								url : "<?php echo base_url(); ?>admin/driveravailability/ajaxGetAvailability",
								method : "post",
								data: 'eid='+event.id,		
								type : "post",
								success : function(data) {
										if(data==0){
											$('input:radio[name=edit_available]')[1].checked = true;
										}
										else{
											$('input:radio[name=edit_available]')[0].checked = true;
										}
									}
						});

							var fullStartDate,startDate,startMonth,startYear,startHours,startMinutes,startSeconds;
							startDate = (event.start).getDate();
							if(startDate<10) startDate ="0"+startDate;
							startMonth = (event.start).getMonth()+1;
							if(startMonth<9) startMonth ="0"+startMonth;
							startYear = (event.start).getFullYear();
							startHours = (event.start).getHours();
							if(startHours<10) startHours ="0"+startHours;
							startMinutes = (event.start).getMinutes();
							if(startMinutes<10) startMinutes ="0"+startMinutes;
							startSeconds = (event.start).getSeconds();
							if(startSeconds<10) startSeconds ="0"+startSeconds;
							fullStartDate = startYear+"-"+startMonth+"-"+startDate+" "+startHours+":"+startMinutes+":"+startSeconds;
							$("#edit_time_range_from").val(fullStartDate);
							//alert(start+"=="+end);

							//enddatefunctionality
							var fullendDate,endDate,endMonth,endYear,endHours,endMinutes,endSeconds;
							endDate = (event.end).getDate();
							if(endDate<10) endDate ="0"+endDate;
							endMonth = (event.end).getMonth()+1;
							if(endMonth<9) endMonth ="0"+endMonth;
							endYear = (event.end).getFullYear();
							endHours = (event.end).getHours();
							if(endHours<10) endHours ="0"+endHours;
							endMinutes = (event.end).getMinutes();
							if(endMinutes<10) endMinutes ="0"+endMinutes;
							endSeconds = (event.end).getSeconds();
							if(endSeconds<10) endSeconds ="0"+endSeconds;
							fullendDate = endYear+"-"+endMonth+"-"+endDate+" "+endHours+":"+endMinutes+":"+endSeconds;
							$("#edit_time_range_to").val(fullendDate);
							$("#availablity_id").val(event.id);
							
							$("#responce").empty();
							$('#edit_modal').modal('show');
			}
				
		});

 

  });</script>