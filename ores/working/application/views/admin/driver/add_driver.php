<?php
/*
 * This file is created to add drver. Author:Devi Lal Verma
 */
// created session variable to show previous value.
$address_title = $this->session->userdata ( 'address_title' );
$country = $this->session->userdata ( 'country' );
$status =$this->session->userdata('driverstatus');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
		<!--/span-->
		<div class="span9" id="content">
			<!-- morris stacked chart -->
			<div class="row-fluid">
				<!-- block -->
				<div class="block">
					<div class="navbar navbar-inner block-header">
						<div class="muted pull-left">Add Driver</div>
					</div>
					<div class="block-content collapse in">
						<div class="span12">
							<div id="error"> 
										<?php
										$errors = validation_errors ();
										if (! empty ( $wrong_insert )) {
											echo $wrong_insert;
										}
										// if(!empty($error)){echo $error;}
										?>
										<?php echo form_open();  ?>
																				
                             
									<div class="form-left">		
									<?php if(form_error('first_name')) { ?>
									
											<div class="txtbox1">

										<span class="validation" style="color: #FF0000;"><?php echo form_error('first_name'); ?></span>
									</div> <?php } ?>
											
											<div class="txtbox1">
										<label>First Name<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" maxlength="20"
											class="input-xlarge" name="first_name" id="first_name"
											maxlength="20" value="<?php echo set_value('first_name'); ?>">
									</div>
									</div>
									<div class="form-right">	
											<?php if(form_error('last_name')) { ?>							
											<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('last_name'); ?></span>
										</div> <?php } ?>
											<div class="txtbox1">
											<label>Last Name<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" maxlength="20"
												class="input-xlarge" name="last_name" id="last_name"
												maxlength="20" value="<?php echo set_value('last_name'); ?>">
										</div>
									</div>
									
									<div class="form-left">	
										<?php if(form_error('username')) { ?>							
											<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('username'); ?></span>
										</div> <?php } ?>
											<div class="txtbox1">
											<label>Username<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="username" id="username"
												maxlength="20" value="<?php echo set_value('username'); ?>">
										</div>
									</div>
									<div class="form-right">	
										<?php if(form_error('email')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('email'); ?></span>
										</div> <?php } ?>
	
										<div class="txtbox1">
											<label>Email<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" class="input-xlarge"
												id="email" name="email"
												value="<?php echo set_value('email'); ?>">
										</div>
									</div>
									<div style="clear:both;"></div>

									<!--  <div class="form-left">	
									<?php if(form_error('company_name')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('company_name'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Company Name</label> <input type="text"
												class="input-xlarge" name="company_name" id="company_name"
												maxlength="20" value="<?php echo set_value('company_name'); ?>">
										</div>
									</div>-->
									<div class="form-left">	
										<?php if(form_error('mobile_number')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('mobile_number'); ?></span>
										</div> <?php } ?>
										
										<div class="txtbox1">
											<label>Mobile Number<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="mobile_number" id="mobile_number"
												maxlength="12" value="<?php echo set_value('mobile_number'); ?>">
										</div>
									</div>
									<div class="form-right">	
									<?php if(form_error('emergency_contact')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('emergency_contact'); ?></span>
										</div> <?php } ?>
									
										<div class="txtbox1">
											<label>Emergency Contact</label> <input type="text"
												class="input-xlarge" name="emergency_contact" id="emergency_contact"
												maxlength="12" value="<?php echo set_value('emergency_contact'); ?>">
										</div>
									</div>
									<div class="form-left">
									<?php if(form_error('address_title')) { ?>
										<div class="txtbox1">
										   <span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
										   </div> <?php } ?>
										   <div class="txtbox1">
											<label>Address Title<span class="validation" style="color:#FF0000;">*</span></label>
											<?php
											$options = array ('' => '-- Please select --', '1' => 'Home', '2' => 'Office', '3' => 'Other' );
											if (isset ( $address_title )) {
												$index = $address_title;
											} else {
												$index = 0;
											}
											echo form_dropdown ( 'address_title', $options, $index, 'class="input-xlarge"' );
											?>
									</div>
									</div>
									<div class="form-right">	
										<?php if(form_error('address_1')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('address_1'); ?></span>
										</div> <?php } ?>
	
										<div class="txtbox1">
											<label>Address Line 1<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="address_1" id="address_1"
												maxlength="50" value="<?php echo set_value('address_1'); ?>">
										</div>
									</div>
									<div class="form-left">
										<?php if(form_error('address_2')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('address_2'); ?></span>
										</div> <?php } ?>
	
										<div class="txtbox1">
											<label>Address Line 2<span class="validation" style="color:#FF0000;"></span></label> <input type="text"
												class="input-xlarge" name="address_2" id="address_2"
												maxlength="50" value="<?php echo set_value('address_2'); ?>">
										</div>
									</div>
									<div class="form-right">
										<?php if(form_error('city')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('city'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>City<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" class="input-xlarge"
												name="city" id="city"
												maxlength="30" value="<?php echo set_value('city'); ?>">
										</div>
									</div>
									<div class="form-left">
										<?php if(form_error('state')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('state'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>State<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" class="input-xlarge"
												name="state" id="state"
												maxlength="30" value="<?php echo set_value('state'); ?>">
										</div>
									</div>
									<!--<div class="form-left">
										<?php if(form_error('country')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('country'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Country<span class="validation" style="color:#FF0000;">*</span></label> <select id="country" class="shadow"
												name="country" id='country'>
												<option value="">-- Please select --</option>
													<?php
													/* $ctrb = $this->db->get ( 'country' );
													foreach ( $ctrb->result () as $ctrb ) {
														if ($country == $ctrb->CountryID) {
															$sel = 'selected=selected';
														} else {
															$sel = '';
														}
														echo "<option value='" . $ctrb->CountryID . "' " . $sel . ">";
														echo $ctrb->CountryName;
														echo "</option>";
													} */
													?>
										</select>
										</div>
									</div>-->
								
									<div class="form-right">
										<?php if(form_error('zip_code')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('zip_code'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Zip Code<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="zip_code" id="zip_code"
												maxlength="10" value="<?php echo set_value('zip_code'); ?>">
										</div>
									</div>
										<div class="form-left">
										<?php if(form_error('pay_rate')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Pay Rate Per Hour<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="pay_rate" id="pay_rate"
												maxlength="10" value="<?php echo set_value('pay_rate'); ?>">
										</div>
									</div>
									<!--pay rate per mile-->
										<div class="form-right">
										<?php if(form_error('pay_rate_permile')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate_permile'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Pay Rate Per Mile<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="pay_rate_permile" id="pay_rate_permile"
												maxlength="10" value="<?php echo set_value('pay_rate_permile'); ?>">
										</div>
									</div>
									
									
									
									<!--end-->
									<div class="form-left">
										<?php if(form_error('driver_note')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('driver_note'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Driver Note<span class="validation" style="color:#FF0000;"></span></label>
											<textarea class="expendible-div"  name="driver_note" id="driver_note"><?php echo set_value('driver_note');?></textarea>	
										</div>
									</div>
										<div class="form-right">
										<?php if(form_error('mobile_provider')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('mobile_provider'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
											<label>Mobile Provider<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
												class="input-xlarge" name="mobile_provider" id="mobile_provider"
												 value="<?php echo set_value('mobile_provider'); ?>">
										</div>
									</div>
									
									<!--<div class="form-right">
									<?php if(form_error('status')) { ?>							
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('status'); ?></span>
										</div> <?php } ?>
										<div class="txtbox1">
										<label>Status<span class="validation" style="color:#FF0000;">*</span></label>
										<?php
										
	                                   $options = array ('' => '-- Please select --', '1' => 'Active', '2' => 'Inactive' );
										if (isset ($status )) {
										$index = $status;
											} else {
												$index = 0;
											} echo form_dropdown('status', $options, $index);?>
										</div>
										</div>-->
										<div class="txtbox1 form-right clear">
	
											<input class="btn btn-primary btn-large" type="submit"
												name="add_driver" value="Submit">
										</div>
									
									<?php echo form_close() ; ?>
							
							</div>
							 <script src='<?php echo base_url();?>js/phone_mask2.js'></script>	
							<!--<div class="wrapper">
	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside">
				<?php $this->load->view('template/admin_sidebar'); ?> 
			</td>
  
			<td class="contentbox">
				<div id="mainContent">
					<div class="grayStripRight">Add User</div><br/>
					<div id="error">
							<?php
							$errors = validation_errors ();
							if (! empty ( $wrong_insert )) {
								echo $wrong_insert;
							}
							// if(!empty($error)){echo $error;}
							?>
					</div>
					<?php
					
echo form_open ();
					
					if (form_error ( 'first_name' )) {
						?>
							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>First Name</label>
							<input type="text" class="input-xlarge" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
						</div>	
						<?php if(form_error('last_name')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Last Name</label>
							<input type="text" class="input-xlarge" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>">
						</div>
						
						<?php if(form_error('username')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Username</label>
							<input type="text" class="input-xlarge" name="username" id="username" value="<?php echo set_value('username'); ?>">
						</div>

						<?php if(form_error('email')) { ?>
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Email</label>
							<input type="text" class="input-xlarge" id="email" name="email" value="<?php echo set_value('email'); ?>">
						</div>
						
						<?php if(form_error('password')) { ?>
						<div class="txtbox1"> 
						
						<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
						<label>Password</label>
						<input type="password" class="input-xlarge" name="password" value="<?php echo set_value('password'); ?>">
						</div>
						<div class="txtbox1"> 
						<label>Role</label>
						<?php
						
$options = array (

						'1' => 'User', '2' => 'Admin' )

						;
						?>
						<?php echo form_dropdown('role', $options, '1');?>
						</div>
						<?php if(form_error('company_name')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Company Name</label>
							<input type="text" class="input-xlarge" name="company_name" id="company_name" value="<?php echo set_value('company_name'); ?>">
						</div>
						
						<?php if(form_error('mobile_number')) { ?>
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Mobile Number</label>
							<input type="text" class="input-xlarge" name="mobile_number" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>">
						</div>
						<?php if(form_error('other_number')) { ?>
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Other Number</label>
							<input type="text" class="input-xlarge" name="other_number" id="other_number" value="<?php echo set_value('other_number'); ?>">
						</div>
						<div class="txtbox1"> 
						<label>Address Title</label>
						<?php
						
$options = array ('1' => 'Home', '2' => 'Office', '3' => 'Other' );
						?>
						<?php echo form_dropdown('address_title', $options, '1');?>
						</div>
						
						
						<?php if(form_error('address_1')) { ?>
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 1</label>
							<input type="text" class="input-xlarge" name="address_1" id="address_1" value="<?php echo set_value('address_1'); ?>">
						</div>	
						<?php if(form_error('address_2')) { ?>
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 2</label>
							<input type="text" class="input-xlarge" name="address_2" id="address_2" value="<?php echo set_value('address_2'); ?>">
						</div>	
						
						<?php if(form_error('city')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>City</label>
							<input type="text" class="input-xlarge" name="city" id="city" value="<?php echo set_value('city'); ?>">
						</div>
						<?php if(form_error('state')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>State</label>
							<input type="text" class="input-xlarge" name="state" id="state" value="<?php echo set_value('state'); ?>">
						</div>
						<?php if(form_error('country')) { ?>							
						<div class="txtbox1"> 
							
							<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Country</label>
							<input type="text" class="input-xlarge" name="country" id="country" value="<?php echo set_value('country'); ?>">
						</div>
						
						<?php if(form_error('zip_code')) { ?>							
						<div class="txtbox1"> 
						
						<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Zip Code</label>
						<input type="text" class="input-xlarge" name="zip_code" id="zip_code" value="<?php echo set_value('zip_code'); ?>">
						</div>
							<div class="txtbox1"> 
						<label>Status</label>
						<?php
						
$						$options = array ('1' => 'Active', '2' => 'Inactive' )

						;
						?>
						<?php echo form_dropdown('status', $options, '1');?>
						</div>
						<div class="txtbox1"> 
							
							<input style=" margin-top:10px;" class="submit_c_ebook" type="submit" name="adduser" value="Submit">
						</div>
					
					<?php echo form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>

<script>
$(document).ready(function() {

    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
    $('input.datepicker').Zebra_DatePicker();

 });
 </script>-->