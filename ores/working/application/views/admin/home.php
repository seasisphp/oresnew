<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
				<?php $this->load->view('template/admin_sidebar'); ?>   
			</div>
  <div class="span9" id="content">
                    <div class="row-fluid" style="margin-top:30px;" >
						<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <li>
	                                        <a href="#">Dashboard</a> 
	                                    </li>
	                                   
	                                </ul>
                            	</div>
                        	</div>
	 
				</div>  
				
				<!--BLock Start-->
				<div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Latest Registered users</div>
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php if(!empty($this->data['users'])):
													foreach($this->data['users'] as $users):
										  ?>
										  <tr>
                                                <td><?php echo $users->id;  ?></td>
                                                <td><?php echo $users->first_name;  ?></td>
                                                <td><?php echo $users->username;  ?></td>
                                                <td><?php echo $users->email;  ?></td>
                                            </tr>
                                           <?php 
												endforeach;	
										   
										   endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Latest Drivers</div>
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <?php if(!empty($this->data['drivers'])):
													foreach($this->data['drivers'] as $drivers):
										  ?>
										  <tr>
                                                <td><?php echo $drivers->id;  ?></td>
                                                <td><?php echo $drivers->first_name;  ?></td>
                                                <td><?php echo $drivers->email;  ?></td>
                                            </tr>
                                           <?php 
												endforeach;	
										   
										   endif; ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
					
					
					<div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Latest Completed Trips</div>
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Trip #</th>
                                                <th>Driver Name</th>
                                                <th>Client Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($this->data['reservation'])):
													foreach($this->data['reservation'] as $reservation):
										  ?>
										  <tr>
                                                <td><?php echo $reservation->id;  ?></td>
                                                <td><?php echo $reservation->first_name;  ?>&nbsp;<?php echo $reservation->last_name;  ?></td>
                                                <td><?php echo $reservation->u_fname;  ?>&nbsp;<?php echo $reservation->u_lname;  ?></td>
                                            </tr>
                                           <?php 
												endforeach;	
										   
										   endif; ?>
                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            
                        </div>
                    </div>
					
					
					
				
				<!--Block End-->
				
	 
</div>    