 <?php 
    $home_address=$this->session->userdata('home_address');
    $basic_detail= $this->session->userdata('basic_detail');
    //$is_phone=$this->session->userdata('is_phone');
    ?>
  
  <link href="<?php echo base_url(); ?>css/front_end/jquery-ui.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/timepicker.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/front_end/reservation.css" rel="stylesheet" media="screen">
	<script src='<?php echo base_url(); ?>js/front_end/jquery-ui.js'></script>
	<script src='<?php echo base_url();?>js/front_end/airport_pickup.js'></script>
	<!--<script src='<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js'></script>-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/timeEntry/jquery.timeentry.css"> 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.min.js"></script>
<div id="wrapper">

  <div class="new-container">
    <h2>Airport Pick Up Reservation</h2>
    <?php $attributes = array('id' => 'airport_pickup');
    $hidden = array('service_id' => '2');
    echo form_open('reservation/airport_pickup',$attributes,$hidden); ?>
   <div class="form-container">
  	
  <div class="new-box-text">
      <div class="new-left-form" >
      <!--  <h4>Trip Date</h4>-->
    	
	<?php if(form_error('trip_date_from')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_from'); ?></span>
						</div> <?php } ?>

		<fieldset>  <!--  <h4>Trip Date</h4>-->
		<span class="label-txt" id="date_span">Trip Date<em>*</em></span>
		<input type="text" class="datetime" tabindex="1" readonly="readonly" id="trip_date_from" name="trip_date_from" value="<?php echo set_value('trip_date_from'); ?>">
		</fieldset>
		</div>
        
     
      <div class="new-right-form" >
      <?php if(form_error('pickup_time')) { ?>
		
		<label>&nbsp;</label>
		<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
		 <?php } ?>
						
		<fieldset>  <!--  <h4>Trip Date</h4>-->
		<span class="label-txt" style="width: 100px; id="time_span">Pick Up Time<em>*</em></span>
		<input type="text" class="datetime" tabindex="2" name="pickup_time" id="pickup_time"  placeholder="HH:MM" readonly="readonly" value="<?php echo set_value('pickup_time'); ?>">
		</fieldset>
        
      </div>
      </div>
      <div class="clr"></div>
    </div>
    <div class="address-container">
      <h3>Reservation Information(If different than primary address then please edit below)</h3>
      <div class="form-container">
         <div class="upper-form">
          <fieldset>
         <?php if(form_error('address')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Address Line 1<em>*</em></span>
            <input type="text" id="address" name="address" tabindex="3" title="Address" value="<?php if(!empty($home_address[0]->address_1)){ echo $home_address[0]->address_1; }else {echo set_value('address');} ?>" />
          </fieldset>
            <fieldset>
            <?php if(form_error('address_2')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Address Line 2<em></em></span>
            <input type="text" id="address_2" name="address_2" tabindex="4" title="Address Line 2" value="<?php if(!empty($home_address[0]->address_2)){ echo $home_address[0]->address_2; }else {echo set_value('address_2');} ?>" />
          </fieldset>
          
        </div>
        <div class="upper-form-left-form">
          <fieldset>
             <?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">City <em>*</em></span>
            <input type="text" id="city" name="city" tabindex="4" title="City" value="<?php if(!empty($home_address[0]->city)){ echo $home_address[0]->city; }else { echo  set_value('city'); }?>" />
         
          </fieldset>
     
          <fieldset>
                <?php if(form_error('zip')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Zip <em>*</em></span>
            <input type="text" id="zip" name="zip" title="zip" tabindex="6" value="<?php if(!empty($home_address[0]->zip_code)){ echo $home_address[0]->zip_code; }else {echo set_value('zip'); }?>" />
           
          </fieldset>
            <fieldset>
             	 <?php if(form_error('contact_number')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_number'); ?></span>
						</div> <?php } ?>
            <span class="label-txt-title">Contact Phone<em>*</em></span>
           <input type="text" id="contact_number" tabindex="8" maxlength="12" name="contact_number" title="Contact Phone" value="<?php if(!empty($basic_detail->mobile_number)){ echo $basic_detail->mobile_number; }else { echo set_value('contact_number');} ?>" />
          </fieldset>
              	<fieldset >
           <?php if(form_error('manaual_shift')) { ?>							
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('manaual_shift'); ?></span>
									</div> <?php } ?>
            <span  id="manual_shift_span"  class="label-txt-title">Manual Shift<em>*</em></span>
           <p style="height: 34px;"> <span>
            <input type="radio" tabindex="9" name="manaual_shift" id="manaual_shift_y" value="2" />
            YES</span> <span>
            <input type="radio" tabindex="10" name="manaual_shift" id="manaual_shift_n" value="1" />
            NO  </span></p> 
          </fieldset>
     
        </div>
        <div class="upper-form-right-form">
          <fieldset>
          <?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">State <em>*</em></span>
            <input type="text" id="state" name="state" tabindex="5" title="State" value="<?php if(!empty($home_address[0]->state)){ echo $home_address[0]->state; }else {echo set_value('state');} ?>" />
          </fieldset>
           <fieldset>
           	<?php if(form_error('contact_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_name'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Contact Person<em>*</em></span>
            <input type="text" id="contact_name" tabindex="7" name="contact_name" title="Contact Person" value="<?php if(!empty($basic_detail->first_name)){ echo $basic_detail->first_name.' '.$basic_detail->last_name; }else{ echo set_value('contact_name');} ?>" />
          </fieldset>
        </div>
          
           
        
        </div>
        
        <div class="clr"></div>
       
    <div class="address-container">
      <h3>Airport Information</h3>
      <div class="form-container">
      <div class="upper-form-left-form">
          <fieldset style="height: 51px;width: 405px;">
          <?php if(form_error('airport')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airport'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Airport<em>*</em></span>
          <input type="text" style="width:207px;" title="Airport" tabindex="11" name="airport" id="airport" value="<?php echo set_value('airport'); ?>">
          </fieldset>
         
            <fieldset style="height: 51px;width: 405px;">
               <?php if(form_error('flight_number')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('flight_number'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Flight Number<em>*</em></span>
         <input type="text" style="width:207px;" name="flight_number" tabindex="13" id="flight_number" title="Flight Number" value="<?php echo set_value('flight_number'); ?>">
          </fieldset>
        </div>
         <div class="upper-form-right-form">
         <fieldset style="height: 51px;width: 405px;">
        <?php if(form_error('airline')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airline'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Airline<em>*</em></span>
            <input type="text" style="width:207px;" name="airline" tabindex="12" id="airline" title="Airline" value="<?php echo set_value('airline'); ?>">
          </fieldset >
          </div>
        <div class="login-container-left-form">
        	 <span id="approx_js_error"></span>
      		<!-- <fieldset>
            <?php if(form_error('approx_hours')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('approx_hours'); ?></span>
						</div> <?php } ?>
            <span class="approx_hour_lable">Approx. Length of<em>*</em> Trip in hours</span>
            <input type="text" class="small-input " tabindex="9" id="approx_hours" name="approx_hours" title="Approx. Length of Trip in hours" value="<?php echo set_value('approx_hours'); ?>" />
           </fieldset>	-->
          
          	
       
               
          </div>
          

      
             <div class="clr"></div>
        <div class="login-container-lower-form">
         <div class="form-container">
          <fieldset >	
            <span class="label-txt-title-large">Special Request or <br />Comments</span>
             <textarea class="large-input valid" tabindex="14" id="comments" name="comments" title="Special Request or Comments" cols="50" rows="4"> <?php echo set_value('comments'); ?></textarea>
          </fieldset>
       
          <fieldset class="promo-code-new">
             <span id="promo_code_span"></span>
            <span class="label-txt-crd" id="promo_code_span">Promo Code</span>
            <input type="text" id="promo_code" tabindex="15" name="promo_code" title="Promo Code" value="<?php echo set_value('promo_code"'); ?>" />
          </fieldset>
  
        </div>
         </div>
         <div class="form-container">
         <div class="credit-upper-form">
        	<fieldset id="term_fielset">
            <p>
              <input id="term_condition" tabindex="16"  name="term_condition"  type="checkbox" />
              I accept the <a href="#">Terms & Conditions</a><em style="color: #CB4343;">*</em></p>
          </fieldset>
          </div>
          </div>
  		<div class="clr"></div>
          <div class="submit-btn1 credit-upper-form-submit">
          <span>Schedule An Airport Drop Off?
          </span>
    	<a href="airport_dropoff"><input tabindex="15" type="button" value="Click Here" /></a> 
     
          </div>
          <div class="submit-btn2 credit-upper-form-submit">
          	<span>Submit Reservation
         	 </span>
    		<input tabindex="17" type="submit" value="Click Here" />
          </div>
          <div class="clr"></div>
       
          <div class="credit-upper-form-submit">
        	 <button type="button" tabindex="18"  class="cancel-button" onClick="location.href = '<?php echo base_url()?>reservation/reserve_driver'">Cancel</button>
         </div>
      </div>
      <?php echo form_close(); ?>	
    </div>
    
</div>
 
</div>

