 
 <?php $user = $this->session->userdata('user_record') ;
	$billingdetail = $this->session->userdata('billing_record') ;
/*  echo "<pre>"; 
print_r($_POST);
print_r($this->data['user']) ;
print_r($this->data['billingdetail']) ; */

?>
 <script src='<?php echo base_url();?>js/front_end/account_setting.js'></script>
 <div class="register-container">
    <h2>Change Account Setttings</h2>
	<?php
	$attributes = array('id' => 'account_setting');
	echo form_open('',$attributes);
	?>
    <h3>Change Basic info</h3>
    <div class="form-container">
	<?php if(isset($msg)) { ?>

	<span class="validation" style="color:#009900;"><?php echo $msg; ?></span>

	<?php }?>
	
      <div class="left-form">
        <fieldset>
			 
          <span class="label-txt">First Name<em>*</em></span>
		   <div class="input-container">
          <input type="text" id="first_name" tabindex="1" name= "first_name" maxlength="20" title="First Name" value="<?php if(!empty($user->first_name)) echo $user->first_name; else echo $_POST['first_name'];?>" />
			<?php if(form_error('first_name')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
			</div> <?php } ?>
			</div>
		</fieldset>
        <fieldset>
		
          <span class="label-txt">Email<em>*</em></span>
		   <div class="input-container">
          <input type="text" id="email" name= "email" tabindex="3" title="Email" value="<?php if(isset($user->email)) echo $user->email; else echo $_POST['email'];?>" />
        <?php if(form_error('email')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
			</div> <?php } ?>
			</div>
		</fieldset>
        <fieldset>
		
          <span class="label-txt">Company Name</span>
		   <div class="input-container">
          <input type="text" id="company_name" name= "company_name" tabindex="5" title="Company Name" maxlength="100" value="<?php if(isset($user->company_name)) echo $user->company_name; else echo $_POST['company_name'];?>" />
        <?php if(form_error('company_name')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
			</div> <?php } ?>
			</div>
		</fieldset>
        <fieldset>
		
          <span class="label-txt">Other Phone</span>
		   <div class="input-container">
          <input type="text"  id="other_number" name= "other_number" tabindex="6" title="Other Name" maxlength="12"
           value="<?php if(isset($user->other_number)) echo $user->other_number; else echo $_POST['other_number'];?>" />
        <?php if(form_error('other_number')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
			</div> <?php } ?>
			</div>
		</fieldset>
      </div>
      <div class="right-form">
        <fieldset>
	
          <span class="label-txt">Last Name<em>*</em></span>
		    <div class="input-container">
          <input type="text" id="last_name" tabindex="2" name= "last_name" title="Last Name" maxlength="20" value="<?php if(!empty($user->last_name)) echo $user->last_name; else echo $_POST['last_name'];?>" />
		<?php if(form_error('last_name')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
			</div> <?php } ?>   
</div>			
	   </fieldset>
        <!--<fieldset>
          <span class="label-txt">Re Enter Email</span>
          <input type="text" id="other_number" name= "other_number" title="Email" value="" />
        </fieldset>-->
        <fieldset>
		
          <span class="label-txt">Mobile Phone<em>*</em></span>
		  <div class="input-container">
          <input type="text" id="mobile_number" name="mobile_number" tabindex="4" maxlength="12" title="Mobile Number" 
          value="<?php if(!empty($user->mobile_number)) echo $user->mobile_number; else echo $_POST['mobile_number'];?>" />
        <?php if(form_error('mobile_number')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
			</div> <?php } ?>
			</div>
		</fieldset>
      </div>
	  
      <div class="clr"></div>
	   <!--<div class="credit-upper-form-submit">
     <input type="button" id="basicinfo" value="Submit" />
          </div>-->
    </div>
	
    <div class="address-container">
      <h3>Change Primary Address</h3>
      <div class="form-container">
	   <div class="upper-form-left-form">
          <fieldset>
		  
            <span class="label-txt">Address Title<em>*</em></span>
			<div class="input-container">
           	<?php 
           	$options = array(
						'' => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); 
							 if(isset($user->address_title)){
							   $index=$user->address_title;
								}else if($_POST['address_title']){
								 $index=$_POST['address_title'];
						 	 } else{
						 	  	$index=0;
						 	  }	echo form_dropdown('address_title', $options, $index, 'tabindex="7"', 'style="margin:0px"');?>
			 <?php if(form_error('address_title')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
         
        </div>
        <div class="upper-form">
          <fieldset>
		  
            <span class="label-txt">Address Line 1<em>*</em></span>
			  <div class="input-container">
            <input type="text" id="address_1" name= "address_1" tabindex="8" title="Address line 1" maxlength="225" value="<?php if(!empty($user->address_1)) echo $user->address_1; else echo $_POST['address_1'];?>" />
          <?php if(form_error('address_1')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
          <fieldset>
		
            <span class="label-txt">Address Line 2</span>
			<div class="input-container">
            <input type="text"  id="address_2" name= "address_2" tabindex="9" title="Address line 2" maxlength="225" value="<?php if(isset($user->address_2)) echo $user->address_2; else echo $_POST['address_2'];?>" />
            <?php if(form_error('address_2')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
		  
            <span class="label-txt">City<em>*</em></span>
			<div class="input-container">
            <input type="text" id="city" name= "city" tabindex="10" maxlength="30" title="City" value="<?php if(!empty($user->city)) echo $user->city; else echo $_POST['city'];?>" />
          <?php if(form_error('city')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
		   <!-- <fieldset>
            <span class="label-txt">Country<em>*</em></span>
			<div class="input-container">
            <select style="margin:0px;" id="country" class="shadow" name="country" id='country'>
												<option value="">-- Please select --</option>
												<?php
												$ctrb   = $this->db->get('country');
												foreach($ctrb->result() as $ctrb){
													if($user->country==$ctrb->CountryID || $_POST['country']==$ctrb->CountryID){
													$sel= 'selected=selected';}
													else{
														$sel='';
													}
												echo "<option value='".$ctrb->CountryID."' ".$sel.">";
												echo $ctrb->CountryName;
												echo "</option>";
											}
											?>
									</select>		
			 <?php if(form_error('country')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>-->
         
		   <fieldset>
		  
            <span class="label-txt">State<em>*</em></span>
			<div class="input-container">
            <input type="text" id="state" name= "state" tabindex="11" title="State" maxlength="30" value="<?php if(!empty($user->state)) echo $user->state; else echo $_POST['state'];?>" />
			<?php if(form_error('state')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
			</div> <?php } ?>  
</div>			
		 </fieldset>
		   <fieldset>
		
            <span class="label-txt">Zip<em>*</em></span>
			<div class="input-container">
            <input type="text" id="zip_code" name= "zip_code" tabindex="12" maxlength="10" title="Zip Code"  value="<?php if(!empty($user->zip_code)) echo $user->zip_code; else echo $_POST['zip_code'];?>" />
            <?php if(form_error('zip_code')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
			</div> <?php } ?>
		  </fieldset>
        </div>
       
        <div class="clr"></div>
		<div class="form-container-text">
          <p>Is this address also your billing address( the address that appears on your credit card or bank statement?) </p>
          <div class="clr"></div>
          <p> 
		   <?php if(form_error('billing_address_status')) { ?>
			<div class="txtbox1"> 
			<label>&nbsp;</label>
			<span class="validation" style="color:#FF0000;"><?php echo form_error('billing_address_status'); ?></span>
			</div> <?php } ?>
		  <span>
		 

            <input type="radio" id="billingaddressyes" tabindex="13" <?php if(isset($user->billing_address_status) && $user->billing_address_status== 0){ ?> checked="checked"  <?php } else if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==0){ ?> checked="checked"  <?php } ?> name="billing_address_status" value="0">
            YES</span> <span>
            <input type="radio" id= "billingaddressno"  tabindex="14" <?php if(isset($user->billing_address_status) && $user->billing_address_status== 1){ ?> checked="checked"  <?php }else if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==1){ ?> checked="checked"  <?php } ?>name="billing_address_status" value="1">
            NO (If not then please fill below the details) </span> 
        </p></div>
        <div class="clr"></div>
		<!-- <div class="credit-upper-form-submit">
     <input type="button" id="primaryaddress" value="Submit" />
          </div>-->
      </div>
    </div>
	 <div class="address-container">
      <h3>Change Password</h3>
      <div class="form-container">
        <div class="upper-form">
      
        </div>
        <div class="upper-form-left-form">
		<?php if(isset($msgp)) { ?>

	<span class="validation" style="color:#FF0000;"><?php echo $msgp; ?></span>

	<?php } ?>
          <fieldset>
		  
            <span class="label-txt">Old Password<em>*</em></span>
			<div class="input-container">
            <input type="password" id="oldpassword" name= "oldpassword"  tabindex="15" maxlength="20" title="Old Password" value="" />
			<?php if(form_error('oldpassword')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('oldpassword'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
          <fieldset>
		 
            <span class="label-txt">New Password<em>*</em></span>
			<div class="input-container">
            <input type="password" id="password" name= "password" tabindex="16" title="Password" maxlength="20" title="First Name" value="" />
			<?php if(form_error('password')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
			</div> <?php } ?> 
</div>			
		 </fieldset>
		   <fieldset>
		  
            <span class="label-txt">Re-Enter Password<em>*</em></span>
			<div class="input-container">
            <input type="password" id="repassword" name= "repassword" tabindex="17" maxlength="20" title="Re-Enter password" value="" />
           <?php if(form_error('repassword')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('repassword'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
        </div>
   
        <div class="clr"></div>
		 <!--<div class="credit-upper-form-submit">
     <input type="button" id="changepassword" value="Submit" />
          </div>-->
                  <div class="credit-upper-form-right" style="margin-top: 60px;">
          <h4> Exp<em style="color: #CB4343;">*</em></h4>
		   <div class="input-container">
          <select name="exp_month" id="exp_month" class="select-large">
			 <option value="">Month</option>
            <option value="01" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==01){?> selected <?php }?>>Jan</option>
            <option value="02" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==02){?> selected <?php }?>>Feb</option>
			<option value="03" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==03){?> selected <?php }?>>Mar</option>
			<option value="04" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==04){?> selected <?php }?>>Apr</option>
			<option value="05" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==05){?> selected <?php }?>>May</option>
			<option value="06" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==06){?> selected <?php }?>>Jun</option>
			<option value="07" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==07){?> selected <?php }?>>July</option>
			<option value="08" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==8){?> selected <?php }?>>Aug</option>
			<option value="09" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==9){?> selected <?php }?>>Sep</option>
			<option value="10" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==10){?> selected <?php }?>>Oct</option>
			<option value="11" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==11){?> selected <?php }?>>Nov</option>
			<option value="12" <?php if(!empty($_POST['exp_month'])&& $_POST['exp_month']==12){?> selected <?php }?>>Dec</option>
			
		
          </select>
		 
          <select name="exp_year" id="exp_year" class="select-large" >
			<option value="">Year</option>
			<?php for($i=2013; $i<=2030; $i++){?>
            <option value="<?php echo $i ?>" <?php if(isset($_POST['exp_year'])&& $_POST['exp_year']== $i){?> selected <?php }?>><?php echo $i?></option>
			<?php }?>
          </select>
		  <?php if(form_error('exp_month')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_month'); ?></span>
			</div> <?php } ?>
		  <?php if(form_error('exp_year')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_year'); ?></span>
			</div> <?php } ?>
			</div>
        </div>
      </div>
    </div>
    <div class="address-container">
      <h3>Change Billing Address And Credit Card Info</h3>
      <div class="form-container">
        <div class="upper-form">
        <h4>( Enter credit card number to change billing address )</h4>
          <fieldset>
            <span  class="label-txt">Credit Card Number<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="creditcard_number" maxlength="19" name= "creditcard_number" placeholder="xxxxxxxxxxxxx" tabindex="6" title="Credit Card number" value="<?php  echo set_value('creditcard_number');?>" />
			<?php if(form_error('creditcard_number')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('creditcard_number'); ?></span>
			</div> <?php }
			if($this->session->flashdata('errmsg')!='' && $this->session->flashdata('errmsg')!=null){?>
				<div class="txtbox1">
				<span class="validation" style="color:#FF0000;"><?php echo $this->session->flashdata('errmsg'); ?></span>
			</div>
			<?php }
			 ?>
			</div>
		</fieldset>
		
          <fieldset>
		
            <span class="label-txt">Address Line 1<em>*</em></span>
				<div class="input-container">
            <input type="text" id="address_1b" name= "address_1b" tabindex="18" maxlength="225" title="Address line 1" value="<?php if(!empty($billingdetail->address_1)) echo $billingdetail->address_1; elseif(isset($_POST['address_1b'])) echo $_POST['address_1b']; ?>" />
			<?php if(form_error('address_1b')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1b'); ?></span>
			</div> <?php } ?>     
</div>			
		</fieldset>
          <fieldset>
		
            <span class="label-txt">Address Line 2</span>
				<div class="input-container">
            <input type="text" id="address_2b" name= "address_2b" tabindex="19" maxlength="225" title="Address line 2" value="<?php if(!empty($billingdetail->address_2)) echo $billingdetail->address_2; elseif(isset($_POST['address_2b'])) echo $_POST['address_2b']; ?>" />
            <?php if(form_error('address_2b')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2b'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
        </div>
        <div class="upper-form-left-form">
		<!-- <fieldset>
            <span class="label-txt">Country<em>*</em></span>
				<div class="input-container">
            <select style="margin:0px;" id="country_b" class="formselectbox" name="country_b">
												<option value="">-- Please select --</option>
												<?php
												$ctrb   = $this->db->get('country');
												foreach($ctrb->result() as $ctrb){
													if($billingdetail->country==$ctrb->CountryID || $_POST['country_b']==$ctrb->CountryID){
													$sel= 'selected=selected';}
													else{
														$sel='';
													}
												echo "<option value='".$ctrb->CountryID."' ".$sel.">";
												echo $ctrb->CountryName;
												echo "</option>";
											}
											?>
									</select>		
			 <?php if(form_error('country')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>-->
          <fieldset>
		  
            <span class="label-txt">City<em>*</em></span>
			<div class="input-container">
            <input type="text" id="city_b" name= "city_b" tabindex="20" title="City" maxlength="20" value="<?php if(!empty($billingdetail->city)) echo $billingdetail->city; elseif(isset($_POST['city_b'])) echo $_POST['city_b']; ?>" />
         <?php if(form_error('city_b')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('city_b'); ?></span>
			</div> <?php } ?>
			</div>
		 </fieldset>
          <fieldset>
		  
            <span class="label-txt">Zip<em>*</em></span>
				<div class="input-container">
            <input type="text" id="zip_code_b" name= "zip_code_b" tabindex="21" title="Zip Code"  maxlength="10" value="<?php if(!empty($billingdetail->zip_code)) echo $billingdetail->zip_code; elseif(isset($_POST['zip_code_b'])) echo $_POST['zip_code_b']; ?>" />
          <?php if(form_error('zip_code_b')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code_b'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
		   <fieldset>
		  
            <span class="label-txt">State<em>*</em></span>
			<div class="input-container">
            <input type="text" id="state_b" name= "state_b" title="State" tabindex="22" maxlength="20" value="<?php if(!empty($billingdetail->state)) echo $billingdetail->state; elseif(isset($_POST['state_b'])) echo $_POST['state_b']; ?>" />
           <?php if(form_error('state_b')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('state_b'); ?></span>
			</div> <?php } ?>
			</div>
		  </fieldset>
        </div>
   
        <div class="clr"></div>
		 <div class="credit-upper-form-submit">
		 
     <input type="submit" id="billingcredit" tabindex="23" value="Update" />
          </div>
           <div class="credit-upper-form-submit">
        	 <button type="button" class="cancel-button" onClick="location.href = '<?php echo base_url()?>dashboard'">Cancel</button>
         </div>
		  <?php form_close() ; ?>
      </div>
    </div> 
  </div>
 <script>
  $(document).ready(function() {
  $('#billingaddressyes').click(function(){
 $address1 = $('#address_1').val();
  $('#address_1b').val($address1);
   $address2 = $('#address_2').val();
  $('#address_2b').val($address2);
  
   $city = $('#city').val();
  $('#city_b').val($city);
  
   $state = $('#state').val();
  $('#state_b').val($state);
  
   $zip_code = $('#zip_code').val();
  $('#zip_code_b').val($zip_code);
  
  
  var country =  $('#country').val();
  $('#country_b').val(country);
  });
  
  $('#billingaddressno').click(function(){

  $('#address_1b').val(''); 
  $('#address_2b').val('');
  $('#city_b').val('');
   $('#state_b').val('');
   $('#zip_code_b').val('');
   $('#country_b').val('');
  });
  $('#billingcredit').click(function(){
	  ShowDialogBox(' ','Ok','Cancel',null,null); 
  });
	  
	}); 

  function ShowDialogBox(title, content, btn1text, btn2text, functionText, parameterList) {
      var btn1css;
      var btn2css;	
      if (btn1text == '') {
          btn1css = "hidecss";
      } else {
          btn1css = "showcss";
      }

      if (btn2text == '') {
          btn2css = "hidecss";
      } else {
          btn2css = "showcss";
      }
  
      $("#lblMessage").html(content);

      $("#dialog").dialog({
          resizable: false,
          title: title,
          modal: true,
          width: '400px',
          height: 'auto',
          bgiframe: false,
          hide: { effect: 'scale', duration: 400 },

          buttons: [
                          {
                              text: btn1text,
                              "class": btn1css,
                              click: function () {
                                  $("#dialog").dialog('close');

                              }
                          },
                          {
                              text: btn2text,
                              "class": btn2css,
                              click: function () {
                          		  $( this ).dialog( "close" );
                              }
                          }
                     
                      ]
      });
  }
 </script>
 
 
 <div id="dialog" style="display: none">
<div class="clr"></div>
  <div class="signup-outerbox">
  <div class="signup-box" style="width: 513px; height: 100px;">
  To update your billing address click on cancel button and enter credit card number.
  Otherwise click ok to without update billing address.<br />

<div class="signup-box-btn">
<input type="button"  style="margin-top: 20px;width:100px" value="ok"/>
<input type="button"  style="width:100px;" value="cancel">
</div>
  </div>
  </div>
</div>