    <?php 
    $home_address=$this->session->userdata('home_address');
    $basic_detail= $this->session->userdata('basic_detail');
    $is_phone=$this->session->userdata('is_phone');
    ?>
    
    <link href="<?php echo base_url(); ?>css/front_end/jquery-ui.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/timepicker.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/front_end/reservation.css" rel="stylesheet" media="screen">
	<script src='<?php echo base_url(); ?>js/front_end/jquery-ui.js'></script>
	<script src='<?php echo base_url();?>js/front_end/reservation.js'></script>
<!-- <script src='<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js'></script>	-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/timeEntry/jquery.timeentry.css"> 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.min.js"></script>
	

<div id="wrapper">
  
  <div class="new-container">
    <h2>Personal Driver Reservation </h2>
    <?php $attributes = array('id' => 'newreservation');
    $hidden = array('service_id' => '1');
    echo form_open('reservation/newreservation',$attributes,$hidden); ?>
  <div class="form-container">
  	
  <div class="new-box-text">
      <div class="new-left-form" >
      <!--  <h4>Trip Date</h4>-->
    	
	<?php if(form_error('trip_date_from')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_from'); ?></span>
						</div> <?php } ?>

		<fieldset>  <!--  <h4>Trip Date</h4>-->
		<span class="label-txt" id="date_span">Trip Date<em>*</em></span>
		<input type="text" tabindex="1" class="datetime" readonly="readonly" id="trip_date_from" name="trip_date_from" value="<?php echo set_value('trip_date_from'); ?>">
		</fieldset>
		</div>
        
     
      <div class="new-right-form" >
      <?php if(form_error('pickup_time')) { ?>
		
		<label>&nbsp;</label>
		<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
		 <?php } ?>
						
		<fieldset >  <!--  <h4>Trip Date</h4>-->
		<span class="label-txt" style="width: 100px; id="time_span">Pick Up Time<em>*</em></span>
		<input type="text" tabindex="2" class="datetime" name="pickup_time" placeholder="HH:MM" id="pickup_time"  value="<?php echo set_value('pickup_time'); ?>">
		</fieldset>
        
      </div>
      </div>
      <div class="clr"></div>
    </div>
    <div class="address-container">
      <h3>Pick Up Address(If different than primary address then please edit below)</h3>
      <div class="form-container">
        <div class="upper-form">
          <fieldset>
         <?php if(form_error('address')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Address Line 1<em>*</em></span>
            <input type="text" id="address" name="address" tabindex="3" title="Address Line 1" value="<?php if(!empty($home_address[0]->address_1)){ echo $home_address[0]->address_1; }else {echo set_value('address');} ?>" />
          </fieldset>
          
          <fieldset>
            <?php if(form_error('address_2')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Address Line 2<em></em></span>
            <input type="text" id="address_2" name="address_2" tabindex="4" title="Address Line 2" value="<?php if(!empty($home_address[0]->address_2)){ echo $home_address[0]->address_2; }else {echo set_value('address_2');} ?>" />
          </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
          
            <span class="label-txt">City <em>*</em></span>
            <input type="text" id="city" name="city" tabindex="5" title="City" value="<?php if(!empty($home_address[0]->city)){ echo $home_address[0]->city; }else { echo  set_value('city'); }?>" />
            <?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>
          </fieldset>
     
          <fieldset>
              
            <span class="label-txt">Zip <em>*</em></span>
            <input type="text" id="zip" name="zip" tabindex="7" title="zip" value="<?php if(!empty($home_address[0]->zip_code)){ echo $home_address[0]->zip_code; }else {echo set_value('zip'); }?>" />
             <?php if(form_error('zip')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip'); ?></span>
						</div> <?php } ?>
          </fieldset>
        </div>
        <div class="upper-form-right-form">
          <fieldset>
          <?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">State <em>*</em></span>
            <input type="text" id="state" name="state" tabindex="6" title="State" value="<?php if(!empty($home_address[0]->state)){ echo $home_address[0]->state; }else {echo set_value('state');} ?>" />
          </fieldset>
        </div>
        <div class="clr"></div>
           </div>
    </div>
    <div class="address-container">
      <h3>Destination</h3>
      <div class="form-container">
      <div class="upper-form">
          <fieldset>
          	<?php if(form_error('desination_1')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_1'); ?></span>
						</div> <?php } ?>
            <span class="label-txt">Destination 1<em>*</em></span>
            <textarea class="large-input-textarea" id="desination_1" name="desination_1" tabindex="8" title="Desination 1"><?php echo set_value('desination_1'); ?></textarea>
          </fieldset>
          <fieldset>
          <?php if(form_error('desination_2')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_2'); ?></span>
						</div> <?php } ?>	
            <span class="label-txt">Destination 2<em></em></span>
            <textarea class="large-input-textarea" id="desination_2" name="desination_2" tabindex="9" title="Desination 2"><?php echo set_value('desination_2'); ?></textarea>
          </fieldset>
        </div>
        <div class="login-container-left-form">
          <span id="approx_js_error"></span>
          <fieldset>
            <?php if(form_error('approx_hours')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('approx_hours'); ?></span>
						</div> <?php } ?>
            <span class="approx_hour_lable">Approx. Length of <em>*</em> Trip in hours</span>
            <input type="text" class="small-input " tabindex="10" id="approx_hours" name="approx_hours" title="Approx. Length of Trip in hours" value="<?php echo set_value('approx_hours'); ?>" />
          </fieldset>
          	
          	 <?php if(form_error('contact_number')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_number'); ?></span>
						</div> <?php } ?>
          <fieldset>
            <span class="label-txt-title">Contact Phone<em>*</em></span>
            <input type="text" id="contact_number" tabindex="12" maxlength="12" name="contact_number" title="Contact Phone" 
            value="<?php if(!empty($basic_detail->mobile_number)){ echo $basic_detail->mobile_number; }else { echo set_value('contact_number');} ?>" />
          </fieldset>
          <fieldset >
           <?php if(form_error('manaual_shift')) { ?>							
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('manaual_shift'); ?></span>
									</div> <?php } ?>
            <span  id="manual_shift_span"  class="label-txt-title">Manual Shift<em>*</em></span>
           <p style="height: 34px;"> <span>
            <input type="radio" tabindex="12" name="manaual_shift" id="manaual_shift_y" value="2" />
            YES</span> <span>
            <input type="radio" tabindex="13" name="manaual_shift" id="manaual_shift_n" value="1" />
            NO  </span></p> 
          </fieldset>   
          </div>
          
           <div class="login-container-right-form">
         <div class="form-container">
        
          <fieldset>
           	<?php if(form_error('contact_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_name'); ?></span>
						</div> <?php } ?>
            <span class="label-txt-rt-title">Contact Person<em>*</em></span>
            <input type="text" id="contact_name" tabindex="11" name="contact_name" title="Contact Person" value="<?php if(!empty($basic_detail->first_name)){ echo $basic_detail->first_name.' '.$basic_detail->last_name; }else{ echo set_value('contact_name');} ?>" />
          </fieldset>
          </div>
        </div>
        </div>
        
        <div class="clr"></div>
        <div class="login-container-lower-form">
         <div class="form-container">
        
		  <fieldset >	
            <span class="label-txt-title-large">Special Request or <br />Comments</span>
             <textarea class="large-input valid" tabindex="13" id="comments" name="comments" title="Special Request or Comments" cols="50" rows="4"><?php echo set_value('comments');?></textarea>
          </fieldset>
          
       	
          <fieldset class="promo-code-new">
             <span id="promo_code_span"></span>
            <span class="label-txt-crd" id="promo_code_span">Promo Code</span>
            <input type="text" id="promo_code" tabindex="14" name="promo_code" title="Promo Code" value="<?php echo set_value('promo_code"'); ?>" />
          </fieldset>
  		
          
        </div>
         </div>
         <div class="form-container">
         <div class="credit-upper-form">
        	<fieldset id="term_fielset">
            <p>
              <input id="term_condition" tabindex="15" name="term_condition"  type="checkbox" />
              I accept the <a href="#">Terms & Conditions</a><em style="color: #CB4343;">*</em></p>
          </fieldset>
          </div>
          </div>
         <div class="clr"></div>
          <div class="credit-upper-form-submit">
     <input type="submit" tabindex="16" id="newreservation_submit"  value="Submit" />
          </div>
         <div class="credit-upper-form-submit">
        	 <button type="button" tabindex="17" class="cancel-button" onClick="location.href = '<?php echo base_url()?>reservation/reserve_driver'">Cancel</button>
         </div>
      </div>
      <?php echo form_close(); ?>	
    </div>
    
 
    

  <div class="clr"></div>
 
</div>

