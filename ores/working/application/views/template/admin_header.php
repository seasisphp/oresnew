<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Index</title>	
	     <link href="<?php echo base_url(); ?>css/admin/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>css/admin/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>css/admin/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		<link href='<?php echo base_url(); ?>css/admin/fullcalendar.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>css/admin/fullcalendar.print.css' rel='stylesheet' media='print' />
        <link rel="icon" href="<?php echo base_url();?>images/favicon.gif" type="image/gif" />
        <script src="<?php echo base_url(); ?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
         <script src="<?php echo base_url(); ?>js/common.js"></script>
         <script src="<?php echo base_url(); ?>js/dispatchlog.js"></script>
         <script src="<?php echo base_url(); ?>js/jqeury_input_mask.js"></script>	
         <script src='<?php echo base_url(); ?>js/front_end/phone_mask.js'></script>
     
	   <!--  <script src='<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js'></script>-->
	

</head>
<body>
 <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">ORES - Admin Panel</a>
                 <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> Admin <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/home/change_password">Change Password</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/home/signout">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      <ul class="nav">
                            <li>  <!--class="active"-->
                                <a href="<?php echo base_url(); ?>admin/home">Dashboard</a>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo base_url(); ?>admin/home/alluser" data-toggle="dropdown" class="dropdown-toggle">Manage Users <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" >
								    <li>
                                        <a href="<?php echo base_url(); ?>admin/home/alluser">Users List</i>

                                        </a>
                                        
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>admin/home/add_user">Add Users</i>

                                        </a>
                                        
                                    </li>
                                
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo base_url(); ?>admin/driver/all_driver" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage Drivers <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
									<li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/driver/all_driver">Driver List</a>
                                    </li>
                                    <li>
                                        <a tabindex="-2" href="<?php echo base_url(); ?>admin/driver/add_driver">Add Driver</a>
                                    </li>
										<li>
                                        <a tabindex="-3" href="<?php echo base_url(); ?>admin/driveravailability">Manage Driver Availability</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo base_url(); ?>admin/dispatchlog/dlogs" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage Dispatch Logs <i class="caret"></i>

                                </a>
								<ul class="dropdown-menu">
									<li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/dispatchlog/dlogs">Dispatch Logs Listing</a>
                                    </li>
                  
                                </ul>
                                
                            </li>
							  <li  class="dropdown-menu">
                                <a href="<?php echo base_url(); ?>admin/driveravailability" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage Driver Availability <i class="caret"></i>

                                </a>
								<ul class="dropdown-menu">
									<li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/driveravailability">Driver Availability</a>
                                    </li>
                  
                                </ul>
						
                                
                            </li>
							
					<li  class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Reservation <i class="caret"></i>

                                </a>
								<ul class="dropdown-menu">
								

									
									<li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/trip/personal_reserve">Personal Driver Reservation</a>
                                    </li>
									
									<li>
                                        <a tabindex="-2" href="<?php echo base_url(); ?>admin/trip/airport_pickup">Airport Pick-Up Reservation</a>
                                    </li>
									<li>
                                        <a tabindex="-3" href="<?php echo base_url(); ?>admin/trip/airport_dropoff">Airport Drop-Off Reservation</a>
                                    </li>
									<li>
                                        <a tabindex="-3" href="<?php echo base_url(); ?>admin/trip/double_driver">New Double Driver Pick-Up Reservation</a>
                                    </li>
                  
                                </ul>
						
                                
                            </li>
							
					<li  class="dropdown" >
                                <a href="<?php echo base_url(); ?>admin/completedtrips/clogs" role="button" class="dropdown-toggle" data-toggle="dropdown">Completed Trips <i class="caret"></i>

                                </a>
								<ul class="dropdown-menu">
								<li >
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/completedtrips/clogs">View Completed Trips</a>
                                    </li>
								</ul>
						
                                
                            </li>
							
	
							
					 
									


                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
<!--
  <div id="header">
  <div class="topHeader">
   <div class="logo">
    <h1><a href="<?php echo base_url();?>admin/home"><img alt="Site Logo" src="<?php echo base_url();?>images/site_images/logo.png" /></a></h1>
   </div>
   
   
   </div>
    <div class="loginTop">
		<?php 
			$sess_name = $this->session->userdata('username');
			if(!empty($sess_name)){
				$this->data['session'] = $sess_name;
			}else{
				redirect('admin/home');
			} ?>
		<ul>
			<li> Welcome, 
				<a href="<?php echo base_url();?>admin/home" class="menu_head"><?php  echo $this->data['session']; ?> </a>
             
			</li>
            <li><a href="<?php echo base_url(); ?>admin/home/signout">Signout</a></li>
        </ul>
   </div>
  </div>
  -->

