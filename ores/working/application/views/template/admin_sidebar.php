
<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li >
                            <a href="<?php echo base_url(); ?>admin/home"> <i class="icon-chevron-right"></i>Dashboard</a>
                        </li>
						 <li>
                            <a href="<?php echo base_url(); ?>admin/home/alluser"> <i class="icon-chevron-right"></i>Users</a>
                        </li>
  						<li>
                            <a href="<?php echo base_url(); ?>admin/driver/all_driver"> <i class="icon-chevron-right"></i>Drivers</a>
                        </li>
						 <li>
                            <a href="<?php echo base_url(); ?>admin/dispatchlog/dlogs"> <i class="icon-chevron-right"></i>Dispatch Log<?php if(!empty($this->data['notification'])){ ?> <span style="color:red">(<?php echo $this->data['notification'].')';}?></sapn></a>
                        </li>
							 <li>
                            <a href="<?php echo base_url(); ?>admin/driveravailability"> <i class="icon-chevron-right"></i>Manage Driver Availability</a>
                        </li>
                         <li>
                            <a href="<?php echo base_url(); ?>admin/trip/personal_reserve"> <i class="icon-chevron-right"></i>Personal Driver Reservation</a>
                        </li>
                         <li>
                            <a href="<?php echo base_url(); ?>admin/trip/airport_pickup"> <i class="icon-chevron-right"></i>Airport Pick-Up  Reservation</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/trip/airport_dropoff"> <i class="icon-chevron-right"></i>Airport Drop-Off  Reservation</a>
                        </li>
                          <li>
                            <a href="<?php echo base_url(); ?>admin/trip/double_driver"> <i class="icon-chevron-right"></i>Double Driver Pick-Up Reservation</a>
                        </li>
						  <li>
                            <a href="<?php echo base_url(); ?>admin/completedtrips/clogs"> <i class="icon-chevron-right"></i>Completed Trips</a>
                        </li>
</ul>