<div id="footer">
      <div class="follow-us">
        <h3>Follow Us</h3>
        <div class="news-letter">
          <input type="text" placeholder="Enter email address" />
          <br/>
          <input type="button" value="Subscribe" class="subscribe-btn" />
          <br />
          <br />
          <br />
          <a href="javascript:;"><img src="<?php echo base_url()?>images/rss-icon.jpg" alt="icon" /></a> </div>
      </div>
      <div class="look-driver">
        <h3>Looking for Drivers...</h3>
        <p> Le Ride is looking for professional drivers. <a href="javascript:;">Learn more...</a> </p>
      </div>
      <div class="col-placeholder">
        <h3>Column Placeholder</h3>
      </div>
      <div class="sitemap">
        <h3>Sitemap</h3>
        <ul>
          <li><a href="javascript:;">Contact Us</a></li>
          <li><a href="javascript:;">How It Works</a></li>
          <li><a href="javascript:;">Services &amp; Prices</a></li>
          <li><a href="javascript:;">Testimonials</a></li>
          <li><a href="javascript:;">Privacy Policy</a></li>
          <li><a href="<?php echo base_url().'home/oresTermofService'?>">Terms of Use</a></li>
        </ul>
      </div>
      <div class="clr"></div>
    </div>
    <div class="copyright">
      <p><span style="width: 174px;">&copy; 2013 &nbsp; <a  style="width: 129px;" href="javascript:;" >Site Development</a></span> <a href="javascript:;">Custom theme by DrivePlease </a></p>
    </div>
</body>
</html>
