<?php
$address_title=$this->session->userdata('address_title'); 
?>
 <link href="<?php echo base_url(); ?>css/front_end/jquery-ui.css" rel="stylesheet" media="screen">	
 <script src='<?php echo base_url(); ?>js/front_end/jquery-ui.js'></script>
 <script src='<?php echo base_url();?>js/front_end/signup.js'></script>
 <div class="register-container">
    <h2>New Member Registration :Step 1</h2>
    <h3>Tell Us About Yourself</h3>
	<?php 
	$attributes = array('id' => 'signup');
	echo form_open('',$attributes);  ?>
    <div class="form-container">
      <div class="left-form">
        <fieldset>
			 
          <span class="label-txt">First Name<em>*</em></span>
          <div class="input-container">
		  <input type="text" id="first_name" name= "first_name" tabindex="1" title="First Name" maxlength="20" title="First Name" value="<?php echo set_value('first_name'); ?>" />
		   <?php if(form_error('first_name')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
        <fieldset>
          <span class="label-txt">Email<em>*</em></span>
		   <div class="input-container">
          <input type="text" id="email" name= "email" title="Email" tabindex="3" value="<?php echo set_value('email'); ?>" />
		   <?php if(form_error('email')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
        <fieldset>
          <span class="label-txt">Company Name</span>
		   <div class="input-container">
          <input type="text" id="company_name" name= "company_name" tabindex="5" title="Company Name" maxlength="255" value="<?php echo set_value('company_name'); ?>" />
		  <?php if(form_error('company_name')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
        <fieldset>
          <span class="label-txt">Other Phone</span>
		   <div class="input-container">
          <input type="text" id="other_number" name= "other_number" tabindex="7" title="Other Name" maxlength="12" 
          value="<?php echo set_value('other_number'); ?>" />
		  <?php if(form_error('other_number')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
      </div>
      <div class="right-form">
        <fieldset>
          <span class="label-txt">Last Name<em>*</em></span>
		   <div class="input-container">
          <input type="text" id="last_name" name= "last_name" tabindex="2" title="Last Name"  maxlength="20" value="<?php echo set_value('last_name'); ?>" />
		  	<?php if(form_error('last_name')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
        <fieldset>
          <span class="label-txt">Re Enter Email</span>
		   <div class="input-container">
          <input type="text" id="re_email" name= "re_email" tabindex="4" title="Re Enter Email"  value="<?php echo set_value('re_email'); ?>" />
		  <?php if(form_error('re_email')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('re_email'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
        <fieldset>
          <span class="label-txt">Mobile Phone<em>*</em></span>
		   <div class="input-container">
          <input type="text" id="mobile_number" name= "mobile_number" tabindex="6" title="Mobile Number" maxlength="12" 
          value="<?php echo set_value('mobile_number'); ?>" />
		  <?php if(form_error('mobile_number')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
			</div> <?php } ?>
			</div>
        </fieldset>
      </div>
      <div class="clr"></div>
    </div>
    <div class="address-container">
      <h3>Primary Address</h3>
      <div class="form-container">
	   <div class="upper-form-left-form">
          <fieldset>
            <span class="label-txt">Address Title<em>*</em></span>
			<div class="input-container">
           	<?php $options = array(
						'' => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); 
							 if(isset($address_title)){
							   $index=$address_title;
								}
						 	  else{
						 	  	$index=0;
						 	  }	echo form_dropdown('address_title', $options, $index, 'tabindex="8"','style="margin:0px"');?>
			 <?php if(form_error('address_title')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
         
        </div>
        <div class="upper-form">
		 
          <fieldset>
            <span class="label-txt">Address Line 1<em>*</em></span>
			 <div class="input-container">
            <input type="text"  id="address_1" name= "address_1" tabindex="9" title="Address line 1" maxlength="255" value="<?php echo set_value('address_1'); ?>" />
			<?php if(form_error('address_1')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
			</div> <?php } ?>
			</div>
		 </fieldset>
          <fieldset>
            <span class="label-txt">Address Line 2</span>
			 <div class="input-container">
            <input type="text" id="address_2" name= "address_2"  tabindex="10" title="Address line 2"  maxlength="255" value="<?php echo set_value('address_2'); ?>" />
			<?php if(form_error('address_2')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
            <span class="label-txt">City<em>*</em></span>
			<div class="input-container">
            <input type="text" id="city" name= "city" title="City" tabindex="11" maxlength="255" value="<?php echo set_value('city'); ?>" />
			 <?php if(form_error('city')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
		   <!--<fieldset>
            <span class="label-txt">Country<em>*</em></span>
			 <div class="input-container">
            <select style="margin:0px;" id="country" class="shadow" name="country" id='country'>
												<option value="">-- Please select --</option>
												<?php
											/* 	$ctrb   = $this->db->get('country');
												foreach($ctrb->result() as $ctrb){
													if($country==$ctrb->CountryID){
													$sel= 'selected=selected';}
													else{
														$sel='';
													}
												echo "<option value='".$ctrb->CountryID."' ".$sel.">";
												echo $ctrb->CountryName;
												echo "</option>";
											} */
											?>
									</select>		
			 <?php if(form_error('country')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>-->
           <fieldset>
            <span class="label-txt">Zip<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="zip_code" name= "zip_code" title="Zip Code"  tabindex="13" maxlength="10" value="<?php echo set_value('zip_code'); ?>" />
			 <?php if(form_error('zip_code')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        </div>
        <div class="upper-form-right-form">
          <fieldset>
            <span class="label-txt">State<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="state" name= "state" title="State" tabindex="12" maxlength="255" value="<?php echo set_value('state'); ?>" />
			<?php if(form_error('state')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        </div>
        
        <div class="clr"></div>
        <div class="form-container-text">
          <p >Is this address also your billing address( the address that appears on your credit card or bank statement?) </p>
          <div class="clr"></div>
          <p> <span>
            <input type="radio" id="billingaddressyes" tabindex="14" <?php if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==0){?> checked <?php }else {?> checked <?php }?> name="billing_address_status" value="0" />
            YES</span> <span>
            <input type="radio" id="billingaddressno" tabindex="15" <?php if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==1){?> checked <?php } ?> name="billing_address_status" value="1" />
            NO (If not then we will ask you for it in a moment) </span> 
        </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="address-container">
      <h3>Choose Your Login</h3>
      <div class="form-container">
        <div class="login-container-left-form">
          <fieldset>
            <span class="label-txt-title">Username<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="username" name= "username" title="Username" tabindex="16" maxlength="20" value="<?php echo set_value('username'); ?>" />
			 <?php if(form_error('username')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
          <fieldset>
            <span class="label-txt-title">Re-Enter Password<em>*</em></span>
			 <div class="input-container">
            <input type="password" id="repassword" name= "repassword" tabindex="18" title="Re Enter Password" maxlength="20" value="<?php echo set_value('repassword'); ?>" />
			 <?php if(form_error('repassword')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('repassword'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
          
          <fieldset>
           <p style=" font-size: 14px;">We would like to give you a special offer on your birthday! </p>
           <span class="label-txt-title">Month of birth</span>
			 <div class="input-container">
            <select name="dob" id="dob" class="select-small">
			 <option value="">Month</option>
            <option value="01" <?php if(isset($_POST['dob'])&& $_POST['dob']==01){?> selected <?php }?>>Jan</option>
            <option value="02" <?php if(isset($_POST['dob'])&& $_POST['dob']==02){?> selected <?php }?>>Feb</option>
			<option value="03" <?php if(isset($_POST['dob'])&& $_POST['dob']==03){?> selected <?php }?>>Mar</option>
			<option value="04" <?php if(isset($_POST['dob'])&& $_POST['dob']==04){?> selected <?php }?>>Apr</option>
			<option value="05" <?php if(isset($_POST['dob'])&& $_POST['dob']==05){?> selected <?php }?>>May</option>
			<option value="06" <?php if(isset($_POST['dob'])&& $_POST['dob']==06){?> selected <?php }?>>Jun</option>
			<option value="07" <?php if(isset($_POST['dob'])&& $_POST['dob']==07){?> selected <?php }?>>July</option>
			<option value="08" <?php if(isset($_POST['dob'])&& $_POST['dob']==08){?> selected <?php }?>>Aug</option>
			<option value="09" <?php if(isset($_POST['dob'])&& $_POST['dob']==09){?> selected <?php }?>>Sep</option>
			<option value="10" <?php if(isset($_POST['dob'])&& $_POST['dob']==10){?> selected <?php }?>>Oct</option>
			<option value="11" <?php if(isset($_POST['dob'])&& $_POST['dob']==11){?> selected <?php }?>>Nov</option>
			<option value="12" <?php if(isset($_POST['dob'])&& $_POST['dob']==12){?> selected <?php }?>>Dec</option>
		
          </select>
          <?php if(form_error('dob')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('dob'); ?></span>
			</div> <?php } ?>
        </div>
         </fieldset>
        </div>
        <div class="login-container-right-form">
          <fieldset>
            <span class="label-txt-rt-title">Password<em>*</em></span>
			 <div class="input-container">
            <input type="password" id="password" name="password" tabindex="17" title="Password" maxlength="20" value="<?php echo set_value('password'); ?>" />
         <?php if(form_error('password')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
			</div> <?php } ?>
			</div>
		 </fieldset>
          <fieldset>
            <span class="label-txt-rt-title">How did you hear about us?</span>
            <input type="text" id="heardfrom" name= "heardfrom" tabindex="19" title="Heard From" tabindex="16" maxlength="255" value="<?php echo set_value('heardfrom'); ?>" />
          
		 </fieldset>
        </div>
        <div class="clr"></div>
		 <div class="credit-upper-form-submit">
     <input id="signup_submit" type="submit" tabindex="20" value="Continue" />
          </div>
      </div>
    </div>
     <?php form_close() ; ?>
  </div>