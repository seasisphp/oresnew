<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct() {
	
	
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->model('dashboard_model');
		$this->load->model('dlogs_model','dlogs');
		$this->load->model('driver_model','driver');
		$this->load->helper('url');
		$this->load->library('phpmailer');
		$this->load->helper('directory');
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->library('authorizecimlib');
		$this->load->library('pagination');
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i ") . ' GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	} 
	public function checkAdmin(){
	//print_r($this->session->userdata('username')); die;
		$front_sess_name = $this->session->userdata('username');
		//check admin is in session or not
		$sess_user_id = $front_sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role==2))
		{
		 redirect("admin/");
		} 
	}
	
	public function index()
	{
		
		$front_sess_name = $this->session->userdata('frontuser');
		if(empty($front_sess_name)){
			$this->template->page('home');
		}else{	
		$this->checkAdmin();
			$this->session->userdata('userId');
			$this->session->userdata('username'); //
			$this->data['session'] = $front_sess_name;
			$this->template->page('dashboard',$this->data);
			}
	}
	 public function login_check()
		{
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[3]|max_length[10]');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]'); 
				 
				$username=$this->input->post('username');
				$password=md5($this->input->post('password'));
				
				if ($this->form_validation->run()!=false)
				{
				$data = $this->admin_model->user_check($username,$password,1);
				if(!is_array($data)){
				echo 0;			 
				}else{
				//need to save id 
				$sessdata = array(
                   'username'  => $data['username'],
                   'id'     => $data['id']
				  
                   
               );
				//$this->session->set_userdata('username',$data['username'],'id',$data['id']);
				$this->session->set_userdata('frontuser',$sessdata);
				$front_sess_name = $this->session->userdata('frontuser');
				
				if($front_sess_name){
				echo 'ok';
				if($_POST['remember'] == 1)
						{
							$usercookies = array(
							'name' => 'username',
							'value' => $_POST['username'],
							'expire' => '1209600', // Two weeks
							);
							set_cookie($usercookies);
							
							$passcookies = array(
							'name' => 'password',
							'value' =>$_POST['password'],
							'expire' => '1209600', // Two weeks
							);

							set_cookie($passcookies);
						} else {

						delete_cookie('username');
						delete_cookie('password');
								}
							}
					}
				}else{
					echo validation_errors();
					}	
				 
		}
		
/* 	public function signup(){
	//$this->template->page('signup');
	}	 */
	
	public function signup(){
		$this->form_validation->set_rules('first_name', 'First Name', 'htmlspecialchars|required|max_length[20]|min_length[3]|alpha');
		$this->form_validation->set_rules('last_name', 'Last Name', 'htmlspecialchars|required|max_length[20]|min_length[3]|alpha');
		$this->form_validation->set_rules('username', 'User Name', 'htmlspecialchars|required|is_unique[user.username]|max_length[20]|min_length[3]');
		$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password', 'Password', 'htmlspecialchars|required|min_length[6]|max_length[20]');
		$this->form_validation->set_rules('repassword', 'Re Enter Password', 'htmlspecialchars|matches[repassword]required|min_length[6]|max_length[20]');
		$this->form_validation->set_rules('re_email', 'Re Enter Email', 'htmlspecialchars|matches[re_email]|valid_email|is_unique[user.email]');
		
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'htmlspecialchars|max_length[100]|min_length[3]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'htmlspecialchars|required|max_length[12]|min_length[8]|is_unique[user.mobile_number]');
		$this->form_validation->set_rules('other_number', 'Other Number', 'htmlspecialchars|max_length[12]|min_length[8]|is_unique[user.other_number]');		
		$this->form_validation->set_rules('address_title', 'Address Title', 'required');
		$this->form_validation->set_rules('address_1', 'Address Line 1', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]|min_length[3]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('state', 'State', 'required|max_length[255]|min_length[2]');		
		//$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');		
		$this->form_validation->set_rules('billing_address_status', 'Billing Address Type', 'required');
		$this->form_validation->set_rules('dob', 'Date of birth', '');
		$this->session->set_userdata('address_title',$this->input->post('address_title'));
		//$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
			if($this->form_validation->run() != false){
		
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$realpassword = $this->input->post('password');
			$password = md5($this->input->post('password'));
			$role = 1;
			$company_name = $this->input->post('company_name');
			$mobile_number =$this->input->post('mobile_number');			
			$other_number  =$this->input->post('other_number');
			$address_title = $this->input->post('address_title');
			
			$address_1 = $this->input->post('address_1');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city'); 		
			$state = $this->input->post('state');
			//$country = $this->input->post('country');
			$zip_code = $this->input->post('zip_code');
			$billing_address_status = $this->input->post('billing_address_status');
			$birthday=$this->input->post('dob');
			$status = 0;
			 
					$data =  array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'username' => $username,
					'email' => $email,
					'role' => $role,
					'password' => $password,
					'company_name' => $company_name,
					'mobile_number' => $mobile_number,
					'other_number' => $other_number,
					'address_title' => $address_title,	
					'address_1' => $address_1,
					'address_2' => $address_2,				
					'city' => $city,
					'state' => $state,
					//'country' => $country,
					'zip_code' => $zip_code,
					'billing_address_status' => $billing_address_status,
					'status' => $status,
					'birthday'=>$birthday	
					
				);
				$this->session->unset_userdata('address_title');
				$this->session->set_userdata('basic_detail',$data);
				$last_id = $this->admin_model->add_user($data);
				
				if($last_id > 0){
			
				$step1 = array(
                   'step1'  => 'true',
					'id' => $last_id,
					'password' => $realpassword
               );

				$this->session->set_userdata($step1);
				
			
				redirect('home/step2');
				}else{
					$this->data['msg'] = 'Your entered information is not added into database.';
					$this->template->page('signup',$this->data);
				}
				
		}else{			
			$this->template->page('signup');
		}
	
	
	
	}	
	
	public function step2(){
			if(isset($this->session->userdata['step1'])) {	
			$id	= $this->session->userdata['id'] ;
			$realpassword= $this->session->userdata['password'] ;
			$data = $this->admin_model->get_user($id);
			//echo $data->username ; die;
			$this->data['userrecords'] = $this->admin_model->get_user($id);
			//print_r($userrecords->address_1) ; die; 
			$this->form_validation->set_rules('address_1b', 'Address 1', 'required');
			//$this->form_validation->set_rules('address_2b', 'Address 2', 'required');
			//$this->form_validation->set_rules('country_b', 'Country', 'required');
			$this->form_validation->set_rules('city_b', 'City', 'required');
			$this->form_validation->set_rules('state_b', 'State', 'required');
			$this->form_validation->set_rules('terms', 'Terms ', 'required');
			$this->form_validation->set_rules('zip_code_b', 'Zip Code', 'required');
			$this->form_validation->set_rules('creditcard_number', 'Creditcard Number', 'required|callback_validate_credit_card_number');
			$this->form_validation->set_rules('creditcard_name', 'Creditcard Name', 'required');
			$this->form_validation->set_rules('exp_month', 'Exp month', 'required');
			$this->form_validation->set_rules('exp_year', 'Exp year', 'required');
			$this->form_validation->set_rules('terms', 'Terms & Condition', 'required');
			if($this->form_validation->run() != false){
		
				$address_1b  = $this->input->post('address_1b');	
				$address_2b  = $this->input->post('address_2b');	
				$city_b  = $this->input->post('city_b');	
				$state_b  = $this->input->post('state_b');	
				$zip_code_b  = $this->input->post('zip_code_b');
				//$country_b = $this->input->post('country_b');
				$promo_code = $this->input->post('promo_code');
				$status = 1;
				$creditcard_number=$this->input->post('creditcard_number');
				$exp_date=$this->input->post('exp_year').'-'.$this->input->post('exp_month');
					$data_b =  array(
						'user_id' => $id,
						'address_1' => $address_1b,
						'address_2' => $address_2b,
						'city' => $city_b,
						'state' => $state_b,
						'zip_code' => $zip_code_b
						//'country' => $country_b
					
				); 
				//$this->admin_model->update_user($data,$id);	
				$last_id=$this->dashboard_model->insert_billingdetail($data_b);
				$besic_detail_step1=$this->session->userdata('basic_detail');
				if(!empty($besic_detail_step1)){
					$this->create_customer_profile($besic_detail_step1,$creditcard_number,$exp_date,$id,$data_b);
				}
				$verification = sha1(microtime());
				$link = site_url("home/verification/".$verification);
				$imglink=base_url()."emailtemplates/logo.png";
				$userdetail = 	$this->admin_model->get_user($id);
				$vemail = $userdetail->email ;
				$name = $userdetail->first_name ;
				$this->admin_model->insert_key($verification,$vemail);
				$this->send_confirmation_mail($vemail,$name,$link,$verification);
				$this->session->set_flashdata('signupmsg', 'Please check your email for a confirmation email to activate your account');
				//$this->session->sess_destroy();
				redirect('home');	
				}else{			
					$this->template->page('step2',$this->data);
				}
		}else{	
			
			redirect('home/signup');
		}
	
	
	}	
	
	public function verification(){
	$code = $this->uri->segment(3);
	echo $code;
	$res = $this->admin_model->verify_key($code);
		if(isset($res))
		{
		 $data =  array(
			'status' => 1,
			'key' => ''
					);
			
		$this->session->set_flashdata('signupmsg', 'You have been verified,Please login with your credentials');
		$result = $this->admin_model->send_username($code);
		$this->admin_model->update_userkey($data,$code); 
		$this-> send_login_detail_mail($result[0]->username,$result[0]->email,$result[0]->first_name);
		redirect('home');
		
		}else{
		$this->session->set_flashdata('signupmsg', 'Please verify again,there is something wrong with this email or the time may out for this'); 
		redirect('home');
		}
	}
	
	public function signupconfirmed(){
	
	$this->template->page('signupconfirmed');
	}
	/* Function LOGOUT
	*  Successfully destroy session and redirect to admin home.
	*  
	*/
	public function signout(){
		$front_sess_name = $this->session->userdata('frontuser');
		$this->session->unset_userdata($front_sess_name);
		$this->session->sess_destroy($front_sess_name);
		$this->session->sess_destroy();
		redirect('/');
	}
	public function reservation_list(){
	//$this->checkAdmin();
		$this->load->helper('date');
		$time = time()-60*60*24*30*12;
		$datestring = " %Y/%m/%d %h:%i:%s";
		$date=mdate($datestring, $time);
		$username=$this->session->userdata('frontuser');
		$result=$this->admin_model->get_login_userId($username['username']);
		$count = $this->dlogs->count_all_reservations();
		
		$config['base_url'] = base_url().'home/reservation_list';
		$config['total_rows'] = $count;
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['dlogs'] = $this->dlogs->get_user_reservations($config['per_page'], $page,$result[0]->id,$date);
		
		$driver=$this->driver->get_driver($data['dlogs'][0]->driver_id);
		 if(!empty($driver)){
		 $driver_name=$driver->first_name.' '.$driver->last_name;
		 $this->session->set_userdata('driver_name',$driver_name);
		 }
		$this->data['dlog'] = $data['dlogs'];
		$this->template->page('reservation_list',$this->data);
		
		
	}
	
	/* This function is creted for create customer profile
	 * on authorized.net.
	 *  Author:Devi Lal Verma
	*/
	public function create_customer_profile($userInfo,$cardno,$expdate,$last_id,$b_data){
	//$this->checkAdmin();
		$this->load->library('authorizecimlib');
		$this->authorizecimlib->initialize('36Gd5M6rz','6nLVxa495X9P7qt8');
		$this->authorizecimlib->set_validationmode('testMode');
		$this->authorizecimlib->set_data('email',$userInfo['email']);
		$this->authorizecimlib->set_data('description','Monthly Membership No. ' . md5(uniqid(rand(), true)));
		$this->authorizecimlib->set_data('merchantCustomerId',429112);
		// echo '<h1>Creating Customer Profile - create_customer_profile()</h1>';
                
                if(! $this->data['profileid'] = $this->authorizecimlib->create_customer_profile())
                {
                        echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
                        die();
                }
                                
            	  //  echo '<p> Customer Id: ' . $this->data['profileid'] . '</p>';

 
    			// Create the Payment Profile
                $this->authorizecimlib->set_data('customerProfileId', $this->data['profileid']);
                $this->authorizecimlib->set_data('billToFirstName', $userInfo['first_name']);
                $this->authorizecimlib->set_data('billToLastName',$userInfo['last_name']);
                $this->authorizecimlib->set_data('billToAddress', $b_data['address_1']);
                $this->authorizecimlib->set_data('billToCity', $userInfo['city']);
                $this->authorizecimlib->set_data('billToState', $userInfo['state']);
                $this->authorizecimlib->set_data('billToZip', $userInfo['zip_code']);
               // $this->authorizecimlib->set_data('billToCountry', $userInfo['country']);
                $this->authorizecimlib->set_data('billToPhoneNumber', $userInfo['mobile_number']);
                //$this->authorizecimlib->set_data('billToFaxNumber', '800-555-2345');
               //$this->authorizecimlib->set_data('cardNumber', '6111111111111111'); // will produce a decline
                $this->authorizecimlib->set_data('cardNumber', $cardno);
                $this->authorizecimlib->set_data('expirationDate', $expdate);
                
               // echo '<h1>Creating Payment Profile - create_customer_payment_profile()</h1>';
                
                if(! $this->data['paymentprofileid'] = $this->authorizecimlib->create_customer_payment_profile())
                {
                      // echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
                      // die();
                       $this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
                       redirect(current_url());
                       
                }
                
                
             	 //  echo '<p> Payment Profile Id: ' . $this->data['paymentprofileid'] . '</p>';
   				 // Create the shipping profile
                $this->authorizecimlib->set_data('customerProfileId', $this->data['profileid']);
                $this->authorizecimlib->set_data('shipToFirstName', $userInfo['first_name']);
                $this->authorizecimlib->set_data('shipToLastName', $userInfo['last_name']);
                $this->authorizecimlib->set_data('shipToAddress', $b_data['address_1']);
                $this->authorizecimlib->set_data('shipToCity', $userInfo['city']);
                $this->authorizecimlib->set_data('shipToState', $userInfo['state']);
                $this->authorizecimlib->set_data('shipToZip', $userInfo['zip_code']);
                //$this->authorizecimlib->set_data('shipToCountry',$userInfo['country']);
                $this->authorizecimlib->set_data('shipToPhoneNumber', $userInfo['mobile_number']);
               // $this->authorizecimlib->set_data('shipToFaxNumber', '800-555-4567');

                //echo '<h1>Creating Shipping Profile - create_customer_shipping_profile()</h1>';
                
                if(! $this->data['shippingprofileid'] = $this->authorizecimlib->create_customer_shipping_profile())
                {
                       // echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
                        //die();
                        $this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
                        redirect(current_url());
                }
                
              //  echo '<p> Shipping Profile Id: ' . $this->data['shippingprofileid'] . '</p>';
                
                $profile_data=array('customer_profile_id'=>$this->data['profileid'], 'customer_payment_profile_id'=>$this->data['paymentprofileid'],'customer_shipping_address_id'=>$this->data['shippingprofileid']);
                $this->dashboard_model->update_billingdetail($profile_data,$last_id);
                $this->authorizecimlib->clear_data();
                
	
	}
	public function oresTermofService(){
		$this->template->page('oresTermsofService');
	}
	function send_confirmation_mail($email,$name,$link,$verification){
		$html  =  @file_get_contents(base_url().'emailtemplates/confirmation_email.html');
		$find = array('{name}','{link}');
		$replace   = array($name,$link);
		$messegefinal = str_replace($find,$replace,$html);
		$mail = $this->phpmailer;
		$mail->WordWrap = 100;
		$mail->SetFrom('admin@admin.com', 'ORES');
		$mail->Subject    = 'Verification Email';
		$mail->addAttachment("emailtemplates/logo.png");
		$mail->addAttachment("emailtemplates/forward-glyph.png");
		$mail->MsgHTML($messegefinal);
			
		$mail->AddAddress($email,$name);
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Message sent!";
		}
	}
	function send_login_detail_mail($username,$email,$name){
		$html  =  @file_get_contents(base_url().'emailtemplates/login_detail_email.html');
		$link = site_url();
		$find = array('{name}','{username}','{link}');
		$replace   = array($name,$username,$link);
		$messegefinal = str_replace($find,$replace,$html);
		$mail = $this->phpmailer;
		$mail->WordWrap = 100;
		$mail->SetFrom('admin@admin.com', 'ORES');
		$mail->Subject    = 'login dtail Email';
		$mail->addAttachment("emailtemplates/logo.png");
		$mail->addAttachment("emailtemplates/forward-glyph.png");
		$mail->MsgHTML($messegefinal);
			
		$mail->AddAddress($email,$username);
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Message sent!";
		}
		
	}
	
	function validate_credit_card_number( $string ) {
		if ( preg_match( '/^([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{2,4})$/', $string ) ) {
			return TRUE;
		} else {
			$this->form_validation->set_message('validate_credit_card_number','Invalid credit card number');
			return FALSE;
		}
	}
	
}

