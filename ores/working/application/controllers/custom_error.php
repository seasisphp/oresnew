<?php
class Custom_error extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->library('authorizecimlib');
	}
	function _404(){
	$this->template->page("my404_view");
	}
}