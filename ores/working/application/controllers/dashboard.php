<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	 
	 function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->model('dashboard_model');
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->library('authorizecimlib');
	 	 if( !$this->session->userdata('frontuser') )
		{
			redirect(base_url());
		}  
		//print_r($this->session->userdata); die;
		 $front_sess_name = $this->session->userdata('frontuser');
		$sess_user_id = $front_sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=1))
		{
		 redirect("admin/");
		} 
	} 
	
	
	public function index()
	{
	//echo "<pre>"; print_r($this->session->userdata); die;
	if(isset($this->session->userdata)){
	$userid = $this->session->userdata['frontuser']['id'];
	
	
		$pastbookings = $this->dashboard_model->getpastbookings($userid); 
		$currentbookings = $this->dashboard_model->getcurrentbookings($userid); 
		$reservedbookings = $this->dashboard_model->getreservedbookings($userid); 
		//echo "<pre>"; print_r($reservedbookings); die;
		$this->data['pastbookings'] = $pastbookings;
		$this->data['currentbookings'] = $currentbookings;
		$this->data['reservedbookings'] = $reservedbookings;
	$this->template->page('dashboard');
	}else{
	$this->template->page('home');
	}
	}
	
	/* 
	*  Edit user according to user id
	*  
	*/
public function accountsettings(){

 $session_name = $this->session->userdata['frontuser'] ;
$id = $session_name['id'];
$username = $session_name['username'];
	//echo $username ; 
	if($id){
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean|max_length[20]|min_length[3]');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean|max_length[20]|min_length[3]');
			//$this->form_validation->set_rules('username', 'User Name', 'trim|required|xss_clean|max_length[10]|min_length[3]|callback_check_user_name['.$id.']');
			$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email|callback_check_user_email['.$id.']');
			$this->form_validation->set_rules('company_name', 'Company Name', 'max_length[100]|min_length[3]');
			$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|max_length[12]|min_length[10]|callback_check_user_mobile_no['.$id.']'); 
			$this->form_validation->set_rules('other_number', 'Other Number', 'max_length[12]|min_length[10]|callback_check_user_other_no['.$id.']'); 
			$this->form_validation->set_rules('address_1', 'Address 1', 'required|max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_2', 'Address 2', 'max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_title', 'Address Title', 'required');
			$this->form_validation->set_rules('city', 'City', 'required|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('state', 'State', 'required|max_length[30]|min_length[2]');
			//$this->form_validation->set_rules('country', 'Country', 'required');
			$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');
			$this->form_validation->set_rules('billing_address_status', 'billingaddress', 'required');
			//password info 
			$this->form_validation->set_rules('oldpassword', 'Old Password', 'min_length[6]|max_length[20]');
			//$this->form_validation->set_rules('password', 'Password', 'matches[repassword]|min_length[6]|max_length[20]');
			//$this->form_validation->set_rules('repassword', 'Re-Enter Password', 'min_length[6]|max_length[20]');		
			if($this->input->post('oldpassword')!=""){
			$this->form_validation->set_rules('password', 'Password', 'required|matches[repassword]|min_length[6]|max_length[20]');
			$this->form_validation->set_rules('repassword', 'Re-Enter Password', 'required|min_length[6]|max_length[20]');	
			}else{
			$this->form_validation->set_rules('password', 'Password', 'matches[repassword]|min_length[6]|max_length[20]');
			$this->form_validation->set_rules('repassword', 'Re-Enter Password', 'min_length[6]|max_length[20]');	
			
			}
			//billing info 
			$this->form_validation->set_rules('address_1b', 'Address 1', 'required|max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_2b', 'Address 2', 'max_length[255]|min_length[3]');
			//$this->form_validation->set_rules('address_title', 'Address Title', 'required');
			$this->form_validation->set_rules('city_b', 'City', 'required|max_length[10]|min_length[3]');
			$this->form_validation->set_rules('state_b', 'State', 'required|max_length[20]|min_length[2]');
			//$this->form_validation->set_rules('country_b', 'Country', 'required');
			$this->form_validation->set_rules('zip_code_b', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');
			if($this->input->post('creditcard_number')!=''){
			$this->form_validation->set_rules('creditcard_number','Credit Card Number','callback_validate_credit_card_number');
			$this->form_validation->set_rules('exp_month', 'Exp month', 'required');
			$this->form_validation->set_rules('exp_year', 'Exp year', 'required');
			}
			$this->data['user'] = $this->admin_model->get_user($id);
			$this->data['billingdetail'] = $this->dashboard_model->get_billingdetail($id);
			
			if ($this->input->server('REQUEST_METHOD') === 'POST')
			{
			$this->session->unset_userdata('user_record');	
			$this->session->unset_userdata('billing_record');	
			}
			else{
			$this->session->set_userdata('user_record', $this->data['user']);
			$this->session->set_userdata('billing_record', $this->data['billingdetail']);
			$profile=$this->dashboard_model->get_autorizednet_profileId($id);
			}
			//password check 
			
			if($this->form_validation->run() != false){
			
			
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$address_title = $this->input->post('address_title');
				$country = $this->input->post('country');
				$email = $this->input->post('email');
				$role = 1;
				$oldpassword	= $this->input->post('oldpassword');			
				$company_name  = $this->input->post('company_name');
				$mobile_number =$this->input->post('mobile_number');			
				$other_number  =$this->input->post('other_number');	
				$password  = $this->input->post('password');	
				$address_1  = $this->input->post('address_1');	
				$address_2  = $this->input->post('address_2');	
				$city  = $this->input->post('city');	
				$state  = $this->input->post('state');	
				$billing_address_status  = $this->input->post('billingaddress');	
				$zip_code  = $this->input->post('zip_code');	
				$status  = 1;	
				$address_1b  = $this->input->post('address_1b');	
				$address_2b  = $this->input->post('address_2b');	
				$city_b  = $this->input->post('city_b');	
				$state_b  = $this->input->post('state_b');	
				$zip_code_b  = $this->input->post('zip_code_b');	
				$country_b = $this->input->post('country_b');
				$creditcard_number = $this->input->post('creditcard_number');
				if($creditcard_number!=''&& $creditcard_number!=null){
				$creditcard_number = $this->input->post('creditcard_number');
				}
				else{
					$creditcard_number='';
				}
				$exp_date=$this->input->post('exp_year').'-'.$this->input->post('exp_month');
			    $profile=$this->dashboard_model->get_autorizednet_profileId($id);
				if(($this->input->post('oldpassword')!="")){
				$oldpass = $this->input->post('oldpassword');
				$result = $this->admin_model->password_check(md5($oldpass),$username);	
				
				if(!empty($result)){
				$password = md5($this->input->post('password'));
				$this->admin_model->update_password($username,$password);												
				}

				}else {
				 $password =$this->data['user']->password;
				
				}
				
		
				
				if(empty($this->data['error'])){			
		
					$data =  array(
						'first_name' => $first_name,
						'last_name' => $last_name,
						//'username' => $username,
						'email' => $email,
						'role' => $role,
						//'password' => $password,
						'company_name' => $company_name,
						'mobile_number' => $mobile_number, 
						'other_number' => $other_number,
						'address_title' => $address_title,
						'country' => $country,
						'address_1' => $address_1,
						'address_2' => $address_2,
						'city' => $city,
						'state' => $state,
						'billing_address_status' => $billing_address_status,
						'zip_code' => $zip_code,
						'status' => $status
					);
				//billing address
				$data_b =  array(
						'user_id' =>$this->data['user']->id,
						'address_1' => $address_1b,
						'address_2' => $address_2b,
						'city' => $city_b,
						'state' => $state_b,
						'zip_code' => $zip_code_b,
						'country' => $country_b,
						'creditcard_number'=> $creditcard_number
					);
				
					$get_true_false = $this->admin_model->update_user($data,$id);
					if(isset($this->data['billingdetail'])){
					//$get_true_false1 = $this->dashboard_model->update_billingdetail($data_b,$id);
					// to update authorized.net profile
					if($profile!=false && $creditcard_number!='' && $creditcard_number!=null){
					if($profile->customer_payment_profile_id!=-1 && $profile->customer_payment_profile_id!=0){
					$this->update_customer_profile($data,$data_b,$profile,$exp_date);
					$get_true_false1 = $this->dashboard_model->update_billingdetail($data_b,$id);
					}
					}
					}else{
					$get_true_false1 = $this->dashboard_model->insert_billingdetail($data_b);
					}
					if($get_true_false > 0 ){
				
					if($this->input->post('oldpassword')!="" && !empty($result)){
					
						$this->data['msg'] = 'Changes updated successfully.';
						$this->template->page('accountsettings',$this->data);
					
						}else if($this->input->post('oldpassword')!="" && empty($result)){
						
						$this->data['msgp'] = 'Invalid Old Password.';
						$this->template->page('accountsettings',$this->data);
						}else{
					
						$this->data['msg'] = 'Changes updated successfully.';
						$this->template->page('accountsettings',$this->data);
						}
						
					}else{
					
					
						$this->data['wrong_update'] = 'Your entered information is not correct, please try again.';
						$this->template->page('accountsettings',$this->data);
						
						
					}
				}	
			}else{	
				
				$this->template->page('accountsettings',$this->data);
			}
		}else{

				redirect('home');
				
			}	

}
/* This function is creted for update customer profile
 * on authorized.net.
*  Author:Devi Lal Verma
*/
public function update_customer_profile($userInfo,$b_data,$profile,$exp_date){
	$this->load->library('authorizecimlib');
	$this->authorizecimlib->initialize('36Gd5M6rz','6nLVxa495X9P7qt8');
	$this->authorizecimlib->set_validationmode('testMode');
	$this->authorizecimlib->set_data('email',$userInfo['email']);
	//$this->authorizecimlib->set_data('description','Monthly Membership No. ' . md5(uniqid(rand(), true)));
	$this->authorizecimlib->set_data('merchantCustomerId',429112);
	// echo '<h1>Creating Customer Profile - create_customer_profile()</h1>';

 	if(! $this->authorizecimlib->update_customer_profile($profile->customer_profile_id))
    {
         // echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
         //die();
         $this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
         //die();
         redirect(current_url());
    }

	//  echo '<p> Customer Id: ' . $this->data['profileid'] . '</p>';
  
	// update the Payment Profile
	$this->authorizecimlib->set_data('customerProfileId', $profile->customer_profile_id);
	$this->authorizecimlib->set_data('billToFirstName', $userInfo['first_name']);
	$this->authorizecimlib->set_data('billToLastName',$userInfo['last_name']);
	$this->authorizecimlib->set_data('billToAddress', $b_data['address_1']);
	$this->authorizecimlib->set_data('billToCity', $userInfo['city']);
	$this->authorizecimlib->set_data('billToState', $userInfo['state']);
	$this->authorizecimlib->set_data('billToZip', $userInfo['zip_code']);
	$this->authorizecimlib->set_data('billToCountry', $userInfo['country']);
	$this->authorizecimlib->set_data('billToPhoneNumber', $userInfo['mobile_number']);
	//$this->authorizecimlib->set_data('billToFaxNumber', '800-555-2345');
	 $this->authorizecimlib->set_data('cardNumber', $b_data['creditcard_number']);
	
	// will produce a decline
	//$this->authorizecimlib->set_data('cardNumber', $cardno);
	//$this->authorizecimlib->set_data('expirationDate', $expdate);
	 $this->authorizecimlib->set_data('expirationDate',$exp_date);
	// echo '<h1>Creating Payment Profile - create_customer_payment_profile()</h1>';

    if(! $this->authorizecimlib->update_customer_payment_profile( $profile->customer_profile_id,$profile->customer_payment_profile_id))
    {
      // echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
       $this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
       //die();
       redirect(current_url());
    }


	$this->authorizecimlib->clear_data();
}
function check_user_mobile_no($mobile_no,$id){
	if(!empty($mobile_no)){
		$result = $this->admin_model->check_user_mobile_no($mobile_no,$id);
		if($result==0){

			$this->form_validation->set_message('check_user_mobile_no', 'This mobile number already exist.');
			return false;
		}else{
			return true;
		}
	}

}
function check_user_other_no($mobile_no,$id){
	if(!empty($mobile_no)){
		$result = $this->admin_model->check_user_other_no($mobile_no,$id);
		if($result==0){

			$this->form_validation->set_message('check_user_other_no', 'This mobile number already exist.');
			return false;
		}else{
			return true;
		}
	}

}
function validate_credit_card_number( $string ) {
	if ( preg_match( '/^([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{2,4})$/', $string ) ) {
		return TRUE;
	} else {
		$this->form_validation->set_message('validate_credit_card_number','Invalid credit card number');
		return FALSE;
	}
}

}