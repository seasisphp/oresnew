<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation extends CI_Controller{
	
	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('trip_model','trip');
		$this->load->model('admin_model','admin');
		$this->load->library('session');
		$this->load->library('Pagination');
		$this->load->library('phpmailer');
		//$this->load->library('upload');
		 if( !$this->session->userdata('frontuser') )
		{
			redirect(base_url());
		} 
		$front_sess_name = $this->session->userdata('frontuser');
		$sess_user_id = $front_sess_name['id'];
		$role = $this->admin->get_role($sess_user_id);

		if(($role!=1))
		{
		 redirect("admin/");
		}
	}
	public function index(){
		$this->template->page('newreservation');
	}
	public function dispatch_log(){
		$result = $this->trip->get_all_reservation();
		$this->data['trip'] = $result;
		$this->template->page('admin/trip/dispatch_log',$this->data);
	}
	
	/* This function is creted for airport dropoff reservation page.
	*  Author:Devi Lal Verma
	*/
	public function airport_dropoff(){
	$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		// apply validation rule on form field.
		$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
		$this->form_validation->set_rules('pickup_time', 'Pick Up Time', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('state', 'State', 'htmlspecialchars|required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip', 'Zip', 'required|numeric|max_length[10]|min_length[3]');
		$this->form_validation->set_rules('airport', 'Airport', 'required|max_length[30]|min_length[3]');
		//$this->form_validation->set_rules('airline', 'Airline', 'required|max_length[30]|min_length[3]');
		//$this->form_validation->set_rules('flight_number', 'Flight No', 'required|max_length[30]|min_length[3]');
		//$this->form_validation->set_rules('desination_1', 'Desination #1', 'required|max_length[255]');
		//$this->form_validation->set_rules('desination_2', 'desination #2','max_length[255]');
		//$this->form_validation->set_rules('approx_hours', 'Approx. Length Of Trip (in Hours)', 'numeric|max_length[4]');
		$this->form_validation->set_rules('contact_name', 'Contact Person', 'trim|htmlspecialchars|required|max_length[30]|min_length[3]|callback_check_string');
		$this->form_validation->set_rules('contact_number', 'Contact Phone', 'htmlspecialchars|required|max_length[12]|min_length[8]');
		//$this->form_validation->set_rules('is_contact_phone', 'Contact Phone', 'required');
		//$this->form_validation->set_rules('comments', 'Special Request or Comments','required');
		//$this->form_validation->set_rules('term_conditions', 'Term&Condition', 'required');
		$this->form_validation->set_rules('manaual_shift', 'Manual Shift', 'required');
		$post =  $this->input->post();
		
		//get login userid and email
		$username=$this->session->userdata('frontuser');
		$result=$this->admin->get_login_userId($username['username']);
		
		$post =  $this->input->post();
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{      
				$this->session->unset_userdata('home_address');
				//$this->session->set_userdata('is_phone',$this->input->post('is_contact_phone'));
		}
		else {
			$home_address=$this->trip->get_home_address($result[0]->id);
			$basic_detail=$this->admin->get_user($result[0]->id);
			$this->session->set_userdata('basic_detail',$basic_detail);
			$this->session->set_userdata('home_address',$home_address);
		}
		if($this->form_validation->run()!= false){
			$service_id = $this->input->post('service_id');
			$trip_date_from = $this->input->post('trip_date_from');
			$pickup_time = date("H:i", strtotime($this->input->post('pickup_time')));
			$address = $this->input->post('address');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zip = $this->input->post('zip');
			$desination_1 = $this->input->post('desination_1');
			$desination_2 = $this->input->post('desination_2');
			$approx_hours = $this->input->post('approx_hours');
			$contact_name = $this->input->post('contact_name');
			$contact_number = str_replace('-','',$this->input->post('contact_number'));
			$is_contact_phone = $this->input->post('is_contact_phone');
			$manaual_shift= $this->input->post('manaual_shift');
			$comments = $this->input->post('comments');
			$notification = 1;
	
			$data =  array(
					'service_id' => $service_id ,
					'user_id' =>$result[0]->id,
					'trip_date_from' =>date('Y-m-d', strtotime($trip_date_from)).' '.$pickup_time,
					'pickup_time' => $pickup_time,
					'address' => $address,
					'address_2'=>$address_2,
					'city' => $city,
					'state' => $state,
					'zip' => $zip,
					'desination_1' => $desination_1,
					'desination_2' => $desination_2,
					'approx_hours' => $approx_hours,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'comments' => $comments,
					'manaual_shift' => $manaual_shift,
					'notification'=> $notification
			);
			$last_id = $this->trip->trip_reservation($data);
			if($last_id > 0){
					$this->load->library('email');
					$this->send_reservation_confirmation_email($result[0]->email,$contact_name);
					redirect('reservation/reservationconfirmed');
				}else{
					$this->data['wrong_insert'] = 'Your entered information is not added into database.';
					$this->template->page('airport_dropoff',$this->data);
				}
			
			}else{
				$this->template->page('airport_dropoff');
			}
	}
	
	/* This function is creted for airport dropoff reservation page.
	 *  Author:Devi Lal Verma
	*/
	public function airport_pickup(){
		$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		
		// apply validation rule on form field.
		$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
		$this->form_validation->set_rules('pickup_time', 'Pick Up Time', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('state', 'State', 'htmlspecialchars|required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip', 'Zip', 'required|numeric|max_length[10]|min_length[3]');
		$this->form_validation->set_rules('airport', 'Airport', 'required|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('airline', 'Airline', 'required|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('flight_number', 'Flight No', 'required|max_length[30]|min_length[3]');
		//$this->form_validation->set_rules('desination_1', 'Desination #1', 'required|max_length[255]');
		//$this->form_validation->set_rules('desination_2', 'desination #2','max_length[255]');
		//$this->form_validation->set_rules('approx_hours', 'Approx. Length Of Trip (in Hours)', 'numeric|max_length[20]');
		$this->form_validation->set_rules('contact_name', 'Contact Person', 'trim|htmlspecialchars|required|max_length[30]|min_length[3]|callback_check_string');
		$this->form_validation->set_rules('contact_number', 'Contact Phone', 'htmlspecialchars|required|max_length[12]|min_length[8]');
		//$this->form_validation->set_rules('is_contact_phone', 'Contact Phone', 'required');
		//$this->form_validation->set_rules('comments', 'Special Request or Comments','required');
		//$this->form_validation->set_rules('term_conditions', 'Term&Condition', 'required');
		$this->form_validation->set_rules('manaual_shift', 'Manual Shift', 'required');
		$post =  $this->input->post();
		//get login userid and email
		$username=$this->session->userdata('frontuser');
		$result=$this->admin->get_login_userId($username['username']);
		
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{      
				$this->session->unset_userdata('home_address');
				$this->session->set_userdata('is_phone',$this->input->post('is_contact_phone'));
		}
		else {
			$home_address=$this->trip->get_home_address($result[0]->id);
			$basic_detail=$this->admin->get_user($result[0]->id);
			$this->session->set_userdata('basic_detail',$basic_detail);
			$this->session->set_userdata('home_address',$home_address);
		}
		if($this->form_validation->run()!= false){
			$service_id = $this->input->post('service_id');
			$trip_date_from = $this->input->post('trip_date_from');
			$pickup_time = date("H:i", strtotime($this->input->post('pickup_time')));
			$address = $this->input->post('address');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zip = $this->input->post('zip');
			$desination_1 = $this->input->post('desination_1');
			$desination_2 = $this->input->post('desination_2');
			//$approx_hours = $this->input->post('approx_hours');
			$contact_name = $this->input->post('contact_name');
			$contact_number = str_replace('-','',$this->input->post('contact_number'));
			$is_contact_phone = $this->input->post('is_contact_phone');
			$manaual_shift= $this->input->post('manaual_shift');
			$comments = $this->input->post('comments');
			$notification = 1;
		
	
			$data =  array(
					'service_id' => $service_id ,
					'user_id' =>$result[0]->id,
					'trip_date_from' =>date('Y-m-d', strtotime($trip_date_from)).' '.$pickup_time,
					'pickup_time' => $pickup_time,
					'address' => $address,
					'address_2'=>$address_2,
					'city' => $city,
					'state' => $state,
					'zip' => $zip,
					'desination_1' => $desination_1,
					'desination_2' => $desination_2,
					//'approx_hours' => $approx_hours,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'comments' => $comments,
					'manaual_shift' => $manaual_shift,
					'notification'=> $notification
			);
			$last_id = $this->trip->trip_reservation($data);
			if($last_id > 0){
					$this->send_reservation_confirmation_email($result[0]->email,$contact_name);
					redirect('reservation/reservationconfirmed');
				}else{
					$this->data['wrong_insert'] = 'Your entered information is not added into database.';
					$this->template->page('airport_pickup',$this->data);
				}
			
			}else{
				$this->template->page('airport_pickup');
			}
	}
	
	/* This function is creted for personal reservation page.
	 *  Author:Devi Lal Verma
	*/
	public function newreservation(){
		$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		
		
		
		$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
		$this->form_validation->set_rules('pickup_time', 'Pick Up Time', 'required');
		$this->form_validation->set_rules('address', 'Address Line 1', 'required|max_length[255]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('state', 'State', 'htmlspecialchars|required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip', 'Zip', 'required|numeric|max_length[10]|min_length[3]');
		
		$this->form_validation->set_rules('desination_1', 'Desination #1', 'required|max_length[255]');
		$this->form_validation->set_rules('desination_2', 'desination #2','max_length[255]');
		$this->form_validation->set_rules('approx_hours', 'Approx. Length Of Trip (in Hours)', 'numeric|max_length[20]');
		$this->form_validation->set_rules('contact_name', 'Contact Person', 'trim|htmlspecialchars|required|max_length[30]|min_length[3]|callback_check_string');
		$this->form_validation->set_rules('contact_number', 'Contact Phone', 'htmlspecialchars|required|max_length[12]|min_length[8]');
		//$this->form_validation->set_rules('is_contact_phone', 'Contact Phone', 'required');
		//$this->form_validation->set_rules('comments', 'Special Request or Comments','required');
		//$this->form_validation->set_rules('term_conditions', 'Term&Condition', 'required');
		$this->form_validation->set_rules('manaual_shift', 'Manual Shift', 'required');
		$post =  $this->input->post();
		//get login userid and email
		$username=$this->session->userdata('frontuser');
		$result=$this->admin->get_login_userId($username['username']);
		
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{      
				$this->session->unset_userdata('home_address');
				$this->session->set_userdata('is_phone',$this->input->post('is_contact_phone'));
		}
		else {
			$home_address=$this->trip->get_home_address($result[0]->id);
			$basic_detail=$this->admin->get_user($result[0]->id);
			$this->session->set_userdata('basic_detail',$basic_detail);
			$this->session->set_userdata('home_address',$home_address);
		}
		if($this->form_validation->run()!= false){
			$service_id = $this->input->post('service_id');
			$trip_date_from = $this->input->post('trip_date_from');
			$pickup_time = date("H:i", strtotime($this->input->post('pickup_time')));
			$address = $this->input->post('address');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zip = $this->input->post('zip');
			$desination_1 = $this->input->post('desination_1');
			$desination_2 = $this->input->post('desination_2');
			$approx_hours = $this->input->post('approx_hours');
			$contact_name = $this->input->post('contact_name');
			$contact_number = str_replace('-','',$this->input->post('contact_number'));
			//$is_contact_phone = $this->input->post('is_contact_phone');
			$comments = $this->input->post('comments');
			$manaual_shift= $this->input->post('manaual_shift');
			$notification = 1;
		
	
			$data =  array(
					'service_id' => $service_id ,
					'user_id' =>$result[0]->id,
					'trip_date_from' =>date('Y-m-d', strtotime($trip_date_from)).' '.$pickup_time,
					'pickup_time' => $pickup_time,
					'address' => $address,
					'address_2'=>$address_2,
					'city' => $city,
					'state' => $state,
					'zip' => $zip,
					'desination_1' => $desination_1,
					'desination_2' => $desination_2,
					'approx_hours' => $approx_hours,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'comments' => $comments,
					'manaual_shift' => $manaual_shift,
					'notification'=> $notification
			);
			$last_id = $this->trip->trip_reservation($data);
			if($last_id > 0){
				$this->send_reservation_confirmation_email($result[0]->email,$contact_name);
				redirect('reservation/reservationconfirmed');
			}else{
				$this->data['wrong_insert'] = 'Your entered information is not added into database.';
				$this->template->page('newreservation',$this->data);
			}
		
		}else{
			$this->template->page('newreservation');
		}
	}
	
	/* This function is creted for double driver reservation page.
	 *  Author:Devi Lal Verma
	*/
	public function double_driver(){
		$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		
		// apply validation rule on form field.
		$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
		$this->form_validation->set_rules('pickup_time', 'Pick Up Time', 'required');
		//$this->form_validation->set_rules('trip_date_to', 'Drof Off Date', 'required');
		//$this->form_validation->set_rules('dropoff_time', 'Drop Off Time', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('state', 'State', 'htmlspecialchars|required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip', 'Zip', 'required|numeric|max_length[10]|min_length[3]');
		
		$this->form_validation->set_rules('desination_1', 'Drop Off #1', 'required|max_length[255]');
		$this->form_validation->set_rules('desination_2', 'Drop Off #2','max_length[255]');
		$this->form_validation->set_rules('contact_name', 'Contact Person', 'trim|htmlspecialchars|required|max_length[30]|min_length[3]|callback_check_string');
		$this->form_validation->set_rules('contact_number', 'Contact Phone', 'htmlspecialchars|required|max_length[12]|min_length[8]');
		$this->form_validation->set_rules('manaual_shift', 'Manual Shift', 'required');
		//$this->form_validation->set_rules('comments', 'Special Request or Comments');
		//$this->form_validation->set_rules('promo_code', 'Promo Code', 'required');
		//$this->form_validation->set_rules('approx_hours', 'Approx. Length Of Trip (in Hours)', 'required|numeric|max_length[20]');
		$post =  $this->input->post();
		//$this->session->set_userdata ( 'manaual_shift',$this->input->post('manaual_shift'));
		
		//get login userid and email
		$username=$this->session->userdata('frontuser');
		$result=$this->admin->get_login_userId($username['username']);
		
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->session->unset_userdata('home_address');
			//$this->session->set_userdata('is_phone',$this->input->post('is_contact_phone'));
		}
		else {
			$home_address=$this->trip->get_home_address($result[0]->id);
			$basic_detail=$this->admin->get_user($result[0]->id);
			$this->session->set_userdata('basic_detail',$basic_detail);
			$this->session->set_userdata('home_address',$home_address);
		}
		
		if($this->form_validation->run() != false){
			$service_id = $this->input->post('service_id');
			$trip_date_from = $this->input->post('trip_date_from');
			$pickup_time = date("H:i", strtotime($this->input->post('pickup_time')));
			//$trip_date_to = $this->input->post('trip_date_to');
			//$dropoff_time = $this->input->post('dropoff_time');
			$address = $this->input->post('address');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zip = $this->input->post('zip');
			$desination_1 = $this->input->post('desination_1');
			$desination_2 = $this->input->post('desination_2');
			$contact_name = $this->input->post('contact_name');
			$contact_number = str_replace('-','',$this->input->post('contact_number'));
			$manaual_shift= $this->input->post('manaual_shift');
			$comments = $this->input->post('comments');
			$promo_code = $this->input->post('promo_code');
			$notification = 1;
			//$approx_hours = $this->input->post('approx_hours');
			// call model's get_user_id method to access user_id
			$user_id['record']=$this->trip->get_user_id($contact_number);
			// check user availability
			
			$data =  array(
					'service_id' => $service_id ,
					'user_id' =>$result[0]->id,
					'trip_date_from' =>date('Y-m-d', strtotime($trip_date_from)).' '.$pickup_time,
					
					'pickup_time' => $pickup_time,
					'address' => $address,
					'address_2'=>$address_2,
					'city' => $city,
					'state' => $state,
					'zip' => $zip,
					'desination_1' => $desination_1,
					'desination_2' => $desination_2,
					'contact_name' => $contact_name,
					'contact_number' => $contact_number,
					'manaual_shift' => $manaual_shift,
					'comments' => $comments,
					'promo_code' => $promo_code,
					'notification'=> $notification
			);
			// call the trip_reservation method of model to register trip.
			$last_id = $this->trip->trip_reservation($data);
			if($last_id > 0){
				$this->send_reservation_confirmation_email($result[0]->email,$contact_name);
				redirect('reservation/reservationconfirmed');
			}else{
				$this->data['wrong_insert'] = 'Your entered information is not added into database.';
				$this->template->page('reservation/double_driver',$this->data);
			}
		
		}else{
			$this->template->page('double_driver');
		}
	}
	
	function reserve_driver(){
		$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		$reserve_drive_type = $this->input->post('reserve_driver_type');
		if(!empty($reserve_drive_type)){
			if($reserve_drive_type==1){
				redirect('reservation/newreservation');
			}
			elseif ($reserve_drive_type==2){
				redirect('reservation/airport_transport');
			}
			elseif ($reserve_drive_type==3){
				redirect('reservation/double_driver');
			}
			else{
				redirect('ores/dashboard/accountsettings');
			}	
		}
		else{
			$this->template->page('reserve_driver');
		}
	
	}
	function airport_transport(){
		$front_sess_name = $this->session->userdata('frontuser');
		if(!empty($front_sess_name)){
			$this->data['session'] = $front_sess_name;
		}
		$reserve_drive_type = $this->input->post('reserve_driver_type');
		if(!empty($reserve_drive_type)){
			if($reserve_drive_type==1){
				redirect('reservation/airport_dropoff');
			}
			else{
				redirect('reservation/airport_pickup');
			}	
		}
		else{
			$this->template->page('airport_transport');
		}
	
	}
	
	function reservationconfirmed(){
		$this->template->page('reservationconfirmed');
	}
	function sometext_check($str) {
		if ($str == "00:00") {
			$this->form_validation->set_message('sometext_check','The Time Pick-Up field is required');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	/* This function is creted auto complete search on contact_number.
	 *  Author:Devi Lal Verma
	*/
	public function getResult($referencia){
		if(!empty($referencia) || isset($referencia))
		{ 
			$result=$this->admin->get_all_mobile_number($referencia);
			echo json_encode($result);
		}
	}
	
	function check_string($str)
	{
		$result=ctype_alpha(str_replace(' ','',$str)) ? true : false;
		if($result==false){
			$this->form_validation->set_message('check_string', 'Please enter only aplha character.');
			return false;
		}
		else{
			return true;
		}
	}
	
	/* This function is used to send reservation mail when user reserved in rservation.
	 *  Author:Devi Lal Verma
	*/
	public function send_reservation_confirmation_email($email,$name){
		$link=base_url();
		$html  =  @file_get_contents(base_url().'emailtemplates/reservation_email.html');
		$find = array('{name}','{link}');
		$replace   = array($name,$link);
		$messegefinal = str_replace($find,$replace,$html);
		$mail = $this->phpmailer;
		$mail->SetFrom('admin@admin.com');
		$mail->WordWrap = 100;               // set word wrap
		//$mail->Priority = 1;
		//$mail->IsHTML(true);
		$mail->Subject = 'Reservation Email';
		$mail->addAttachment("emailtemplates/logo.png");
		$mail->addAttachment("emailtemplates/forward-glyph.png");
		$mail->MsgHTML($messegefinal);
		$mail->AddAddress($email);
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Message sent!";
		}
	}
}


