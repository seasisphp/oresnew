<?php 
/**
* @author       Dev Verma
* @web         http://www.asim.pk/
* @date     13th May, 2009
* @copyright    No Copyrights, but please link back in any way
*/
 
class Orescron extends CI_Controller {
 
function __construct()
{
parent::__construct();		
	$this->load->model('dashboard_model');
	$this->load->helper('url');
}
 
function index()
{
	$current_booking=$this->dashboard_model->getallcurrentbookings();
	if($current_booking!=false){
		foreach($current_booking as $booking){
			$pick_date=strtotime($booking->trip_date_from);
			$current_date = strtotime(date('Y-m-d H:i:s'));
			$total_hours = round(($pick_date - $current_date) / 3600);
			if($total_hours==24){
				$this->send_reservation_confirmation_email($booking->email,$booking->contact_name);
				//var_dump($result);
				$data=array('is_confirmed'=>'yes');
				$this->dashboard_model->set_confirmed($booking->email,$data);
			}
			
		}
	} 
	$dispatched_booking=$this->dashboard_model->getalldiapatchedbookings();
	if($dispatched_booking!=false){
		foreach($dispatched_booking as $booking){
			$pick_date=strtotime($booking->trip_date_from);
			$current_date = strtotime(date('Y-m-d H:i:s'));
			$total_hours = round(($current_date-$pick_date) / 3600);
			if($total_hours==12){
				$this->send_reservation_confirmation_email($booking->email,$booking->contact_name);
				//var_dump($result);
				$data=array('is_acknowled'=>'yes');
				$this->dashboard_model->set_acknowled($booking->email,$data);
			}
		}
	}
}

/* This function is used to send reservation mail when user reserved in reservation.
 *  Author:Devi Lal Verma
*/
public function send_reservation_confirmation_email($email,$name){
	$html  =  @file_get_contents(base_url().'emailtemplates/alert_resrvation_email.html');
	$link = site_url();
	$find = array('{name}','{link}');
	$replace   = array($name, $link);
	$messegefinal = str_replace($find,$replace,$html);
	$mail = $this->phpmailer;
	$mail->WordWrap = 100;
	$mail->SetFrom('admin@admin.com', 'ORES');
	$mail->Subject    = 'Alert Reservation Email';
	$mail->addAttachment("emailtemplates/logo.png");
	$mail->addAttachment("emailtemplates/forward-glyph.png");
	$mail->MsgHTML($messegefinal);
		
	$mail->AddAddress($email);
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		echo "Message sent!";
	}
}
}
