<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Termsconditions extends CI_Controller {

	 
	 function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->library('session');
	
	} 
	
	
	public function index()
	{
	if(!empty($front_sess_name)){
			$this->template->page('home');
			}else{
			$this->template->page('termsconditions');
			}
	
	
	}
	
	
}

