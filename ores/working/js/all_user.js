/*
 * This file is applied on all_user page.
 * Author: Devi Lal Verma
 */
    $(document).ready(function(){
    	
    	 // To hide error message
    	setInterval(function(){
    		$('.alert-success').css('visibility', 'hidden');
    		$('.alert-msg').css('visibility', 'hidden');
    	},10000);
    	$(".remove_record").click(function() {
    		var pos=$(this).attr('id');
    		var r=confirm("Do you really want to delete this record? This may effect some reservation(s)");
    		if (r==true)
    		  {
    			$.ajax({
    				url:'remove_user',
    				data: 'id='+pos,
    				type:'POST',
    				beforeSend: function() {
    					// alert("loading")
    				},
    				success:function(data){
    					$('.alert-msg').html('');
    					$('.alert-success').css('visibility', 'hidden');
    					$('.alert-msg').css('visibility', 'visible').removeClass('alert-error').addClass('alert-success').html('Success! User deleted successfully');
    					$('#'+pos).remove();
    				},
    				error:function() {
    					$('.alert-msg').html('');
    					$('.alert-success').css('visibility', 'hidden');
    					$('.alert-msg').css('visibility', 'visible').removeClass('alert-success').addClass('alert-error').html('You can not delete this User as it is engaged in some reservation');
    				}
    			});
    		  }
    		else
    		  {
    			  return false;
    		  }
    	});
    	
    });
