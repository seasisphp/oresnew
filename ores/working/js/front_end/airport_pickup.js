$(document).ready(function() {

   // add the date time plugin control
   $('#trip_date_from').datepicker({beforeShowDay: NotBeforeToday });
  // $('#pickup_time').timepicker();
   jQuery('#pickup_time').timeEntry({ampmPrefix: ' '}).change(function() { 
	    var log = $('#log'); 
	    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	});

   function NotBeforeToday(date)
   {
       var now = new Date();//this gets the current date and time
       if (date.getFullYear() == now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() >= now.getDate())
           return [true];
       if (date.getFullYear() >= now.getFullYear() && date.getMonth() > now.getMonth())
          return [true];
        if (date.getFullYear() > now.getFullYear())
          return [true];
       return [false];
   }
   // define lettersonly function to verify letters only in text field
  jQuery.validator.addMethod("lettersonly", function(value, element) {
   	return this.optional(element) || /^[a-z]+$/i.test(value);
   }, "Letters only please"); 
  
  // define alpha function to verify full name in text field.
  jQuery.validator.addMethod("alpha", function(value, element) {
	    return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
	},"Only Characters Allowed.");
	// validate form.....
	  // set mask for phone field
	  $("#contact_number").mask("999-999-9999");
	validator = $("form#airport_pickup").validate({	
		
		 errorElement: "span",
		 errorPlacement: function (error, element) {
			
			  if( element.attr("name") == "trip_date_from"){
				 error.insertAfter("#date_span");
			 }
			 else if( element.attr("name") == "time_pickup"){
				 error.insertAfter("#time_span");
			 }
			 else if( element.attr("name") == "manaual_shift"){
				 error.insertBefore("#manual_shift_span");
			 }  
			 else if( element.attr("name") == "approx_hours"){
				 error.insertAfter("#approx_js_error").css('margin-left','135px');
			 }
			 else if( element.attr("name") == "promo_code"){
				 error.insertBefore(".promo-code-new");
			 }
			 else if( element.attr("name") == "term_condition"){
				 error.insertBefore("#term_fielset").css('margin-left','185px');;
			 }  
			 else{
	            error.insertBefore(element);}
	            },
		rules: {
			trip_date_from:{
				required: true
			},	
			pickup_time:{
				required: true
			},
			address:{
				required: true,
				maxlength:50,
				minlength:3
			},
			
			city:{
				required: true,
				maxlength:255,
				minlength:3
			},
			zip:{
				required: true,
				maxlength:10,
				minlength:3,
				number:true
			},
			state:{
				required: true,
				maxlength:255,
				minlength:2
			},
			airport:{
				required: true,
				maxlength:30,
				minlength:3
			},
			airline:{
				required: true,
				maxlength:50,
				minlength:3
			},
			flight_number:{
				required: true,
			},
			contact_number:{
				required: true,
				phoneUS:true,
				maxlength:12,
				minlength:8
			},
			
			contact_name:{
				required: true,
				maxlength:30,
				minlength:3,
				alpha:true
			},
			comments:{
				maxlength:50
			},
			promo_code:{
				maxlength:30
			},
			approx_hours:{
				required: true,
				number:true,
				maxlength:20
			},
			manaual_shift:{
				required:true
			},
			term_condition:{
				required:true
			}
		},
		messages: {
				trip_date_from:{
					required:"This field is required",
				},
				pickup_time:{
					required:"This field is required",
				},
				address:{
					required:"This field is required",
					maxlength:"You cannot enter more than 50 character",
					minlength:"You cannot enter less than 3 character"
				},
				city:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 3 character"
				},
				zip:{
					required:"This field is required",
					maxlength:"You cannot enter more than 10 digit",
					minlength:"You cannot enter less than 3 digit",
					number:"Enter number only"
				},
				state:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 2 character"
				},
				airport:{
					required:"This field is required",
					maxlength:"You cannot enter more than 50 character",
					minlength:"You cannot enter less than 3 character"
				},
				airline:{
					required:"This field is required",
					maxlength:"You cannot enter more than 50 character",
					minlength:"You cannot enter less than 3 character"
				},
				flight_number:{
					required:"This field is required"
				},
				contact_number:{
					required:"This field is required",
					maxlength:"You cannot enter more than 12 digit",
					minlength:"You cannot enter less than 8 digit",
					phoneUS:"Please specify a valid phone number"	
				},
				contact_name:{
					required:"This field is required",
					maxlength:"You cannot enter more than 30 character",
					minlength:"You cannot enter less than 3 character",
					alpha:"Enter Character Only"
						
				},
				comments:{
					maxlength:"You cannot enter more than 50 character"
				},
				promo_code:{
					maxlength:"You cannot enter more than 30 character"
				},
				approx_hours:{
					required:"This field is required",
					maxlength:"You cannot enter more than 20 digit",
					number:"Enter number only"	
				},
				manaual_shift:{
					required:"Select manual shift type"
				},
				term_condition:{
					required:"This field is required",
				}
				
	        }
		});

	$('#airport_pickup_submit').click(function() {
	    var valid=$("form#airport_pickup").valid();
	  
	});
	
	
});


