tinyMCE.init({
//General options
//mode : "textareas",
 mode : "specific_textareas", 
 editor_selector : "txtarea",
elements : 'descr',
theme : "advanced",
skin : "o2k7",
skin_variant : "silver",
plugins :
"safari,pagebreak,style,layer,table,advhr,advlink,iespell,inlinepopups,insertdatetime,preview,media",

// Theme options
theme_advanced_buttons1 : "bold,italic,underline,fontselect,fontsizeselect",
theme_advanced_buttons2 : "cut,copy,paste,|,search,bullist,numlist,|blockquote,|,undo,redo,|,anchor,image,cleanup,code,",
theme_advanced_buttons3 : "",
theme_advanced_toolbar_location : "top",
theme_advanced_toolbar_align : "left",
theme_advanced_statusbar_location : "bottom",
theme_advanced_resizing :false,

// Example content CSS (should be your site CSS)
content_css : "css/content.css",

// Drop lists for link/image/media/template dialogs
template_external_list_url : "lists/template_list.js",
external_link_list_url : "lists/link_list.js",
external_image_list_url : "lists/image_list.js",
media_external_list_url : "lists/media_list.js",

// Replace values for the template plugin
template_replace_values : {
username : "Some User",
staffid : "991234"
}
});