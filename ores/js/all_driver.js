/*
 * This file is used to remove record from page.
 * Hide alert message.
 * Author: Devi Lal Verma
 */
$(document).ready(function () {

	 // To hide error message
	setInterval(function(){
		$('.alert-success').css('visibility', 'hidden');
		$('.alert-msg').css('visibility', 'hidden');
	},10000);		
	// remove record from page without refresh.
	$(".remove_record").click(function() {
		var pos=$(this).attr('id');
		var r=confirm("Do you really want to delete this record?");
		if (r==true)
		  {
			$.ajax({
				url:'remove_driver',
				data: 'id='+pos,
				type:'POST',
				beforeSend: function() {
					// alert("loading")
				},
				success:function(data){
					$('.alert-msg').html('');
					$('.alert-success').css('visibility', 'hidden');
					$('.alert-msg').css('visibility', 'visible').removeClass('alert-error').addClass('alert-success').html('Success! record deleted successfully');
					$('#'+pos).remove();
				},
				error:function() {
					$('.alert-msg').html('');
					$('.alert-success').css('visibility', 'hidden');
					$('.alert-msg').css('visibility', 'visible').removeClass('alert-success').addClass('alert-error').html('You can not delete this Driver as it is engaged in some reservation');
				}
			});
		  }
		else
		  {
			  return false;
		  }
	});
});
