/*
 This file is used to add date time puglin control
 * And auto complete search.
 * Author: Devi Lal Verma
 */

$(document).ready(function() {
	
	
	var d = new Date();
	var year = d.getFullYear();
	d.setFullYear(year);
    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
   $('#trip_date_from').datepicker({dateFormat: 'mm/dd/yy', changeYear: true, changeMonth: true,yearRange: '1900:' + year+' ',defaultDate:d});

  // $('#pickup_time').timepicker();
  /* $('#pickup_time').timeEntry({ampmPrefix: ' '}).change(function() { 
	    var log = $('#log'); 
	    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	});*/
   $('#trip_date_to').datepicker();
   $('#dropoff_time').timepicker();
   // auto complete search on contact_number
   $('#contact_number').keypress(function(e){
       if(e.which == 13)
       {
           e.preventDefault();
       }
       var searched = $('#contact_number').val();
       var fullurl ='getResult/'+ searched;
       $.getJSON(fullurl,function(result){
           //display suggestion code goes here
           var elements = [];
           $.each(result, function(i, val){
               elements.push(val.mobile_number);
           });
           $('#contact_number').autocomplete({
               source : elements
           });
       });
   });
   
   $('#contact_name').keypress(function(e){
       if(e.which == 13)
       {
           e.preventDefault();
       }
       var searched = $('#contact_name').val();
       var fullurl ='get_contact_name/'+ searched;
       $.getJSON(fullurl,function(result){
           //display suggestion code goes here
           var elements = [];
           $.each(result, function(i, val){
               elements.push(val.first_name+' '+ val.last_name);
           });
           $('#contact_name').autocomplete({
               source : elements
           });
       });
   });
   
$("#contact_number").blur(function() { 
	   //var mobileno = $(this).val();
       $.ajax(
       {
           type:"POST",
           url: '/ores/admin/home/get_username',
           data:{mobileno:$(this).val()},
           cache: false,
           success: function(res)
           {
        	   var data=res;
        	  console.log(data);
        	   var obj =jQuery.parseJSON(data);
               $("#first_name").val(obj[0]['first_name']);
               $("#last_name").val(obj[0]['last_name']);
               $("#city").val(obj[0]['city']);
               $("#state").val(obj[0]['state']);
               $("#zip").val(obj[0]['zip_code']);
               $("#address").val(obj[0]['address_1']);
               $("#address_2").val(obj[0]['address_2']);
           }
       }); //end of AJAX call
   
 });
   
});
/*jQuery(function($){
	 
	   $("#contact_number").mask("999-99-9999");
	});*/