$(document).ready(function() {

   // add the date time plugin control
   $('#trip_date_from').datepicker({beforeShowDay: NotBeforeToday });
  // $('#pickup_time').timeEntry();
   /*jQuery('#pickup_time').timeEntry({ampmPrefix: ' '}).change(function() { 
	    var log = $('#log'); 
	    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	});*/
  // $.timeEntry.setDefaults(settings);
   

   function NotBeforeToday(date)
   {
       var now = new Date();//this gets the current date and time
       if (date.getFullYear() == now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() >= now.getDate())
           return [true];
       if (date.getFullYear() >= now.getFullYear() && date.getMonth() > now.getMonth())
          return [true];
        if (date.getFullYear() > now.getFullYear())
          return [true];
       return [false];
   }
   // define lettersonly function to verify letters only in text field
  jQuery.validator.addMethod("lettersonly", function(value, element) {
   	return this.optional(element) || /^[a-z]+$/i.test(value);
   }, "Letters only please"); 
  // define alpha function to verify full name in text field.
  jQuery.validator.addMethod("alpha", function(value, element) {
	    return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
	},"Only Characters Allowed.");
	// validate form.....
  // set mask for phone field
  $("#contact_number").mask("999-999-9999");
  
	validator = $("form#newreservation").validate({	
		
		 errorElement: "span",
		 errorPlacement: function (error, element) {
			 if (element.attr("name") == "contact_phone"){
			        error.insertBefore("contact_phone_span");
			 }
			 else if( element.attr("name") == "trip_date_from"){
				 error.insertAfter("#date_span");
			 }
			 else if( element.attr("name") == "manaual_shift"){
				 error.insertBefore("#manual_shift_span");
			 }
			 else if( element.attr("name") == "pickup_time"){
				 error.insertAfter("#time_after").css('margin-left','290px');
			 }
			 else if( element.attr("name") == "approx_hours"){
				 error.insertAfter("#approx_js_error").css('margin-left','135px');
			 }
			 else if( element.attr("name") == "promo_code"){
				 error.insertBefore(".promo-code-new");
			 }
			 else if( element.attr("name") == "term_condition"){
				 error.insertBefore("#term_fielset").css('margin-left','185px');;
			 }  
			 else{
	            error.insertBefore(element);}
	            },
		rules: {
			trip_date_from:{
				required: true
			},	
			pickup_time:{
				required: true
			},
			address:{
				required: true,
				maxlength:50,
				minlength:3
			},
			address_2:{
				maxlength:50,
				minlength:3
			},
			city:{
				required: true,
				maxlength:255,
				minlength:3
			},
			zip:{
				required: true,
				maxlength:10,
				minlength:3,
				number:true
			},
			state:{
				required: true,
				maxlength:255,
				minlength:2
			},
			desination_1:{
				required: true,
				maxlength:255,
				minlength:3
			},
			desination_2:{
				maxlength:255,
				minlength:3
			},
			approx_hours:{
				required: true,
				maxlength:20,
				number:true
			},
			contact_number:{
				required: true,
				maxlength:12,
				minlength:8
			},
			contact_name:{
				required: true,
				maxlength:30,
				minlength:3,
				alpha:true
			},
			comments:{
				maxlength:50
			},
			promo_code:{
				maxlength:30
			},
			manaual_shift:{
				required:true
			},
			term_condition:{
				required:true
			}
		},
		messages: {
				trip_date_from:{
					required:"This field is required",
				},
				pickup_time:{
					required:"This field is required",
				},
				address:{
					required:"This field is required",
					maxlength:"You cannot enter more than 50 character",
					minlength:"You cannot enter less than 3 character"
				},
				address_2:{
					maxlength:"You cannot enter more than 50 character",
					minlength:"You cannot enter less than 3 character"
				},
				city:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 3 character"
				},
				zip:{
					required:"This field is required",
					maxlength:"You cannot enter more than 10 digit",
					minlength:"You cannot enter less than 3 digit",
					number:"Enter number only"
				},
				state:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 2 character"
				},
				desination_1:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 3 character"
				},
				desination_2:{
					required:"This field is required",
					maxlength:"You cannot enter more than 255 character",
					minlength:"You cannot enter less than 3 character"
				},
				approx_hours:{
					required:"This field is required",
					maxlength:"You cannot enter more than 20 digit",
					number:"Enter number only"	
				},
				contact_number:{
					required:"This field is required",
					maxlength:"You cannot enter more than 12 digit",
					minlength:"You cannot enter less than 8 digit"
				},
				contact_name:{
					required:"This field is required",
					maxlength:"You cannot enter more than 30 character",
					minlength:"You cannot enter less than 3 character",
					alpha:"Enter Character Only"
				},
				comments:{
					maxlength:"You cannot enter more than 50 character"
				},
				promo_code:{
					maxlength:"You cannot enter more than 30 character"
				},
				manaual_shift:{
					required:"Select manual shift type"
				},
				term_condition:{
					required:"This field is required",
				}
				
	        }
		});

	$('#newreservation_submit').click(function() {
	    var valid=$("form#newreservation").valid();
	  
	});
	
	function get_form_data_and_submit()
	{  
	    document.forms["student_form"].student_id.value = document.getElementById('sid').value;
	    document.forms["student_form"].student_name.value = document.getElementById('sname').value;
	    document.forms["student_form"].student_address.value = document.getElementById('saddress').value;
	    document.forms["student_form"].student_mark1.value = document.getElementById('smark1').value;
	    document.forms["student_form"].student_mark2.value = document.getElementById('smark2').value;
	    document.forms["student_form"].method = 'post';
	    document.forms["student_form"].action = '<?php echo base_url().$this->config->item("test_form_uri"); ?>';
	    document.forms["student_form"].submit();
	    return false;
	}
});


