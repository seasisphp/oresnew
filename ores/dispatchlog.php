<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	
class Dispatchlog extends CI_Controller {
	/*  dispatch log */
	function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('dlogs_model','admin');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('file_helper');
		$this->load->helper('download_helper');
	
	} 
	public function index(){
		$sess_name = $this->session->userdata('username');
		if(empty($sess_name)){
			$this->load->view('admin/login');
		}else{		
			$this->data['session'] = $sess_name;
			$this->template->page('admin/dispatchlog');
			
		}
	}
	
  function create_csv(){
  
  $this->load->dbutil();

$query = $this->db->query("SELECT reservation.trip_date_from, reservation.trip_date_to, reservation.pickup_time, reservation.address, reservation.state, reservation.city, reservation.zip, reservation.desination_1, reservation.desination_2, reservation.approx_hours, user.first_name, driver.first_name AS driver_name FROM `reservation` JOIN user JOIN driver WHERE reservation.user_id = user.id AND reservation.driver_id = driver.id AND reservation.status !=3 AND reservation.status !=4");

 $csv_data = $this->dbutil->csv_from_result($query);

 $filename =  $filename = "dispatch" . date('Ymd') . ".csv";
 if (!write_file("./csv/".$filename, $csv_data)) {
	$csv_data = $this->dbutil->csv_from_result($query);
        echo 'Unable to write the file';
    } else {

        $data = file_get_contents("./csv/".$filename);
        force_download($filename, $data); 

    }
   } 
	/* Function dlogs
	*  Getting all list of reservations.
	*  
	*/
	public function dlogs(){
	 
	
		$count = $this->admin->count_all_reservations();
		
		$config['base_url'] = base_url().'admin/dispatchlog/dlogs';
		$config['total_rows'] = $count;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['dlogs'] = $this->admin->get_all_reservations($config['per_page'], $page);
		
			if(!empty($data['dlogs'])){
		$tempArray = array();
			
		foreach($data['dlogs'] as $dlogs){
			$city_drivers = $this->admin->getADriversByCity($dlogs->city);
			$tempArray[$dlogs->id] = $city_drivers;
		}
	
		 $tempArray1 = array();
			
		foreach($data['dlogs'] as $dlogs){
			$city_drivers = $this->admin->getNADriversByCity($dlogs->city);
			$tempArray1[$dlogs->id] = $city_drivers;
		} 
		$tempArray2 = array();
			
		foreach($data['dlogs'] as $dlogs){
		$noncity_drivers = $this->admin->getAnonCityDrivers($dlogs->city);
		$tempArray2[$dlogs->id] = $noncity_drivers;
		} 
		
		$tempArray3 = array();
			
		foreach($data['dlogs'] as $dlogs){
		$noncity_drivers = $this->admin->getNAnonCityDrivers($dlogs->city);
		$tempArray3[$dlogs->id] = $noncity_drivers;
		} 
		
		
		$this->data['AdriverByCity'] = $tempArray;
		
		$this->data['NACityDrivers'] = $tempArray1;
		
		$this->data['AnonCityDrivers'] = $tempArray2;
		
		$this->data['NAnonCityDrivers'] = $tempArray3;
	
		}
		$result = 	$data['dlogs'];
		$this->data['dlog'] = $result;
		
		$this->template->page('admin/dispatchlog',$this->data);

	}
	/* Function driverpostAjax
	*  post driver id to assign driver
	*  
	*/ 
		public function driverpostAjax(){		
			$driver_id = $_POST['driver_id'];
			$id = $_POST['id'];
						$data =  array(
						'driver_id' => $driver_id,
						'status' => 1,
						);
					 $this->admin->assign_driver($data,$id);
					//send mail to driver for his approval 
					$driversch = $this->admin->getDriversTripDetail($id,$driver_id);	
					
					$this->load->library('email');
					$this->email->from('admin@admin.com', 'Your Name');
					$this->email->to($driversch->email);
					$this->email->subject('Assign Trip');
					$html  =  @file_get_contents(base_url().'emailtemplates/driver_email.html');
					$find = array('{name}','{from}','{to}','{city}');
					$replace   = array($driversch->first_name,$driversch->trip_date_from,$driversch->trip_date_to,$driversch->city);
					$messegefinal = str_replace($find,$replace,$html);
					$this->email->message($messegefinal);
					$this->email->send();
					
					
					//send mail to user that driver has been assigned for the trip
					$usersch = $this->admin->getuserTripDetail($id);	
					
					$this->email->from('admin@admin.com', 'Your Name');
					$this->email->to($usersch->email);
					$this->email->subject('Assign Trip');
					$html  =  @file_get_contents(base_url().'emailtemplates/user_email.html');
					$find = array('{name}','{from}','{to}','{city}');
					$replace   = array($driversch->first_name,$driversch->trip_date_from,$driversch->trip_date_to,$driversch->city);
					$messegefinal = str_replace($find,$replace,$html);
					$this->email->message($messegefinal);
					$this->email->send();
					
					
					 $data_d =  array(
						'driver_id' => $driver_id,
						'reservation_id' => $id,
						);
					 
					 $this->admin->driver_schdule($data_d,$id);
						
			
		
		} 
	
	
	
	public function tripdetail($id = Null){
	    
		if($id){
			$id = $id; 		
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
			$this->form_validation->set_rules('pickup_time', 'pick Up time', 'required');
			$this->form_validation->set_rules('trip_date_to', 'Drop off Time', 'required');
			$this->form_validation->set_rules('driver_id', 'Driver Name', 'required');
			$this->form_validation->set_rules('service_id', 'Service Type', 'required'); 
			$this->form_validation->set_rules('user_id', 'User name', 'required'); 
			$this->form_validation->set_rules('city', 'City', 'required');
			$this->form_validation->set_rules('desination_1', 'Drop off location1', 'required');
			$this->form_validation->set_rules('desination_2', 'drop off location2', 'required');
			$this->form_validation->set_rules('comments', 'Notes', 'required');
			
			$this->data['tripdetail'] = $this->admin->get_reservationdetail($id);
	
		
		
		
			if($this->form_validation->run() != false){
	
				$status = $this->input->post('status');
				$trip_date_from = $this->input->post('trip_date_from')." ".$this->input->post('pickup_time');
				
				$pickup_time = $this->input->post('pickup_time');
				$trip_date_to = $this->input->post('trip_date_todate')." ".$this->input->post('trip_date_to');		
				$driver_id  = $this->input->post('driver_id');
				$service_id  = $this->input->post('service_id');				
				$user_id  = $this->input->post('user_id');
				$city  = $this->input->post('city');	
				$desination_1  = $this->input->post('desination_1');	
				$desination_2  = $this->input->post('desination_2');	
				$comments  = $this->input->post('comments');
	
				if(empty($this->data['error'])){			
			
					$data =  array(
						'status' => $status,
						'trip_date_from' => $trip_date_from,
						'pickup_time' => $pickup_time,
						'trip_date_to' => $trip_date_to,
						'driver_id' => $driver_id,
						'service_id' => $service_id,
						'user_id' => $user_id, 
						'city' => $city,
						'desination_1' => $desination_1,
						'desination_2' => $desination_2,
						'comments' => $comments
						
					);
					$get_true_false = $this->admin->update_reservationdetail($data,$id);
					if($get_true_false > 0){
					$this->session->set_flashdata('msg', 'Successfully Updated');
					redirect('admin/dispatchlog/dlogs');
					//$this->template->page('admin/tripdetail',$this->data);
					
					}else{
						$this->data['wrong_update'] = 'Your entered information is not correct, please try again.';
						$this->template->page('admin/tripdetail',$this->data);
					}
				}	
			}else{	
				
				$this->template->page('admin/tripdetail',$this->data);
			}
		}else{
			redirect('admin/dispatchlog/dlogs');
		}
	
	}
	
	}
	