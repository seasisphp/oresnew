<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver extends CI_Controller {
	/*  ADMIN HOME */
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('driver_model','driver');
		$this->load->model('admin_model');
		$this->load->library('session');
		$this->load->library('Pagination');
		$this->load->helper('file_helper');
		$this->load->helper('download_helper');
		$sess_name = $this->session->userdata('username');
		
		$notification =  $this->admin_model->count_notifications();
		//$newreservationcount ;
		$this->data['notification'] = $notification;
		$sess_name = $this->session->userdata('username');
		
		$sess_user_id = $sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=2))
		{
		 redirect("dashboard");
		}
	}
	
	public function index(){
		
	}
	/*
	 * This function is created for add driver.
	 * Author:Devi Lal Verma
	 */
	public function add_driver(){
		$sess_name = $this->session->userdata('username');
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|htmlspecialchars|required|alpha|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|htmlspecialchars|required|alpha|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('username', 'User Name', 'htmlspecialchars|required|is_unique[driver.username]|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email|is_unique[driver.email]');
		//$this->form_validation->set_rules('company_name', 'Company Name', 'htmlspecialchars|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'htmlspecialchars|required|max_length[12]|min_length[8]|is_unique[driver.mobile_number]');
		$this->form_validation->set_rules('emergency_contact', 'Emergency Contact', 'htmlspecialchars|max_length[12]|min_length[8]|is_unique[driver.emergency_contact]');		
		$this->form_validation->set_rules('address_title', 'Address Title', 'required');
		$this->form_validation->set_rules('address_1', 'Address Line 1', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]|min_length[3]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[30]');
		$this->form_validation->set_rules('state', 'State', 'required|max_length[30]|min_length[2]');		
		//$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');	
		$this->form_validation->set_rules('driver_note', 'Driver Note', 'max_length[255]');
		$this->form_validation->set_rules('mobile_provider', 'Mobile Provider', 'required|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('pay_rate', 'Pay Rate Per Hour', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('pay_rate_permile', 'Pay Rate Per Mile', 'required|numeric|max_length[10]');
		//$this->form_validation->set_rules('status', 'status', 'required');
		$post =  $this->input->post();
        
		// set session variable.
		$this->session->set_userdata('address_title',$this->input->post('address_title'));
		$this->session->set_userdata('country',$this->input->post('country'));
		$this->session->set_userdata('driverstatus',$this->input->post('status'));
		
		if($this->form_validation->run() != false){
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$username = $this->input->post('username');
			$email = $this->input->post('email');	
			$mobile_number = $this->input->post('mobile_number');	
			$emergency_contact =$this->input->post('emergency_contact');	
			$address_title = $this->input->post('address_title');			
			$address_1 = $this->input->post('address_1');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city'); 		
			$state = $this->input->post('state');
			//$country = $this->input->post('country');
			$zip_code = $this->input->post('zip_code');
			$pay_rate=$this->input->post('pay_rate');
			$pay_rate_permile=$this->input->post('pay_rate_permile');
			$driver_note=$this->input->post('driver_note');
			//$status = $this->input->post('status');
			$mobile_provider=$this->input->post('mobile_provider');
			// unset session variable.
			$this->session->unset_userdata('address_title');
			$this->session->unset_userdata('country'); 
			$this->session->unset_userdata('driverstatus');
				$data =  array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'username' => $username,
					'email' => $email,
					//'company_name' => $company_name,
					'mobile_number' => $mobile_number,
					'emergency_contact' => $emergency_contact,
					'address_title' => $address_title,	
					'address_1' => $address_1,
					'address_2' => $address_2,				
					'city' => $city,
					'state' => $state,
					//'country' => $country,
					'zip_code' => $zip_code,
					'pay_rate'=> $pay_rate,
					'pay_rate_permile'=> $pay_rate_permile,
					'driver_note'=> $driver_note,
					//'status' => $status,
					'mobile_provider'=>$mobile_provider	
					
				);
				
				$last_id = $this->driver->add_driver($data);
				
				if($last_id > 0){
					$this->session->set_flashdata('msg', 'driver registered successfully ');
					redirect('admin/driver/all_driver');
				}else{
					$this->data['wrong_insert'] = 'Your entered information is not added into database.';
					$this->template->page('admin/driver/add_driver',$this->data);
				}
				
		}else{			
			$this->template->page('admin/driver/add_driver');
		}
		
	}
	

	/* This function is creted for driver edit page
	*  Edit driver according to driver id
	*  param driver_id(int)
	*  Author:Devi Lal Verma
	*/
	

	public function edit_driver($id = Null){
		if($id){
			$driver_id = $id; 		
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|htmlspecialchars|required|alpha|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|htmlspecialchars|required|alpha|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('username', 'User Name', 'trim|htmlspecialchars|required|callback_check_user_name['.$id.']|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email||callback_check_driver_email['.$id.']');
			//$this->form_validation->set_rules('company_name', 'Company Name', 'required|max_length[30]');
			$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'htmlspecialchars|required|max_length[12]|min_length[8]|callback_check_driver_mobile_no['.$id.']'); 
			$this->form_validation->set_rules('emergency_contact', 'Emergency Contact', 'htmlspecialchars|max_length[12]|min_length[8]|callback_check_driver_emergency_no['.$id.']');		
			$this->form_validation->set_rules('address_1', 'address_1', 'required|max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_2', 'address_2', 'max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_title', 'Address Title', 'required');
			$this->form_validation->set_rules('city', 'City', 'required|max_length[30]');
			$this->form_validation->set_rules('state', 'State', 'required|max_length[30]|min_length[2]');
			//$this->form_validation->set_rules('country', 'Country', 'required');
			$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|max_length[10]|min_length[3]');
			$this->form_validation->set_rules('driver_note', 'Driver Note', 'max_length[255]');
			$this->form_validation->set_rules('pay_rate', 'Pay Rate Per Hour', 'required|numeric|max_length[10]');
			$this->form_validation->set_rules('pay_rate_permile', 'Pay Rate Per Mile', 'required|numeric|max_length[10]');
			//$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('mobile_provider', 'Mobile Provider', 'required|max_length[30]|min_length[3]');
			$this->data['user'] = $this->driver->get_driver($id);
			if ($this->input->server('REQUEST_METHOD') === 'POST')
			{
				$this->session->unset_userdata('driver_record');
				$this->session->set_userdata('address_title',$this->input->post('address_title'));
				$this->session->set_userdata('country',$this->input->post('country'));
				$this->session->set_userdata('driverstatus',$this->input->post('status'));
			}
			else {
				$this->session->set_userdata('driver_record', $this->data['user']);
			}
			if($this->form_validation->run() != false){
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$username = $this->input->post('username');
				$email = $this->input->post('email');		
				//$company_name  = $this->input->post('company_name');
				$mobile_number = $this->input->post('mobile_number');	
				$emergency_contact =$this->input->post('emergency_contact');		
				$address_title  = $this->input->post('address_title');
				$address_1  = $this->input->post('address_1');	
				$address_2  = $this->input->post('address_2');	
				$city  = $this->input->post('city');	
				$state  = $this->input->post('state');	
				//$country  = $this->input->post('country');	
				$zip_code  = $this->input->post('zip_code');
				$pay_rate=$this->input->post('pay_rate');
				$pay_rate_permile=$this->input->post('pay_rate_permile');
				$driver_note=$this->input->post('driver_note');
				//$status  = $this->input->post('status');	
				$mobile_provider=$this->input->post('mobile_provider');
				//$current_date = date('Y-m-d h:i:s a', time());
				$this->session->unset_userdata('address_title');
				$this->session->unset_userdata('country');
				$this->session->unset_userdata('driverstatus');
				if(empty($this->data['error'])){			
			
					$data =  array(
						'first_name' => $first_name,
						'last_name' => $last_name,
						'username' => $username,
						'email' => $email,
						//'company_name' => $company_name,
						'mobile_number' => $mobile_number, 
						'emergency_contact' => $emergency_contact,
						'address_title' => $address_title,
						'address_1' => $address_1,
						'address_2' => $address_2,
						'city' => $city,
						'state' => $state,
						//'country' => $country,
						'zip_code' => $zip_code,
						'pay_rate'=> $pay_rate,
						'pay_rate_permile'=> $pay_rate_permile,
						'driver_note'=> $driver_note,
						'mobile_provider'=>$mobile_provider,
						//'status' => $status
					);
					$get_true_false = $this->driver->update_driver($data,$id);
					if($get_true_false > 0){
						$this->session->set_flashdata('msg', 'Updated Successfully ');
						redirect('admin/driver/all_driver');
					}else{
						$this->data['wrong_update'] = 'Your entered information is not correct, please try again.';
						$this->template->page('admin/driver/edit_driver',$this->data);
					}
				}	
			}else{	
				
				$this->template->page('admin/driver/edit_driver',$this->data);
			}
		}else{
			redirect('driver/driver/all_driver');
		}		
		
	}
	
	/* 
	* This function is creted for all driver list page.
	*  Author:Devi Lal Verma
	*/
	public function all_driver(){
		$this->session->unset_userdata('searchterm');
		$count = $this->driver->count_all_driver();
		$config['base_url'] = base_url().'admin/driver/all_driver';
		$config['total_rows'] = $count;
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['drivers'] = $this->driver->get_all_driver($config['per_page'], $page);
		
		$result = 	$data['drivers'];
		$this->data['driver'] = $result;
		$this->template->page('admin/driver/all_driver',$this->data);
		
	} 
	
	/* This function is creted for remove driver.
	 * remove driver according to driver id
	*  param driver_id(int)
	*  Author:Devi Lal Verma
	*/
	public function remove_driver($id=null){
		$id= $this->input->post('id');
	    if(!empty($id)){
		$this->driver->remove_driver($id);
		echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
				
				
	}
	public function check_availability($driver_id){
		
	}
	
	/*
	 *  This function is used to return all driver
	*  searched based on first_name,last_name, email address.
	*  Author: Devi Lal Verma
	*/
	public function search_driver(){
		$sess_name = $this->session->userdata('username');
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		$searchterm = $this->driver->searchterm_handler(trim($this->input->get_post('searchterm', TRUE)));
		$limit = ($this->uri->segment(4) > 0)?$this->uri->segment(4):0;
		$config['base_url'] = base_url().'driver/driver/search_driver';
		$config['total_rows'] = $this->driver->count_search_driver($searchterm);
		$config['per_page'] = 4;
		$config['uri_segment'] = 4;
		$choice = $config['total_rows']/$config['per_page'];
		$config['num_links'] = round($choice);
		$this->pagination->initialize($config);
		$data['drivers'] = $this->driver->search_driver($searchterm,$config['per_page'], $limit);
		$result = 	$data['drivers'];
		$this->data['driver'] = $result;
		$data['links'] = $this->pagination->create_links();
		$data['searchterm'] = $searchterm;
		$this->template->page('admin/driver/search_driver',$this->data);
	
	}
	function check_driver_email($email,$id){
		$sess_name = $this->session->userdata('username');
		//check session
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		else{
			redirect('base_url()');
		}
		if(!empty($email)){
			$result = $this->driver->check_driver_email($email,$id);
			if($result==0){
	
				$this->form_validation->set_message('check_driver_email', 'This email already exist.');
				return false;
			}else{
				return true;
			}
		}
	}
	function check_driver_mobile_no($mobile_no,$id){
		if(!empty($mobile_no)){
			$result = $this->driver->check_driver_mobile_no($mobile_no,$id);
			if($result==0){
	
				$this->form_validation->set_message('check_driver_mobile_no', 'This mobile number already exist.');
				return false;
			}else{
				return true;
			}
		}
	
	}
	
	function check_driver_emergency_no($mobile_no,$id){
		if(!empty($mobile_no)){
			$result = $this->driver->check_driver_emergency_no($mobile_no,$id);
			if($result==0){
	
				$this->form_validation->set_message('check_driver_emergency_no', 'This mobile number already exist.');
				return false;
			}else{
				return true;
			}
		}
	
	}
	/*
	 *  This function is used to check username unique validation
	*   Author: Devi Lal Verma
	*/
	function check_user_name($username,$id){
		if(!empty($username)){
			$result = $this->driver->check_user_name($username,$id);
			if($result==0){
	
				$this->form_validation->set_message('check_user_name', 'This user name is already exist.');
				return false;
			}else{
				return true;
			}
		}
	}
	
	/*
	 *  This function is used to download all driver
	*   infornation in csv file.
	*   Author: Devi Lal Verma
	*/
	public function download_driver(){  
		$this->load->dbutil();
		$driver = $this->driver->download_drivers_detail();
	 	$this->load->dbutil();
   	    $csv_data = $this->dbutil->csv_from_result($driver);
		$filename =  $filename = "allDriverInfo" . date('Ymd') . ".csv";
	    if (!write_file("./csv/".$filename, $csv_data)) {
			$csv_data = $this->dbutil->csv_from_result($driver);
       	    echo 'Unable to write the file';
   		 } 
   		else {
	 		$data = file_get_contents("./csv/".$filename);
       		force_download($filename, $data); 
		}
	}
	
	
}

