<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class driveravailability extends CI_Controller {
	 // Define 
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('driver_model','admin');
		$this->load->model('admin_model');
		$this->load->library('session');
		
		$notification =  $this->admin_model->count_notifications();
		//$newreservationcount ;
		$this->data['notification'] = $notification;
		$sess_name = $this->session->userdata('username');
		$sess_user_id = $sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=2))
		{
		 redirect("dashboard");
		}
	}
	/**
	 * Main Loading functions
	 * @author Gurpreet 
	 */
	public function index()
	{
		$drivers = $this->admin->getActiveDrivers();
		if(isset($_GET['did']) && $_GET['did']!=""){
			$getDriverById = $this->admin->get_driver($_GET['did']); 
			$this->data['driverById'] = $getDriverById;
			}
		$this->data['drivers'] = $drivers;
		$this->template->page('admin/driver/driver_availability',$this->data);
	}
	/**
	 * Data Post Via Ajax
	 * @author Gurpreet 
	 */
	public  function ajaxPostAvailability(){
	
		$data = array(
	          'driver_id'=>$this->input->post('adriver_id'),
			  'driver_available_from'=>$this->input->post('time_range_from'),
			  'driver_available_to'=>$this->input->post('time_range_to'),
			  'status'=>$this->input->post('available')
	        );
	$getDriverById = $this->admin->addDriverAvailabilty($data); 
	
	}
	
	/**
	 * Calendar output responce
	 * @author Gurpreet 
	 */
	public function getCalResponce(){
		$tempArray = array();
		$getTime = $this->input->get();
		$driver_id = $getTime['driver_id'];
		$start = date('Y-m-d H:m:s',$getTime['start']); 
		$end = date('Y-m-d H:m:s',$getTime['end']); 
		$getValues = $this->admin->getCalendarValues($start,$end,$driver_id);
		//Thu Oct 03 2013 10:30:00
		if(count($getValues)>0):
			foreach($getValues as $calValues):
				$driver = $this->admin->get_driver($calValues->driver_id);
				$tempArray['id'] = $calValues->id;
				$tempArray['title'] = "";
				$tempArray['start'] = date('D M d Y H:i:s',strtotime($calValues->driver_available_from));
				$tempArray['end'] = date('D M d Y H:i:s',strtotime($calValues->driver_available_to));
				$tempArray['allDay'] = false;
				if($calValues->status==0) {	$tempArray['className'] = 'unAvailable'; }
				else{ $tempArray['className'] = 'available'; }
				$calArray[] =  $tempArray;
			endforeach;
		endif;
		if(!empty($calArray)) echo json_encode($calArray);	
		else
		return NULL;
	}
	/**
	 * Calendar Availabilty Status Ajax
	 * @author Gurpreet 
	 */
	function ajaxGetAvailability(){
		$getValues = $this->admin->getAvailabiltydetailsById($_POST['eid']);
		echo $getValues;
	}
	
	function ajaxEditAvailability(){
		$data = array(
	          'driver_id'=>$this->input->post('adriver_id'),
			  'driver_available_from'=>$this->input->post('edit_time_range_from'),
			  'driver_available_to'=>$this->input->post('edit_time_range_to'),
			  'status'=>$this->input->post('edit_available')
	        );
	$getDriverById = $this->admin->updateAvailabilty($data,$this->input->post('availablity_id')); 
	
	}
		
		

	
public function driverid(){ 
		echo "<pre>"; print_r($this->input->get());
	}	
	
	

}
