<?php
$username=$current_user->user_login ; 
if($username=='admin'){
$user = get_userdatabylogin($username);
$sess_name=1;
}
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	/*  ADMIN HOME */
	function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('admin_model','admin');
		$this->load->model('dashboard_model');
		$this->load->library('session');
		$this->load->library('Pagination');
		$this->load->library('phpmailer');
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i ") . ' GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$sess_name = $this->session->userdata('username');
		$notification =  $this->admin->count_notifications();
		$this->data['notification'] = $notification;
		
	}
	
	public function checkAdmin(){

		if ( is_user_logged_in() ) {
			$role = $this->admin->get_role('1');
		}
		else{

				$sess_name = $this->session->userdata('username');
		//check admin is in session or not
		$sess_user_id = $sess_name['id'];
		$role = $this->admin->get_role($sess_user_id);
		}
		if(($role!=2 ))
		{
		 redirect("dashboard");
		} }
   
   /* 
    *Function Main Function 
	*  get latest driver register,user and latest reservation
	*/
	public function index(){
		$sess_name = $this->session->userdata('username');
		$users = $this->admin->getLatestUsers(); 
		$drivers = $this->admin->getLatestDrivers();
		$reservation = $this->admin->getLatestreservations(); // echo "<pre>"; print_r($reservation); die;
	
	
		$this->data['users'] = $users;
		$this->data['drivers'] = $drivers;
		$this->data['reservation'] = $reservation;
		
		$sess_user_id = $sess_name['id'];
		$role = $this->admin->get_role($sess_user_id);
		
		if( is_user_logged_in() ) {
		$sess_name=1;	
		}
		if(empty($sess_name)){
			$this->load->view('admin/login');
			
			
		}
 else if( is_user_logged_in() ) {
            $this->automate_login();
		}
		else {	
			$this->checkAdmin();
			$this->session->userdata('userId');
			$this->data['session'] = $sess_name;
			$this->template->page('admin/home',$this->data);
			
		}
		

		
	}

	
	/* Function LOGIN CHECK
	*  checking for valid login name and password. 
	*/
	public function login_check(){
		$username = $this->input->post('ad_name');
		$password = $this->input->post('ad_pass');		
		$this->form_validation->set_rules('ad_name', 'Username', 'trim|required|xss_clean|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('ad_pass', 'Password', 'trim|required|min_length[6]|max_length[20]');
		if ($this->form_validation->run()!=false)
		{
		$data = $this->admin->user_check($username,md5($password),2);
			if(!is_array($data)){
				echo 0;			 
			}else{
				//$this->session->set_userdata('username',$data['username']);
				$sessdata = array(
                   'username'  => $data['username'],
                   'id'     => $data['id']
				  
                   
               );
			   	$this->session->set_userdata('username',$sessdata);
				$sess_name = $this->session->userdata('username');
				if($sess_name){
					echo 'ok';
				}
			}
		}else{
			echo validation_errors();
		}		
	}


public function automate_login(){

		global $user;
		$sessdata = array(
		'username'  => $user->user_login,
		'id'     => $user->id
		);
		$this->session->set_userdata('username',$sessdata);
		$sess_name = $this->session->userdata('username');
			$this->checkAdmin();
			$this->session->userdata('userId');
			$this->data['session'] = $sess_name;
			$this->template->page('admin/home',$this->data);				
	}


	/* Function Users
	*  Getting all list of users.
	*  
	*/
	public function alluser(){
	
		$this->checkAdmin();
		
		$sess_name = $this->session->userdata('username');
		
		//check session
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		
		$this->session->unset_userdata('searchterm');
		
		$count = $this->admin->count_all_user();
		
		$config['base_url'] = base_url().'admin/home/alluser';
		$config['total_rows'] = $count;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['users'] = $this->admin->get_all_user($config['per_page'], $page);
		
		$result = 	$data['users'];
		$this->data['user'] = $result;
		$this->template->page('admin/alluser',$this->data);
		
	}
	
	/* Function ADD NEW user
	*  Adding new user.
	*  
	*/
	public function add_user(){
	
		$this->checkAdmin();
		
		
		$sess_name = $this->session->userdata('username');
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		$controler=$this->uri->segment(4);
		$index=$this->uri->segment(5);
		//$controler=$this->uri->segment(2);
		
		if($this->session->flashdata('errmsg')==''&& $this->session->flashdata('errmsg')==null){
			$this->session->unset_userdata('basic_data');
			$this->session->unset_userdata('billing_data');
			$this->session->unset_userdata('credit_card');
			$this->session->unset_userdata('exp_date');
			$this->session->unset_userdata('address_title');
			$this->session->unset_userdata('userstatus');
		}
		
		//$index=$this->uri->segment(3);
		If(isset($controler)&& isset($index)){
	
			$pre_url=base_url().'admin/'.$controler."/".$index;
		}
		
		$this->form_validation->set_rules('first_name', 'First Name', 'htmlspecialchars|required|max_length[20]|min_length[3]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'htmlspecialchars|required|max_length[20]|min_length[3]');
		$this->form_validation->set_rules('username', 'User Name', 'htmlspecialchars|required|is_unique[user.username]|max_length[30]|min_length[3]');
		$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'htmlspecialchars|required|min_length[6]|max_length[20]');

		//$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email|is_unique[user.email]');
		
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'htmlspecialchars|max_length[100]|min_length[3]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'htmlspecialchars|required|max_length[12]|min_length[8]|is_unique[user.mobile_number]');
		$this->form_validation->set_rules('other_number', 'Other Number', 'htmlspecialchars|max_length[12]|min_length[8]|is_unique[user.other_number]');		
		$this->form_validation->set_rules('address_title', 'Address Title', 'required');
		$this->form_validation->set_rules('address_1', 'Address Line 1', 'required|max_length[255]|min_length[3]');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]|min_length[3]');
		$this->form_validation->set_rules('city', 'City', 'required|max_length[30]');
		$this->form_validation->set_rules('state', 'State', 'required|max_length[30]|min_length[2]');		
	//	$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');		
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('billing_address_status', 'Billing address', 'required');
		//billing address details
		$this->form_validation->set_rules('address_1b', 'Address Line 1', 'max_length[255]|min_length[3]|required');
		$this->form_validation->set_rules('address_2b', 'Address Line 2', 'max_length[255]|min_length[3]');
		$this->form_validation->set_rules('city_b', 'City', 'required|max_length[30]');
		$this->form_validation->set_rules('state_b', 'State', 'required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip_code_b', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');
		//$this->form_validation->set_rules('pay_rate_perhour', 'Pay Rate Per Hour', 'required|numeric|max_length[10]');
		//$this->form_validation->set_rules('pay_rate_permile', 'Pay Rate Per Mile', 'required|numeric|max_length[10]');
		//$this->form_validation->set_rules('creditcard_number', 'Creditcard Number', 'required|callback_validate_credit_card_number');
		$this->form_validation->set_rules('creditcard_number', 'Creditcard Number', 'required|callback_validate_credit_card_number');
		$this->form_validation->set_rules('exp_month', 'Exp month', 'required');
		$this->form_validation->set_rules('exp_year', 'Exp year', 'required');
		
		$this->session->set_userdata('address_title',$this->input->post('address_title'));
		//$this->session->set_userdata('country',$this->input->post('country')); 
		$this->session->set_userdata('userstatus',$this->input->post('status'));
		
		if($this->form_validation->run() != false){
		
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$realpassword = $this->input->post('password');
			$password = md5($this->input->post('password'));
			$role = 1;
			$company_name = $this->input->post('company_name');
			$mobile_number =$this->input->post('mobile_number');
			$other_number = $this->input->post('other_number');	
			$address_title = $this->input->post('address_title');			
			$address_1 = $this->input->post('address_1');
			$address_2 = $this->input->post('address_2');
			$city = $this->input->post('city'); 		
			$state = $this->input->post('state');
			//$country = $this->input->post('country');
			$zip_code = $this->input->post('zip_code');
			//$pay_rate_perhour=$this->input->post('pay_rate_perhour');
			//$pay_rate_permile=$this->input->post('pay_rate_permile');
			$status = $this->input->post('status');
			$billing_address_status = $this->input->post('billing_address_status');
			$exp_date=$this->input->post('exp_year').'-'.$this->input->post('exp_month');
			//billing info 
				$address_1b  = $this->input->post('address_1b');	
				$address_2b  = $this->input->post('address_2b');	
				$city_b  = $this->input->post('city_b');	
				$state_b  = $this->input->post('state_b');	
				$zip_code_b  = $this->input->post('zip_code_b');
				$creditcard_number = str_replace('-','',$this->input->post('creditcard_number'));
				$data =  array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'username' => $username,
					'email' => $email,
					'role' => $role,
					'password' => md5($password),
					'company_name' => $company_name,
					'mobile_number' => $mobile_number,
					'other_number' => $other_number,
					'address_title' => $address_title,	
					'address_1' => $address_1,
					'address_2' => $address_2,				
					'city' => $city,
					'state' => $state,
					//'country' => $country,
					'zip_code' => $zip_code,
					//'pay_rate_perhour'=> $pay_rate_perhour,
					//'pay_rate_permile'=> $pay_rate_permile,
					'status' => $status,
					
				);
				
				$this->session->unset_userdata('address_title');
				//$this->session->unset_userdata('country');
				$this->session->unset_userdata('userstatus');
				$this->session->set_userdata('data',$data);
				$last_id = $this->admin->add_user($data);
	
				//billing info in 
				$data_b =  array(
						'user_id' => $last_id,
						'address_1' => $address_1b,
						'address_2' => $address_2b,
						'city' => $city_b,
						'state' => $state_b,
						'zip_code' => $zip_code_b
				
					
				);
				$this->dashboard_model->insert_billingdetail($data_b);
				if(!empty($data_b)){
					$this->create_customer_profile($data,$creditcard_number,$exp_date,$last_id,$data_b);
				}
				if($last_id > 0){
				
				//send mail to the registered user
				$link=site_url();
				$html  =  @file_get_contents(base_url().'emailtemplates/email.html');
				$find = array('{name}','{username}','{password}','{link}');
				$replace   = array($first_name,$username,$realpassword,$link );
				$messegefinal = str_replace($find,$replace,$html);
				$mail = $this->phpmailer;
				$mail->WordWrap = 100;
				$mail->SetFrom('admin@admin.com', 'ORES');
				$mail->Subject    = 'Registration mail';
				$mail->addAttachment("emailtemplates/logo.png");
				$mail->addAttachment("emailtemplates/forward-glyph.png");
				$mail->MsgHTML($messegefinal);	
				$mail->AddAddress($email);
				$mail->Send();
				$this->session->set_flashdata('msg', ' user registered successfully ');
				if(!empty($controler) && $last_id>0){
					$this->session->set_flashdata('error', ' user registered successfully ');
					redirect($pre_url);
				}
				redirect('admin/home/alluser');
				}else{
					$this->data['wrong_insert'] = 'Your entered information is not added into database.';
					$this->template->page('admin/add_user',$this->data);
				}
				
		}else{			
			$this->template->page('admin/add_user');
		}
		
	}
		
	/* 
	*  Edit user according to user id
	*  
	*/
	public function edit_user($id = Null){
	
		$this->checkAdmin();
		
	if($id){
			$artist_id = $id; 		
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean|max_length[20]|min_length[3]');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean|max_length[20]|min_length[3]');
			$this->form_validation->set_rules('username', 'User Name', 'trim|required|xss_clean|max_length[30]|min_length[3]|callback_check_user_name['.$id.']');
			$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email');
			$this->form_validation->set_rules('company_name', 'Company Name', 'max_length[100]|min_length[3]');
			$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|max_length[12]|min_length[8]|callback_check_user_mobile_no['.$id.']'); 
			$this->form_validation->set_rules('other_number', 'Other Number', 'max_length[12]|min_length[8]|callback_check_user_other_no['.$id.']'); 
			$this->form_validation->set_rules('address_1', 'Address 1', 'required|max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_2', 'Address 2', 'max_length[255]|min_length[3]');
			$this->form_validation->set_rules('address_title', 'Address Title', 'required');
			$this->form_validation->set_rules('city', 'City', 'required|max_length[30]');
			$this->form_validation->set_rules('state', 'State', 'required|max_length[30]|min_length[2]');
			//$this->form_validation->set_rules('country', 'Country', 'required');
			$this->form_validation->set_rules('zip_code', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]');
			//$this->form_validation->set_rules('pay_rate_perhour', 'Pay Rate Per Hour', 'required|numeric|max_length[10]');
			//$this->form_validation->set_rules('pay_rate_permile', 'Pay Rate Per Mile', 'required|numeric|max_length[10]');
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('billing_address_status', 'billing address status', 'required');
			//billing address details
		$this->form_validation->set_rules('address_1b', 'Address Line 1', 'max_length[255]|min_length[3]|required');
		$this->form_validation->set_rules('address_2', 'Address Line 2', 'max_length[255]|min_length[3]');
		$this->form_validation->set_rules('city_b', 'City', 'required|max_length[30]');
		$this->form_validation->set_rules('state_b', 'State', 'required|max_length[255]|min_length[2]');
		$this->form_validation->set_rules('zip_code_b', 'Zip Code', 'required|numeric|max_length[10]|min_length[3]'); 
		//$this->form_validation->set_rules('creditcard_number', 'Creditcard Number', 'required');
		if($this->input->post('creditcard_number')!=''){
			$this->form_validation->set_rules('creditcard_number','Credit Card Number','callback_validate_credit_card_number');
			$this->form_validation->set_rules('exp_month', 'Exp month', 'required');
			$this->form_validation->set_rules('exp_year', 'Exp year', 'required');
			}
		//$this->form_validation->set_rules('exp_month', 'Exp month', 'required');
		
		
			$this->data['user'] = $this->admin->get_user($id);
			$this->data['billingdetail'] = $this->dashboard_model->get_billingdetail($id);
			
		//print_r($this->data['billingdetail']->id); die; 
			if ($this->input->server('REQUEST_METHOD') === 'POST')
			{
				$this->session->unset_userdata('user_record');
				$this->session->unset_userdata('billing_detail');
				$this->session->set_userdata('address_title',$this->input->post('address_title'));
				//$this->session->set_userdata('country',$this->input->post('country'));
				$this->session->set_userdata('userstatus',$this->input->post('status'));
			}
			else {
				$this->session->set_userdata('user_record', $this->data['user']);
				$this->session->set_userdata('billing_detail', $this->data['billingdetail']);
			}
		
		
		
			if($this->form_validation->run() != false){
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$username = $this->input->post('username');
				$email = $this->input->post('email');
				$role = 1;
				$billing_address_status	= $this->input->post('billing_address_status');			
				$company_name  = $this->input->post('company_name');
				$mobile_number =$this->input->post('mobile_number');
				$other_number = $this->input->post('other_number');		
				$address_title  = $this->input->post('address_title');	
				$address_1  = $this->input->post('address_1');	
				$address_2  = $this->input->post('address_2');	
				$city  = $this->input->post('city');	
				$state  = $this->input->post('state');	
				//$country  = $this->input->post('country');	
				$zip_code  = $this->input->post('zip_code');
				//$pay_rate_perhour = $this->input->post('pay_rate_perhour');
				//$pay_rate_permile = $this->input->post('pay_rate_permile');				
				$status  = $this->input->post('status');	
				//billing info 
				$address_1b  = $this->input->post('address_1b');	
				$address_2b  = $this->input->post('address_2b');	
				$city_b  = $this->input->post('city_b');	
				$state_b  = $this->input->post('state_b');	
				$zip_code_b  = $this->input->post('zip_code_b');
				$exp_date=$this->input->post('exp_year').'-'.$this->input->post('exp_month');
				$creditcard_number = str_replace('-','',$this->input->post('creditcard_number'));
				//$current_date = date('Y-m-d h:i:s a', time());
				if($this->input->post('password')!=""){
				$updatedpassword = $this->input->post('password');
				
				//send mail to the registered user if password get updated
				$link=site_url();
				$html  =  @file_get_contents(base_url().'emailtemplates/reset_password_email.html');
				$find = array('{name}','{link}','{password}');
				$replace   = array($first_name,$link,$updatedpassword );
				$messegefinal = str_replace($find,$replace,$html);
				$mail = $this->phpmailer;
				$mail->WordWrap = 100;
				$mail->SetFrom('admin@admin.com', 'ORES');
				$mail->Subject    = 'Account Update';
				$mail->addAttachment("emailtemplates/logo.png");
				$mail->addAttachment("emailtemplates/forward-glyph.png");
				$mail->MsgHTML($messegefinal);
				$mail->AddAddress($email);
				$mail->Send();
			
				$password	= md5($this->input->post('password'));	
				}else{
				 $password =$this->data['user']->password;
				}
				if(empty($this->data['error'])){			
			
					$data =  array(
						'first_name' => $first_name,
						'last_name' => $last_name,
						'username' => $username,
						'email' => $email,
						'role' => $role,
						'password' => $password,
						'company_name' => $company_name,
						'mobile_number' => $mobile_number, 
						'other_number' => $other_number,
						'address_title' => $address_title,
						'address_1' => $address_1,
						'address_2' => $address_2,
						'city' => $city,
						'state' => $state,
						//'country' => $country,
						'zip_code' => $zip_code,
						//'pay_rate_perhour'=> $pay_rate_perhour,
						//'pay_rate_permile'=> $pay_rate_permile,
						'billing_address_status' => $billing_address_status,
						'status' => $status
					);
					
					//billing address
				$data_b =  array(
						'user_id' =>$id,
						'address_1' => $address_1b,
						'address_2' => $address_2b,
						'city' => $city_b,
						'state' => $state_b,
						'zip_code' => $zip_code_b
						
					);
					$profile=$this->dashboard_model->get_autorizednet_profileId($id);
					if($profile!=false && $creditcard_number!='' && $creditcard_number!=null){
						if($profile->customer_payment_profile_id!=-1 && $profile->customer_payment_profile_id!=0){
							$this->update_customer_profile($data,$data_b,$profile,$creditcard_number,$exp_date,$id);
						}
					}
					
					$get_true_false = $this->admin->update_user($data,$id);
					if($get_true_false > 0 ){
						$this->session->set_flashdata('msg', ' user updated successfully ');
						redirect('admin/home/alluser');
					}else{
						$this->data['wrong_update'] = 'Your entered information is not correct, please try again.';
						$this->template->page('admin/edit_user',$this->data);
					}
				}	
			}else{	
				
				$this->template->page('admin/edit_user',$this->data);
			}
		}else{
			redirect('admin/home/alluser');
		}		

	
	}
	function check_user_name($username,$id){
	
		$this->checkAdmin();
		
		
		if(!empty($username)){
			$result = $this->admin->check_user_name($username,$id);
			if($result==0){
				
				$this->form_validation->set_message('check_user_name', 'This user name is already exist.');
				return false;
			}else{
				return true;
			}	
		}
	}
	function check_user_email($email,$id){
	
		$this->checkAdmin();
			
		
		if(!empty($email)){
			$result = $this->admin->check_user_email($email,$id);
			if($result==0){
				
				$this->form_validation->set_message('check_user_email', 'This email already exist.');
				return false;
			}else{
				return true;
			}	
		}
	}
	
	function check_user_mobile_no($mobile_no,$id){
		if(!empty($mobile_no)){
			$result = $this->admin->check_user_mobile_no($mobile_no,$id);
			if($result==0){
		
				$this->form_validation->set_message('check_user_mobile_no', 'This mobile number already exist.');
				return false;
			}else{
				return true;
			}
		}
		
	}
	function check_user_other_no($mobile_no,$id){
		if(!empty($mobile_no)){
			$result = $this->admin->check_user_other_no($mobile_no,$id);
			if($result==0){
	
				$this->form_validation->set_message('check_user_other_no', 'This mobile number already exist.');
				return false;
			}else{
				return true;
			}
		}
	
	}
	public function remove_user(){	
	
		$this->checkAdmin();
		
		$id= $this->input->post('id');
	if(!empty($id)){
					$this->admin->delete_user($id);
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
		
	}
	public function change_password(){
	
		$this->checkAdmin();
		
		$data['session'] = $this->session->all_userdata();	
		//echo "<pre>"; print_r($data['session']); die;
		if(isset($data['session']['username'])){
			$this->form_validation->set_rules('oldpassword', 'Old Password', 'required|min_length[6]|max_length[20]');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[repassword]|min_length[6]|max_length[20]');
			$this->form_validation->set_rules('repassword', 'Re-Enter Password', 'required|min_length[6]|max_length[20]');		
			if ($this->form_validation->run() == FALSE){
					$this->template->page('admin/change_password');
			}else{						
				$session_name = $data['session']['username'];
				$oldpass = $this->input->post('oldpassword');
				$result = $this->admin->password_check(md5($oldpass),$session_name);							
				if($result){
						$password = md5($this->input->post('password'));
						$this->admin->update_password($session_name,$password);									
						$this->template->page('admin/password_changed');						
				}else{							
					$pass['wrong']="Invalid Old Password"; 
					$this->template->page('admin/change_password',$pass);
				}
			}					
		}
		else{
			redirect('admin');
		}
	}
	
	/* Function LOGOUT
	*  Successfully destroy session and redirect to admin home.
	*  
	*/
	public function signout(){
		wp_logout();
		$sess_name = $this->session->userdata('username');
		$this->session->unset_userdata($sess_name);
		$this->session->sess_destroy($sess_name);
		redirect('/');
	}
	
	/* 
	 *  This function is used to return all user
	 *  searched based on first_name,last_name, email address.
	 *  Author: Devi Lal Verma
	*/
	public function search_user(){
	
		$this->checkAdmin();
		
		$sess_name = $this->session->userdata('username');
		if(!empty($sess_name)){
			$this->data['session'] = $sess_name;
		}
		$searchterm = $this->admin->searchterm_handler(trim($this->input->get_post('searchterm', TRUE)));
		$limit = ($this->uri->segment(4) > 0)?$this->uri->segment(4):0;
		$config['base_url'] = base_url().'admin/home/search_user';
		$config['total_rows'] = $this->admin->count_search_user($searchterm);
		$config['per_page'] = 4;
		$config['uri_segment'] = 4;
		$choice = $config['total_rows']/$config['per_page'];
		$config['num_links'] = round($choice);
		$this->pagination->initialize($config);
		$data['users'] = $this->admin->search_user($searchterm,$config['per_page'], $limit);	
		$result = 	$data['users'];
		$this->data['user'] = $result;
		$data['links'] = $this->pagination->create_links();
		$data['searchterm'] = $searchterm;
		$this->template->page('admin/search_user',$this->data);
	

	
	


}		/* Function change Profile
	*  Author:Gurpreet
	*  
	*/
	public function profile(){
	
		$this->checkAdmin();
		
		
		$this->template->page('admin/profile');
	
	}
	
	public function get_username(){
	
		$this->checkAdmin();
		
		$mobile_number=$this->input->get_post('mobileno');
		
		if(!empty($mobile_number)){
			$result=$this->admin->get_username($mobile_number);
			echo json_encode($result);
		}
		else{
			echo json_encode(false);
		}
		
	}
	function doforget(){
		$email = $this->input->post('email');
		$result=$this->admin->check_forgot_email($email,2);
		if($email=='' || $email==null){
			$error= "Enter your email";
			echo $error;
		}
		else if ($result==true){
			$this->resetpassword($email);
			echo "true";
		}
		else{
			$error= "The email id you entered not found on our database ";
			echo $error;
		}
		
   
	}
	private function resetpassword($email){
		date_default_timezone_set('GMT');
		$this->load->helper('string');
		$password= random_string('alnum', 16);
		$this->admin->reset_password($password,$email);
		$this->load->library('email');
		
		$this->email->from('admin@domain.com', 'Admin');
		$this->email->to($email);
		$this->email->subject('Password reset');
		$this->email->message('You have requested the new password, Here is you new password:'. $password);
		$this->email->send();
	}
	function check_email($email) {
		$result=$this->admin->check_forgot_email($email);
		if ($result == false) {
			$this->form_validation->set_message('Email doesnot exits in database');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function validate_credit_card_number( $string ) {
		if ( preg_match( '/^([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{4})[-|s]*([0-9]{2,4})$/', $string ) ) {
			return TRUE;
		} else {
			$this->form_validation->set_message('validate_credit_card_number','Invalid credit card number');
			return FALSE;
		}
	}
	
	public function create_customer_profile($userInfo,$cardno,$expdate,$last_id,$b_data){
		//$this->checkAdmin();
		$this->load->library('authorizecimlib');
		$this->authorizecimlib->initialize('36Gd5M6rz','6nLVxa495X9P7qt8');
		$this->authorizecimlib->set_validationmode('liveMode');
		$this->authorizecimlib->set_data('email',$userInfo['email']);
		$this->authorizecimlib->set_data('description','Monthly Membership No. ' . md5(uniqid(rand(), true)));
		$this->authorizecimlib->set_data('merchantCustomerId',1341179);
		// echo '<h1>Creating Customer Profile - create_customer_profile()</h1>';
	
		if(!$this->data['profileid'] = $this->authorizecimlib->create_customer_profile())
		{
			//echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
			//die();
			$this->admin->delete_user($last_id);
			/*$this->session->set_userdata('basic_data',$userInfo);
			$this->session->set_userdata('billing_data',$b_data);
			$this->session->set_userdata('credit_card',$cardno);
			$this->session->set_userdata('exp_date',$expdate);*/
			//$this->session->set_userdata('address_title',$userInfo['address_title']);
			//$this->session->set_userdata('userstatus',$userInfo['status']);
			$this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
			redirect(current_url());
		}
	
		//  echo '<p> Customer Id: ' . $this->data['profileid'] . '</p>';
	
	
		// Create the Payment Profile
		$this->authorizecimlib->set_data('customerProfileId', $this->data['profileid']);
		$this->authorizecimlib->set_data('billToFirstName', $userInfo['first_name']);
		$this->authorizecimlib->set_data('billToLastName',$userInfo['last_name']);
		$this->authorizecimlib->set_data('billToAddress', $b_data['address_1']);
		$this->authorizecimlib->set_data('billToCity', $userInfo['city']);
		$this->authorizecimlib->set_data('billToState', $userInfo['state']);
		$this->authorizecimlib->set_data('billToZip', $userInfo['zip_code']);
		// $this->authorizecimlib->set_data('billToCountry', $userInfo['country']);
		$this->authorizecimlib->set_data('billToPhoneNumber', $userInfo['mobile_number']);
		//$this->authorizecimlib->set_data('billToFaxNumber', '800-555-2345');
		// $this->authorizecimlib->set_data('cardNumber', '6111111111111111'); // will produce a decline
		$this->authorizecimlib->set_data('cardNumber', $cardno);
		$this->authorizecimlib->set_data('expirationDate', $expdate);
	
		// echo '<h1>Creating Payment Profile - create_customer_payment_profile()</h1>';
	
		if(! $this->data['paymentprofileid'] = $this->authorizecimlib->create_customer_payment_profile())
		{
			//echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
			//die();
			$this->admin->delete_user($last_id);
			/*$this->session->set_userdata('basic_data',$userInfo);
			$this->session->set_userdata('billing_data',$b_data);
			$this->session->set_userdata('credit_card',$cardno);
			$this->session->set_userdata('exp_date',$expdate);*/
			$this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
			redirect(current_url());
		}
	
	
		//  echo '<p> Payment Profile Id: ' . $this->data['paymentprofileid'] . '</p>';
		// Create the shipping profile
		$this->authorizecimlib->set_data('customerProfileId', $this->data['profileid']);
		$this->authorizecimlib->set_data('shipToFirstName', $userInfo['first_name']);
		$this->authorizecimlib->set_data('shipToLastName', $userInfo['last_name']);
		$this->authorizecimlib->set_data('shipToAddress', $b_data['address_1']);
		$this->authorizecimlib->set_data('shipToCity', $userInfo['city']);
		$this->authorizecimlib->set_data('shipToState', $userInfo['state']);
		$this->authorizecimlib->set_data('shipToZip', $userInfo['zip_code']);
		//$this->authorizecimlib->set_data('shipToCountry',$userInfo['country']);
		$this->authorizecimlib->set_data('shipToPhoneNumber', $userInfo['mobile_number']);
		// $this->authorizecimlib->set_data('shipToFaxNumber', '800-555-4567');
	
		//echo '<h1>Creating Shipping Profile - create_customer_shipping_profile()</h1>';
	
		if(! $this->data['shippingprofileid'] = $this->authorizecimlib->create_customer_shipping_profile())
		{
			//echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
			//die();
			$this->admin->delete_user($last_id);
			/*$this->session->set_userdata('basic_data',$userInfo);
			$this->session->set_userdata('billing_data',$b_data);
			$this->session->set_userdata('credit_card',$cardno);
			$this->session->set_userdata('exp_date',$expdate);*/
			$this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
			redirect(current_url());
		}
	
		//  echo '<p> Shipping Profile Id: ' . $this->data['shippingprofileid'] . '</p>';
	
		
		$profile_data=array('customer_profile_id'=>$this->data['profileid'], 'customer_payment_profile_id'=>$this->data['paymentprofileid'],'customer_shipping_address_id'=>$this->data['shippingprofileid']);
		$this->dashboard_model->update_billingdetail($profile_data,$last_id);
		$this->authorizecimlib->clear_data();
		
	
	}
	/* This function is creted for update customer profile
	 * on authorized.net.
	*  Author:Devi Lal Verma
	*/
	public function update_customer_profile($userInfo,$b_data,$profile,$cardno,$expdate,$id){
		$this->load->library('authorizecimlib');
		$this->authorizecimlib->initialize('36Gd5M6rz','6nLVxa495X9P7qt8');
		$this->authorizecimlib->set_validationmode('liveMode');
		$this->authorizecimlib->set_data('email',$userInfo['email']);
		//$this->authorizecimlib->set_data('description','Monthly Membership No. ' . md5(uniqid(rand(), true)));
		$this->authorizecimlib->set_data('merchantCustomerId',1341179);
		// echo '<h1>Creating Customer Profile - create_customer_profile()</h1>';
	
		if(! $this->authorizecimlib->update_customer_profile($profile->customer_profile_id))
		{
			//echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
			//die();
			$this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
			//die();
			redirect(current_url());
		}
	
		//  echo '<p> Customer Id: ' . $this->data['profileid'] . '</p>';
	
		// update the Payment Profile
		$this->authorizecimlib->set_data('customerProfileId', $profile->customer_profile_id);
		$this->authorizecimlib->set_data('billToFirstName', $userInfo['first_name']);
		$this->authorizecimlib->set_data('billToLastName',$userInfo['last_name']);
		$this->authorizecimlib->set_data('billToAddress', $b_data['address_1']);
		$this->authorizecimlib->set_data('billToCity', $userInfo['city']);
		$this->authorizecimlib->set_data('billToState', $userInfo['state']);
		$this->authorizecimlib->set_data('billToZip', $userInfo['zip_code']);
		$this->authorizecimlib->set_data('billToCountry', $userInfo['country']);
		$this->authorizecimlib->set_data('billToPhoneNumber', $userInfo['mobile_number']);
		//$this->authorizecimlib->set_data('billToFaxNumber', '800-555-2345');
		$this->authorizecimlib->set_data('cardNumber', $cardno);
	
		// will produce a decline
		//$this->authorizecimlib->set_data('cardNumber', $cardno);
		//$this->authorizecimlib->set_data('expirationDate', $expdate);
		$this->authorizecimlib->set_data('expirationDate', $expdate);
		// echo '<h1>Creating Payment Profile - create_customer_payment_profile()</h1>';
	
		if(! $this->authorizecimlib->update_customer_payment_profile( $profile->customer_profile_id,$profile->customer_payment_profile_id))
		{
			// echo '<p> Error: ' . $this->authorizecimlib->get_error_msg() . '</p>';
			$this->session->set_flashdata('errmsg', $this->authorizecimlib->get_error_msg());
			//die();
			redirect(current_url());
		}
	
		$this->dashboard_model->update_billingdetail($b_data,$id);
		$this->authorizecimlib->clear_data();
	}
}
	
