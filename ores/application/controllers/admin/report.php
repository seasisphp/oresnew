<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('driver_model','driver');
		$this->load->model('admin_model');
		$this->load->library('session');
		$this->load->library('Pagination');
		$this->load->helper('file_helper');
		$this->load->helper('download_helper');
		$notification =  $this->admin_model->count_notifications();
		//$newreservationcount ;
		$this->data['notification'] = $notification;
		$sess_name = $this->session->userdata('username');
		
		$sess_user_id = $sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=2))
		{
		 redirect("dashboard");
		}
	}
	/*
	 * This function create reportdriver
	 * Author:Devi Lal Verma
	 */
	public function driver_report(){
		
	}
}