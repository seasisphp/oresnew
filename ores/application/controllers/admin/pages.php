<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Pages extends CI_Controller 
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->model('pages_model');
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->library('email');
		}
	
		public function list_pages()
		{
				$data['session'] = $this->session->all_userdata();		
				if(isset($data['session']['username']))
					{					
						$page['list_pages'] = $this->pages_model->list_pages();						
						$this->template->page('admin/pages/list_pages',$page);				
					}
				else
				 {
						redirect('admin');
				 }
		}	
			
		public function edit_page(){
			$data['session'] = $this->session->all_userdata();
			$id = $this->uri->segment(4);	
			$image_in_db ='';		
			$image_up='';			
			if(isset($data['session']['username'])){										
				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('page_description', 'Description', 'required');
				$result['data'] = $this->pages_model->get_page_content($id);	
				if ($this->form_validation->run() == FALSE){					
					
					$this->template->page('admin/pages/edit_page',$result);
				}else{
					$image_in_db = $result['data']['image'];
					//echo"<pre>";print_r($result['data']);
					$id = $this->uri->segment(4);	
					$result['data'] = $this->pages_model->get_page_content($id);
					$id = $this->uri->segment(4);	
					$result['data'] = $this->pages_model->get_page_content($id);
					
					if(empty($_FILES['image']['name'])){ 
						$image_up = $image_in_db;
					}else{ 
						/**  IMAGE UPLOAD  **/
						$config['upload_path'] = './uploads/page_images/';
						$config['allowed_types'] = 'png|jpg|jpeg';
						$config['max_size']	= '60000000';
						$config['max_width']  = '6024';
						$config['max_height']  = '2068';
					
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						if ( ! $this->upload->do_upload('image')){ 
							$this->data['error'] = $this->upload->display_errors();					
							$this->template->page('admin/pages/edit_page', $this->data);
						}else{ 
							$data = array('upload_data' => $this->upload->data()); 
							$image_up = $data['upload_data']['file_name'];						
						}	
					}if(empty($this->data['error'])){
						$success = "Product added successfully";
						$this->session->set_flashdata('message', $success); 
						$this->template->page('admin/pages/edit_page',$data);
						$page_data = array('id' => $id['id'],
							'title'=> $this->input->post('title'),
							'meta_description'=> $this->input->post('meta_description'),
							'meta_keyword'=> $this->input->post('meta_keyword'),
							'page_description'=> $this->input->post('page_description'),
							'meta_tags'=>$this->input->post('meta_tags'),
							'image' => $image_up
						);
						$this->pages_model->update_page($page_data);								
						$this->session->set_flashdata('updated', 'Page is updated successfully');					
						redirect('admin/pages/list_pages');	
					}		
				}
			}else{
				redirect('admin');
			}
		}
	
	
		
	
		
		function image($image_path,$new_image_path, $width = 0, $height = 0) 
		{
			$CI = & get_instance();
			   
			//The new generated filename we want
			$fileinfo = pathinfo($image_path);
			$new_image_path = $new_image_path . $fileinfo['filename'] . '_thumb'. '.' . $fileinfo['extension'];
			
			//Or the original image is newer than our cache image
			if ((! file_exists($new_image_path)) || filemtime($new_image_path) < filemtime($image_path)) 
			{
				$CI->load->library('image_lib');
			   
				//The original sizes
				$original_size = getimagesize($image_path);
				$original_width = $original_size[0];
				$original_height = $original_size[1];
				$ratio = $original_width / $original_height;
			   
				//The requested sizes
				$requested_width = $width;
				$requested_height = $height;
			  
				//Initialising
				$new_width = 0;
				$new_height = 0;
			   
				//Calculations
				if ($requested_width > $requested_height) 
				{
					$new_width = $requested_width;
					$new_height = $new_width / $ratio;
					if ($requested_height == 0)
						$requested_height = $new_height;
				   
					if ($new_height < $requested_height) 
					{
						$new_height = $requested_height;
						$new_width = $new_height * $ratio;
					}
			   
				}
				else 
					{
						$new_height = $requested_height;
						$new_width = $new_height * $ratio;
						if ($requested_width == 0)
						$requested_width = $new_width;
				   
						if ($new_width < $requested_width) 
						{
							$new_width = $requested_width;
							$new_height = $new_width / $ratio;
						}
					}
			   
					$new_width = ceil($new_width);
					$new_height = ceil($new_height);
			   
					//Resizing
					$config = array();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $image_path;
					$config['new_image'] = $new_image_path;
					$config['maintain_ratio'] = FALSE;
					$config['height'] = $new_height;
					$config['width'] = $new_width;
					$CI->image_lib->initialize($config);
					$CI->image_lib->resize();
					$CI->image_lib->clear();
			   
					//Crop if both width and height are not zero
					if (($width != 0) && ($height != 0)) 
					{
						$x_axis = floor(($new_width - $width) / 2);
						$y_axis = floor(($new_height - $height) / 2);
					   
						//Cropping
						$config = array();
						$config['source_image'] = $new_image_path;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $new_image_path;
						$config['width'] = $width;
						$config['height'] = $height;
						$config['x_axis'] = $x_axis;
						$config['y_axis'] = $y_axis;
						$CI->image_lib->initialize($config);
						$CI->image_lib->crop();
						$CI->image_lib->clear();
					}
			}
			
			return $new_image_path;
		}
		
	
		
	}

?>