<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dispatchlog extends CI_Controller {
	/*  dispatch log */
	function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('directory');
		$this->load->model('dlogs_model','admin');
		$this->load->library('phpmailer');
		$this->load->model('admin_model');
		$this->load->model('driver_model');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('file_helper');
		$this->load->helper('download_helper');
		$sess_name = $this->session->userdata('username');
		$notification =  $this->admin_model->count_notifications();
		//$newreservationcount ;
		$this->data['notification'] = $notification;
		$sess_name = $this->session->userdata('username');
		$sess_user_id = $sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=2))
		{
		 redirect("dashboard");
		}
	} 
	public function index(){
		$sess_name = $this->session->userdata('username');
		if(empty($sess_name)){
			$this->load->view('admin/login');
		}else{		
			$this->data['session'] = $sess_name;
			$this->template->page('admin/dispatchlog');
			
			
		}
	}
	
  function create_csv(){
  
  $this->load->dbutil();

$query = $this->db->query("SELECT reservation.trip_date_from, reservation.trip_date_to, reservation.pickup_time, reservation.address, reservation.state, reservation.city, reservation.zip, reservation.desination_1, reservation.desination_2, reservation.approx_hours, `user`.`first_name`
FROM (
`reservation`
)
JOIN `user` ON `user`.`id` = `reservation`.`user_id`
WHERE `reservation`.`status` !=3
AND `reservation`.`status` !=4");
 $csv_data = $this->dbutil->csv_from_result($query);

 $filename =  $filename = "dispatch" . date('Ymd') . ".csv";
 if (!write_file("./csv/".$filename, $csv_data)) {
	$csv_data = $this->dbutil->csv_from_result($query);
        echo 'Unable to write the file';
    } else {

        $data = file_get_contents("./csv/".$filename);
        force_download($filename, $data); 

    }
   } 
	/* Function dlogs
	*  Getting all list of reservations.
	*  
	*/
	public function dlogs(){
	 
		
		$count = $this->admin->count_all_reservations();
		
		$config['base_url'] = base_url().'admin/dispatchlog/dlogs';
		$config['total_rows'] = $count;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['dlogs'] = $this->admin->get_all_reservations($config['per_page'], $page);
		
			if(!empty($data['dlogs'])){
		$tempArray = array();

		foreach($data['dlogs'] as $dlogs){
			$city_drivers = $this->admin->getADriversByCity($dlogs->city,$dlogs->trip_date_from,$dlogs->trip_date_to);
			$tempArray[$dlogs->id] = $city_drivers;
		}
		//var_dump($tempArray);
		 $tempArray1 = array();
		
		foreach($data['dlogs'] as $dlogs){
		
			$city_drivers = $this->admin->getNADriversByCity($dlogs->city,$dlogs->trip_date_from,$dlogs->trip_date_to);
			$tempArray1[$dlogs->id] = $city_drivers;
			
		} 
		//var_dump($tempArray1);
		$tempArray2 = array();
		
		foreach($data['dlogs'] as $dlogs){
			
		$noncity_drivers = $this->admin->getAnonCityDrivers($dlogs->city,$dlogs->trip_date_from,$dlogs->trip_date_to);
		$tempArray2[$dlogs->id] = $noncity_drivers;
		} 
		//var_dump($tempArray2);
		$tempArray3 = array();
			
		foreach($data['dlogs'] as $dlogs){
		$noncity_drivers = $this->admin->getNAnonCityDrivers($dlogs->city,$dlogs->trip_date_from,$dlogs->trip_date_to);
		$tempArray3[$dlogs->id] = $noncity_drivers;
		} 
		//var_dump($tempArray3);
		
		$this->data['AdriverByCity'] = $tempArray;
		
		$this->data['NACityDrivers'] = $tempArray1;
		
		$this->data['AnonCityDrivers'] = $tempArray2;
		
		$this->data['NAnonCityDrivers'] = $tempArray3;
	
		}
		$result = 	$data['dlogs'];
		$this->data['dlog'] = $result;
		
		$this->template->page('admin/dispatchlog',$this->data);

	}
	/* Function driverpostAjax
	*  post driver id to assign driver
	*  
	*/ 
		public function driverpostAjax(){		
			$driver_id = $_POST['driver_id'];
			$id = $_POST['id'];
						$data =  array(
						'driver_id' => $driver_id,
						'status' => 1,
						);
					 $this->admin->assign_driver($data,$id);
					//send mail to driver for his approval 
					$driversch = $this->admin->getDriversTripDetail($id,$driver_id);
					$pickup_date = $driversch->trip_date_from;
					$pickup_time=date("g:ia", strtotime($driversch->pickup_time));
					$address=$driversch->address;
					$city=$driversch->city;
					$state=$driversch->state;
					$zip=$driversch->zip;
					$desination_1 = $driversch->desination_1;
					$desination_2 = $driversch->desination_2;
					if(!empty($driversch->airport)){
					$desination_1 = $driversch->airport;
					}
					$user_id=$this->admin_model->get_user_id($id);
					$usersrh=$this->admin_model->get_user($user_id[0]->user_id);
					// check notification then update.	
					$result = 	$this->admin_model->notification_check($id);
					
					if($result == 1){
					
						$this->admin_model->notification_update($id);
					}
					$date1 =  explode(" ",$pickup_date) ;
					$pickup_date= date ("m/d/Y",strtotime($date1[0]));
					//send mail to driver that trip has been assigned .
					$html  =  @file_get_contents(base_url().'emailtemplates/driver_email.html');
					$link = site_url();
					$find = array('{name}','{date}','{time}','{address}','{city}','{state}','{zip}','{desination_1}','{desination_2}','{link}');
					$replace   = array($driversch->first_name,$pickup_date,$pickup_time,$address,$city,$state,$zip,$desination_1,$desination_2,$link);
					$messegefinal = str_replace($find,$replace,$html);
					$mail = $this->phpmailer;
					$mail->WordWrap = 100;
					$mail->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
					$mail->Subject = 'Driver Confirmation';
					$mail->addAttachment("emailtemplates/logo.png");
					$mail->addAttachment("emailtemplates/forward-glyph.png");
					$mail->MsgHTML($messegefinal);
					$mail->AddAddress($driversch->email);
					$mail->Send();
					unset($driversch->email);
					$mail->ClearAddresses();  // each AddAddress add to list
					$mail->ClearCustomHeaders();
					$mail->ClearAttachments();
					$mail1 = $this->phpmailer;
					//send mail to user that driver has been assigned for the trip
					$usersch = $this->admin->getuserTripDetail($id);	
					$html  =  @file_get_contents(base_url().'emailtemplates/Confirm_user_driver.html');
					$link = site_url();
					$find = array('{name}','{date}','{time}','{city}','{link}');
					$replace   = array($usersch->first_name,$pickup_date,$pickup_time,$city,$link);
					$messegefinal = str_replace($find,$replace,$html);
					$mail1 = $this->phpmailer;
					$mail1->WordWrap = 100;
					$mail1->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
					$mail1->Subject = 'Driver Confirmation';
					$mail1->addAttachment("emailtemplates/logo.png");
					$mail1->addAttachment("emailtemplates/forward-glyph.png");
					$mail1->MsgHTML($messegefinal);
					$mail1->AddAddress($usersrh->email);
					$mail1->Send();
					
					
					 $data_d =  array(
						'driver_id' => $driver_id,
						'reservation_id' => $id,
						);
					 
					 $this->admin->driver_schdule($data_d,$id);
						
			
		
		} 
	
	//unassign driver from that reservation
	
	public function unassigndriver(){
						$id = $_POST['id'];
						$data =  array(
						'driver_id' => null,
						'status' => 0,
						);
						$this->admin->unassign_driver($id);
						$this->admin->deletedriver_schdule($id);
						
	}
	
	public function tripdetail($id=null){
		
		if($id){	
		
			$result = 	$this->admin_model->notification_check($id);
		
			if($result == 1){
		
			$this->admin_model->notification_update($id);
			}
			
			$this->data['tripdetail'] = $this->admin->get_reservationdetail($id);
			if ($this->input->server('REQUEST_METHOD') === 'POST')
			{
				$this->session->unset_userdata('tripdetail');
			}
			else {
				$this->session->set_userdata('tripdetail', $this->data['tripdetail']);
			}
			
			
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('trip_date_from', 'Pick Up Date', 'required');
			$this->form_validation->set_rules('pickup_time', 'pick Up time', 'required');
			$this->form_validation->set_rules('dropoff_time', 'Drop off Time', '');
			//$this->form_validation->set_rules('driver_id', 'Driver Name', 'required');
			$this->form_validation->set_rules('service_id', 'Service Type', 'required'); 
			$this->form_validation->set_rules('user_id', 'Client name', 'required'); 
			$this->form_validation->set_rules('city', 'City', 'required');
			$this->form_validation->set_rules('state', 'State', 'htmlspecialchars|required|max_length[255]|min_length[2]');
			$this->form_validation->set_rules('zip', 'Zip', 'required|numeric|max_length[10]|min_length[3]');
			if($this->input->post('service_id')==4){
			$this->form_validation->set_rules('desination_1', 'Drop off location1', 'required');
			}
			if($this->input->post('service_id')==2){
			$this->form_validation->set_rules('airport', 'Airport', 'required|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('airline', 'Airline', 'required|max_length[30]|min_length[3]');
			$this->form_validation->set_rules('flight_number', 'Flight No', 'required|max_length[30]|min_length[3]');
			}
			if($this->input->post('service_id')==3){
			$this->form_validation->set_rules('airport', 'Airport', 'required|max_length[30]|min_length[3]');
			//$this->form_validation->set_rules('airline', 'Airline', 'required|max_length[30]|min_length[3]');
			}
			$this->form_validation->set_rules('desination_2', 'drop off location2', '');
			if($this->input->post('service_id')==1){
			$this->form_validation->set_rules('address', 'Pick Up Address', 'required');
			}
			$this->form_validation->set_rules('address_2', 'Pick Up Address2', '');
			//$this->form_validation->set_rules('comments', 'Notes', 'required');else
			
			
			$dropoff_time=explode(' ',$this->data['tripdetail']->trip_date_to);
			//driver
			$data['dlogs'] = $this->admin->get_reservations_detail($id);
			
			if(!empty($data['dlogs'])){
				$tempArray = array();
			$tempArray1 = array();
				
			foreach($data['dlogs'] as $dlogs){
				$city_drivers = $this->admin->getNADriversByCity($dlogs->city);
				$tempArray1[$dlogs->id] = $city_drivers;
			}
			$tempArray2 = array();
				
			foreach($data['dlogs'] as $dlogs){
				$noncity_drivers = $this->admin->getAnonCityDrivers($dlogs->city);
				$tempArray2[$dlogs->id] = $noncity_drivers;
			}
			
			$tempArray3 = array();
				
			foreach($data['dlogs'] as $dlogs){
				$noncity_drivers = $this->admin->getNAnonCityDrivers($dlogs->city);
				$tempArray3[$dlogs->id] = $noncity_drivers;
			}
			
			
			$this->data['AdriverByCity'] = $tempArray;
			
			$this->data['NACityDrivers'] = $tempArray1;
			
			$this->data['AnonCityDrivers'] = $tempArray2;
			
			$this->data['NAnonCityDrivers'] = $tempArray3;
			}
			//end
		
			if($this->form_validation->run() != false){
			
			
				$status = $this->input->post('status');
				$time=$this->input->post('pickup_time').' '.$this->input->post('am_pm');
				$pickup_time = date("H:i", strtotime($time));
				$trip_date_from = $this->input->post('trip_date_from')." ".$pickup_time;
				if($this->input->post('trip_date_todate')==''){
					$trip_date_todate='0000-00-00';
				}
				else{
				$trip_date_todate = date("y-m-d",strtotime($this->input->post('trip_date_todate')));
				}
				//$dropoff_time = date("H:i", strtotime($this->input->post('dropoff_time')));
				//$trip_date_to = $this->input->post('trip_date_to');	
				$driver_id  = $this->input->post('driver_id');
				if($driver_id!="" && $status == 0){
				$status = 1;
				} else if ($driver_id=="" && ($status == 1 ||$status == 2 || $status == 3)){
				$status = 0;
				} else{
				$status = $status;
				}
				if($driver_id==''){
					$driver_id=null;
				}
				$service_id  = $this->input->post('service_id');				
				$user_id  = $this->input->post('user_id');
				$city  = $this->input->post('city');	
				$state = $this->input->post('state');
				$zip = $this->input->post('zip');
				$desination_1  = $this->input->post('desination_1');	
				$desination_2  = $this->input->post('desination_2');	
				$airport = $this->input->post('airport');
				$airline = $this->input->post('airline');
				$flight_number = $this->input->post('flight_number');
				$comments  = $this->input->post('comments');
				$address = $this->input->post('address');
				$address_2 = $this->input->post('address_2');
				$comments = $this->input->post('comments');
				$promo_code = $this->input->post('promo_code');
				$manaual_shift= $this->input->post('manaual_shift');
				$approx_hours = $this->input->post('approx_hours');
				if(empty($this->data['error'])){			
			
					$data =  array(
						'status' => $status,
						'trip_date_from' =>date("y-m-d",strtotime($this->input->post('trip_date_from')))." ".$pickup_time,
						'pickup_time' => $pickup_time,
						'trip_date_to' => $trip_date_todate." ".$dropoff_time[1],
						'driver_id' => $driver_id,
						'service_id' => $service_id,
						'user_id' => $user_id, 
						'city' => $city,
						'state' => $state,
						'zip' => $zip,
						'desination_1' => $desination_1,
						'desination_2' => $desination_2,
						'comments' => $comments,
						'address' => $address,
						'address_2' => $address_2,
						'airport' => $airport,
						'airline' => $airline,
						'flight_number' => $flight_number,
						'comments '	=>$comments,
						'promo_code'=>$promo_code,	
						'manaual_shift'=>$manaual_shift,
						'approx_hours'=>$approx_hours		
					);
				
					$get_true_false = $this->admin->update_reservationdetail($data,$id);
					if($get_true_false > 0){
					if($driver_id!=null && $driver_id!='' && $status==2){
					$this->sendConfirmationMail($id,$service_id);
					}
					$this->session->set_flashdata('msg', 'Successfully Updated');
					redirect('admin/dispatchlog/dlogs');
					//$this->template->page('admin/tripdetail',$this->data);
					
					}else{
						$this->data['wrong_update'] = 'Your entered information is not correct, please try again.';
						$this->template->page('admin/tripdetail',$this->data);
					}
				}	
			}else{	
				
				$this->template->page('admin/tripdetail',$this->data);
			}
		}else{
			redirect('admin/dispatchlog/dlogs');
		}
	
	}
	function reload_slide_bar(){
		$this->load->view('template/admin_sidebar');
	}
	
	public function sendConfirmationMail($id,$service_id){
		$trip_detail=$this->admin->get_reservationdetail($id);
		$pickup_data=$trip_detail->trip_date_from;
		$date1 =  explode(" ",$pickup_data) ;
		$pickup_date= date ("m/d/Y",strtotime($date1[0]));
		$pickup_time = $putime = date("g:iA", strtotime($trip_detail->pickup_time));
		$address = $trip_detail->address;
		$city = $trip_detail->city;
		$state = $trip_detail->state;
		$zip = $trip_detail->zip;
		$desination_1 = $trip_detail->desination_1;
		$desination_2 = $trip_detail->desination_2;
		$airport = $trip_detail->airport;
		$airline = $trip_detail->airline;
		$flight_number = $trip_detail->flight_number;
		$approx_hours = $trip_detail->approx_hours;
		$comments = $trip_detail->comments;
		$user_id=$this->admin_model->get_user_id($id);
		$usersrh=$this->admin_model->get_user($user_id[0]->user_id);
		$name = $usersrh->first_name.' '.$usersrh->last_name;
		$link = site_url();
		//send mail to user that driver has been assigned for the trip
		$html='';
		$messegefinal='';
		if($service_id == 1){
			$html  =  @file_get_contents(base_url().'emailtemplates/personal_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{desination_1}','{desination_2}','{approx_length}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$approx_hours,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 2){
			$html  =  @file_get_contents(base_url().'emailtemplates/airportpickup_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{airport}','{airline}','{flight_number}','{link}','{comments}');
			$replace = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$airport,$airline,$flight_number,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 3){
			$html  =  @file_get_contents(base_url().'emailtemplates/airportdropoff_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{dropoff_1}','{dropoff_2}','{airport}','{airline}','{flight_number}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$airport,$airline,$flight_number,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 4){
			$html  =  @file_get_contents(base_url().'emailtemplates/doubledriver_reservation_confirnation_mail.html');
		    $find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{dropoff_1}','{dropoff_2}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		$mail = $this->phpmailer;
		$mail->WordWrap = 100;
		$mail->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
		$mail->Subject = 'Reservation Confirmation';
		$mail->addAttachment("emailtemplates/logo.png");
		$mail->addAttachment("emailtemplates/forward-glyph.png");
		$mail->MsgHTML($messegefinal);
		$mail->AddAddress($usersrh->email);
		$mail->Send();
		$mail->ClearAddresses();  // each AddAddress add to list
		$mail->ClearCustomHeaders();
		$mail->ClearAttachments();
		// send confirmation mail to driver
		$mail1 = $this->phpmailer;
		$driver_id = $this->driver_model->get_driver_id($id);
		$driversrh=$this->driver_model->get_driver($driver_id[0]->driver_id);
		if($trip_detail->service_id==1 || $trip_detail->service_id==4){
			$html  =  @file_get_contents(base_url().'emailtemplates/driver_confirmation_mail.html');
			$link = site_url();
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{city}','{state}','{zip}','{desination_1}','{desination_2}','{link}');
			$replace   = array($driversrh->first_name,$pickup_date,$pickup_time,$address,$city,$state,$zip,$desination_1,$desination_2,$link);
			$messegefinal = str_replace($find,$replace,$html);
			}
		else{
			$html  =  @file_get_contents(base_url().'emailtemplates/driver_confirmation_airport.html');
			$link = site_url();
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{city}','{state}','{zip}','{airport}','{link}');
			$replace   = array($driversrh->first_name,$pickup_date,$pickup_time,$address,$city,$state,$zip,$airport,$link);
			$messegefinal = str_replace($find,$replace,$html);
		}
		$mail1 = $this->phpmailer;
		$mail1->WordWrap = 100;
		$mail1->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
		$mail1->Subject = 'YOU HAVE BEEN ASSIGNED THE TRIP BELOW';
		$mail1->addAttachment("emailtemplates/logo.png");
		$mail1->addAttachment("emailtemplates/forward-glyph.png");
		$mail1->MsgHTML($messegefinal);
		$mail1->AddAddress($driversrh->email);
		$mail1->Send();
	}
}
	