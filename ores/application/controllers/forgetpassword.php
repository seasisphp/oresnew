<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgetpassword extends CI_Controller {

	 
	 function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->helper('url');
		$this->load->library('phpmailer');
		$this->load->helper('cookie');
		$this->load->library('session');
		
		
	/* 	$front_sess_name = $this->session->userdata('frontuser');
		$sess_user_id = $front_sess_name['id'];
		$role = $this->admin_model->get_role($sess_user_id);

		if(($role!=1))
		{
		 redirect("admin/");
		} */
	} 
	
	
	public function index()
	{
	if(!empty($front_sess_name)){
			$this->template->page('home');
			}else{
			$this->template->page('forgetpassword');
			}
	
	
	}
	
	//forgetpassword
	public function doforget(){
		$this->form_validation->set_rules('email', 'Email', 'htmlspecialchars|required|valid_email');
		$email = $this->input->post('email');
		
		//$data=$this->admin_model->check_forgot_email($email,1);
		if ($this->form_validation->run()!=false){
			$data=$this->admin_model->check_forgot_email($email,1);

			if($data==0){
				echo 0;			 
			}else{
				echo "ok";
					
				// send reset password maail to user.
				$verification = sha1(microtime());
				$this->admin_model->insert_key($verification,$email);
				//send mail to the registered user
				$link2 = site_url("forgetpassword/resetpassword/".$verification);
				$html  =  @file_get_contents(base_url().'emailtemplates/forgot_password_email.html');
				$link = site_url();
				$find = array('{name}','{link}','{link2}');
				$replace   = array($data[0]->first_name,$link,$link2);
				$messegefinal = str_replace($find,$replace,$html);
				$mail = $this->phpmailer;
				$mail->WordWrap = 100;
				$mail->SetFrom('admin@admin.com', 'ORES');
				$mail->Subject    = 'Reset password';
				$mail->addAttachment("emailtemplates/logo.png");
				$mail->addAttachment("emailtemplates/forward-glyph.png");
				$mail->MsgHTML($messegefinal);
				$mail->AddAddress($email);
				$mail->Send();
			}
		}else{
			echo validation_errors();
		
		}
}

	public function resetpassword()
		{
		$code = $this->uri->segment(3);
	
		$res = $this->admin_model->verify_key($code); 
		
		if(isset($res))
		{
		$this->form_validation->set_rules('password', 'Password', 'required|matches[repassword]|min_length[6]|max_length[20]');
		$this->form_validation->set_rules('repassword', 'Re-Enter Password', 'required|min_length[6]|max_length[20]');		
			
			if ($this->form_validation->run() == FALSE){
			
			$this->template->page('resetpassword');
			}else{						
				$password = md5($this->input->post('password'));
				$this->admin_model->update_key_password($code,$password);	
				
				 $login_link = base_url();
				$pass['msg']="Your password has been reset. Please click here <a href = ".$login_link.">click here</a> to Login."; 
				$this->template->page('resetpassword',$pass);	
				$this->admin_model->empty_key($code,$password);					
			
			}
			
		}else{
		
		
			$pass['msg']="Please verify again,there is something wrong with this email or the time may out for this"; 
			$this->template->page('resetpassword',$pass); 
		}
      
	}
}

