<?php 
/**
* @author       Dev Verma
* @web         http://www.asim.pk/
* @date     13th May, 2009
* @copyright    No Copyrights, but please link back in any way
*/
 
class Orescron extends CI_Controller {
 
function __construct()
{
parent::__construct();		
	$this->load->model('dashboard_model');
	$this->load->helper('url');
}
 
function index()
{
	$current_booking=$this->dashboard_model->getallcurrentbookings();
	//var_dump($current_booking);
	if($current_booking!=false){
		foreach($current_booking as $booking){
			$pick_date=strtotime($booking->trip_date_from);
			$current_date = strtotime(date('Y-m-d H:i:s'));
			$total_hours = round(($pick_date - $current_date) / 3600);
			//echo $booking->service_id;
			if($total_hours==24){
				$this->sendAlertMail($booking->id,$booking->service_id);
				//var_dump($result);
				$data=array('is_confirmed'=>'yes');
				$this->dashboard_model->set_confirmed($booking->email,$data);
			}
			
		}
	} 
	$dispatched_booking=$this->dashboard_model->getalldiapatchedbookings();
	if($dispatched_booking!=false){
		foreach($dispatched_booking as $booking){
			$pick_date=strtotime($booking->trip_date_from);
			$current_date = strtotime(date('Y-m-d H:i:s'));
			$total_hours = round(($current_date-$pick_date) / 3600);
			if($total_hours==8){
				$this->send_thanks_mail($booking->email,$booking->contact_name);
				//var_dump($result);
				$data=array('is_acknowled'=>'yes');
				$this->dashboard_model->set_acknowled($booking->email,$data);
			}
		}
	}
}

/* This function is used to send reservation mail when user reserved in reservation.
 *  Author:Devi Lal Verma
*/
public function sendAlertMail($id,$service_id){
		$trip_detail=$this->admin->get_reservationdetail($id);
		$pickup_data=$trip_detail->trip_date_from;
		$date1 =  explode(" ",$pickup_data) ;
		$pickup_date= date ("m/d/Y",strtotime($date1[0]));
		$pickup_time = $putime = date("g:iA", strtotime($trip_detail->pickup_time));
		$address = $trip_detail->address;
		$city = $trip_detail->city;
		$state = $trip_detail->state;
		$zip = $trip_detail->zip;
		$desination_1 = $trip_detail->desination_1;
		$desination_2 = $trip_detail->desination_2;
		$airport = $trip_detail->airport;
		$airline = $trip_detail->airline;
		$flight_number = $trip_detail->flight_number;
		$approx_hours = $trip_detail->approx_hours;
		$comments = $trip_detail->comments;
		$user_id=$this->admin_model->get_user_id($id);
		$usersrh=$this->admin_model->get_user($user_id[0]->user_id);
		$name = $usersrh->first_name.' '.$usersrh->last_name;
		$link = site_url();
		//send mail to user that driver has been assigned for the trip
		$html='';
		$messegefinal='';
		if($service_id == 1){
			$html  =  @file_get_contents(base_url().'emailtemplates/personal_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{desination_1}','{desination_2}','{approx_length}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$approx_hours,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 2){
			$html  =  @file_get_contents(base_url().'emailtemplates/airportpickup_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{airport}','{airline}','{flight_number}','{link}','{comments}');
			$replace = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$airport,$airline,$flight_number,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 3){
			$html  =  @file_get_contents(base_url().'emailtemplates/airportdropoff_reservation_confirmation_mail.html');
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{dropoff_1}','{dropoff_2}','{airport}','{airline}','{flight_number}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$airport,$airline,$flight_number,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		elseif($service_id == 4){
			$html  =  @file_get_contents(base_url().'emailtemplates/doubledriver_reservation_confirnation_mail.html');
		    $find = array('{name}','{date}','{pickup_time}','{pu_address}','{state}','{city}','{zip}','{dropoff_1}','{dropoff_2}','{link}','{comments}');
			$replace   = array($name,$pickup_date,$pickup_time,$address,$state,$city,$zip,$desination_1,$desination_2,$link,$comments);
			$messegefinal = str_replace($find,$replace,$html);
		}
		$mail = $this->phpmailer;
		$mail->WordWrap = 100;
		$mail->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
		$mail->Subject = 'Reservation Confirmation';
		$mail->addAttachment("emailtemplates/logo.png");
		$mail->addAttachment("emailtemplates/forward-glyph.png");
		$mail->MsgHTML($messegefinal);
		$mail->AddAddress($usersrh->email);
		$mail->Send();
		$mail->ClearAddresses();  // each AddAddress add to list
		$mail->ClearCustomHeaders();
		$mail->ClearAttachments();
		// send confirmation mail to driver
		$mail1 = $this->phpmailer;
		$driver_id = $this->driver_model->get_driver_id($id);
		$driversrh=$this->driver_model->get_driver($driver_id[0]->driver_id);
		if($trip_detail->service_id==1 || $trip_detail->service_id==4){
			$html  =  @file_get_contents(base_url().'emailtemplates/driver_confirmation_mail.html');
			$link = site_url();
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{city}','{state}','{zip}','{desination_1}','{desination_2}','{link}');
			$replace   = array($driversrh->first_name,$pickup_date,$pickup_time,$address,$city,$state,$zip,$desination_1,$desination_2,$link);
			$messegefinal = str_replace($find,$replace,$html);
			}
		else{
			$html  =  @file_get_contents(base_url().'emailtemplates/driver_confirmation_airport.html');
			$link = site_url();
			$find = array('{name}','{date}','{pickup_time}','{pu_address}','{city}','{state}','{zip}','{airport}','{link}');
			$replace   = array($driversrh->first_name,$pickup_date,$pickup_time,$address,$city,$state,$zip,$airport,$link);
			$messegefinal = str_replace($find,$replace,$html);
		}
		$mail1 = $this->phpmailer;
		$mail1->WordWrap = 100;
		$mail1->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
		$mail1->Subject = 'YOU HAVE BEEN ASSIGNED THE TRIP BELOW';
		$mail1->addAttachment("emailtemplates/logo.png");
		$mail1->addAttachment("emailtemplates/forward-glyph.png");
		$mail1->MsgHTML($messegefinal);
		$mail1->AddAddress($driversrh->email);
		$mail1->Send();
	}
	public function send_thanks_mail($email,$first_name){
		$mail = $this->phpmailer;
		$mail->ClearAddresses();  // each AddAddress add to list
		$mail->ClearCustomHeaders();
		$mail->ClearAttachments();
		$mail1 = $this->phpmailer;
		$html  =  @file_get_contents(base_url().'emailtemplates/thanks_email.html');
		$link = site_url();
		$find = array('{name}','{link}');
		$replace   = array($first_name,$link);
		$messegefinal = str_replace($find,$replace,$html);
		$mail1->WordWrap = 100;
		$mail1->SetFrom('admin@beyourpersonaldriver.com', 'ORES');
		$mail1->Subject = 'THANK YOU';
		$mail1->addAttachment("emailtemplates/logo.png");
		$mail1->addAttachment("emailtemplates/forward-glyph.png");
		$mail1->MsgHTML($messegefinal);
		$mail1->AddAddress($email);
		$mail1->Send();
	}
}
