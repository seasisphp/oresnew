<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct() {
        parent::__construct();		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->library('session');
	} 
	
	
	public function index()
	{
	//echo "<pre>"; print_r($this->session->userdata); die;
	if(isset($this->session->userdata)){
	$this->template->page('test');
	}else{
	$this->template->page('home');
	}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */