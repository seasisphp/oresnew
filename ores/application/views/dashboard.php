
<div class="register-container">
  	<h2>Welcome </h2>
    <h3></h3>
    <div class="form-container">
    <ul id="tabs">
    <li><a href="#" name="tab1">Current Bookings</a></li>
    <li><a href="#" name="tab2">Past Bookings</a></li>
 
  
</ul>

<div id="content"> 
   <div id="tab1">
	  <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Trip #</th>
                                               <!-- <th>Driver Status</th>--> 
                                                <th>Client Name</th>
												 <th>Trip Date</th>
                                                <th>Duration</th>
                                                <th>City</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($this->data['currentbookings'])){
											$serial_no=1;
													foreach($this->data['currentbookings'] as $currentbookings):
										  ?>
										  <tr>
                                                <td><?php echo $serial_no;  ?></td>
                                                <!-- <td><?php //echo $currentbookings->first_name;  ?>&nbsp;<?php //echo $currentbookings->last_name; 
												if($currentbookings->status == 0){ ?>
												Pending
												<?php }else{?>
												Waiting
												<?php }?>
												</td>--> 
                                                <td><?php echo $currentbookings->u_fname;  ?>&nbsp;<?php echo $currentbookings->u_lname;  ?></td>
												<?php $trip_date = explode(" ",$currentbookings->trip_date_from);  ?>
												 <td><?php echo date ("m/d/Y",strtotime($trip_date[0]));?></td>
                                                <td><?php echo $currentbookings->approx_hours;  ?></td>
                                                <td><?php echo $currentbookings->city;  ?></td>
                                            </tr>
                                           <?php 
										    $serial_no++;
												endforeach;	
										    } else{ ?>
										   <tr><td colspan="6" style="text-align:center">No Results Found </td> </tr>
										   <?php }?>
                                    
										  
                                       
                                        </tbody>
                                    </table>
	
	</div>
    <div id="tab2">
	     <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Trip #</th>
                                                <th>Driver Name</th>
                                                <th>Client Name</th>
												 <th>Trip Date</th>
                                                <th>Duration</th>
                                                <th>City</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($this->data['pastbookings'])){
												$serial_no=1;
													foreach($this->data['pastbookings'] as $pastbookings):
										  ?>
										  <tr>
                                                <td><?php  echo $serial_no; ?></td>
                                                <td><?php 
												
												if($pastbookings->driver_id == 0){ ?>
												No Driver Assigned
											<?php	}else{
												echo $pastbookings->first_name;  ?>&nbsp;<?php echo $pastbookings->last_name; 
												}
												?></td>
                                                <td><?php echo $pastbookings->u_fname;  ?>&nbsp;<?php echo $pastbookings->u_lname;  ?></td>
												<?php $trip_date = explode(" ",$pastbookings->trip_date_from);  ?>
												 <td><?php echo date ("m/d/Y",strtotime($trip_date[0]));?></td>
                                                <td><?php echo $pastbookings->approx_hours;  ?></td>
                                                <td><?php echo $pastbookings->city;  ?></td>
                                            </tr>
                                           <?php 
										   $serial_no++;
												endforeach;	
										   
										   } else{ ?>
										    <tr><td colspan="6" style="text-align:center">No Results Found </td> </tr>
										   <?php }?>
                                        
                                        </tbody>
                                    </table>
									</div>
 

 
</div>
        
        <div class="clr"></div>
    </div>
    
    
  </div>
  <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script>
$(document).ready(function() {
    $("#content div").hide(); // Initially hide all content
    $("#tabs li:first").attr("id","current"); // Activate first tab
    $("#content div:first").fadeIn(); // Show first tab content
    
    $('#tabs a').click(function(e) {
        e.preventDefault();
        if ($(this).closest("li").attr("id") == "current"){ //detection for current tab
         return       
        }
        else{             
        $("#content div").hide(); //Hide all content
        $("#tabs li").attr("id",""); //Reset id's
        $(this).parent().attr("id","current"); // Activate this
        $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
        }
    });
});
</script>