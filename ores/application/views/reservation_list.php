<?php 
 $driver_name=$this->session->userdata('driver_name');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  
  
  <div class="register-container">
  	<h2> All Reservation </h2>
    <h3>Tell Us About Yourself</h3>
        <div class="block-content collapse in">
                                <div class="span12">
  									<table class="table  table-striped" cellspacing="15">
						              <thead>
						                <tr>
						       <th class="ck_field" align="left">#Id </th>		
								<th align="left">Service Type</th>		
								<th align="left">Status</th>
								<th align="left">Client Name</th>										
									
								<th align="left">PU Time</th>	
							
								<th align="left">Duration</th>		
								<th align="left">Driver Name</th>	
						
								<th align="left">City</th>
								<th align="left">Action</th>
								
							
						                </tr>
						              </thead>
						              <tbody>
						              <?php 
						            if(!empty($dlog)){
									$serial_no=1;
								foreach($dlog as $key=>$dispatch_log){ 	?>	
									<tr id="<?php echo $dispatch_log->id; ?>">
										<td><?php  echo $serial_no; ?></td>
										
										<td>
										<?php if($dispatch_log->service_id==1){ ?>
											Personal Driver Service
											<?php } else if ($dispatch_log->service_id==2){ ?>
											Airport Pick up
											<?php } else if ($dispatch_log->service_id==3){ ?>
											Airport Drop Off
											<?php } else if ($dispatch_log->service_id==4){ ?>
											Double driver Pick Up
											<?php } ?>
											</td>
										
										
									
										<td> <span id="changeStatus<?php echo $dispatch_log->id ?>"><?php if($dispatch_log->status==0){ ?>
											Pending
											<?php } else if($dispatch_log->status==1) { ?>
											Waiting
											<?php } else if($dispatch_log->status==2) { ?>
											Dispatched
											<?php } else if($dispatch_log->status==3) { ?>
											Completed
											<?php } else if($dispatch_log->status==4) { ?>
											Cancelled
											<?php } ?></span>
											</td>
										<td><?php  echo $dispatch_log->contact_name; ?></td>
										<!--<td><?php  echo $dispatch_log->trip_date_from; ?></td>-->
										<td><?php  echo $dispatch_log->pickup_time; ?></td>
										<td><?php  echo $dispatch_log->approx_hours; ?> hours</td>
										
										<td><?php  echo $driver_name?></td>
										<td><?php  echo $dispatch_log->city; ?></td>
									
										<td>
										<span>
										<a href="<?php echo base_url(); ?>admin/dispatchlog/tripdetail/<?php echo $dispatch_log->id; ?>"><img src="<?php echo base_url();?>images/icons/view.png" title="view"></a>
										</span>
										</td>
									</tr><?php 
									$serial_no++;
								}
							}
							else{?>
						
							<tr><td style='text-align:center' colspan="8"> No Results Found</td></tr>
							
							<?php }?>
						              </tbody>
						            </table>
						                <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
    
    
  </div>
  
  
  
  

</div>
</body>
</html>

