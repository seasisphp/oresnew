<script type="text/javascript">$(document).ready(function() {$("#Userdetail .col_filter").index();$("#Userdetail .ck_field").index();var ck_ignore = $("#Userdetail .ck_field").index();var filter_ignore = $("#Userdetail .col_filter").index();var myHeaders = {};myHeaders[ck_ignore] = { sorter: false };myHeaders[filter_ignore] = { sorter: false };$("#Userdetail").tablesorter({ widgets: ['zebra'], headers: myHeaders });});</script>	
<div class="wrapper">
	<table cellpadding="0" cellspacing="0" border="1" id="mainTableContent">
		<tr>
			<td class="aside"> <!-- sideBatr goes here --> <?php $this->load->view('template/admin_sidebar'); ?>   </td>    <td class="contentbox"> 
				<div id="mainContent">  
					<div class="grayStripRight">Manage Pages</div>
						<div class="validation"><?php echo $this->session->flashdata('created'); ?></div>
						<div class="validation"><?php echo $this->session->flashdata('updated'); ?></div>	
							
						<table id="Userdetail" class="tablesorter">	
								<thead>
									<tr>
										<th class="ck_field" align="left">Id</th>
										<th align="left">Title</th>
										<th style="width:300px" align="left">Page Description</th>
										<th align="left">Meta Tags</th>
										<th align="left">Meta Description</th>	
										<th align="left">Page Name </th>
										<th align="left">Status </th>
										<th align="left">Action</th>
									</tr>
								</thead>
							<tbody><?php foreach ($list_pages as $page): ?>	
								<tr> 
									<td><?php  echo $page['id'] ?></td>	
									<td><?php  echo ucwords($page['title']);?></td>	
									<td><?php  echo substr($page['page_description'],0,200);?>..</td> 
									<td><?php  echo $page['meta_tags'];?></td> 
									<td><?php  echo $page['meta_description'];?></td> 
									<td><?php  echo $page['page'];?></td>									
									<td><?php  if($page['status']=='1') { ?> <a href="<?php echo base_url();?>admin/pages/mobile_enable/<?php echo $page['id'];?>"><img src="<?php echo base_url() ?>images/admin/enable.gif "></a> <?php } else { ?> <a href="<?php echo base_url();?>admin/pages/mobile_disable/<?php echo $page['id'];?>"><img src="<?php echo base_url(); ?>images/admin/disable.gif "></a><?php } ?>		</td>
									<td><a href="<?php echo base_url();?>admin/pages/mobile_edit/<?php echo $page['id'];?>">Edit</a></td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
				</div>
				</td>
		</tr>
	</table>
</div>