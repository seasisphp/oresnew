<?php 
$sd_tripdetail=$this->session->userdata('tripdetail');
$sd_tripcloseoutdetail=$this->session->userdata('tripcloseoutdetail');
//var_dump($sd_tripdetail);
$sd_driverdetail=$this->session->userdata('driverdetail');
//var_dump($sd_tripcloseoutdetail);
$sd_userdetail=$this->session->userdata('userdetail');
?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); 
					$date =  explode(" ",$tripdetail->trip_date_from) ; 
					$fdate = date ("m/d/Y",strtotime($date[0]));
					$pudate =  $fdate;
				    $putime =  $date[1];
					$date1 =  explode(" ",$tripdetail->trip_date_to) ; 
					if($date1[0]=='0000-00-00'){
						$fdate1='';
					}
					else{
					$fdate1 = date ("m/d/Y",strtotime($date1[0]));
					}
					$doffdate =  $fdate1;
					if($date1[0]=='0000-00-00'){
						$dofftime ='';
					}
					else{
					$dofftime =  $date1[1];
					}
					$ts1 = strtotime($tripdetail->trip_date_to);
					$ts2 = strtotime($tripdetail->trip_date_from);
					$total_hours = abs($ts1 - $ts2) / 3600;
					
					$posi = strpos ( $total_hours, "." );
					$hour = substr ( $total_hours, 0, $posi );
					$mint=round(substr($total_hours,$posi+1,2)*(6/10));
					if($hour==''){
						$hour='00';
					}
					$total_time=$hour.':'.$mint;
					?> 
                </div>
                <!--/span-->
				
<div class="span9" id="content">
                      <!-- morris stacked chart -->
					  
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Admin: Trip Close Out Screen</div>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12">
										<div id="error"> 
										<?php 
											$errors = validation_errors();
											if(!empty($wrong_insert)){echo $wrong_insert;}
											//if(!empty($error)){echo $error;}
										?>
										<?php
										$hidden = array('total_hours' => $total_hours);
										 echo form_open('','',$hidden); 
										?>
						<div class="form-left">	
							<?php if(form_error('status')) { ?>
									
								<div class="txtbox1"> 
									<label>&nbsp;</label>
									<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
								</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Trip Status<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						''  => '-- Please select --',
						'0' => 'Pending',
						'1' => 'Waiting',
						'2' => 'Dispatched',
						'3' => 'Completed',
						'4' => 'cancelled',
						); ?>
						<?php if(!empty($sd_tripdetail)){
							$index=$sd_tripdetail->status;
							}
							elseif($_POST['status']!=''){
								$index=$_POST['status'];
							}
							else{
								$index='';
							}
							echo form_dropdown('status', $options,$index);
						?>
						</div>
							
						</div>
						<div class="form-right">	
							<?php if(form_error('trip_date_from')) { ?>							
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_from'); ?></span>
							</div> <?php
		
							} ?>
							<div class="txtbox1"> 
								<label>Pick-Up Date<span class="validation" style="color:#FF0000;">*</span></label>
								<input type="text" readonly="readonly" class="shadow datetime" name="trip_date_from" id="trip_date_from" value="<?php if(!empty($sd_tripdetail->trip_date_from)){ $date=explode(" ",$sd_tripdetail->trip_date_from); 	$fdate = date ("m/d/Y",strtotime($date[0])); echo $fdate;}elseif($_POST['trip_date_from']!=''){ echo $_POST['trip_date_from'];}else{ echo '';} ?>">
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php
						if(form_error('pickup_time')) { ?>						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Pick-Up Time<span class="validation" style="color:#FF0000;">*</span></label>
							<select name="pickup_time" id="pickup_time" class="" style="width:110px;">
									   <option value="">Select Time</option>
								        <?php 
								        if(!empty($sd_tripdetail->pickup_time)) {
								        	$timeVal = date("g:iA", strtotime($sd_tripdetail->pickup_time ));
								        	$pos = strpos($timeVal, ':');
								        	$am_pm = substr($timeVal, $pos+3, 2);
								        	$t1 = substr($timeVal, 0, $pos+1);
								        	$t2 = substr($timeVal, $pos+1, 2);
								        	$t3 = $t1.$t2;
											}
								        	else{
								        		$t3 =  $_POST['pickup_time'];
								        		$am_pm = $_POST['am_pm'];
								        	}
								         $a = 00;
								         $b=00;
								         $sel='';
								         for($a=1;$a<=12;$a++){
											for($b=00;$b<60;$b=$b+15)
											{
												if($b==0){
													$b='00';
													}
													if(!empty($t3) &&  $t3 == $a.':'.$b){
														$sel='selected';
														}
														else{
															$sel='';
															}		   
								         ?>
							            <option value="<?php echo $a.':'.$b;?>" <?php echo $sel;?>><?php echo $a.':'.$b;?></option>
							            <?php } }?>
						          	</select> 
						            <select id="am_pm" name="am_pm" class="" style="width:85px;" >
						            <option value="AM" <?php if($am_pm == 'AM'){ echo 'selected';}?>>AM</option>
						            <option value="PM" <?php if($am_pm == 'PM'){ echo 'selected';}?>>PM</option>
						          	</select>	
							
						</div>
						</div>
						<div class="form-right">	
							<?php if(form_error('trip_date_todate')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_todate'); ?></span>
							</div> <?php } ?>
					
						<div class="txtbox1"> 
							<label>Drop Off Date<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" readonly="readonly" class="shadow datetime" id="trip_date_todate" name="trip_date_todate" value="<?php  if(!empty($sd_tripdetail->trip_date_to)){ echo $fdate1;}elseif($_POST['trip_date_todate']!=''){ echo $_POST['trip_date_todate'];}else{ echo '';} ?>">
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-left">	
							<?php if(form_error('trip_date_to')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_to'); ?></span>
							</div> <?php } ?>
				
						<div class="txtbox1"> 
							<label>Drop Off Time<span class="validation" style="color:#FF0000;">*</span></label>
								<select name="trip_date_to" id="trip_date_to" class="" style="width:110px;">
									   <option value="">Select Time</option>
								        <?php 
								        if(!empty($sd_tripdetail->trip_date_from) && $dofftime!='') {
								        	$timeVal = date("g:iA", strtotime($dofftime));
								        	$pos = strpos($timeVal, ':');
								        	$am_pm = substr($timeVal, $pos+3, 2);
								        	$t1 = substr($timeVal, 0, $pos+1);
								        	$t2 = substr($timeVal, $pos+1, 2);
								        	$t3 = $t1.$t2;
											}
								        	else{
								        		$t3 = set_value('trip_date_to');
								        		$am_pm = $_POST['am_pm1'];
								        	}
								         $a = 00;
								         $b=00;
								         $sel='';
								         for($a=1;$a<=12;$a++){
											for($b=00;$b<60;$b=$b+15)
											{
												if($b==0){
													$b='00';
													}
													if(!empty($t3) &&  $t3 == $a.':'.$b){
														$sel='selected';
														}
														else{
															$sel='';
															}		   
								         ?>
							            <option value="<?php echo $a.':'.$b;?>" <?php echo $sel;?>><?php echo $a.':'.$b;?></option>
							            <?php } }?>
						          	</select> 
						            <select id="am_pm1" name="am_pm1" class="" style="width:85px;" >
						            <option value="AM" <?php if($am_pm == 'AM'){ echo 'selected';}?>>AM</option>
						            <option value="PM" <?php if($am_pm == 'PM'){ echo 'selected';}?>>PM</option>
						          	</select>	
						</div>
					</div>
					
				<div class="form-left">						
						<?php if(form_error('driver_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('driver_id'); ?></span>
						</div> <?php } ?>
					
					<div class="txtbox1"> 
							<label>Driver<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="driver_id" class="shadow" name="driver_id" id='driver_id'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('driver');
						foreach($ctrb->result() as $ctrb){
							if(!empty($sd_tripdetail)){
							if($ctrb->id==$sd_tripdetail->driver_id){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
							else{
								if($ctrb->id==$_POST['driver_id']){
									$sel='selected="selected"';
								}else{$sel='';
								}
								echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
						}
						?>
						</select>
						</div> 
				</div>
			<div class="clear"></div>
				<!-- <div class="form-left">		
						<?php if(form_error('service_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('service_id'); ?></span>
						</div> <?php } ?>
							<div class="txtbox1"> 
							<label>Service Type<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="service_id" class="shadow" name="service_id" id='service_id'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('service_type');
						foreach($ctrb->result() as $ctrb){
							if(!empty($sd_tripdetail)){
							if($ctrb->service_type_id==$sd_tripdetail->service_id){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->service_type_id."' $sel>". $ctrb->service_type_desc."</option>";
							}
							else{
								if($ctrb->service_type_id==$_POST['service_id']){
									$sel='selected="selected"';
								}else{$sel='';
								}
								echo "<option value='".$ctrb->service_type_id."' $sel>". $ctrb->service_type_desc."</option>";
							}
						}
						?>
						</select>
						</div> 
				</div>	-->

				<div class="form-left">	
						<?php if(form_error('user_id')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('user_id'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Client Name<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="user_id" class="shadow" name="user_id" id='user_id'>
							<option value="">-- Please Select --</option>
						<?php 
						$ctrb   = $this->db->get('user');
						foreach($ctrb->result() as $ctrb){
							if(!empty($sd_tripdetail)){
							if($ctrb->id==$sd_tripdetail->user_id){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
							else{
								if($ctrb->id==$_POST['user_id']){
									$sel='selected="selected"';
								}else{$sel='';
								}
								echo "<option value='".$ctrb->id."' $sel>". $ctrb->first_name."</option>";
							}
						}
						?>
						</select>
						</div> 
				</div>

				<div class="form-right">	
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Pick Up location<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="city" id="city" value="<?php if(!empty($tripdetail->city)) echo $tripdetail->city; elseif(isset($_POST['city'])) echo $_POST['city']; ?>">
						</div> 
				</div>
				<div class="clear"></div>
				<div class="form-left">	
						<?php if(form_error('desination_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Drop Off location-1</label>
							<input type="text" class="shadow" name="desination_1" id="desination_1" value="<?php if(!empty($tripdetail->desination_1)) echo $tripdetail->desination_1;  elseif(isset($_POST['desination_1'])) echo $_POST['desination_1']; ?>">
						</div> 
				</div>
					<div class="form-right">		
						<?php if(form_error('desination_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('desination_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Drop Off location-2</label>
							<input type="text" class="shadow" name="desination_2" id="desination_2" value="<?php if(!empty($tripdetail->desination_2)) echo $tripdetail->desination_2; elseif(isset($_POST['desination_2'])) echo $_POST['desination_2']; ?>">
						</div> 
					</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php if(form_error('mileage_start')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mileage_start'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Mileage Start</label>
							<input type="text" class="shadow number_only" name="mileage_start"  id="mileage_start" value="<?php if(!empty($sd_tripcloseoutdetail->mileage_start)) echo $sd_tripcloseoutdetail->mileage_start; elseif(isset($_POST['mileage_start'])) echo $_POST['mileage_start'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						<?php if(form_error('mileage_end')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mileage_end'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Mileage End</label>
							<input type="text" class="shadow number_only" name="mileage_end"  id="mileage_end" value="<?php if(!empty($sd_tripcloseoutdetail->mileage_end)) echo $sd_tripcloseoutdetail->mileage_end; elseif(isset($_POST['mileage_end'])) echo $_POST['mileage_end'];else echo 0;?>">
						</div> 
						</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php if(form_error('total_miles')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_miles'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Total Miles</label>
							<input type="text" class="shadow number_only" name="total_miles" id="total_miles" value="<?php if(!empty($sd_tripcloseoutdetail->total_miles)) echo $sd_tripcloseoutdetail->total_miles; elseif(isset($_POST['total_miles'])) echo $_POST['total_miles'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						<?php if(form_error('total_time')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_time'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Total Time</label>
							<input type="text" class="shadow" name="total_time" id="total_time" value="<?php if(!empty($sd_tripcloseoutdetail->total_time)) echo $sd_tripcloseoutdetail->total_time;
							elseif(isset($_POST['total_time']))echo $_POST['total_time'];else echo'0';?>">
						</div> 
						</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php if(form_error('hourly_rate')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('hourly_rate'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Client Hourly Rate</label>
							<input type="text" class="shadow number_only" name="hourly_rate"  id="hourly_rate" value="<?php if(!empty($sd_tripcloseoutdetail->hourly_rate)) echo $sd_tripcloseoutdetail->hourly_rate;elseif(isset($_POST['hourly_rate'])) echo $_POST['hourly_rate'];else echo 0; ?>">
						</div> 
						</div>
						
						<div class="form-right">	
						<?php if(form_error('fee_per_mile')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('fee_per_mile'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Client Rate Per Mile</label>
							<input type="text" class="shadow number_only" name="fee_per_mile"  id="fee_per_mile" value="<?php if(!empty($sd_tripcloseoutdetail->fee_per_mile)) echo $sd_tripcloseoutdetail->fee_per_mile;elseif(isset($_POST['fee_per_mile'])) echo $_POST['fee_per_mile'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-left">	
						<?php if(form_error('client_fee_airport_dd')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('client_fee_airport_dd'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Client Fee Airport/DD</label>
							<input type="text" class="shadow number_only" name="client_fee_airport_dd"  id="client_fee_airport_dd" value="<?php if(isset($_POST['client_fee_airport_dd'])) echo set_value('client_fee_airport_dd'); else echo 0;?>">
						</div> 
						</div>
						<div class="clear"></div>
						<hr />
						<span style="position: relative; padding-left: 314px; bottom: 18px;">CUSTOMER TOTALS</span>
						<div class="form-left">	
						<?php if(form_error('total_hourly_fee')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_hourly_fee'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Total Hourly Fee<span class="validation" style="color:#FF0000;"></span></label>
							<input type="text" class="shadow number_only" name="total_hourly_fee"  id="total_hourly_fee" value="<?php if(!empty($sd_tripcloseoutdetail->total_hourly_fee)) echo $sd_tripcloseoutdetail->total_hourly_fee; elseif(isset($_POST['total_hourly_fee'])) echo $_POST['total_hourly_fee'];else echo 0;?>">
						</div> 
						</div>
					
						<div class="form-right">	
						<?php if(form_error('total_mileage_fee')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_mileage_fee'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Total Mileage Fee</label>
							<input type="text" class="shadow number_only" name="total_mileage_fee"  id="total_mileage_fee" value="<?php if(!empty($sd_tripcloseoutdetail->total_mileage_fee)) echo $sd_tripcloseoutdetail->total_mileage_fee; elseif(isset($_POST['total_mileage_fee'])) echo $_POST['total_mileage_fee'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-left">	
						<?php if(form_error('misc_fee_toll_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('misc_fee_toll_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Misc Fee Toll 1</label>
							<input type="text" class="shadow number_only" name="misc_fee_toll_1"  id="misc_fee_toll_1" value="<?php if(!empty($sd_tripcloseoutdetail->misc_fee_toll_1)) echo $sd_tripcloseoutdetail->misc_fee_toll_1; elseif(isset($_POST['misc_fee_toll_1'])) echo $_POST['misc_fee_toll_1'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						

						<div class="txtbox1"> 
							<label>Misc Fee Toll 1 Notes</label>
								<textarea class="txtarea" rows="2" cols="30" name="misc_fee_toll_1_notes"><?php if(!empty($sd_tripcloseoutdetail->misc_fee_toll_1_notes)) echo $sd_tripcloseoutdetail->misc_fee_toll_1_notes; elseif(isset($_POST['misc_fee_toll_1_notes'])) echo $_POST['misc_fee_toll_1_notes'];?></textarea>
							
							</div> 
						</div>
						<div class="clear"></div>
						<div class="form-left">	
					<?php if(form_error('misc_fee_toll_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('misc_fee_toll_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Misc Fee Toll 2</label>
							<input type="text" class="shadow number_only" name="misc_fee_toll_2"  id="misc_fee_toll_2" value="<?php if(!empty($sd_tripcloseoutdetail->misc_fee_toll_2)) echo $sd_tripcloseoutdetail->misc_fee_toll_2; elseif(isset($_POST['misc_fee_toll_2'])) echo $_POST['misc_fee_toll_2'];else echo 0; ?>">
						</div> 
						</div>
						<div class="form-right">	
					

						<div class="txtbox1"> 
							<label>Misc Fee Toll 2 Notes</label>
							<textarea class="txtarea" rows="2" cols="30" name="misc_fee_toll_2_notes"><?php if(!empty($sd_tripcloseoutdetail->misc_fee_toll_2_notes)) echo $sd_tripcloseoutdetail->misc_discount_notes; elseif(isset($_POST['misc_discount_notes'])) echo $_POST['misc_discount_notes'];?></textarea>
							
							</div> 
						</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php if(form_error('tip_amount')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('tip_amount'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Tip Amount</label>
							<input type="text" class="shadow number_only" name="tip_amount"  id="tip_amount" value="<?php if(!empty($sd_tripcloseoutdetail->tip_amount)) echo $sd_tripcloseoutdetail->tip_amount; elseif(isset($_POST['tip_amount'])) echo $_POST['tip_amount'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						<?php if(form_error('promo_discount')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('promo_discount'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Promo Discount</label>
							<input type="text" class="shadow number_only" name="promo_discount"  id="promo_discount" value="<?php if(!empty($sd_tripcloseoutdetail->promo_discount)) echo $sd_tripcloseoutdetail->promo_discount; elseif(isset($_POST['promo_discount'])) echo $_POST['promo_discount'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-left">	
						<?php if(form_error('misc_discount_amount')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('misc_discount_amount'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Misc Discount</label>
							<input type="text" class="shadow number_only" name="misc_discount_amount"  id="misc_discount_amount" value="<?php if(!empty($sd_tripcloseoutdetail->misc_discount_amount)) echo $sd_tripcloseoutdetail->misc_discount_amount; elseif(isset($_POST['misc_discount_amount'])) echo $_POST['misc_discount_amount'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
					

						<div class="txtbox1"> 
							<label>Misc Discount Notes</label>
							<textarea class="txtarea" rows="5" cols="30" name="misc_discount_notes"><?php if(!empty($sd_tripcloseoutdetail->misc_discount_notes)) echo $sd_tripcloseoutdetail->misc_discount_notes; elseif(isset($_POST['misc_discount_notes'])) echo $_POST['misc_discount_notes'];?></textarea>
							</div> 
						</div>
						
						<div class="form-left">	
						<?php if(form_error('total_due_to_dp')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_due_to_dp'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Total Trip Cost</label>
							<input type="text" class="shadow number_only" name="total_due_to_dp"  id="total_due_to_dp" value="<?php if(!empty($sd_tripcloseoutdetail->total_due_to_dp)) echo $sd_tripcloseoutdetail->total_due_to_dp; elseif(isset($_POST['total_due_to_dp'])) echo $_POST['total_due_to_dp'];else echo 0;?>">
						</div> 
						</div>
						<div class="clear"></div>
						<hr />
						<span class="form-left" style="position: relative; padding-left: 314px; bottom: 18px;">DRIVER FEE</span>
						<div class="form-left">	
						<?php if(form_error('driver_fee_per_hour')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('driver_fee_per_hour'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Driver Fee Per Hour</label>
							<input type="text" class="shadow number_only" name="driver_fee_per_hour"  id="driver_fee_per_hour" value="<?php if(!empty($sd_driverdetail->pay_rate)) echo $sd_driverdetail->pay_rate;elseif(isset($_POST['driver_fee_per_hour'])) echo $_POST['driver_fee_per_hour'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						<?php if(form_error('driver_fee_per_mile')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('driver_fee_per_mile'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Driver Fee Per Mile</label>
							<input type="text" class="shadow number_only" name="driver_fee_per_mile"  id="driver_fee_per_mile" value="<?php if(!empty($sd_driverdetail->pay_rate_permile)) echo $sd_driverdetail->pay_rate_permile;elseif(isset($_POST['driver_fee_per_mile'])) echo $_POST['driver_fee_per_mile'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-left">	
						<?php if(form_error('driver_fee_airport_dd')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('driver_fee_airport_dd'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Driver Fee Airport/DD</label>
							<input type="text" class="shadow number_only" name="driver_fee_airport_dd"  id="driver_fee_airport_dd" value="<?php if(isset($_POST['driver_fee_airport_dd'])) echo set_value('driver_fee_airport_dd'); else echo 0;?>">
						</div> 
						</div>
						<div class="clear"></div>
						<div class="form-left">	
						<?php if(form_error('driver_misc_fee')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('driver_misc_fee'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Driver Misc Fee</label>
							<input type="text" class="shadow number_only" name="driver_misc_fee"  id="driver_misc_fee" value="<?php if(!empty($sd_tripcloseoutdetail->driver_misc_fee)) echo $sd_tripcloseoutdetail->driver_misc_fee; elseif(isset($_POST['driver_misc_fee'])) echo $_POST['driver_misc_fee'];else echo 0;?>">
						</div> 
						</div>
						<div class="form-right">	
						

						<div class="txtbox1"> 
							<label>Driver Misc Fee Notes</label>
							<textarea class="txtarea" rows="5" cols="30" name="driver_misc_fee_notes"><?php if(!empty($sd_tripcloseoutdetail->driver_misc_fee_notes)) echo $sd_tripcloseoutdetail->driver_misc_fee_notes; elseif(isset($_POST['driver_misc_fee_notes'])) echo $_POST['driver_misc_fee_notes'];?></textarea>
						</div> 
						</div>
						
							<div class="form-left">	
						<?php if(form_error('total_due_to_driver')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('total_due_to_driver'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Total due to driver</label>
							<input type="text" class="shadow number_only" name="total_due_to_driver"  id="total_due_to_driver" value="<?php if(!empty($sd_tripcloseoutdetail->total_due_to_driver)) echo $sd_tripcloseoutdetail->total_due_to_driver; elseif(isset($_POST['total_due_to_driver'])) echo $_POST['total_due_to_driver'];else echo 0;?>">
						</div> 
						</div>
						<div class="clear"></div>
						<hr />
						<span class="form-left" style="position: relative; padding-left: 314px; bottom: 18px;">MISC NOTES</span>
						<div class="form-left">		
						<div class="txtbox1"> 
							<label>Trip Notes</label>
							<textarea class="txtarea" rows="5" cols="30" name="trip_notes"><?php if(!empty($sd_tripcloseoutdetail->trip_notes)) echo $sd_tripcloseoutdetail->trip_notes; elseif(!empty($sd_tripdetail->comments)) echo $sd_tripdetail->comments;  elseif(isset($_POST['trip_notes'])) echo $_POST['trip_notes'];?></textarea>
							</div> 
						</div>
						<div class="form-right">	
						<?php if(form_error('member_age')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('member_age'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Member Age</label>
							<input type="text" class="shadow" name="member_age" id="member_age" maxlength= "3" value="<?php if(!empty($sd_tripcloseoutdetail->member_age)) echo $sd_tripcloseoutdetail->member_age; elseif(isset($_POST['member_age'])) echo $_POST['member_age']; ?>">
						</div> 
						</div>
						<div class="form-right clear">	
						<div class="txtbox1 "> 
							<label>&nbsp;</label>
							<span><input  class="update_record btn btn-primary btn-large" id="update_record"  type="submit" name="adddetails" value="Update"></span>
							<span>
		
							<a class="btn btn-primary btn-large" href="<?php echo base_url();?>admin/completedtrips/clogs">Close Out log</a></span>
						
						</div>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span><input  class="btn btn-primary btn-large" id="calc_record"  type="button" name="calc_record" value="Make Calculation"></span>
						</div>
						</div>


					<?php form_close() ; ?>
							
							
							
					
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {

    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
   //$('#trip_date_from').datepicker({ dateFormat: 'yy-mm-dd' });
  // $('#pickup_time').timepicker();
   // $('#trip_date_todate').datepicker({ dateFormat: 'yy-mm-dd' });
  // $('#trip_date_to').timepicker();
	//$('#total_time').timepicker();
 });
 
 </script>
 <script>
$(document).ready(function() {

	var d = new Date();
	var year = d.getFullYear();
	d.setFullYear(year);
    // assuming the controls you want to attach the plugin to 
    // have the "datepicker" class set
   $('#trip_date_from').datepicker({dateFormat: 'mm/dd/yy', changeYear: true, changeMonth: true,yearRange: '1900:' + year+' ',defaultDate:d});


	//$('#pickup_time').timepicker();
	 /*jQuery('#pickup_time').timeEntry({ampmPrefix: ' '}).change(function() { 
		    var log = $('#log'); 
		    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	  });*/
	
	<?php //if(!empty($tripdetail->trip_date_to)) echo $dofftime; ?>
	

	
	
    $('#trip_date_todate').datepicker({dateFormat: 'mm/dd/yy', changeYear: true, changeMonth: true, yearRange: '1920:' + year + '',defaultDate:d});
	//$('#trip_date_to').timepicker();
	 /*jQuery('#trip_date_to').timeEntry({ampmPrefix: ' '}).change(function() { 
		    var log = $('#log'); 
		    log.val(log.val() + ($('#defaultEntry').val() || 'blank') + '\n'); 
	  });*/
	
	//$('#total_time').timepicker();
 });
 
 </script>
 <script type="text/javascript">
$(function() {	
	 // To hide error message
	setInterval(function(){
		$('.alert-success').css('visibility', 'hidden');
	},10000);
	$(".update_record").click(function() {
		var val=$("#update_record").val();
		var r=confirm("Do you really want to update this record");
		if (r==true)
		  {
			$.ajax({
				url:'/ores/admin/completedtrips/tripcloseoutdetail',
				data: 'id='+val,
				type:'POST',
				beforeSend: function() {
					// alert("loading")
				},
				success:function(data){
					$('.alert-success').css('visibility', 'visible');
					  $('#'+val).remove();
				}
			});
		  }
		else
		  {
			  return false;
		  }
	});
});
</script>
 <script>
 $(document).ready(function() {
     function convertTo24Hour(time) {
		    var hours = parseInt(time.substr(0, 2));
		    if(time.indexOf('am') != -1 && hours == 12) {
		        time = time.replace('12', '0');
		    }
		    if(time.indexOf('pm')  != -1 && hours < 12) {
		        time = time.replace(hours, (hours + 12));
		    }
		    return time.replace(/(am|pm)/, '');
		}
	
	 function calculation(mile_fee,hour_fee,airport_dd){
			var pickUp_date=$('#trip_date_from').val();
			var dropOff_date=$('#trip_date_todate').val();	
			var pickUp_time=convertTo24Hour($('#pickup_time').val() + $('#am_pm').val().toLowerCase());
			var dropOff_time=convertTo24Hour($('#trip_date_to').val() + $('#am_pm1').val().toLowerCase());
			pickUp_date=pickUp_date+' '+ pickUp_time;
			dropOff_date=dropOff_date+' '+ dropOff_time;
			var date1 = new Date(pickUp_date);
			var date2 = new Date(dropOff_date);
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			if(!isNaN(timeDiff))
			{
			var total_hours = Math.abs(timeDiff / (1000 * 3600)); 
			}
			else{
				var total_hours=0;
			}
			var total_hour = ""+ total_hours +"";
		
			 var posi = total_hour.indexOf(".");
			if(posi!== -1){
			var hour = total_hour .substr(0,posi);
			console.log(hour);
			var mint=total_hour.substr(posi+1,2);
			console.log(mint);
			mint=Math.ceil(mint*(6/10));
			if(hour==''){
				hour='00';
			}
			var total_time=hour+':'+mint;
			//console.log(total_time);
			$('#total_time').val(total_time);
			}
			else{
			$('#total_time').val(total_hours+':'+'00');
			}	
		  	var mileageend = ($('#mileage_end').val()=='') ? 0:parseFloat($('#mileage_end').val());
		
			var mileagestart = ($('#mileage_start').val()=='') ? 0:parseFloat($('#mileage_start').val());
	
			var totalmiles =parseFloat(mileageend - mileagestart);
			
			$('#total_miles').val(totalmiles.toFixed(2));

			var feepermile = ($('#fee_per_mile').val()=='') ? 0:parseFloat($('#fee_per_mile').val());
			if(mile_fee==true  || airport_dd==true){
				var totalmileagefee = Math.abs(totalmiles * feepermile);
				var total_hourly_fee = 0;
			}
			else{
				var totalmileagefee = 0;
			}
			//alert(totalmileagefee);
			var hourly_rate=($('#hourly_rate').val()=='') ? 0:parseFloat($('#hourly_rate').val());
			
			//var total_hours=document.getElementsByName("total_hours")[0].value;

	
			if(hour_fee==true){
				var total_hourly_fee= Math.abs(hourly_rate*total_hours);
				var totalmileagefee = 0;
			}
			else{
				var total_hourly_fee= 0;
			}
			
			$('#total_hourly_fee').val(total_hourly_fee.toFixed(2));
			var misc_toll_fee1=($('#misc_fee_toll_1').val()=='') ? 0:parseFloat($("#misc_fee_toll_1").val());
			var misc_toll_fee2=($('#misc_fee_toll_2').val()=='') ? 0:parseFloat($("#misc_fee_toll_2").val());
			var tip_amount=($('#tip_amount').val()=='') ? 0:parseFloat($("#tip_amount").val());
			var promo_discount=($('#promo_discount').val()=='') ? 0:parseFloat($("#promo_discount").val());
			var misc_discount_amount=($('#misc_discount_amount').val()=='') ? 0:parseFloat($("#misc_discount_amount").val());
			//alert(misc_discount_amount);
			var total_due_to_dp=Math.abs(total_hourly_fee+totalmileagefee+misc_toll_fee1+misc_toll_fee2+ tip_amount-promo_discount-misc_discount_amount);
			//alert(total_due_to_dp);
			$("#total_due_to_dp").val(total_due_to_dp.toFixed(2));
			if(hour_fee==true){
				var driver_fee_per_hour=($('#driver_fee_per_hour').val()=='') ? 0:parseFloat($("#driver_fee_per_hour").val());
				var  driver_fee_per_mile = 0;
			}
			else{
				var driver_fee_per_hour= 0;
			}
			if(mile_fee==true || airport_dd==true){
				var driver_fee_per_mile=($('#driver_fee_per_mile').val()=='') ? 0:parseFloat($("#driver_fee_per_mile").val());
				driver_fee_per_hour= 0;
			}
			else{
				 driver_fee_per_mile = 0;
			}
			
			var driver_misc_fee=($('#driver_misc_fee').val()=='') ? 0:parseFloat($("#driver_misc_fee").val());
			var total_due_to_driver=Math.abs(driver_fee_per_hour*total_hours)+(totalmiles*driver_fee_per_mile)+tip_amount+misc_toll_fee1+misc_toll_fee2+driver_misc_fee;
			$("#total_due_to_driver").val(total_due_to_driver.toFixed(2));	 

			if(airport_dd==true){
				var driver_fee_airport_dd=($('#driver_fee_airport_dd').val()=='') ? 0:parseFloat($("#driver_fee_airport_dd").val());
				var client_fee_airport_dd=($('#client_fee_airport_dd').val()=='') ? 0:parseFloat($("#client_fee_airport_dd").val());
				var total_due_to_dp=Math.abs(client_fee_airport_dd+totalmileagefee+misc_toll_fee1+misc_toll_fee2+ tip_amount-promo_discount-misc_discount_amount);
				var total_due_to_driver=(driver_fee_airport_dd)+(totalmiles*driver_fee_per_mile)+tip_amount+misc_toll_fee1+misc_toll_fee2+driver_misc_fee;
				$("#total_due_to_dp").val(total_due_to_dp.toFixed(2));
				$("#total_due_to_driver").val(total_due_to_driver.toFixed(2));	 
				} 
	 } 
	 

  $('#calc_record').click(function(){
	  ShowDialogBox('Trip Calculation','How do you like to charge user?','Per Hour','Per Mile','Airport/DD','Cancel',null,null); 
  });
  function ShowDialogBox(title, content, btn1text, btn2text,btn4text,btn3text, functionText, parameterList) {
      var btn1css;
      var btn2css;
	  var btn3css;
	  var btn4css;		
      if (btn1text == '') {
          btn1css = "hidecss";
      } else {
          btn1css = "showcss";
      }

      if (btn2text == '') {
          btn2css = "hidecss";
      } else {
          btn2css = "showcss";
      }
      if (btn3text == '') {
    	  btn3css = "hidecss";
      } else {
    	  btn3css = "showcss";
      }
      if (btn4text == '') {
    	  btn4css = "hidecss";
      } else {
    	  btn4css = "showcss";
      }
      $("#lblMessage").html(content);

      $("#dialog").dialog({
          resizable: false,
          title: title,
          modal: true,
          width: '400px',
          height: 'auto',
          bgiframe: false,
          hide: { effect: 'scale', duration: 400 },

          buttons: [
                          {
                              text: btn1text,
                              "class": btn1css,
                              click: function () {
                            	  var mile_fee=false;
                        		  var hour_fee=true;
                        		  var airport_dd=false;
                        		  calculation(mile_fee,hour_fee,airport_dd);	 
                                  $("#dialog").dialog('close');

                              }
                          },
                          {
                              text: btn2text,
                              "class": btn2css,
                              click: function () {
                            	  var mile_fee=true;	  
                          		  var hour_fee=false;
                            	  var airport_dd=false;
                          		  calculation(mile_fee,hour_fee,airport_dd);		  
                          		  $( this ).dialog( "close" );
                              }
                          },
                          {
                              text: btn4text,
                              "class": btn4css,
                              click: function () {
                            	  var mile_fee=false;	  
                          		  var hour_fee=false;
                          		  var airport_dd=true;
                          		  calculation(mile_fee,hour_fee,airport_dd);		  
                          		  $( this ).dialog( "close" );
                              }
                          },
                          {
                              text: btn3text,
                              "class": btn3css,
                              click: function () {	  
                          		  $( this ).dialog( "close" );
                              }
                          }
                      ]
      });
  }

  /*jQuery("#mileage_end").keypress(function(){
	    var value = jQuery(this).val();
	    value = value.replace(/[^0-9]+/g, '');
	    jQuery(this).val(value);
	});*/
}); 
 </script>

  <!--   confirm box html -->
   <div id="dialog" title="Alert message" style="display: none">
        <div class="ui-dialog-content ui-widget-content">
            <p>
                <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0"></span>
                <label id="lblMessage">
                </label>
            </p>
        </div>
    </div>