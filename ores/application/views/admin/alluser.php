 <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?>
			</div>
			 <div class="span9" id="content">

             
					
                    <div class="row-fluid">
					<?php $msg = $this->session->flashdata('msg');?>
					<?php if($msg):?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
					</div>
					<?php endif;?>
						<div class="alert alert-msg" style="visibility: hidden">
					</div>
					
                        <!-- block -->


	
		<table style="float:left;">
			<tr>
				<td><form style="margin:0px;" action ="<?php echo base_url()?>admin/home/search_user" method="post" id="searchform">
						<input type="text" name="searchterm" id="searchterm" value="" placeholder="Search User By Email or Name" />&nbsp;
						<input style="vertical-align:top;" class="btn btn-inverse" type="submit" value="Search" id="submit" /></form></td>
				<td>&nbsp;</td>
			</tr>
		
		</table>
		<table style="float:right;">
			<tr>
				
				<td ><a class="btn"  href="<?php echo base_url();?>admin/home/add_user">+ Add New User</a></td>
			</tr>
		
		</table>
		<div style="clear:both;"></div>
						
					
                        
						

						<div style="clear:both;"></div>
                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Users</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
  									<table class="table">
						              <thead>
						                <tr>
						        <th class="ck_field" align="left">Id #</th>		
								<th align="left">Username</th>		
								<th align="left">First Name</th>		
								<th align="left">Last Name</th>		
								<th align="left">Email Address</th>		
								<th align="left">Mobile Number</th>		
								<th align="left">City</th>		
								
								<th align="left">Action</th>
						                </tr>
						              </thead>
						              <tbody>
						               <?php  
									if(!empty($user)){
								
									//$serial_no=1;
								foreach($user as $user_name){ 	?>		
									<tr id="<?php echo $user_name->id; ?>"> 
										<td><?php  echo $user_name->id; ?></td>				
										<td><?php   $len=strlen($user_name->username); 
if($len<=15){
echo ucwords($user_name->username);}
else { echo substr(ucwords($user_name->username),0,13).'..'; }?></td>
										<td><?php  echo ucwords($user_name->first_name); ?></td>
										<td><?php  echo ucwords($user_name->last_name); ?></td>
										<td><?php  echo $user_name->email; ?></td>
										<td><?php  echo $user_name->mobile_number; ?></td>
										<td><?php  echo $user_name->city; ?></td>
									
										<td>
										<span>
											   
												<a href="<?php echo base_url(); ?>admin/home/edit_user/<?php echo $user_name->id; ?>"><img src="<?php echo base_url();?>images/icons/edit.png" title="Edit"></a>
											</span> &nbsp;
											<span>
												 <input id="<?php echo $user_name->id; ?>" class="remove_record"  type="image" value=""   src="<?php echo base_url();?>images/icons/delete.png" title="Delete">
											</span>										</td>
									</tr><?php 
									//$serial_no++;
								} 
							}else{?>
						
							<tr><td style='text-align:center' colspan="8"> No Results Found</td></tr>
							
							<?php }?>
						              </tbody>
						            </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
					<?php echo $this->pagination->create_links(); ?>
					
          <div class="clr"></div>
                    </div>

</div>			
<script src='<?php echo base_url(); ?>js/all_user.js'></script>	
<script src='<?php echo base_url(); ?>js/jquery.json-2.4.js'></script>	
</script>
