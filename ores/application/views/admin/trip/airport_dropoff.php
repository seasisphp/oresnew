 <?php
 /*
  * This file is created to reserve airpot dropoff trip.
  * Author:Devi Lal Verma
  */ 
 // created array variable from session varable.
 $sess_data=$this->session->userdata('data');
 $manaual_shift=$this->session->userdata ( 'manaual_shift');
 ?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
                <!--/span-->
<div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                    <div id="error" class="alert-success"> 
								<?php 
									echo "<strong>".$this->session->flashdata('error')."</strong>";
								?>
					</div>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Airport Drop-Off Reservation</div>
                                 <a class="btn" style="float:right;" href="<?php echo base_url();?>admin/home/add_user/trip/airport_dropoff">+ Add New User</a>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12">
                        
										
					<?php echo form_open(); ?>	
					<input type="hidden" value="3" id="service_id" name="service_id">
						<div class="form-left">		
							<?php if(form_error('first_name')) { ?>
									
							<div class="txtbox1">

							<span class="validation" style="color: #FF0000;"><?php echo form_error('first_name'); ?></span>
							</div> <?php } ?>
											
							<div class="txtbox1">
							<label>First Name<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" maxlength="20"
							class="shadow" name="first_name" id="first_name"
							maxlength="20" value="<?php  if(!empty($sess_data)){echo $sess_data['first_name'];}else{echo set_value('first_name');} ?>">
							</div>
						</div>
						<div class="form-right">	
							<?php if(form_error('last_name')) { ?>							
							<div class="txtbox1">
	
							<span class="validation" style="color: #FF0000;"><?php echo form_error('last_name'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1">
							<label>Last Name<span class="validation" style="color:#FF0000;">*</span></label> <input type="text" maxlength="20"
							class="shadow" name="last_name" id="last_name"
							maxlength="20" value="<?php  if(!empty($sess_data)){echo $sess_data['last_name'];}else{ echo set_value('last_name');} ?>">
							</div>
						</div>
						<!--<div class="form-left">	
						<?php if(form_error('contact_name')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_name'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Contact Name<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="contact_name" id="contact_name" value="<?php if(!empty($sess_data)){echo $sess_data['first_name'].' '.$sess_data['last_name'];}else{echo set_value('contact_name');}  ?>">
						</div>
						</div>-->
					    <div class="form-left">	
						<?php if(form_error('contact_number')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('contact_number'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Contact Number<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="12" name="contact_number" id="contact_number" value="<?php  if(!empty($sess_data)){echo $sess_data['mobile_number'];}else{echo set_value('contact_number');} ?>">
						</div>
						</div>
						<div class="form-right">	
						<?php if(form_error('trip_date_from')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_from'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Pick Up Date<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow datetime" readonly="readonly" id="trip_date_from" name="trip_date_from" value="<?php echo set_value('trip_date_from'); ?>">
						</div>
						</div>
						<!-- <div class="form-left">	
						<?php if(form_error('pickup_time')) { ?>
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
						<label>Pick Up Time<span class="validation" style="color:#FF0000;">*</span></label>
						<input type="text" class="shadow datetime" placeholder="HH:MM" name="pickup_time" id="pickup_time" readonly="readonly" value="<?php echo set_value('pickup_time'); ?>">
						</div>
						</div>-->
						
							<div class="form-left">	
								  <?php if(form_error('pickup_time')) { ?>
									<div class="txtbox1"> 
									<span class="validation" style="color:#FF0000;"><?php echo form_error('pickup_time'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
									<label>Pick Up Time<span class="validation" style="color:#FF0000;">*</span></label>
						          	<select name="pickup_time" id="pickup_time" class="" style="width:110px;">
									   <option value="">Select Time</option>
								        <?php 
								         $a = 00;
								         $b=00;
								         $sel='';
								         for($a=1;$a<=12;$a++){
											for($b=00;$b<60;$b=$b+15)
											{
												if($b==0){
													$b='00';
													}
													$timeVal=set_value('pickup_time');
													if(!empty($timeVal) &&  $timeVal == $a.':'.$b){
														$sel='selected';
														}
														else{
															$sel='';
															}		   
								         ?>
							            <option value="<?php echo $a.':'.$b;?>" <?php echo $sel;?>><?php echo $a.':'.$b;?></option>
							            <?php } }?>
						          	</select> 
						            <select id="am_pm" name="am_pm" class="" style="width:85px;" >
						             <?php $am_pm=$_POST['am_pm'];?>	   
						            <option value="AM" <?php if($am_pm == 'AM'){ echo 'selected';}?>>AM</option>
						            <option value="PM" <?php if($am_pm == 'PM'){ echo 'selected';}?>>PM</option>
						          	</select>
									</div>
								 </div>
						<div class="form-right">	
						<?php if(form_error('address')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Pick Up Location<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text"  class="shadow" name="address" id="address" value="<?php if(!empty($sess_data))echo $sess_data['address_1']; else echo set_value('address'); ?>">
						</div>
						</div>
						<div class="clear"></div>
						<div class="form-left">
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>City<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text"  class="shadow" name="city" id="city" value="<?php  if(!empty($sess_data)){echo $sess_data['city'];}else{echo set_value('city');} ?>">
						</div>	
						</div>
						<div class="form-right">	
						<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text"  class="shadow" name="state" id="state" value="<?php if(!empty($sess_data)){echo $sess_data['state'];}else{echo set_value('state');} ?>">
						</div>
						</div>
						<div class="form-left">		
						<?php if(form_error('zip')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="zip" id="zip" value="<?php  if(!empty($sess_data)){echo $sess_data['zip_code'];}else{echo set_value('zip');} ?>">
						</div>
						</div>
						<div class="form-right">		
				      	<?php if(form_error('airport')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airport'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Airport<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text"  class="shadow" name="airport" id="airport" value="<?php echo set_value('airport'); ?>">
						</div>
						</div>
						<!--<div class="form-left">	
						<?php if(form_error('airline')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('airline'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Airline<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="airline" id="airline" value="<?php echo set_value('airline'); ?>">
						</div>
						</div>
						<div class="form-right">	
						<?php if(form_error('flight_number')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('flight_number'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Flight No<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="flight_number" id="flight_number" value="<?php echo set_value('flight_number'); ?>">
						</div>
						</div>-->
					
						<!--<div class="form-right">	
						<?php if(form_error('trip_date_to')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('trip_date_to'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Drop Off Date<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="trip_date_to" id="trip_date_to" value="<?php echo set_value('trip_date_to');  ?>">
						</div>
						</div>-->
						<!-- <div class="form-left">	
						<?php if(form_error('dropoff_time')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('dropoff_time'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Drop Off Time<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="dropoff_time" id="dropoff_time" value= " <?php  echo set_value('dropoff_time'); ?>">
						</div>
						</div>-->
						<!--<div class="form-left">	
						<?php if(form_error('approx_hours')) { ?>							
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('approx_hours'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Approx. Length Of Trip (in Hours)<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="approx_hours" id="approx_hours" value="<?php echo set_value('approx_hours'); ?>">
						</div>
						</div>-->
						
						<div class="form-left">	
						<?php if(form_error('comments')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('comments'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Trip Notes</label>
						<input type="text" class="shadow" name="comments" id="comments" value="<?php echo set_value('comments'); ?>">
						</div>
						</div>
						<div class="form-left">	
						<?php if(form_error('promo_code')) { ?>							
						<div class="txtbox1"> 
						<label>&nbsp;</label>
						<span class="validation" style="color:#FF0000;"><?php echo form_error('promo_code'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
							<label>Promo Code</label>
						<input type="text" class="shadow" name="promo_code" id="promo_code" value="<?php echo set_value('promo_code'); ?>">
						</div>
						</div>
						<div class="form-left">
							<?php if(form_error('manaual_shift')) { ?>							
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('manaual_shift'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
									<label>Manual Shift<span class="validation" style="color:#FF0000;">*</span></label>
									<p style="height: 34px;"> <span>
									<input style="vertical-align:top;"type="radio" name="manaual_shift" id="manaual_shift_y" value="2" <?php if (isset ($manaual_shift) && $manaual_shift==2 ){ echo "checked";}?> />
									<span style="vertical-align: bottom;"> YES</span></span> <span>
									<input style="vertical-align:top;" type="radio"  name="manaual_shift" id="manaual_shift_n" value="1" <?php if (isset ($manaual_shift) && $manaual_shift==1 ){ echo "checked";}?>/>
									<span style="vertical-align: bottom;">NO</span>  </span></p> 
									</div>
						</div>
						<div class="form-right clear">	
						<div class="txtbox1"> 
							<label>&nbsp;</label>
								<input class="btn btn-primary btn-large" type="submit" name="trip_reservation" value="Submit">
						</div>
					<?php echo form_close(); $this->session->unset_userdata('data');  ?>
				</div>
				</div>
			</td>
		</tr>
	</table>
</div>

 <script src='<?php echo base_url(); ?>js/trip.js'></script>	
