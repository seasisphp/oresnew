 <?php
 /*
  * This file is created to display all driver list.
  * Author:Devi Lal Verma
  */ 
 ?>
 <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
				<!-- sideBatr goes here --> 
				<?php $this->load->view('template/admin_sidebar'); ?>
			</div>
			 <div class="span9" id="content">

                    
                    <div class="row-fluid">
                    <?php $msg = $this->session->flashdata('msg');?>
					<?php if($msg):?>
					<div class="alert alert-success">
					 <?php echo $this->session->flashdata('msg'); ?>
					</div>
					<?php endif;?>
					<div class="alert alert-msg" style="visibility: hidden;">
					
					</div>
				
                        <!-- block -->
      <table style="float:left;">
						<tr>
							<td>
							<form style="margin:0px;" action ="<?php echo base_url()?>admin/driver/search_driver" method="post" id="searchform">
						<input placeholder="Search Driver By Email or Name " type="text" name="searchterm" id="searchterm" value="" />&nbsp;
						<input style="vertical-align:top;" class="btn btn-inverse"  type="submit" value="Search" id="submit" /></form>
							</td>
							<td>&nbsp;</td>
						</tr>
		
		</table>
		<table style="float:right;">
			<tr>
				
				<td >
				<a class="btn"  href="<?php echo base_url();?>admin/driver/add_driver">+ Add New Driver</a>
					&nbsp;<a class="btn btn-success"  href="<?php echo base_url();?>admin/driver/download_driver">
					Export Driver</a></td>
			</tr>
		
		</table>				
						
						
						<div style="clear:both;"></div>
                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Driver</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
  									<table class="table">
						              <thead>
						                <tr>
						        <th class="ck_field" align="left">Id #</th>			
								<th align="left">First Name</th>		
								<th align="left">Last Name</th>		
								<th align="left">Email Adress</th>		
								<th align="left">Mobile Number</th>		
								<th align="left">City</th>		
								
								<th align="left">Action</th>
						                </tr>
						              </thead>
						              <tbody>
						               <?php  
							if(!empty($driver)){
								$serial_no=1;
								foreach($driver as $driver_name){?>		
									<tr id="<?php echo $driver_name->id; ?>"> 
										<td><?php  echo $serial_no; ?></td>				
										<td><?php  echo ucwords($driver_name->first_name); ?></td>
										<td><?php  echo ucwords($driver_name->last_name); ?></td>
										<td><?php  echo $driver_name->email; ?></td>
										<td><?php  echo $driver_name->mobile_number; ?></td>
										<td><?php  echo $driver_name->city; ?></td>
									
										<td>
											
											
											<span>
												<a href="<?php echo base_url(); ?>admin/driver/edit_driver/<?php echo $driver_name->id; ?>"><img src="<?php echo base_url();?>images/icons/edit.png" title="Edit"></a>
											</span> &nbsp;
											<span>
												<input id="<?php echo $driver_name->id; ?>" class="remove_record"  type="image" value=""   src="<?php echo base_url();?>images/icons/delete.png" title="Delete">
											</span>&nbsp;
												<span>
											</span>
										</td>
										
									
									</tr><?php 
									$serial_no++;
								} 
							}else{?>
						
							<tr><td style='text-align:center' colspan="8"> No Results Found</td></tr>
							
							<?php }?>
						              </tbody>
						            </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                        <?php echo $this->pagination->create_links(); ?>
                    </div>

</div>		

<script src='<?php echo base_url(); ?>js/all_driver.js'></script>	