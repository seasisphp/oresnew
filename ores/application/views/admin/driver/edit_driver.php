 <?php
 /*
  * This file is created to edit driver detail and update.
  * Author:Devi Lal Verma
  */ 
 $driver=$this->session->userdata('driver_record');
 $address_title = $this->session->userdata ( 'address_title' );
 $country = $this->session->userdata ('country');
 $status =$this->session->userdata('driverstatus');
 ?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
                <!--/span-->
<div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Edit Driver</div>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12">
										<div id="error"> 
										<?php 
											$errors = validation_errors();
											if(!empty($wrong_insert)){echo $wrong_insert;}
											//if(!empty($error)){echo $error;}
										?>
										<?php echo form_open();  ?>
						<div class="form-left">	
							<?php if(form_error('first_name')) { ?>
									
								<div class="txtbox1"> 
									<label>&nbsp;</label>
									<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
								</div> <?php } ?>
								
								<div class="txtbox1"> 
									<label>First Name<span class="validation" style="color:#FF0000;">*</span></label>
									<input type="text" class="shadow" name="first_name" maxlength="20" id="first_name" value="<?php if(!empty($driver->first_name)) echo $driver->first_name; else echo set_value('first_name');?>">
								</div>	
						</div>
						<div class="form-right">	
							<?php if(form_error('last_name')) { ?>							
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1"> 
								<label>Last Name<span class="validation" style="color:#FF0000;">*</span></label>
								<input type="text" class="shadow" name="last_name" maxlength="20" id="last_name" value="<?php if(!empty($driver->last_name)) echo $driver->last_name; else echo set_value('last_name'); ?>">
							</div>
						</div>
						<div style="clear:both;"></div>
						<div class="form-left">	
						<?php
						if(form_error('username')) { ?>						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							
							<label>Username<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="20" name="username" id="username" value="<?php if(!empty($driver->username)) echo $driver->username; else echo set_value('username');?>">
						</div>
						</div>
						<div class="form-right">	
							<?php if(form_error('email')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
							</div> <?php } ?>
						</div>	
					<div class="form-left">	
						<div class="txtbox1"> 
							<label>Email<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" id="email" name="email" value="<?php if(!empty($driver->email)){ echo $driver->email; } else {echo set_value('email');}?>">
						</div>
					</div>
			
				<!-- <div class="form-left">						
						<?php if(form_error('company_name')) { ?>
										<div class="txtbox1">
	
											<span class="validation" style="color: #FF0000;"><?php echo form_error('company_name'); ?></span>
										</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Company Name</label>
							<input type="text" class="shadow" maxlength="20" name="company_name" id="company_name" value="<?php if(!empty($driver->company_name)) echo $driver->company_name; echo set_value('company_name');?>">
						</div>
				</div>  -->
				<div class="form-left">					
						<?php if(form_error('mobile_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Mobile Number<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="12" name="mobile_number" id="mobile_number" value="<?php if(!empty($driver->mobile_number)) echo $driver->mobile_number; echo set_value('mobile_number');?>">
						</div>
					</div>
				<div class="form-right">	
					<?php if(form_error('emergency_contact')) { ?>
					<div class="txtbox1">
	
					<span class="validation" style="color: #FF0000;"><?php echo form_error('emergency_contact'); ?></span>
					</div> <?php } ?>
								
					<div class="txtbox1">
					<label>Emergency Contact</label> <input type="text"
					class="shadow" name="emergency_contact" id="emergency_contact"
					maxlength="12" value="<?php if(!empty($driver->emergency_contact)) echo $driver->emergency_contact; echo set_value('emergency_contact'); ?>">
					</div>
				</div>
				<div class="form-left">		
						<?php if(form_error('address_title')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
						</div> <?php } ?>
						<div class ="txtbox1">
						<label>Address Title<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						''  => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); 
						if (!empty( $address_title )) {
							$index = $address_title;
							}
						elseif(!empty($driver->address_title)){
							$index = $driver->address_title;
						}	
						else {
							$index = 0;
							}
						echo form_dropdown ( 'address_title', $options, $index, 'class="shadow"' );?>
						</div>
				</div>
				<div class="form-right">	
						<?php if(form_error('address_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 1<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="50" name="address_1" id="address_1" value="<?php if(!empty($driver->address_1)) echo $driver->address_1; echo set_value('address_1');?>">
						</div>
				</div>
				<div class="form-left">	
							<?php if(form_error('address_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 2<span class="validation" style="color:#FF0000;"></span></label>
							<input type="text" class="shadow" maxlength="50" name="address_2" id="address_2" value="<?php if(!empty($driver->address_2)) echo $driver->address_2; echo set_value('address_2');?>">
						</div> 
				</div>
				<div class="form-right">	
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>City<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="30" name="city" id="city" value="<?php if(!empty($driver->city)) echo $driver->city; echo set_value('city');?>">
						</div> 
				</div>
				<div class="form-left">	
						<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="30" name="state" id="state" value="<?php if(!empty($driver->state)) echo $driver->state; echo set_value('state');?>">
						</div> 
				</div>
				<!--<div class="form-left">		
						<?php if(form_error('country')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('country'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Country<span class="validation" style="color:#FF0000;">*</span></label>
							<select id="country" class="shadow" name="country" id='country'>
							<option value="">-- Please select --</option>
						<?php 
						/* $ctrb   = $this->db->get('country');
						foreach($ctrb->result() as $ctrb){
							if(!empty($driver->country)){
							if($ctrb->CountryID==$driver->country){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->CountryID."' $sel>". $ctrb->CountryName."</option>";
							}
							else{
								if($ctrb->CountryID==$country){
									$sel='selected="selected"';
								}else{$sel='';
								}
								echo "<option value='".$ctrb->CountryID."' $sel>". $ctrb->CountryName."</option>";
							}
						} */
						?>
						</select>
						</div> 
					</div>-->
					<div class="form-right">		
						<?php if(form_error('zip_code')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip Code<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" maxlength="10" name="zip_code" id="zip_code" value="<?php if(!empty($driver->zip_code)) echo $driver->zip_code; echo set_value('zip_code');?>">
						</div> 
					</div>
						<div class="form-left">
							<?php if(form_error('pay_rate')) { ?>							
							<div class="txtbox1">
	
							<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1">
							<label>Pay Rate Per Hour<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
							class="shadow" name="pay_rate" id="pay_rate"
							maxlength="10" value="<?php if(!empty($driver->pay_rate)) echo $driver->pay_rate; echo set_value('pay_rate'); ?>">
							</div>
					</div>
					<!--pay rate per mile-->
						<div class="form-right">
							<?php if(form_error('pay_rate_permile')) { ?>							
							<div class="txtbox1">
	
							<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate_permile'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1">
							<label>Pay Rate Per Mile<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
							class="shadow" name="pay_rate_permile" id="pay_rate_permile"
							maxlength="10" value="<?php if(!empty($driver->pay_rate_permile)) echo $driver->pay_rate_permile; echo set_value('pay_rate_permile'); ?>">
							</div>
					</div>
					
					
					<!---->
					<div class="form-left">
						<?php if(form_error('driver_note')) { ?>							
						<div class="txtbox1">
	
						<span class="validation" style="color: #FF0000;"><?php echo form_error('driver_note'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1">
						<label>Driver Note<span class="validation" style="color:#FF0000;"></span></label> <textarea
						class="shadow" name="driver_note" id="driver_note"><?php if(!empty($driver->driver_note)) echo $driver->driver_note; echo set_value('driver_note'); ?></textarea>
						</div>
					</div>
					
					<div class="form-right">
						<?php if(form_error('mobile_provider')) { ?>							
						<div class="txtbox1">
	
						<span class="validation" style="color: #FF0000;"><?php echo form_error('mobile_provider'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1">
						<label>Mobile Provider<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
						class="shadow" name="mobile_provider" id="mobile_provider"
						 value="<?php if(!empty($driver->mobile_provider)) echo $driver->mobile_provider; echo set_value('mobile_provider'); ?>">
						</div>
					</div>
					
					<!-- <div class="form-right">
						<?php if(form_error('status')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Status<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						'' => '-- Please select --',
						'1' => 'Active',
						'2' => 'Inactive',
					
						);
						if (!empty( $status )) {
							$index = $status;
							}
							elseif(!empty($driver->status)){
								$index =$driver->status;
							}else {
								$index = 0;
							}
								
						 echo form_dropdown('status', $options, $index);?>
						</div>
					</div>-->	
						<div class="txtbox1 form-right clear"> 
							<label>&nbsp;</label>
							<input  class="btn btn-primary btn-large" type="submit" name="update_driver" value="Update">
						</div>


					<?php form_close() ; ?>
	 
				</div>
			</td>
		</tr>
	</table>
</div>
 <script src='<?php echo base_url();?>js/phone_mask2.js'></script>	