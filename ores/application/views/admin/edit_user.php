<?php 
$user=$this->session->userdata('user_record');
$billing_detail=$this->session->userdata('billing_detail');
$address_title = $this->session->userdata ( 'address_title' );
$country = $this->session->userdata ('country');
$status =$this->session->userdata('userstatus');
?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
                <!--/span-->
<div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Edit User</div>
                            </div>
							 <div class="block-content collapse in">
                                <div class="span12">
										<div id="error"> 
										<?php 
											$errors = validation_errors();
											if(!empty($wrong_insert)){echo $wrong_insert;}
											//if(!empty($error)){echo $error;}
										?>
										<?php echo form_open();  ?>
						<div class="form-left">	
							<?php if(form_error('first_name')) { ?>
									
								<div class="txtbox1"> 
									<label>&nbsp;</label>
									<span class="validation" style="color:#FF0000;"><?php echo form_error('first_name'); ?></span>
								</div> <?php } ?>
								
								<div class="txtbox1"> 
									<label>First Name<span class="validation" style="color:#FF0000;">*</span></label>
									<input type="text" class="shadow" name="first_name" id="first_name" maxlength= "20" value="<?php if(!empty($user->first_name)) echo $user->first_name; else echo $_POST['first_name'];?>">
								</div>	
						</div>
						<div class="form-right">	
							<?php if(form_error('last_name')) { ?>							
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('last_name'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1"> 
								<label>Last Name<span class="validation" style="color:#FF0000;">*</span></label>
								<input type="text" class="shadow" name="last_name" id="last_name" maxlength= "20" value="<?php if(!empty($user->last_name)) echo $user->last_name; else echo $_POST['last_name'];?>">
							</div>
						</div>
						<div style="clear:both;"></div>
						<div class="form-left">	
						<?php
						if(form_error('username')) { ?>						
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('username'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							
							<label>Username<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="username" id="username" maxlength= "10" value="<?php if(!empty($user->username)) echo $user->username; else echo $_POST['username'];?>">
						</div>
						</div>
						<div class="form-right">	


						<div class="txtbox1"> 
							<label>Password</label>
							<input type="password" class="shadow" id="password" name="password" value="">
						</div>
					</div>
						<div class="form-left">	
							<?php if(form_error('email')) { ?>
							<div class="txtbox1"> 
								<label>&nbsp;</label>
								<span class="validation" style="color:#FF0000;"><?php echo form_error('email'); ?></span>
							</div> <?php } ?>
						</div>	
					<div class="form-left">	
						<div class="txtbox1"> 
							<label>Email<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" id="email" name="email" value="<?php if(!empty($user->email)) echo $user->email; else echo $_POST['email']; ?>">
						</div>
					</div>
					
				<div class="form-right">						
			<?php if(form_error('company_name')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('company_name'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Company Name</label>
							<input type="text" class="shadow" name="company_name" id="company_name" maxlength= "100" value="<?php if(isset($user->company_name)) echo $user->company_name; else echo $_POST['company_name']; ?>">
						</div>
				</div>
				<div class="form-left">					
						<?php if(form_error('mobile_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('mobile_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Mobile Number<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="mobile_number" id="mobile_number" maxlength= "12" value="<?php if(!empty($user->mobile_number)) echo $user->mobile_number; else echo $_POST['mobile_number'];?>">
						</div>
					</div>
				<div class="form-right">		
						<?php if(form_error('other_number')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('other_number'); ?></span>
						</div> <?php } ?>
						
						<div class="txtbox1"> 
							<label>Other Number</label>
							<input type="text" class="shadow" name="other_number" id="other_number" maxlength= "12" value="<?php if(isset($user->other_number)) echo $user->other_number; else echo $_POST['other_number']; ?>">
						</div>
				</div>	
				<div class="form-left">		
						<?php if(form_error('address_title')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_title'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Address Title<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						''  => '-- Please select --',
						'1' => 'Home',
						'2' => 'Office',
						'3' => 'Other',
						); 	
						if (!empty( $address_title )) {
							$index = $address_title;
							}
						elseif(!empty($user->address_title)){
							$index = $user->address_title;
						}	
						else {
							$index = 0;
							}echo form_dropdown('address_title', $options,$index);?>
						</div>
				</div>
				<div class="form-right">	
						<?php if(form_error('address_1')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 1<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="address_1" id="address_1" maxlength="50" value="<?php if(!empty($user->address_1)) echo $user->address_1; else echo $_POST['address_1']; ?>">
						</div>
				</div>
				<div class="form-left">	
							<?php if(form_error('address_2')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Address Line 2</label>
							<input type="text" class="shadow" name="address_2" id="address_2" maxlength="255" value="<?php if(isset($user->address_2)) echo $user->address_2; else echo $_POST['address_2']; ?>">
						</div> 
				</div>
				<div class="form-right">	
						<?php if(form_error('city')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('city'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>City<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="city" id="city" maxlength="30" value="<?php if(!empty($user->city)) echo $user->city; else echo $_POST['city']; ?>">
						</div> 
				</div>
				<div class="form-left">	
						<?php if(form_error('state')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('state'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>State<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="state" id="state"  maxlength="30" value="<?php if(!empty($user->state)) echo $user->state; else echo $_POST['state'];?>">
						</div> 
				</div>
				
					<div class="form-left">		
						<?php if(form_error('zip_code')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code'); ?></span>
						</div> <?php } ?>

						<div class="txtbox1"> 
							<label>Zip Code<span class="validation" style="color:#FF0000;">*</span></label>
							<input type="text" class="shadow" name="zip_code" id="zip_code"  maxlength="10" value="<?php if(!empty($user->zip_code)) echo $user->zip_code; else echo $_POST['zip_code']; ?>">
						</div> 
					</div>
					<!--pay rate per hour nd mile-->
						<!-- <div class="form-left">
							<?php if(form_error('pay_rate_perhour')) { ?>							
							<div class="txtbox1">
	
							<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate_perhour'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1">
							<label>Pay Rate Per Hour<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
							class="shadow" name="pay_rate_perhour" id="pay_rate_perhour"
							maxlength="10" value="<?php if(!empty($user->pay_rate_perhour)) echo $user->pay_rate_perhour; echo set_value('pay_rate_perhour'); ?>">
							</div>
					</div>
					
						<div class="form-right">
							<?php if(form_error('pay_rate_permile')) { ?>							
							<div class="txtbox1">
	
							<span class="validation" style="color: #FF0000;"><?php echo form_error('pay_rate_permile'); ?></span>
							</div> <?php } ?>
							<div class="txtbox1">
							<label>Pay Rate Per Mile<span class="validation" style="color:#FF0000;">*</span></label> <input type="text"
							class="shadow" name="pay_rate_permile" id="pay_rate_permile"
							maxlength="10" value="<?php if(!empty($user->pay_rate_permile)) echo $user->pay_rate_permile; echo set_value('pay_rate_permile'); ?>">
							</div>
					</div>-->
					<div style="clear:both; width:90%;" >
						Is this address also user billing address( the address that appears on your credit card or bank statement?)
						<?php if(form_error('billing_address_status')) { ?>							
									<div class="txtbox1"> 
									
									<span class="validation" style="color:#FF0000;"><?php echo form_error('billing_address_status'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
								 
									 <span>
									<input type="radio" id="billingaddressyes"  name="billing_address_status" value="0" <?php if(isset($user->billing_address_status) && $user->billing_address_status== 0){ ?> checked="checked"  <?php } ?> <?php if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==0){ ?> checked="checked"  <?php } ?>/>
									YES</span> <span>
									<input type="radio" id= "billingaddressno"  name="billing_address_status" value="1" <?php if(isset($user->billing_address_status) && $user->billing_address_status== 1){ ?> checked="checked"  <?php } ?> <?php if(isset($_POST['billing_address_status']) && $_POST['billing_address_status']==1){ ?> checked="checked"  <?php } ?>/>
									NO (If not then please fill below the details) </span> 
									</div>
									<br />
						</div>
								<h5> Enter credit card number to chande billing address</h5>
								<div class="form-left">
								 <?php if($this->session->flashdata('errmsg')!='' && $this->session->flashdata('errmsg')!=null ) { ?>							
									<div class="txtbox1"> 
									
									<span class="validation" style="color:#FF0000;"><?php echo $this->session->flashdata('errmsg'); ?></span>
									</div> <?php } ?>
									<?php if(form_error('creditcard_number')) { ?>							
									<div class="txtbox1"> 
									
									<span class="validation" style="color:#FF0000;"><?php echo form_error('creditcard_number'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
										<label>Credit Card Number<span class="validation" style="color:#FF0000;"></span></label>
									<input type="text" class="input-xlarge" name="creditcard_number" id="creditcard_number" maxlength= "19" value="<?php if(!empty($credit_card)){echo $credit_card;}else{echo set_value('creditcard_number');} ?>">
									</div>
								</div>
								<div class="form-right">	
								  <?php if(form_error('exp_month')) { ?>
									<div class="txtbox1" > 
							
									<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_month'); ?></span>
									</div> <?php } ?>
								  <?php if(form_error('exp_year')) { ?>
									<div class="txtbox1"> 
							
									<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_year'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1" style="margin-left: -50px;"> 
									<label>Exp<span class="validation" style="color:#FF0000;"></span></label>
								   
						          <select name="exp_month" id="exp_month" class="" style="width:80px;">
									 <option value="">Month</option>
						            <option value="01" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==01){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==01){?> selected <?php }}?>>Jan</option>
						            <option value="02" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==02){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==02){?> selected <?php }}?>>Feb</option>
									<option value="03" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==03){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==03){?> selected <?php }}?>>Mar</option>
									<option value="04" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==04){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==04){?> selected <?php }}?>>Apr</option>
									<option value="05" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==05){?> selected <?php }elseif(!empty($exp_date[1])){if( $exp_date[1]==05){?> selected <?php }}?>>May</option>
									<option value="06" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==06){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==06){?> selected <?php }}?>>Jun</option>
									<option value="07" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==07){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==07){?> selected <?php }}?>>July</option>
									<option value="08" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==08){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==08){?> selected <?php }}?>>Aug</option>
									<option value="09" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==09){?> selected <?php }elseif(!empty($exp_date[1])){if( $exp_date[1]==09){?> selected <?php }}?>>Sep</option>
									<option value="10" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==10){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==10){?> selected <?php }}?>>Oct</option>
									<option value="11" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==11){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==11){?> selected <?php }}?>>Nov</option>
									<option value="12" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==12){?> selected <?php }elseif(!empty($exp_date[1])){if($exp_date[1]==12){?> selected <?php }}?>>Dec</option>
									
								
						          </select>
								 
						          <select name="exp_year" id="exp_year" class="" style="width:80px;" >
									<option value="">Year</option>
									<?php for($i=2013; $i<=2030; $i++){?>
						            <option value="<?php echo $i ?>" <?php if(isset($_POST['exp_year'])&& $_POST['exp_year']== $i){?> selected <?php }elseif(isset($exp_date) && $exp_date[0]==$i){?> selected <?php }?>><?php echo $i?></option>
									<?php }?>
						          </select>
								
									</div>
								 </div>
							<div style="clear:both; width:90%;" >
								<div class="form-left">	
									<?php if(form_error('address_1b')) { ?>
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1b'); ?></span>
									</div> <?php } ?>

									<div class="txtbox1"> 
										<label>Address Line 1<span class="validation" style="color:#FF0000;">*</span></label>
										<input type="text" class="input-xlarge" name="address_1b" id="address_1b" maxlength="50" value="<?php if(!empty($billing_detail->address_1)) echo $billing_detail->address_1; elseif(isset($_POST['address_1b'])) echo $_POST['address_1b']; ?>">
									</div>	
								</div>
								<div class="form-right">
									<?php if(form_error('address_2b')) { ?>
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2b'); ?></span>
									</div> <?php } ?>

									<div class="txtbox1"> 
										<label>Address Line 2</label>
										<input type="text" class="input-xlarge" name="address_2b" id="address_2b" maxlength="255" value="<?php if(!empty($billing_detail->address_2)) echo $billing_detail->address_2; elseif(isset($_POST['address_2b'])) echo $_POST['address_2b']; ?>">
									</div>	
								</div>	
								<div class="form-left">
									<?php if(form_error('city_b')) { ?>							
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('city_b'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
										<label>City<span class="validation" style="color:#FF0000;">*</span></label>
										<input type="text" class="input-xlarge" name="city_b" id="city_b" maxlength= "30" value="<?php if(!empty($billing_detail->city)) echo $billing_detail->city; elseif(isset($_POST['city_b'])) echo $_POST['city_b']; ?>">
									</div>
								</div>
								<div class="form-right">
									<?php if(form_error('state_b')) { ?>							
									<div class="txtbox1"> 
										
										<span class="validation" style="color:#FF0000;"><?php echo form_error('state_b'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
										<label>State<span class="validation" style="color:#FF0000;">*</span></label>
										<input type="text" class="input-xlarge" name="state_b" id="state_b" maxlength= "30" value="<?php if(!empty($billing_detail->state)) echo $billing_detail->state; elseif(isset($_POST['state_b'])) echo $_POST['state_b']; ?>">
									</div>
									</div>
								<div class="form-left">
									<?php if(form_error('zip_code_b')) { ?>							
									<div class="txtbox1"> 
									
									<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code_b'); ?></span>
									</div> <?php } ?>
									<div class="txtbox1"> 
										<label>Zip Code<span class="validation" style="color:#FF0000;">*</span></label>
									<input type="text" class="input-xlarge" name="zip_code_b" id="zip_code_b" maxlength= "10" value="<?php if(!empty($billing_detail->zip_code)) echo $billing_detail->zip_code; elseif(isset($_POST['zip_code_b'])) echo $_POST['zip_code_b']; ?>">
									</div>
								</div>	
					<div class="form-right">
						<?php if(form_error('status')) { ?>
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<span class="validation" style="color:#FF0000;"><?php echo form_error('status'); ?></span>
						</div> <?php } ?>
						<div class="txtbox1"> 
						<label>Status<span class="validation" style="color:#FF0000;">*</span></label>
						<?php $options = array(
						'' => '-- Please select --',
						'1' => 'Active',
						'2' => 'Inactive',
					
						); 
						if (!empty( $status )) {
							$index = $status;
							}
							elseif(!empty($user->status)){
								$index =$user->status;
							}else {
								$index = 0;
							}
								
						 echo form_dropdown('status', $options, $index);?>
						</div>
					</div>	
					<div class="form-right">
						<div class="txtbox1"> 
							<label>&nbsp;</label>
							<input  class="btn btn-primary btn-large" type="submit" name="addartist" value="Update">
						</div>


					<?php form_close() ; ?>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
 <script>
  $(document).ready(function() {
  $('#billingaddressyes').click(function(){
 $address1 = $('#address_1').val();
  $('#address_1b').val($address1);
   $address2 = $('#address_2').val();
  $('#address_2b').val($address2);
  
   $city = $('#city').val();
  $('#city_b').val($city);
  
   $state = $('#state').val();
  $('#state_b').val($state);
  
   $zip_code = $('#zip_code').val();
  $('#zip_code_b').val($zip_code);
  
  });
  
  $('#billingaddressno').click(function(){

  $('#address_1b').val(''); 
  $('#address_2b').val('');
  $('#city_b').val('');
   $('#state_b').val('');
   $('#zip_code_b').val('');
  });
	}); 
 </script>
  <script src='<?php echo base_url();?>js/phone_mask2.js'></script>	