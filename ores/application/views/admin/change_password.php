<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <?php $this->load->view('template/admin_sidebar'); ?> 
                </div>
                <!--/span-->
<div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block" style="margin-top:30px;">
                            <div class="navbar navbar-inner block-header">
								 <div class="muted pull-left">Change Password</div>
							
							</div>
<div class="block-content collapse in">
                                <div class="span12"> 	
<?php echo form_open(); ?>



		<?php if(isset($wrong)) { ?>
	<div class="txtbox1"> 
	<span class="validation" style="color:#FF0000;"><?php echo $wrong; ?></span>
	</div>
	<?php } ?> <br/>
	
	<?php if(form_error('oldpassword')) { ?>
	<div class="txtbox1"> 
	
	<span class="validation" style="color:#FF0000;"><?php echo form_error('oldpassword'); ?></span>
	</div> <?php } ?>
	
	<div class="txtbox1"> 
	<label>Old Password<span class="validation" style="color:#FF0000;">*</span></label>
	<input type="password" class="shadow input-xlarge" name="oldpassword" maxlength= "20" value="<?php echo set_value('oldpassword'); ?>">
	</div>
	
	<?php if(form_error('password')) { ?>
	<div class="txtbox1"> 
	
	<span class="validation" style="color:#FF0000;"><?php echo form_error('password'); ?></span>
	</div> <?php } ?>	
	
	<div class="txtbox1"> 
	<label>New Password<span class="validation" style="color:#FF0000;">*</span></label>
	<input type="password" class="shadow input-xlarge" name="password" maxlength= "20" value="<?php echo set_value('password'); ?>">
	</div>
	
	<?php if(form_error('repassword')) { ?>
	<div class="txtbox1"> 
	
	<span class="validation" style="color:#FF0000;"><?php echo form_error('repassword'); ?></span>
	</div> <?php } ?>	
	
	<div class="txtbox1"> 
	<label>Re-Enter Password<span class="validation" style="color:#FF0000;">*</span></label>
	<input type="password" class="shadow input-xlarge" name="repassword" maxlength= "20" value="<?php echo set_value('repassword'); ?>">
	</div>
	
	<div class="txtbox1"> 
	
	<input type="submit" value="submit" class="btn btn-primary" name="submit">
	</div>
<?php form_close() ; ?>
	 
			</div>

			</div>
	    </div>