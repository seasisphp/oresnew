<div id="footer">
    <p>&nbsp;</p>
  <!-- end #footer --></div>
    <link href="<?php echo base_url(); ?>css/admin/datepicker.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url(); ?>css/admin/jquery_ui_1_10.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/uniform.default.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/chosen.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/timepicker.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>css/admin/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">
	 <link href="<?php echo base_url(); ?>css/admin/datetimepicker.css" rel="stylesheet" media="screen">
	 <link href="<?php echo base_url(); ?>css/admin/datetime_input.css" rel="stylesheet" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>
		<script src='<?php echo base_url(); ?>js/jquery-ui.custom.min.js'></script>
		<script src='<?php echo base_url(); ?>js/fullcalendar.min.js'></script>
			


        <script src='<?php echo base_url(); ?>js/jquery_ui_1_10.js'></script>	
        <script src='<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js'></script> 	
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/timeEntry/jquery.timeentry.css"> 
		<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/timeEntry/jquery.timeentry.min.js"></script>
        <script src='<?php echo base_url(); ?>js/jqeury_input_mask.js'></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.uniform.min.js"></script>
        <script src="<?php echo base_url(); ?>js/chosen.jquery.min.js"></script>
       <!--  <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script> -->

        <script src="<?php echo base_url(); ?>js/wysihtml5-0.3.0.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-wysihtml5.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery.bootstrap.wizard.min.js"></script>
    

<?php if(isset($_GET['did'])){ ?>
		<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.js"></script>
<?php } ?>
        <script src="<?php echo base_url(); ?>js/scripts.js"></script>
		
        <script>
        $(function() {
            $(".datepicker").datepicker();
					

            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
			
		 $(".from_datetime").datetimepicker({
		 //2013-09-27 15:09:43
					format: "yyyy-mm-dd hh:ii:ss",
					autoclose: true,
					todayBtn: true,
					startDate: "2013-02-14 10:00",
					minuteStep: 5
		});
		 $(".to_datetime").datetimepicker({
		 //2013-09-27 15:09:43
					format: "yyyy-mm-dd hh:ii:ss",
					autoclose: true,
					todayBtn: true,
					startDate: "2013-02-14 10:00",
					minuteStep: 5
		});
	});
        </script>
    </body>

</html>
