 <div class="register-container">
    <h2>New Member Registration</h2>
    <h3>Tell Us About Yourself</h3>
    <div class="form-container">
      <div class="left-form">
        <fieldset>
          <span class="label-txt">First Name<em>*</em></span>
          <input type="text" title="First Name" value="" />
        </fieldset>
        <fieldset>
          <span class="label-txt">Email<em>*</em></span>
          <input type="text" title="First Name" value="" />
        </fieldset>
        <fieldset>
          <span class="label-txt">Company Name</span>
          <input type="text" title="First Name" value="" />
        </fieldset>
        <fieldset>
          <span class="label-txt">Other Phone</span>
          <input type="text" title="First Name" value="" />
        </fieldset>
      </div>
      <div class="right-form">
        <fieldset>
          <span class="label-txt">Last Name<em>*</em></span>
          <input type="text" title="First Name" value="" />
        </fieldset>
        <fieldset>
          <span class="label-txt">Re Enter Email</span>
          <input type="text" title="First Name" value="" />
        </fieldset>
        <fieldset>
          <span class="label-txt">Mobile Phone<em>*</em></span>
          <input type="text" title="First Name" value="" />
        </fieldset>
      </div>
      <div class="clr"></div>
    </div>
    <div class="address-container">
      <h3>Primary Address</h3>
      <div class="form-container">
        <div class="upper-form">
          <fieldset>
            <span class="label-txt">Address Line 1<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt">Address Line 2<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
            <span class="label-txt">City<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt">Zip<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="upper-form-right-form">
          <fieldset>
            <span class="label-txt">State<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="clr"></div>
        <div class="form-container-text">
          <p >Is this address also your billing address( the address that appears on your credit card or bank statement?) </p>
          <div class="clr"></div>
          <p> <span>
            <input type="radio" value="" />
            YES</span> <span>
            <input type="radio" value="" />
            NO (If not then we will ask you for it in a moment) </span> 
        </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="address-container">
      <h3>Choose Your Login</h3>
      <div class="form-container">
        <div class="login-container-left-form">
          <fieldset>
            <span class="label-txt-title">Username<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt-title">Re-Enter Password<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <p>We would like to give you a special offer on your birthday! </p>
          <h4> Enter Month&amp; Year</h4>
          <select class="select-small" >
            <option value="01"></option>
            <option value="02">February</option>
          </select>
          <select name="" id="" class="select-large" >
            <option value="01"></option>
            <option value="02">2011</option>
            <option value="03">2012</option>
          </select>
        </div>
        <div class="login-container-right-form">
          <fieldset>
            <span class="label-txt-rt-title">Password<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt-rt-title">How Did You Heard About This?</span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="address-container">
      <h3>Billing Address And Credit Card Info</h3>
      <div class="form-container">
        <div class="upper-form">
          <fieldset>
            <span class="label-txt">Address Line 1<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt">Address Line 2<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
            <span class="label-txt">City<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt">Zip<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="upper-form-right-form">
          <fieldset>
            <span class="label-txt">State<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
        </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="address-container">
      <h3>Enter Your Credit Card Info</h3>
      <div class="form-container">
        <div class="credit-upper-form">
          <fieldset>
            <span class="label-txt-crd">Credit Card Number<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <span class="label-txt-crd">Name On the Card<em>*</em></span>
            <input type="text" title="First Name" value="" />
          </fieldset>
          <fieldset>
            <p>
              <input type="checkbox" />
              I accept the <a href="#">Terms & Conditions</a></p>
          </fieldset>
         
        </div>
        <div class="credit-upper-form-right">
          <h4> Exp<em>*</em></h4>
          <select name="" id="" class="select-small">
            <option value="01"></option>
            <option value="02">February</option>
          </select>
          <select name="" id="" class="select-large" >
            <option value="01"></option>
            <option value="02">2011</option>
            <option value="03">2012</option>
          </select>
        </div>
        <div class="credit-upper-form-left-form"> <fieldset class="promo-code">
            <span class="label-txt-crd">Promo Code</span>
            <input type="text" title="First Name" value="" />
          </fieldset> </div>
          <div class="clr"></div>
          <div class="credit-upper-form-submit">
     <input type="button" value="Submit" />
          </div>
        
        <div class="clr"></div>
      </div>
    </div>
  </div>