<script src='<?php echo base_url();?>js/front_end/step2.js'></script> 
<link href="<?php echo base_url(); ?>css/front_end/reservation.css" rel="stylesheet" media="screen">
 <div class="register-container">
<h2>New Member Registration :Step 2</h2>
  	<?php 
  	$attributes = array('id' => 'step_2');
  	echo form_open('',$attributes);  ?>
 <div class="address-container">
      <h3>Billing Address And Credit Card Info</h3>
      <div class="form-container">

        <div class="upper-form">
          <fieldset>
            <span class="label-txt">Address Line 1<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="address_1b" name= "address_1b" tabindex="1"title="Address line 1" value="<?php if((!empty($userrecords->address_1)) && ($userrecords->billing_address_status == 0)) echo $userrecords->address_1; elseif(isset($_POST['address_1b'])) echo $_POST['address_1b']; ?>" />
			  <?php if(form_error('address_1b')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_1b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
          <fieldset>
            <span class="label-txt">Address Line 2</span>
			 <div class="input-container">
            <input type="text" id="address_2b" name= "address_2b" tabindex="2" title="Address line 2"  value="<?php if((!empty($userrecords->address_2)) && ($userrecords->billing_address_status == 0)) echo $userrecords->address_2; elseif(isset($_POST['address_2b'])) echo $_POST['address_2b']; ?>" />
			  <?php if(form_error('address_2b')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('address_2b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        </div>
        <div class="upper-form-left-form">
          <fieldset>
            <span class="label-txt">City<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="city_b" name= "city_b" title="City"  tabindex="3" value="<?php if((!empty($userrecords->city)) && ($userrecords->billing_address_status == 0)) echo $userrecords->city; elseif(isset($_POST['city_b'])) echo $_POST['city_b']; ?>" />
			  <?php if(form_error('city_b')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('city_b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        <!-- <fieldset>
		
            <span class="label-txt">Country<em>*</em></span>
			 <div class="input-container">
            <select style="margin:0px;" id="country" class="shadow" name="country_b" id='country_b'>
				<option value="">-- Please select --</option>
						<?php 
						/* $ctrb   = $this->db->get('country');
						foreach($ctrb->result() as $ctrb){
						
							if(!empty($userrecords->country)){
							if((!empty($userrecords->country)) && ($userrecords->billing_address_status == 0) &&($ctrb->CountryID==$userrecords->country)){$sel='selected="selected"';}else{$sel='';}
							 echo "<option value='".$ctrb->CountryID."' $sel>". $ctrb->CountryName."</option>";
							}
							 else{
							 	if($ctrb->CountryID==$country){
							 		$sel='selected="selected"';
							 	}else{$sel='';
							 	}
							 	echo "<option value='".$ctrb->CountryID."' $sel>". $ctrb->CountryName."</option>";
							 }
						} */
						?>							
						</select>		
			 <?php if(form_error('country_b')) { ?>
			<div class="txtbox1"> 
			
			<span class="validation" style="color:#FF0000;"><?php echo form_error('country_b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>-->
        </div>
        <div class="upper-form-right-form">
          <fieldset>
            <span class="label-txt">State<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="state_b" name= "state_b" title="State" tabindex="4" value="<?php if((!empty($userrecords->state)) && ($userrecords->billing_address_status == 0)) echo $userrecords->state; elseif(isset($_POST['state_b'])) echo $_POST['state_b']; ?>" />
			 <?php if(form_error('state_b')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('state_b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
		    <fieldset>
            <span class="label-txt">Zip<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="zip_code_b" name= "zip_code_b" tabindex="5" title="Zip Code" value="<?php if((!empty($userrecords->zip_code)) && ($userrecords->billing_address_status == 0)) echo $userrecords->zip_code; elseif(isset($_POST['zip_code_b'])) echo $_POST['zip_code_b']; ?>" />
			<?php if(form_error('zip_code_b')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('zip_code_b'); ?></span>
			</div> <?php } ?>
			</div>
          </fieldset>
        </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="address-container">
      <h3>Enter Your Credit Card Info</h3>
      <div class="form-container">
        <div class="credit-upper-form">
         <span id="credit_card_span"></span>
          <fieldset>
            <span  class="label-txt-crd">Credit Card Number<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="creditcard_number" maxlength="19" name= "creditcard_number" tabindex="6" title="Credit Card number" value="<?php if(isset($_POST['creditcard_number'])) echo $_POST['creditcard_number']; ?>" />
			<?php if(form_error('creditcard_number')) { ?>
			<div class="txtbox1"> 
			<span class="validation" style="color:#FF0000;"><?php echo form_error('creditcard_number'); ?></span>
			</div> <?php }
				if($this->session->flashdata('errmsg')!='' && $this->session->flashdata('errmsg')!=null){?>
				<div class="txtbox1">
				<span class="validation" style="color:#FF0000;"><?php echo $this->session->flashdata('errmsg'); ?></span>
			</div>
			<?php }
			 ?>
			</div>
		</fieldset>
          <fieldset>
            <span class="label-txt-crd">Name On the Card<em>*</em></span>
			 <div class="input-container">
            <input type="text" id="creditcard_name" name= "creditcard_name" tabindex="7" title="Credit Card name"  value="<?php if(isset($_POST['creditcard_name'])) echo $_POST['creditcard_name']; ?>" />
          <?php if(form_error('creditcard_name')) { ?>
			<div class="txtbox1">
			<span class="validation" style="color:#FF0000;"><?php echo form_error('creditcard_name'); ?></span>
			</div> <?php } ?>
			</div>
		 </fieldset>
          <fieldset id="terms_fieldset">
            <p>
              <input type="checkbox" id="terms" name= "terms" <?php if(isset($_POST['terms']) && $_POST['terms']=="on"){ ?> checked="checked"  <?php } ?> />
              I accept the <a href="#">Terms & Conditions</a></p><em style="color: #CB4343;">*</em>
			  <?php if(form_error('terms')) { ?>
			<div class="txtbox1"> 
			<span id="step2_terms_span" class="validation" style="color:#FF0000;"><?php echo form_error('terms'); ?></span>
			</div> <?php } ?>
          </fieldset>
		  
		 <!-- <fieldset class="promo-code">
            <span class="label-txt-crd">Promo Code</span>
			 <div class="input-containercontainer">
            <input type="text" id="promo_code" name="promo_code" tabindex="8" title="promocode" value="<?php if(isset($_POST['promo_code'])) echo $_POST['promo_code']; ?>" />
		 <?php if(form_error('promo_code')) { ?>
			<div class="txtbox1"> 
		
			<span class="validation" style="color:#FF0000;"><?php echo form_error('promo_code'); ?></span>
			</div> <?php } ?>	
			</div>
			
          </fieldset>-->
         
        </div>
        <div class="credit-upper-form-right">
          <h4> Exp<em style="color: #CB4343;">*</em></h4>
		   <div class="input-container">
          <select name="exp_month" id="exp_month" class="select-large">
			 <option value="">Month</option>
            <option value="01" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==01){?> selected <?php }?>>Jan</option>
            <option value="02" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==02){?> selected <?php }?>>Feb</option>
			<option value="03" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==03){?> selected <?php }?>>Mar</option>
			<option value="04" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==04){?> selected <?php }?>>Apr</option>
			<option value="05" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==05){?> selected <?php }?>>May</option>
			<option value="06" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==06){?> selected <?php }?>>Jun</option>
			<option value="07" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==07){?> selected <?php }?>>July</option>
			<option value="08" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==8){?> selected <?php }?>>Aug</option>
			<option value="09" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==9){?> selected <?php }?>>Sep</option>
			<option value="10" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==10){?> selected <?php }?>>Oct</option>
			<option value="11" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==11){?> selected <?php }?>>Nov</option>
			<option value="12" <?php if(isset($_POST['exp_month'])&& $_POST['exp_month']==12){?> selected <?php }?>>Dec</option>
			
		
          </select>
		 
          <select name="exp_year" id="exp_year" class="select-large" >
			<option value="">Year</option>
			<?php for($i=2013; $i<=2030; $i++){?>
            <option value="<?php echo $i ?>" <?php if(isset($_POST['exp_year'])&& $_POST['exp_year']== $i){?> selected <?php }?>><?php echo $i?></option>
			<?php }?>
          </select>
		  <?php if(form_error('exp_month')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_month'); ?></span>
			</div> <?php } ?>
		  <?php if(form_error('exp_year')) { ?>
			<div class="txtbox1"> 
	
			<span class="validation" style="color:#FF0000;"><?php echo form_error('exp_year'); ?></span>
			</div> <?php } ?>
			</div>
        </div>
          <div class="clr"></div>
          <div class="credit-upper-form-submit">
     <input id="step2_submit" type="submit" value="Submit" />
	 
          </div>
             <?php form_close() ; ?>
        <div class="clr"></div>
      </div>
    </div>
	</div>
