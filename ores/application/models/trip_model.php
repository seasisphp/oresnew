<?php
class Trip_model extends CI_Model{
	
	public function __construct() {
		$this->load->database();
	}
	
	
	public function trip_reservation($data){
		$this->db->insert('reservation', $data);
		$last_id = $this->db->insert_id();
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
	}
	public function get_all_Reservation(){
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}	
	}
	
	public function get_trip($trip_id){
		$this->db->select('*');
		$this->db->from('ores_dispatch_log');
		$this->db->where('id',$trip_id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	
	public function update_trip($data,$trip_id){
		$this->db->where('id',$trip_id);
		$return = $this->db->update('ores_dispatch_log',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	
	public function delete_trip($trip_id,$y_n){
		$this->db->where('id',$trip_id);
		$this->db->delete('ores_dispatch_log');
	}
	public function get_user_id($contact_number){
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('mobile_number',$contact_number);
		$this->db->or_where('other_number', $contact_number);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}
		else{
			return false;
		}
	}
	
	public function get_home_address($user_id){
		$this->db->select('address_1,address_2,city,state,zip_code');
		$this->db->from('user');
		$this->db->where('address_title',1);
		$this->db->where('id',$user_id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/*public function get_Reservation_csv($id){
		$query = $this->db->query("SELECT trip_date_from, pickup_time, address, address_2, 
									city, state, zip,
								    desination_1, desination_2, approx_hours,
								    contact_name, contact_phone,comments,promo_code
									FROM reservation
									WHERE id =".$id);
								
		return $query;
	}*/
}