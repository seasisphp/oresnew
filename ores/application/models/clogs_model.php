<?php

class Clogs_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    public function get_all_reservations($limit, $start){

		$this->db->select('reservation.*,user.first_name,driver.first_name as driver_name');
		$this->db->from('reservation');
		$this->db->where('reservation.status = 3');
		$this->db->or_where('reservation.status = 4'); 
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->join('driver', 'driver.id = reservation.driver_id','left');
		$this->db->order_by("id", "desc"); 
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result();
		
	
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	    public function count_all_reservations(){
		
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->order_by("id", "desc"); 
		$this->db->where('reservation.status = 3');
		$this->db->where('reservation.status = 4');
		$query = $this->db->get();
		$count = $query->num_rows();
		
		if($count){
			return $count;
		}else{
			return false;
		}
	}
		public function get_reservationdetail($id){
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	
	
		public function get_closeouttripdetail($id){
		$this->db->select('*');
		$this->db->from('trip_closeout_detail');
		$this->db->where('reservation_id',$id);
		$query = $this->db->get();
		$result = $query->result();
	
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	public function update_reservationdetail($data,$id){
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	public function insert_closeouttripdetail($data){
		$this->db->insert('trip_closeout_detail', $data); 
		$last_id = $this->db->insert_id();
		
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
	}
	 public function update_closeouttripdetail($data,$id){
		$this->db->where('reservation_id',$id);
		$return = $this->db->update('trip_closeout_detail',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}

} 
?>
