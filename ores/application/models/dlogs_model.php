<?php

class Dlogs_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    public function get_all_reservations($limit, $start){

		$this->db->select('reservation.*,user.first_name,user.last_name');
		$this->db->from('reservation');
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->where('reservation.status != 3');
		$this->db->where('reservation.status != 4');
		$this->db->order_by("reservation.id", "desc");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result();
		
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	    public function count_all_reservations(){
		
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->where('reservation.status != 3');
		$this->db->where('reservation.status != 4');
		$this->db->order_by("reservation.id", "desc"); 
		$query = $this->db->get();
		$count = $query->num_rows();
		
		if($count){
			return $count;
		}else{
			return false;
		}
	}
	//assign driver to the trip
	public function assign_driver($data,$id){
	
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		
		if($return > 0){
		
			return true;
		}else{
			return false;
		}
	} 
	public function driver_schdule($data,$id){
		$this->db->select('*');
		$this->db->from('driver_schdule');
		$this->db->where("reservation_id ", $id); 
		$query = $this->db->get();
		$count = $query->num_rows();
		if($count){
		
		$this->db->where('reservation_id',$id);
		$return = $this->db->update('driver_schdule',$data);
		
		if($return > 0){
		
			return true;
		}else{
			return false;
		}

		}else{
		
		
		$this->db->insert('driver_schdule',$data); 
		$last_id = $this->db->insert_id();
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
		}
	
	} 
	/**
	 * updated by dev verma
	 * Get All Active Drivers By City
	 * @author Gurpreet 
	 */
	 
	public function getADriversByCity($city,$trip_from='0000-00-00 00:00:00',$trip_to='0000-00-00 00:00:00'){
		// unset all variable
		unset($AdriverId,$id,$result1,$AdriverId2,$id2,$result2,$dravail,$all_driver_id,$availdriverbycity,$result);
		
		
		//	 select driver those are free in driver_availabilty and same city
		$this->db->select('driver_availability.driver_id');
		$this->db->from('driver_availability');
		//$this->db->join('reservation', 'reservation.trip_date_from>driver_availability.driver_available_to AND reservation.trip_date_to<driver_availability.driver_available_from');
		$this->db->where('driver_available_to <', $trip_from);
		$this->db->where('driver_available_from >', $trip_to);	
		$query = $this->db->get();
		$result = $query->result(); 
	
		$AdriverId = array();
		foreach($result as $did){
			if(!empty($did->driver_id)){
			$AdriverId[] = $did->driver_id;
		}
		}

		//city driver also available
		if(!empty($AdriverId))	$id = $AdriverId;
		else $id = 0;
		if($id!=0){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		//$this->db->where('status',1);
		$this->db->where('driver.city',$city);
		$this->db->where_in('id',$id);
		$query = $this->db->get();
		$result1 = $query->result();
		}
		else{
			$result1=array();
		}
		
		
		
		
		//	select driver those are assigned free schedule in driver_availabilty and having same city
		if($id!=0){
		$this->db->select('driver_availability.driver_id');
		$this->db->from('driver_availability');
		$this->db->where_not_in('driver_id',$id);
		$this->db->where('status',2);
		$query = $this->db->get();
		$result = $query->result();
		}
		else{
			$this->db->select('driver_availability.driver_id');
			$this->db->from('driver_availability');
			$this->db->where('status',2);
			$query = $this->db->get();
			$result = $query->result();
		}

		
		//
		$AdriverId2 = array();
		foreach($result as $did){
			if(!empty($did->driver_id)){
		$AdriverId2[] = $did->driver_id;
			}
		}
		//city driver also available
		if(!empty($AdriverId2))	$id2 = $AdriverId2;
		else $id2 = 0;
		if($id2!=0){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
				$this->db->from('driver');
				//$this->db->where('status',1);
				$this->db->where('driver.city',$city);
				$this->db->where_in('id',$id2);
				$query = $this->db->get();
				$result2 = $query->result();
		}
		else{
			$result2=array();
		}
		
		
		// select driver those have not yet assigned any schedule but from same city
		$dravail=array();
		$this->db->select('driver_id');
		$this->db->from('driver_availability');
		$query=$this->db->get();
		$result=$query->result();
		if($result){
			$dravail=$result;
		}
		else{
			$dravail=array();
		}
		$all_driver_id=array();
		if(!empty($dravail)){
			foreach($dravail as $driver){
				if($driver!=false){
					$all_driver_id[]=$driver->driver_id;
				}
			}
		}
		$availdriverbycity=array();
		if(!empty($all_driver_id)){
		$this->db->select('first_name,last_name,id');
		$this->db->from('driver');
		$this->db->where('city',$city);
		$this->db->where_not_in('id',$all_driver_id);
		$query=$this->db->get();
		$availdriverbycity=$query->result();
		}
		else{
		$this->db->select('first_name,last_name,id');
		$this->db->from('driver');
		$this->db->where('city',$city);
		//$this->db->where_not_in('id',$all_driver_id);
		$query=$this->db->get();
		$availdriverbycity=$query->result();
		}
		
		// merge all array and return final result	
		if(!empty($result1) && !empty($result2)){
		$result=array_merge((array)$result1,(array)$result2);
		}
		elseif(!empty($result1)){
			$result=$result1;
		}
		elseif(!empty($result2)){
			$result=$result2;
		}
		else{
			$result=false;
		}
		if(!empty($result)){
			if(!empty($availdriverbycity)){
			$result=array_merge((array)$result,(array)$availdriverbycity);
			}
		}
		else{
			if(!empty($availdriverbycity)){
			$result=$availdriverbycity;
			}
		}
		if($result){
			return $result;
		}else{
			return false;
		}
		
	}
	/*
	 * updated by dev verma
	 */
	Public function getNADriversByCity($city,$trip_from='0000-00-00 00:00:00',$trip_to='0000-00-00 00:00:00'){

		unset($nac,$na,$nad,$driver_id,$result);
		//select all available driver and merge in array
		$nac=$this->getADriversByCity($city,$trip_from,$trip_to);
		$na=$this->getAnonCityDrivers($city,$trip_from,$trip_to);
		$nad=array_merge((array)$nac,(array)$na);
		
		$driver_id=array();
		foreach($nad as $driver){
			if($driver!=false){
			$driver_id[]=$driver->id;
			}
		}
		
		// select all not available driver from same city
		if(!empty($driver_id)){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		$this->db->where('driver.city',$city);
		$this->db->where_not_in('id',$driver_id);
		$query = $this->db->get();
		$result = $query->result();
		}
		else{
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		$this->db->where('driver.city',$city);
		$query = $this->db->get();
		$result = $query->result();
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	
	
	/**
	 * update by dev verma
	 * Get All Active Drivers By City
	 * @author pt
	 */
	
	public function getAnonCityDrivers($city,$trip_from='0000-00-00 00:00:00',$trip_to='0000-00-00 00:00:00'){
		//$result=array();
		unset($AdriverId,$id,$result1,$AdriverId2,$id2,$result2,$dravail,$all_driver_id,$availdriverbycity,$result);

		//select driver those are free in driver_availabilty and from another city
		$this->db->select('driver_availability.driver_id');
		$this->db->from('driver_availability');
		//$this->db->join('reservation', 'reservation.trip_date_from>driver_availability.driver_available_to AND reservation.trip_date_to<driver_availability.driver_available_from');
		$this->db->where('driver_available_to <', $trip_from);
		$this->db->where('driver_available_from >', $trip_to);	
		$query = $this->db->get();
		$result = $query->result(); 
		$AdriverId = array();
		foreach($result as $did){
			if(!empty($did->driver_id)){
			$AdriverId[] = $did->driver_id;
			}
		}
		
		if(!empty($AdriverId))	$id = $AdriverId;
		else $id = 0;
		
		
		if($id!=0){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		$this->db->where_not_in('city',$city);
		$this->db->where_in('id',$id);
		$query = $this->db->get();
		$result1 = $query->result();
		}
		else{
			$result1=array();
		}
		
		//select driver those are assigned free schedule in driver_availabilty and from another city
		if($id!=0){
		$this->db->select('driver_availability.driver_id');
		$this->db->from('driver_availability');
		$this->db->where_not_in('driver_id',$id);
		$this->db->where('status',2);
		$query = $this->db->get();
		$result = $query->result();
		}
		else{
			$this->db->select('driver_availability.driver_id');
			$this->db->from('driver_availability');
			$this->db->where('status',2);
			$query = $this->db->get();
			$result = $query->result();
		}
		$AdriverId2 = array();
		foreach($result as $did){
			if(!empty($did->driver_id)){
		$AdriverId2[] = $did->driver_id;
			}
		}
		//city driver also available
		if(!empty($AdriverId2))	$id2 = $AdriverId2;
		else $id2 = 0;
		if($id2!=0){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
				$this->db->from('driver');
				$this->db->where_not_in('city',$city);
				$this->db->where_in('id',$id2);
				$query = $this->db->get();
				$result2 = $query->result();
				//print_r($result); die();
		}
		else{
			$result2=array();
		}
		// select driver those have not yet assigned any schedule but from other city
		$dravail=array();
		$this->db->select('driver_id');
		$this->db->from('driver_availability');
		$query=$this->db->get();
		$result=$query->result();
		if($result){
			$dravail=$result;
		}
		else{
			$dravail=array();
		}
		
		$all_driver_id=array();
		if(!empty($dravail)){
			foreach($dravail as $driver){
				if($driver!=false){
					$all_driver_id[]=$driver->driver_id;
				}
			}
		}
		
		$availdriverbycity=array();
		
		if(!empty($all_driver_id)){
		$this->db->select('first_name,last_name,id');
		$this->db->from('driver');
		$this->db->where_not_in('city',$city);
		$this->db->where_not_in('id',$all_driver_id);
		$query=$this->db->get();
		$availdriverbycity=$query->result();
		}
		else{
		$this->db->select('first_name,last_name,id');
		$this->db->from('driver');
		$this->db->where_not_in('city',$city);
		//$this->db->where_not_in('id',$all_driver_id);
		$query=$this->db->get();
		$availdriverbycity=$query->result();
		} 
	
		// merge all array and return final result
		if(!empty($result1) && !empty($result2)){
		$result=array_merge((array)$result1,(array)$result2);
		}
		elseif(!empty($result1)){
			$result=$result1;
		}
		elseif(!empty($result2)){
			$result=$result2;
		}
		else{
			$result=false;
		}
		if(!empty($result)){
			if(!empty($availdriverbycity)){
				$result=array_merge((array)$result,(array)$availdriverbycity);
			}
		}
		else{
			if(!empty($availdriverbycity)){
			$result=$availdriverbycity;
			}
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	
	}
	/*
	 * updated by dev verma
	 */
	public function getNAnonCityDrivers($city,$trip_from='0000-00-00 00:00:00',$trip_to='0000-00-00 00:00:00'){
	    
		unset($nac,$na,$nad,$driver_id,$result);
		//select all available driver and merge in array
		$nac=$this->getADriversByCity($city,$trip_from,$trip_to);
		$na=$this->getAnonCityDrivers($city,$trip_from,$trip_to);
		$nad=array_merge((array)$nac,(array)$na);
		$driver_id=array();
		foreach($nad as $driver){
			if($driver!=false){
				$driver_id[]=$driver->id;
			}
		}
		// select all not available driver not from same city
		if(!empty($driver_id)){
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		//$this->db->where('status',1);
		$this->db->where_not_in('city',$city);
		$this->db->where_not_in('id',$driver_id);
		$query = $this->db->get();
		$result = $query->result();
		}
		else{
		$this->db->select('driver.first_name,driver.last_name,driver.id');
		$this->db->from('driver');
		$this->db->where_not_in('city',$city);
		$query = $this->db->get();
		$result = $query->result();
		}
		
		if($result){
			return $result;
		}else{
			return false;
		}
}

	public function get_reservationdetail($id){
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}	
	public function update_reservationdetail($data,$id){
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	public function getDriversTripDetail($id,$driver_id){
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->join('driver', 'reservation.driver_id=driver.id');
		$this->db->where('reservation.driver_id',$driver_id);
		$this->db->where('reservation.id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}	
	}
	public function getuserTripDetail($id){
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->join('user', 'reservation.user_id=user.id');
		$this->db->where('reservation.id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}	
	}
		//unassign driver to the trip
	public function unassign_driver($data,$id){
	
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		
		if($return > 0){
		
			return true;
		}else{
			return false;
		}
	} 
	//delete driver schedule
		public function deletedriver_schdule($id){
		$this->db->where('reservation_id', $id);
		$this->db->delete('driver_schdule');
	} 
	
	public function get_user_reservations($limit, $start,$user_id,$date){
	
		$this->db->select('*');
		$this->db->from('reservation');	
		$this->db->where('user_id',$user_id);
		$this->db->where('trip_date_from >',$date);
		$this->db->order_by("id", "desc");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result();
	
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public function get_reservations_detail($id){
	
		$this->db->select('reservation.*,user.first_name,user.last_name');
		$this->db->from('reservation');
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->where('reservation.status != 3');
		$this->db->where('reservation.status != 4');
		$this->db->where('reservation.id',$id);
		$query = $this->db->get();
		$result = $query->result();
	
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	
} 
?>
