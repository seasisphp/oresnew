<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
	
	
	
		public function get_billingdetail($id){
		$this->db->select('*');
		$this->db->from('billing_details');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		$result = $query->result();
	
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	
	public function insert_billingdetail($data){
		$this->db->insert('billing_details', $data); 
		$last_id = $this->db->insert_id();
		
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
	}
	 public function update_billingdetail($data,$id){
		$this->db->where('user_id',$id);
		$return = $this->db->update('billing_details',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	 public function get_autorizednet_profileId($id){
	 	$this->db->select('customer_profile_id,customer_payment_profile_id');
	 	$this->db->from('billing_details');
	 	$this->db->where('user_id',$id);
	 	$query = $this->db->get();
	 	$result = $query->result();
	 	if($result){
	 		return $result[0];
	 	}else{
	 		return false;
	 	}
	 }
	//dashboard bookings
	//past completed bookings
		public function getpastbookings($id){

		$this->db->select('reservation.*,user.first_name as u_fname, user.last_name as u_lname,driver.first_name,driver.last_name');
		$this->db->from('reservation');
		$this->db->where("(reservation.status = 3 || reservation.status = 4)");
		//$this->db->where('reservation.status = 3');
		//$this->db->or_where('reservation.status = 4'); 
		$this->db->where('reservation.user_id',$id);
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->join('driver', 'driver.id = reservation.driver_id','left');
		//$this->db->where('reservation.user_id',$id);
		$this->db->order_by("reservation.id", "desc"); 
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		//print_r($result); die;
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	//current   bookings
		public function getcurrentbookings($id){

		$this->db->select('reservation.*,user.first_name as u_fname, user.last_name as u_lname');
		$this->db->from('reservation');
		$this->db->where("(reservation.status = 1 || reservation.status = 0 || reservation.status = 2)");
		//$this->db->where('reservation.status = 1');
		//$this->db->or_where('reservation.status = 0'); 
		$this->db->where('reservation.user_id',$id);
		$this->db->join('user', 'user.id = reservation.user_id');
		//$this->db->join('driver', 'driver.id = reservation.driver_id');
		$this->db->order_by("id", "desc"); 
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		//print_r($this->db->last_query()); die;
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	//reserved   bookings
		public function getreservedbookings($id){

		$this->db->select('reservation.*,user.first_name as u_fname, user.last_name as u_lname,driver.first_name,driver.last_name');
		$this->db->from('reservation');
		$this->db->where('reservation.status = 2');
		$this->db->where('reservation.user_id',$id);
		$this->db->join('user', 'user.id = reservation.user_id');
		$this->db->join('driver', 'driver.id = reservation.driver_id');
		$this->db->order_by("id", "desc"); 
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	public function update_profile_data($data,$id){
		$this->db->where('user_id',$id);
		$return = $this->db->update('billing_details',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	
	//current   bookings
	public function getallcurrentbookings(){
	
		$this->db->select('*');
		$this->db->from('reservation');
		$this->db->where("(reservation.status = 0||reservation.status = 1 || reservation.status = 2)");
		$this->db->where('reservation.is_confirmed = "No"');
		$this->db->order_by("id", "desc");
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		//print_r($this->db->last_query()); die;
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	//current   bookings
	public function getalldiapatchedbookings(){
	
		$this->db->select('reservation.*,user.first_name as u_fname, user.last_name as u_lname,user.email');
		$this->db->from('reservation');
		$this->db->where("(reservation.status = 2||reservation.status = 3)");
		$this->db->where('reservation.is_acknowled = "No"');
		//$this->db->or_where('reservation.status = 0');
		$this->db->join('user', 'user.id = reservation.user_id');
		//$this->db->join('driver', 'driver.id = reservation.driver_id');
		$this->db->order_by("id", "desc");
		$this->db->limit(3, 0);
		$query = $this->db->get();
		$result = $query->result();
		//print_r($this->db->last_query()); die;
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	public function set_confirmed($id,$data){
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	public function set_acknowled($id,$data){
		$this->db->where('id',$id);
		$return = $this->db->update('reservation',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
} 
?>