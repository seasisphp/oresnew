<?php
class Driver_model extends CI_Model{
    public function __construct(){
		$this->load->database();
	}
	
	public function add_driver($data){
		$this->db->insert('driver', $data);
		$last_id = $this->db->insert_id();
		if($last_id){
			return $last_id;
		}else{
			return false;
		}
	}
	
	public function get_driver($driver_id){
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->where('id',$driver_id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}
	
	public function download_drivers_detail()
	{
		
		$query = $this->db->query("SELECT driver.first_name, driver.last_name, driver.username, driver.email, 
									driver.mobile_number, driver.emergency_contact,
								    address_title.address_title_desc, driver.address_1, driver.address_2,
								    driver.city, driver.state
									FROM driver
									JOIN address_title
									WHERE driver.address_title = address_title.address_title_id"
								
									);
		
		return $query;
	}
	
	public function get_all_driver($limit,$start){
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->order_by("id", "desc");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Get All Active Drivers
	 * @author Gurpreet 
	 */
	 
	public function getActiveDrivers(){
		$this->db->select('*');
		$this->db->from('driver');
		//$this->db->where('status',1);	
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/**
	 * Get Calendar Output
	 * @author Gurpreet 
	 */
	public function getCalendarValues($start,$end,$driver_id){
		$this->db->select('*');
		$this->db->from('driver_availability');
		$st="driver_available_from >='".$start."' AND driver_available_to<='".$end."' AND driver_id=".$driver_id;
		$this->db->where($st);  
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/**
	 * Availabilty By Id
	 * @author Gurpreet 
	 */
	function getAvailabiltydetailsById($id){
		$this->db->select('status');
		$this->db->from('driver_availability');
		$this->db->where('id',$id);  
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result[0]->status;
		}else{
			return false;
		}
	
	}
	/**
	 * Add Driver Availabilty
	 * @author Gurpreet 
	 */
	public function addDriverAvailabilty($data){
		$this->db->insert('driver_availability',$data);
	}
	/**
	 * Edit Driver Availabilty
	 * @author Gurpreet 
	 */
	public function updateAvailabilty($data,$id){
		$this->db->where('id',$id);
		$return = $this->db->update('driver_availability',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function remove_driver($driver_id){
		$this->db->where('id',$driver_id);
		$this->db->delete('driver');
	}
	
	public function update_driver($data,$driver_id){
		$this->db->where('id',$driver_id);
		$return = $this->db->update('driver',$data);
		if($return > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 *  This function is used to return all user
	*  based on search criteria.
	*  Author: Devi Lal Verma
	*/
	public function search_driver($search,$limit,$start){
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->where('first_name',$search);
		$this->db->or_where('last_name',$search);
		$this->db->or_where('email',$search);
		//$this->db->order_by("id", "desc");
		$this->db->limit($limit, $start);
	
	
		$query = $this->db->get();
	
		$result = $query->result();
	
	
		if($result){
			return $result;
		}
		else{
			return false;
		}
	
	}
	/*
	* Counting the records based on the
	* user search term.
	* Author: Devi Lal Verma
	*/
	public function count_search_driver($search){
		 
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->where('first_name',$search);
		$this->db->or_where('last_name',$search);
		$this->db->or_where('email',$search);
		$query = $this->db->get();
		$count = $query->num_rows();
	
	
		if($count){
			return $count;
		}
		else{
			return false;
		}
	}
	/*
	* The purpose of the method,
	* will be to check the various different states
	* the $searchterm variable could be in.
	* Author: Devi Lal Verma
	*/
	public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('searchterm', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('searchterm'))
		{
			$searchterm = $this->session->userdata('searchterm');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}
	
	public function count_all_driver(){
	
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		$count = $query->num_rows();
	
		if($count){
			return $count;
		}else{
			return false;
		}
	}
	
	public function check_driver_email($email,$id){
		$this->db->select('email');
		$this->db->from('driver');
		$this->db->where('email',$email);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function check_driver_mobile_no($mobile_no,$id){
		$this->db->select('mobile_number');
		$this->db->from('driver');
		$this->db->where('mobile_number',$mobile_no);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	
	public function check_driver_emergency_no($mobile_no,$id){
		$this->db->select('emergency_contact');
		$this->db->from('driver');
		$this->db->where('emergency_contact',$mobile_no);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function check_user_name($username,$id){
		$this->db->select('username');
		$this->db->from('driver');
		$this->db->where('username',$username);
		$this->db->where_not_in('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return false;
		}else{
			return true;
		}
	}
	public function get_driver_id($trip_id){
		$this->db->select('driver_id');
		$this->db->from('reservation');
		$this->db->where('id',$trip_id);
		$query=$this->db->get();
		$result=$query->result();
		if($result){
			return $result;
		}
		else{
			return false;
		}
	}

}