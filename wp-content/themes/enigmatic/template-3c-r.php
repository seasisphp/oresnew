<?php
/**
 * Template Name: Three Column Left Navigation 
 *
 * A custom page template for displaying content with two sidebars on the left
 *
 * @package Enigmatic
 * @subpackage Template
 */
get_header();
?>

<div id="left_nav_3c_template" class="layout-3c-r">

    <?php get_template_part('page-content'); // Loads the reusable page-content.php template. ?>

    <?php mo_display_sidebars(); ?>

</div> <!-- #left_nav_3c_template -->

<?php get_footer(); ?>