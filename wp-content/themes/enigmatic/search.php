<?php
/**
 * Search Template
 *
 * This template is loaded when viewing search results and replaces the default template.
 * 
 * @package Enigmatic
 * @subpackage Template
 */



get_header(); 

mo_display_archive_content(); 

mo_display_sidebars(); 

get_footer(); 

?>