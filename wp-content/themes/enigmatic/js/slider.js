jQuery.noConflict();

jQuery(document).ready(function ($) {

    function bool_of(bool_string) {
        var bool_value = ((bool_string == "true" || bool_string == "1") ? true : false);
        return bool_value;
    }

    function toggle_of(bool_string) {
        var toggle_value = (bool_string == "true" ? 1 : 0);
        return toggle_value;
    }

    /*function onBeforeThumbnail(currentSlide, nextSlide, options, forwardFlag) {
        $(nextSlide).find('.thumbnail-caption').hide();
    }

    function onAfterThumbnail(currentSlide, nextSlide, options, forwardFlag) {
        $(nextSlide).find('.thumbnail-caption').show(300);
    }*/

    function onBeforeThumbnail(slider) {
        slider.slides.eq(slider.animatingTo).find('.thumbnail-caption').hide();
    }

    function onAfterThumbnail(slider) {
        slider.slides.eq(slider.animatingTo).find('.thumbnail-caption').show(300);
    }

    function onStartThumbnail(slider) {

        $('#thumbnail-slider-wrapper .slideshow_toggle').click(
            function () {
                $element = $('#thumbnail-slider-wrapper');
                if ($element.hasClass('paused')) {
                    slider.play();
                    $element.removeClass('paused');
                }
                else {
                    slider.pause();
                    $element.addClass('paused');
                }
                return false;
            });
    }

    $('#thumbnail-slider').flexslider({
        animation:'fade',
        easing: 'swing',
        selector:'.hentry',
        animationSpeed:1000,
        slideshowSpeed:2000,
        controlNav: false,
        directionNav: false,
        before:onBeforeThumbnail,
        after:onAfterThumbnail,
        start:onStartThumbnail
    });

    $('#thumbnail-slider-wrapper').hover(
        function () {
            $(this).find(".slideshow_controls").show();
        },
        function () {
            $(this).find(".slideshow_controls").hide();
        }
    );

    if (typeof slider_chosen === 'undefined')
        return;

    if (slider_chosen == 'Nivo') {

        $('#nivo-slider').nivoSlider({
            effect:nivo_effect, // Specify sets like: 'fold,fade,sliceDown'
            slices:parseInt(nivo_slices), // For slice animations
            animSpeed:parseInt(nivo_animation_speed), // Slide transition speed
            pauseTime:parseInt(nivo_pause_time), // How long each slide will show
            startSlide:0, // Set starting Slide (0 index)
            directionNav:bool_of(nivo_dir_navigation), // Next & Prev navigation
            directionNavHide:bool_of(nivo_dir_navigation_hide), // Only show on hover
            controlNav:bool_of(nivo_controls), // 1,2,3... navigation
            keyboardNav:false, // Use left & right arrows
            pauseOnHover:bool_of(nivo_pause_on_hover), // Stop animation while hovering
            manualAdvance:false, // Force manual transitions
            captionOpacity:parseFloat(nivo_caption_opacity), // Universal caption opacity
            randomStart:bool_of(nivo_start_random_slide) // Start on a random slide
        });
    } else if (slider_chosen == 'FlexSlider') {

        $('#flex-slider-wrapper .flexslider').flexslider({
            animation: flex_slider_effect,
            slideshowSpeed:parseInt(flex_slider_pause_time), //Integer: Set the speed of the slideshow cycling, in milliseconds
            animationSpeed:parseInt(flex_slider_animation_speed),
            pauseOnHover:toggle_of(flex_slider_pause_on_hover),
            randomize:toggle_of(flex_slider_display_random_slide)
        });
    }

});