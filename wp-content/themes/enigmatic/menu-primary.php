<?php
/**
 * Primary Menu Template
 *
 * Displays the Primary Menu if it has active menu items.
 *
 * @package Enigmatic
 * @subpackage Template
 */

if (has_nav_menu('primary')) : ?>

<div id="primary-menu" class="menu-container">

    <?php wp_nav_menu(array('theme_location' => 'primary',
    'container' => false,
    'menu_class' => 'menu',
    'menu_id' => '',
    'fallback_cb' => false
)); ?>

</div><!-- #menu-primary .menu-container -->

<?php endif; ?>