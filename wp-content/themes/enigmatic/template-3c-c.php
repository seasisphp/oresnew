<?php
/**
 * Template Name: Three Column Left Right Navigation 
 *
 * A custom page template for displaying content with both left and right sidebars
 *
 * @package Enigmatic
 * @subpackage Template
 */

get_header(); ?>

<div id="left_right_nav_3c_template" class="layout-3c-c">

	<?php get_template_part( 'page-content' ); // Loads the reusable page-content.php template. ?>

	<?php mo_display_sidebars(); ?>

</div> <!-- #left_right_nav_3c_template -->

<?php get_footer();  ?>