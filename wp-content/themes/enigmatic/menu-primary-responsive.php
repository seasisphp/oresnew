<?php
/**
 * Responsive Primary Menu Template
 *
 * Displays the Responsive Primary Menu if it has active menu items.
 *
 * Updated for simulating responsive menu.
 *
 * @package Enigmatic
 * @subpackage Template
 */

class ResponsiveMenuWalker extends Walker_Nav_Menu {

    private $index = 0;

    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        //$output .= "\n$indent<ul class=\"sub-menu\">\n";
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        //$output .= "$indent</ul>\n";
    }

    function start_el(&$output, $item, $depth, $args) {

        global $responsiveMenuSelect;
        global $wp_query;

        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $dashes = ($depth) ? str_repeat('&nbsp;&nbsp;', $depth) . '-&nbsp;' : ''; //"&ndash; "

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $attributes = !empty($item->url) ? ' value="' . esc_attr($item->url) . '"' : '';

        //Do not select parents of currently selected menu
        if (strpos($class_names, 'current-menu-item') > 0 && !(strpos($class_names, 'current-menu-parent') > 0)) {
            $attributes .= ' selected="selected"';
        }

        $output .= $indent . '<option ' . $id . $attributes . $class_names . '>';

        //$item_output = $args->before . ($depth ? '' : '<strong>');
        $item_output = $args->before . ($depth ? '' : '');
        $item_output .= $dashes . $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        //$item_output .= $args->after . ($depth ? '' : '</strong>');
        $item_output .= $args->after . ($depth ? '' : '');

        $output .= $item_output;

        $output .= "</option>\n";
    }

    function end_el(&$output, $item, $depth) {
        //$output .= "</li>\n";
        return;
    }
}


if (has_nav_menu('primary')) :

    echo '<div id="responsive-primary-menu" class="responsive-menu-container">';

    echo '<div id="responsive-menu-wrap" class="responsive-menu-wrap">';
    $first_element = '<option value="">Navigation Menu</option>';
    wp_nav_menu(array('theme_location' => 'primary', 'walker' => new ResponsiveMenuWalker(), 'items_wrap' => '<select id="%1$s" class="%2$s">' . $first_element . '%3$s</select>', 'container' => false, 'menu_class' => 'responsive-select-menu', 'menu_id' => 'responsive-select-menu', 'fallback_cb' => false));
    echo '</div>';

    echo '</div>';

endif;