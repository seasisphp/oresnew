<?php

$skin = $_GET['skin'];

header("Content-type: text/css; charset: UTF-8");

if (empty($skin))
    $skin = 'default';

$output = <<<HTML

#site-title a {
    background: url("{$skin}/logo.png") no-repeat scroll 0 0 transparent;
}

.folded-edge {
    background: url("{$skin}/fancy-hover-fold.png") no-repeat -9999px -9999px #f9f9f9;
}

a.more-link { background-image: url({$skin}/arrow-more.png); }

.testimonials2-slider-container blockquote .footer,
.testimonials3-slider-container blockquote .footer { background-image: url("{$skin}/seperator-testimonials.png"); }

.toggle-label { background-image: url("{$skin}/toggle-button-plus.png"); }

.active-toggle .toggle-label { background-image: url("{$skin}/toggle-button-minus.png"); }


HTML;

echo $output;
?>