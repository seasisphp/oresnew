<?php

/* Called by header.php for initializing all the options for use by the JS scripts */

function setup_theme_options_for_scripts() {

    echo '<script type="text/javascript">';

    setup_general_theme_options();

    setup_slider_options();

    setup_animation_options();

    echo '</script>';
}

function setup_general_theme_options() {

    $ajax_portfolio = mo_get_theme_option('mo_ajax_portfolio');
    if ($ajax_portfolio)
        echo 'var ajax_portfolio = true;';
    else
        echo 'var ajax_portfolio = false;';
}

function setup_slider_options() {

    if (!is_page_template('template-advanced-home.php') || mo_get_theme_option('mo_disable_sliders')) {
        echo 'var slider_chosen="None";';
        return;
    }

    $slider_type = mo_get_theme_option('mo_slider_choice', 'Nivo');
    echo 'var slider_chosen="' . $slider_type . '";'; // output slider option chosen by the user for later use

    if ($slider_type == 'Nivo')
        setup_nivo_slider_options();
    elseif ($slider_type == 'FlexSlider')
        setup_flex_slider_options();

}


function setup_flex_slider_options() {

    global $mo_theme;

    $flex_slider_effect = $mo_theme->get_theme_option('mo_flex_slider_effect', 'fade');
    $flex_slider_animation_speed = $mo_theme->get_theme_option('mo_flex_slider_animation_speed', 1500);
    $flex_slider_pause_time = $mo_theme->get_theme_option('mo_flex_slider_pause_time', 5000);
    $flex_slider_pause_on_hover = $mo_theme->get_theme_option('mo_flex_slider_disable_pause_on_hover') ? 'false' : 'true';
    $flex_slider_display_random_slide = $mo_theme->get_theme_option('mo_flex_slider_display_random_slide') ? 'true' : 'false';

    echo 'var flex_slider_effect="' . $flex_slider_effect . '";';
    echo 'var flex_slider_animation_speed=' . intval($flex_slider_animation_speed) . ';';
    echo 'var flex_slider_pause_time=' . intval($flex_slider_pause_time) . ';';
    echo 'var flex_slider_pause_on_hover="' . $flex_slider_pause_on_hover . '";';
    echo 'var flex_slider_display_random_slide="' . $flex_slider_display_random_slide . '";';
}

function setup_nivo_slider_options() {

    global $mo_theme;

    $nivo_effect = $mo_theme->get_theme_option('mo_nivo_effect', array('random'));
    $nivo_effect = implode(',', $nivo_effect);
    $nivo_slices = $mo_theme->get_theme_option('mo_nivo_slices', 15);
    $nivo_animation_speed = $mo_theme->get_theme_option('mo_nivo_animation_speed', 500);
    $nivo_pause_time = $mo_theme->get_theme_option('mo_nivo_pause_time', 3000);
    $nivo_dir_navigation = $mo_theme->get_theme_option('mo_nivo_hide_dir_navigation') ? 'false' : 'true';
    $nivo_dir_navigation_hide = $mo_theme->get_theme_option('mo_nivo_dir_show_navigation') ? 'false' : 'true'; // show navigation on hover only
    $nivo_controls = $mo_theme->get_theme_option('mo_nivo_hide_controls') ? 'false' : 'true'; // 1,2,3... navigation controls
    $nivo_pause_on_hover = $mo_theme->get_theme_option('mo_nivo_disable_pause_on_hover') ? 'false' : 'true';

    $nivo_caption_opacity = $mo_theme->get_theme_option('mo_nivo_caption_opacity', '0.6');
    if ($nivo_caption_opacity < 0 || $nivo_caption_opacity > 1)
        $nivo_caption_opacity = '0.6';
    $nivo_disable_caption = mo_get_theme_option('mo_disable_nivo_slider_caption');
    if ($nivo_disable_caption)
        $nivo_caption_opacity = '0'; // Hack to hide the caption placeholder
    $nivo_start_random_slide = $mo_theme->get_theme_option('mo_nivo_start_random_slider') ? 'true' : 'false';

    echo 'var nivo_effect="' . $nivo_effect . '";';
    echo 'var nivo_slices=' . intval($nivo_slices) . ';';
    echo 'var nivo_animation_speed=' . intval($nivo_animation_speed) . ';';
    echo 'var nivo_pause_time=' . intval($nivo_pause_time) . ';';
    echo 'var nivo_dir_navigation="' . $nivo_dir_navigation . '";';
    echo 'var nivo_dir_navigation_hide="' . $nivo_dir_navigation_hide . '";';
    echo 'var nivo_controls="' . $nivo_controls . '";';
    echo 'var nivo_pause_on_hover="' . $nivo_pause_on_hover . '";';
    echo 'var nivo_caption_opacity="' . floatval($nivo_caption_opacity) . '";';
    echo 'var nivo_start_random_slide="' . $nivo_start_random_slide . '";';
}

function setup_animation_options() {

    global $mo_theme;

    $disable_smooth_page_load = 'true';
    $disable_animations_on_page = 'true';

    if (mo_browser_supports_css3_animations()) {
        $disable_smooth_page_load = $mo_theme->get_theme_option('mo_disable_smooth_page_load') ? 'true' : 'false';
        $disable_animations_on_page = $mo_theme->get_theme_option('mo_disable_animations_on_page') ? 'true' : 'false';
    }

    echo 'var disable_smooth_page_load=' . $disable_smooth_page_load . ';';
    echo 'var disable_animations_on_page=' . $disable_animations_on_page . ';';
}


?>