<?php
/**
 * Initialize the options before anything else.
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
    /**
     * Get a copy of the saved settings array.
     */
    $saved_settings = get_option( 'option_tree_settings', array() );

    /**
     * Custom settings array that will eventually be
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
        'contextual_help' => array(
            'sidebar'       => ''
        ),
        'sections'        => array(
            array(
                'id'          => 'general_default',
                'title'       => 'General'
            ),
            array(
                'id'          => 'mo_logo_options',
                'title'       => 'Site Logo'
            ),
            array(
                'id'          => 'mo_fonts_tab',
                'title'       => 'Fonts'
            ),
            array(
                'id'          => 'mo_color_tab',
                'title'       => 'Colors'
            ),
            array(
                'id'          => 'mo_backgrounds_tab',
                'title'       => 'Backgrounds'
            ),
            array(
                'id'          => 'mo_sidebar_tab',
                'title'       => 'Sidebar'
            ),
            array(
                'id'          => 'mo_primary_menu_tab',
                'title'       => 'Primary Menu'
            ),
            array(
                'id'          => 'mo_header',
                'title'       => 'Header'
            ),
            array(
                'id'          => 'mo_footer',
                'title'       => 'Footer'
            ),
            array(
                'id'          => 'layout_management',
                'title'       => 'Layout Management'
            ),
            array(
                'id'          => 'mo_portfolio_page',
                'title'       => 'Portfolio Page'
            ),
            array(
                'id'          => 'mo_flex_slider',
                'title'       => 'Flex Slider'
            ),
            array(
                'id'          => 'mo_nivo_slider',
                'title'       => 'Nivo Slider'
            ),
            array(
                'id'          => 'video_options',
                'title'       => 'Video'
            ),
            array(
                'id'          => 'mo_custom_css_tab',
                'title'       => 'Custom CSS'
            ),
            array(
                'id'          => 'mo_miscellaneous_options',
                'title'       => 'Miscellaneous'
            )
        ),
        'settings'        => array(
            array(
                'id'          => 'mo_slider_choice',
                'label'       => 'Slider Choice',
                'desc'        => 'Select your choice of Slider type.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'FlexSlider',
                        'label'       => 'FlexSlider',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nivo',
                        'label'       => 'Nivo',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_disable_sliders',
                'label'       => 'Disable Sliders in Home Page',
                'desc'        => 'Do not display sliders in the home page. Can use Slider Area widgets to show in the slider area static text or the amazing revolution slider.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Disable Sliders',
                        'label'       => 'Disable Sliders',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_disable_smooth_page_load',
                'label'       => 'Disable Smooth Page Load',
                'desc'        => 'Disable the smooth fade animation for page load.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_disable_animations_on_page',
                'label'       => 'Disable Animations on Pages',
                'desc'        => 'Do not animate page elements - fade, bounce, scale etc. as seen in Home Pages and others',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_theme_skin',
                'label'       => 'Theme Skin',
                'desc'        => 'Choose the skin you wish to use for the theme. If none is specified, default.css is applied to the theme. All skins are applied by CSS files placed in the [theme directory]/css/skins folder.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Default',
                        'label'       => 'Default',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'green',
                        'label'       => 'Green',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'cyan',
                        'label'       => 'Cyan',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'orange',
                        'label'       => 'Orange',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'pink',
                        'label'       => 'Pink',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'red',
                        'label'       => 'Red',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'teal',
                        'label'       => 'Teal',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_thumbnail_generation',
                'label'       => 'Thumbnail Generation',
                'desc'        => 'Choose the thumbnail generation method to use for your site. Wordpress is recommended while Aqua Resize helps if you need images of custom height/width for your posts and want to generate thumbnails on the fly at runtime.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Wordpress',
                        'label'       => 'Wordpress',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aqua',
                        'label'       => 'Aqua',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_retain_image_height',
                'label'       => 'Retain Image Height',
                'desc'        => 'Retain the natural image height of the image when shown in the single post or page. No crop is applied height-wise on the image when this option is chosen.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Enable',
                        'label'       => 'Enable',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_excerpt_count',
                'label'       => 'Excerpt Character Count',
                'desc'        => 'Specify the number of characters that needs to be displayed for excerpts shown in archive pages.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_show_content_in_archives',
                'label'       => 'Show Content in Archive Pages',
                'desc'        => 'Specify if content (excerpts through <i>more</i> tag in content) are to be shown in archive pages instead of manual excerpts (recommended for SEO) entered in the post edit window. Read more about WordPress excerpts <a href="http://codex.wordpress.org/Excerpt">here</a>.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'general_default',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_site_logo',
                'label'       => 'Site Logo Image',
                'desc'        => 'Provide the custom logo image for the site. Max allowed dimensions are 300x100.',
                'std'         => '',
                'type'        => 'upload',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_logo_height',
                'label'       => 'Height of Logo Image (px)',
                'desc'        => 'Specify in Pixels the height of the logo image. The max width of the image is 300px.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_logo_width',
                'label'       => 'Width of Logo(px)',
                'desc'        => 'Input width of the logo in pixel units.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_logo_margin_top',
                'label'       => 'Site Logo Top Margin (px)',
                'desc'        => 'Provide the top margin in Pixel units to help the logo image or text to be placed at the exact position of your choice in the header. Around 30 is recommended if you have a single line logo.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_use_text_logo',
                'label'       => 'Use Text Logo',
                'desc'        => 'Check if you want to use text as logo instead of image. The logo image will be ignored.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_logo_font',
                'label'       => 'Text Logo Font',
                'desc'        => 'Choose the font for the text logo of the site.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Arial',
                        'label'       => 'Arial',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Helvetica',
                        'label'       => 'Helvetica',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Georgia',
                        'label'       => 'Georgia',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lucida Sans Unicode',
                        'label'       => 'Lucida Sans Unicode',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tahoma',
                        'label'       => 'Tahoma',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geneva',
                        'label'       => 'Geneva',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Times New Roman',
                        'label'       => 'Times New Roman',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trebuchet MS',
                        'label'       => 'Trebuchet MS',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Verdana',
                        'label'       => 'Verdana',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Courier New',
                        'label'       => 'Courier New',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arial Black',
                        'label'       => 'Arial Black',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Eater *',
                        'label'       => 'Eater *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chango *',
                        'label'       => 'Chango *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Press Start 2P *',
                        'label'       => 'Press Start 2P *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Basic *',
                        'label'       => 'Gentium Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon SC *',
                        'label'       => 'IM Fell French Canon SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Sketch *',
                        'label'       => 'Londrina Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Concert One *',
                        'label'       => 'Concert One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cagliostro *',
                        'label'       => 'Cagliostro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredoka One *',
                        'label'       => 'Fredoka One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Loved by the King *',
                        'label'       => 'Loved by the King *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belgrano *',
                        'label'       => 'Belgrano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quantico *',
                        'label'       => 'Quantico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantata One *',
                        'label'       => 'Cantata One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Berkshire Swash *',
                        'label'       => 'Berkshire Swash *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spirax *',
                        'label'       => 'Spirax *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Graduate *',
                        'label'       => 'Graduate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Swanky and Moo Moo *',
                        'label'       => 'Swanky and Moo Moo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock *',
                        'label'       => 'Overlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bangers *',
                        'label'       => 'Bangers *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pontano Sans *',
                        'label'       => 'Pontano Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light *',
                        'label'       => 'Shadows Into Light *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ceviche One *',
                        'label'       => 'Ceviche One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andada *',
                        'label'       => 'Andada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Boogaloo *',
                        'label'       => 'Boogaloo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fanwood Text *',
                        'label'       => 'Fanwood Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Codystar *',
                        'label'       => 'Codystar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voltaire *',
                        'label'       => 'Voltaire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'EB Garamond *',
                        'label'       => 'EB Garamond *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belleza *',
                        'label'       => 'Belleza *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cutive *',
                        'label'       => 'Cutive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo *',
                        'label'       => 'Bilbo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emblema One *',
                        'label'       => 'Emblema One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Felipa *',
                        'label'       => 'Felipa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lusitana *',
                        'label'       => 'Lusitana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Sans *',
                        'label'       => 'Port Lligat Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lovers Quarrel *',
                        'label'       => 'Lovers Quarrel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crafty Girls *',
                        'label'       => 'Crafty Girls *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alex Brush *',
                        'label'       => 'Alex Brush *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Play *',
                        'label'       => 'Play *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Doppio One *',
                        'label'       => 'Doppio One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Sheppards *',
                        'label'       => 'Mrs Sheppards *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spicy Rice *',
                        'label'       => 'Spicy Rice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Limelight *',
                        'label'       => 'Limelight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Give You Glory *',
                        'label'       => 'Give You Glory *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredericka the Great *',
                        'label'       => 'Fredericka the Great *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oleo Script *',
                        'label'       => 'Oleo Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cardo *',
                        'label'       => 'Cardo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ledger *',
                        'label'       => 'Ledger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italiana *',
                        'label'       => 'Italiana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One *',
                        'label'       => 'Bowlby One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homenaje *',
                        'label'       => 'Homenaje *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Karla *',
                        'label'       => 'Karla *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italianno *',
                        'label'       => 'Italianno *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Condensed *',
                        'label'       => 'Cabin Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dosis *',
                        'label'       => 'Dosis *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Krona One *',
                        'label'       => 'Krona One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Advent Pro *',
                        'label'       => 'Advent Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Podkova *',
                        'label'       => 'Podkova *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chicle *',
                        'label'       => 'Chicle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Didone *',
                        'label'       => 'Antic Didone *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Exo *',
                        'label'       => 'Exo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Syncopate *',
                        'label'       => 'Syncopate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sancreek *',
                        'label'       => 'Sancreek *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marko One *',
                        'label'       => 'Marko One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metamorphous *',
                        'label'       => 'Metamorphous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Outline *',
                        'label'       => 'Londrina Outline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patua One *',
                        'label'       => 'Patua One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Bedfort *',
                        'label'       => 'Mr Bedfort *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nunito *',
                        'label'       => 'Nunito *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inika *',
                        'label'       => 'Inika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta *',
                        'label'       => 'Allerta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosarivo *',
                        'label'       => 'Rosarivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Viga *',
                        'label'       => 'Viga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shojumaru *',
                        'label'       => 'Shojumaru *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Francois One *',
                        'label'       => 'Francois One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'The Girl Next Door *',
                        'label'       => 'The Girl Next Door *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passion One *',
                        'label'       => 'Passion One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Days One *',
                        'label'       => 'Days One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Sans *',
                        'label'       => 'Josefin Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Acme *',
                        'label'       => 'Acme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asul *',
                        'label'       => 'Asul *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Happy Monkey *',
                        'label'       => 'Happy Monkey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Sans Libre *',
                        'label'       => 'Averia Sans Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rancho *',
                        'label'       => 'Rancho *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anonymous Pro *',
                        'label'       => 'Anonymous Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glegoo *',
                        'label'       => 'Glegoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Galdeano *',
                        'label'       => 'Galdeano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Saint Delafield *',
                        'label'       => 'Mrs Saint Delafield *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kranky *',
                        'label'       => 'Kranky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Share *',
                        'label'       => 'Share *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Baumans *',
                        'label'       => 'Baumans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quicksand *',
                        'label'       => 'Quicksand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Habibi *',
                        'label'       => 'Habibi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Seaweed Script *',
                        'label'       => 'Seaweed Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yesteryear *',
                        'label'       => 'Yesteryear *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Economica *',
                        'label'       => 'Economica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Niconne *',
                        'label'       => 'Niconne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sofia *',
                        'label'       => 'Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bonbon *',
                        'label'       => 'Bonbon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Herr Von Muellerhoff *',
                        'label'       => 'Herr Von Muellerhoff *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Duru Sans *',
                        'label'       => 'Duru Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Creepster *',
                        'label'       => 'Creepster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Great Vibes *',
                        'label'       => 'Great Vibes *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Junge *',
                        'label'       => 'Junge *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Condensed *',
                        'label'       => 'Stint Ultra Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya SC *',
                        'label'       => 'Alegreya SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Russo One *',
                        'label'       => 'Russo One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amatic SC *',
                        'label'       => 'Amatic SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Condiment *',
                        'label'       => 'Condiment *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Medula One *',
                        'label'       => 'Medula One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Judson *',
                        'label'       => 'Judson *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Orbitron *',
                        'label'       => 'Orbitron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster Two *',
                        'label'       => 'Lobster Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pinyon Script *',
                        'label'       => 'Pinyon Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unkempt *',
                        'label'       => 'Unkempt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nixie One *',
                        'label'       => 'Nixie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trocchi *',
                        'label'       => 'Trocchi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Comfortaa *',
                        'label'       => 'Comfortaa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Erica One *',
                        'label'       => 'Erica One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Mono *',
                        'label'       => 'Ubuntu Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montaga *',
                        'label'       => 'Montaga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lilita One *',
                        'label'       => 'Lilita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Expanded *',
                        'label'       => 'Stint Ultra Expanded *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Love Ya Like A Sister *',
                        'label'       => 'Love Ya Like A Sister *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oxygen *',
                        'label'       => 'Oxygen *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Frijole *',
                        'label'       => 'Frijole *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arvo *',
                        'label'       => 'Arvo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prata *',
                        'label'       => 'Prata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Magra *',
                        'label'       => 'Magra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oswald *',
                        'label'       => 'Oswald *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flamenco *',
                        'label'       => 'Flamenco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dorsa *',
                        'label'       => 'Dorsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bad Script *',
                        'label'       => 'Bad Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika *',
                        'label'       => 'Signika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Serif *',
                        'label'       => 'Droid Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wellfleet *',
                        'label'       => 'Wellfleet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata SC *',
                        'label'       => 'Diplomata SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Average *',
                        'label'       => 'Average *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Righteous *',
                        'label'       => 'Righteous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento Sans *',
                        'label'       => 'Quattrocento Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rouge Script *',
                        'label'       => 'Rouge Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruluko *',
                        'label'       => 'Ruluko *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aclonica *',
                        'label'       => 'Aclonica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruda *',
                        'label'       => 'Ruda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kotta One *',
                        'label'       => 'Kotta One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Questrial *',
                        'label'       => 'Questrial *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jolly Lodger *',
                        'label'       => 'Jolly Lodger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cookie *',
                        'label'       => 'Cookie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Changa One *',
                        'label'       => 'Changa One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alfa Slab One *',
                        'label'       => 'Alfa Slab One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Slab *',
                        'label'       => 'Josefin Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantarell *',
                        'label'       => 'Cantarell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fjord One *',
                        'label'       => 'Fjord One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merriweather *',
                        'label'       => 'Merriweather *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Philosopher *',
                        'label'       => 'Philosopher *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela Round *',
                        'label'       => 'Varela Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amethysta *',
                        'label'       => 'Amethysta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dawning of a New Day *',
                        'label'       => 'Dawning of a New Day *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coming Soon *',
                        'label'       => 'Coming Soon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light Two *',
                        'label'       => 'Shadows Into Light Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mako *',
                        'label'       => 'Mako *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Black Ops One *',
                        'label'       => 'Black Ops One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rationale *',
                        'label'       => 'Rationale *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lato *',
                        'label'       => 'Lato *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trochut *',
                        'label'       => 'Trochut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gloria Hallelujah *',
                        'label'       => 'Gloria Hallelujah *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Parisienne *',
                        'label'       => 'Parisienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Short Stack *',
                        'label'       => 'Short Stack *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goudy Bookletter 1911 *',
                        'label'       => 'Goudy Bookletter 1911 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Didact Gothic *',
                        'label'       => 'Didact Gothic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo Swash Caps *',
                        'label'       => 'Bilbo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vollkorn *',
                        'label'       => 'Vollkorn *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu *',
                        'label'       => 'Ubuntu *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Slab *',
                        'label'       => 'Port Lligat Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Princess Sofia *',
                        'label'       => 'Princess Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mountains of Christmas *',
                        'label'       => 'Mountains of Christmas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Qwigley *',
                        'label'       => 'Qwigley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Candal *',
                        'label'       => 'Candal *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Old Standard TT *',
                        'label'       => 'Old Standard TT *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Istok Web *',
                        'label'       => 'Istok Web *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Puritan *',
                        'label'       => 'Puritan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montez *',
                        'label'       => 'Montez *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lekton *',
                        'label'       => 'Lekton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cambo *',
                        'label'       => 'Cambo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela *',
                        'label'       => 'Varela *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glass Antiqua *',
                        'label'       => 'Glass Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bigshot One *',
                        'label'       => 'Bigshot One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arimo *',
                        'label'       => 'Arimo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosario *',
                        'label'       => 'Rosario *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rokkitt *',
                        'label'       => 'Rokkitt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Architects Daughter *',
                        'label'       => 'Architects Daughter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans *',
                        'label'       => 'Droid Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cuprum *',
                        'label'       => 'Cuprum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Slim *',
                        'label'       => 'Nova Slim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Book Basic *',
                        'label'       => 'Gentium Book Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aladin *',
                        'label'       => 'Aladin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Corben *',
                        'label'       => 'Corben *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Six Caps *',
                        'label'       => 'Six Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voces *',
                        'label'       => 'Voces *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Original Surfer *',
                        'label'       => 'Original Surfer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kelly Slab *',
                        'label'       => 'Kelly Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda Caption *',
                        'label'       => 'Coda Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cedarville Cursive *',
                        'label'       => 'Cedarville Cursive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playfair Display *',
                        'label'       => 'Playfair Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Actor *',
                        'label'       => 'Actor *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pacifico *',
                        'label'       => 'Pacifico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Caption *',
                        'label'       => 'PT Sans Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Covered By Your Grace *',
                        'label'       => 'Covered By Your Grace *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Convergence *',
                        'label'       => 'Convergence *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Handlee *',
                        'label'       => 'Handlee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Raleway *',
                        'label'       => 'Raleway *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wallpoet *',
                        'label'       => 'Wallpoet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Script *',
                        'label'       => 'Nova Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceland *',
                        'label'       => 'Iceland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Meddon *',
                        'label'       => 'Meddon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Shadow *',
                        'label'       => 'Londrina Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nobile *',
                        'label'       => 'Nobile *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carter One *',
                        'label'       => 'Carter One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Walter Turncoat *',
                        'label'       => 'Walter Turncoat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amaranth *',
                        'label'       => 'Amaranth *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Indie Flower *',
                        'label'       => 'Indie Flower *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Telex *',
                        'label'       => 'Telex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ropa Sans *',
                        'label'       => 'Ropa Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abel *',
                        'label'       => 'Abel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock SC *',
                        'label'       => 'Overlock SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sigmar One *',
                        'label'       => 'Sigmar One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate *',
                        'label'       => 'Fascinate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Hammersmith One *',
                        'label'       => 'Hammersmith One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vast Shadow *',
                        'label'       => 'Vast Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruge Boogie *',
                        'label'       => 'Ruge Boogie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Balthazar *',
                        'label'       => 'Balthazar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Simonetta *',
                        'label'       => 'Simonetta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Schoolbell *',
                        'label'       => 'Schoolbell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pompiere *',
                        'label'       => 'Pompiere *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kenia *',
                        'label'       => 'Kenia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Slab *',
                        'label'       => 'Antic Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caesar Dressing *',
                        'label'       => 'Caesar Dressing *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Gruesa Libre *',
                        'label'       => 'Averia Gruesa Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bevan *',
                        'label'       => 'Bevan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Forum *',
                        'label'       => 'Forum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Narrow *',
                        'label'       => 'PT Sans Narrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturMaguntia *',
                        'label'       => 'UnifrakturMaguntia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vidaloka *',
                        'label'       => 'Vidaloka *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sansita One *',
                        'label'       => 'Sansita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kristi *',
                        'label'       => 'Kristi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Imprima *',
                        'label'       => 'Imprima *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sail *',
                        'label'       => 'Sail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tulpen One *',
                        'label'       => 'Tulpen One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dynalight *',
                        'label'       => 'Dynalight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chewy *',
                        'label'       => 'Chewy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Norican *',
                        'label'       => 'Norican *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Redressed *',
                        'label'       => 'Redressed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One SC *',
                        'label'       => 'Bowlby One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Holtwood One SC *',
                        'label'       => 'Holtwood One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Dafoe *',
                        'label'       => 'Mr Dafoe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bentham *',
                        'label'       => 'Bentham *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento *',
                        'label'       => 'Quattrocento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer SC *',
                        'label'       => 'IM Fell Great Primer SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tinos *',
                        'label'       => 'Tinos *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kreon *',
                        'label'       => 'Kreon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buda *',
                        'label'       => 'Buda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lora *',
                        'label'       => 'Lora *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans *',
                        'label'       => 'PT Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Open Sans Condensed *',
                        'label'       => 'Open Sans Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Source Sans Pro *',
                        'label'       => 'Source Sans Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Esteban *',
                        'label'       => 'Esteban *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Armata *',
                        'label'       => 'Armata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Contrail One *',
                        'label'       => 'Contrail One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'La Belle Aurore *',
                        'label'       => 'La Belle Aurore *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Astloch *',
                        'label'       => 'Astloch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Satisfy *',
                        'label'       => 'Satisfy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oldenburg *',
                        'label'       => 'Oldenburg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allura *',
                        'label'       => 'Allura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homemade Apple *',
                        'label'       => 'Homemade Apple *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Audiowide *',
                        'label'       => 'Audiowide *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo Swash Caps *',
                        'label'       => 'Macondo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans Mono *',
                        'label'       => 'Droid Sans Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Petrona *',
                        'label'       => 'Petrona *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Electrolize *',
                        'label'       => 'Electrolize *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inconsolata *',
                        'label'       => 'Inconsolata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aguafina Script *',
                        'label'       => 'Aguafina Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda *',
                        'label'       => 'Coda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fontdiner Swanky *',
                        'label'       => 'Fontdiner Swanky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Slackey *',
                        'label'       => 'Slackey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'News Cycle *',
                        'label'       => 'News Cycle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Calligraffitti *',
                        'label'       => 'Calligraffitti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic *',
                        'label'       => 'Antic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif Caption *',
                        'label'       => 'PT Serif Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Cut *',
                        'label'       => 'Nova Cut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bree Serif *',
                        'label'       => 'Bree Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike *',
                        'label'       => 'Alike *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montserrat *',
                        'label'       => 'Montserrat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mystery Quest *',
                        'label'       => 'Mystery Quest *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Another Hand *',
                        'label'       => 'Just Another Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fondamento *',
                        'label'       => 'Fondamento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Knewave *',
                        'label'       => 'Knewave *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer *',
                        'label'       => 'IM Fell Great Primer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gruppo *',
                        'label'       => 'Gruppo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif *',
                        'label'       => 'PT Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Adamina *',
                        'label'       => 'Adamina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stardos Stencil *',
                        'label'       => 'Stardos Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Radley *',
                        'label'       => 'Radley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cherry Cream Soda *',
                        'label'       => 'Cherry Cream Soda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patrick Hand *',
                        'label'       => 'Patrick Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anton *',
                        'label'       => 'Anton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jockey One *',
                        'label'       => 'Jockey One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unlock *',
                        'label'       => 'Unlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Round *',
                        'label'       => 'Nova Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Over the Rainbow *',
                        'label'       => 'Over the Rainbow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Megrim *',
                        'label'       => 'Megrim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chelsea Market *',
                        'label'       => 'Chelsea Market *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arbutus *',
                        'label'       => 'Arbutus *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata *',
                        'label'       => 'Diplomata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caudex *',
                        'label'       => 'Caudex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nosifer *',
                        'label'       => 'Nosifer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Leckerli One *',
                        'label'       => 'Leckerli One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica SC *',
                        'label'       => 'IM Fell Double Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rochester *',
                        'label'       => 'Rochester *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passero One *',
                        'label'       => 'Passero One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar *',
                        'label'       => 'Geostar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prosto One *',
                        'label'       => 'Prosto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poller One *',
                        'label'       => 'Poller One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Condensed *',
                        'label'       => 'Ubuntu Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andika *',
                        'label'       => 'Andika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gravitas One *',
                        'label'       => 'Gravitas One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rock Salt *',
                        'label'       => 'Rock Salt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruslan Display *',
                        'label'       => 'Ruslan Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius *',
                        'label'       => 'Delius *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aubrey *',
                        'label'       => 'Aubrey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin *',
                        'label'       => 'Cabin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Swash Caps *',
                        'label'       => 'Delius Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wire One *',
                        'label'       => 'Wire One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'MedievalSharp *',
                        'label'       => 'MedievalSharp *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Mono *',
                        'label'       => 'PT Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Oval *',
                        'label'       => 'Nova Oval *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sue Ellen Francisco *',
                        'label'       => 'Sue Ellen Francisco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian *',
                        'label'       => 'Miltonian *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica *',
                        'label'       => 'IM Fell Double Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yellowtail *',
                        'label'       => 'Yellowtail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sorts Mill Goudy *',
                        'label'       => 'Sorts Mill Goudy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Permanent Marker *',
                        'label'       => 'Permanent Marker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poly *',
                        'label'       => 'Poly *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dancing Script *',
                        'label'       => 'Dancing Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maiden Orange *',
                        'label'       => 'Maiden Orange *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coustard *',
                        'label'       => 'Coustard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Supermercado One *',
                        'label'       => 'Supermercado One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Atomic Age *',
                        'label'       => 'Atomic Age *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cousine *',
                        'label'       => 'Cousine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federant *',
                        'label'       => 'Federant *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Irish Grover *',
                        'label'       => 'Irish Grover *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stoke *',
                        'label'       => 'Stoke *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Snippet *',
                        'label'       => 'Snippet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monsieur La Doulaise *',
                        'label'       => 'Monsieur La Doulaise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crimson Text *',
                        'label'       => 'Crimson Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Della Respira *',
                        'label'       => 'Della Respira *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smokum *',
                        'label'       => 'Smokum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smythe *',
                        'label'       => 'Smythe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya *',
                        'label'       => 'Alegreya *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturCook *',
                        'label'       => 'UnifrakturCook *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neuton *',
                        'label'       => 'Neuton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Zeyada *',
                        'label'       => 'Zeyada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goblin One *',
                        'label'       => 'Goblin One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arizonia *',
                        'label'       => 'Arizonia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Special Elite *',
                        'label'       => 'Special Elite *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Euphoria Script *',
                        'label'       => 'Euphoria Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gudea *',
                        'label'       => 'Gudea *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica *',
                        'label'       => 'IM Fell DW Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Linden Hill *',
                        'label'       => 'Linden Hill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate SC *',
                        'label'       => 'Mate SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kameron *',
                        'label'       => 'Kameron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unna *',
                        'label'       => 'Unna *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceberg *',
                        'label'       => 'Iceberg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federo *',
                        'label'       => 'Federo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carme *',
                        'label'       => 'Carme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'League Script *',
                        'label'       => 'League Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yanone Kaffeesatz *',
                        'label'       => 'Yanone Kaffeesatz *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allan *',
                        'label'       => 'Allan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Squada One *',
                        'label'       => 'Squada One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aldrich *',
                        'label'       => 'Aldrich *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike Angular *',
                        'label'       => 'Alike Angular *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian Tattoo *',
                        'label'       => 'Miltonian Tattoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metrophobic *',
                        'label'       => 'Metrophobic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Michroma *',
                        'label'       => 'Michroma *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kaushan Script *',
                        'label'       => 'Kaushan Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spinnaker *',
                        'label'       => 'Spinnaker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ultra *',
                        'label'       => 'Ultra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crete Round *',
                        'label'       => 'Crete Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye *',
                        'label'       => 'Ribeye *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Mono *',
                        'label'       => 'Nova Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English *',
                        'label'       => 'IM Fell English *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marvel *',
                        'label'       => 'Marvel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playball *',
                        'label'       => 'Playball *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butterfly Kids *',
                        'label'       => 'Butterfly Kids *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Numans *',
                        'label'       => 'Numans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neucha *',
                        'label'       => 'Neucha *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prociono *',
                        'label'       => 'Prociono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Uncial Antiqua *',
                        'label'       => 'Uncial Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abril Fatface *',
                        'label'       => 'Abril Fatface *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monoton *',
                        'label'       => 'Monoton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nothing You Could Do *',
                        'label'       => 'Nothing You Could Do *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yeseva One *',
                        'label'       => 'Yeseva One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruthie *',
                        'label'       => 'Ruthie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lancelot *',
                        'label'       => 'Lancelot *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Artifika *',
                        'label'       => 'Artifika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo *',
                        'label'       => 'Macondo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Paytone One *',
                        'label'       => 'Paytone One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bitter *',
                        'label'       => 'Bitter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Revalia *',
                        'label'       => 'Revalia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Luckiest Guy *',
                        'label'       => 'Luckiest Guy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra *',
                        'label'       => 'Almendra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geo *',
                        'label'       => 'Geo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asap *',
                        'label'       => 'Asap *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Square *',
                        'label'       => 'Nova Square *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Expletus Sans *',
                        'label'       => 'Expletus Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon *',
                        'label'       => 'IM Fell French Canon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Volkhov *',
                        'label'       => 'Volkhov *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster *',
                        'label'       => 'Lobster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Damion *',
                        'label'       => 'Damion *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta Stencil *',
                        'label'       => 'Allerta Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tienne *',
                        'label'       => 'Tienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Libre *',
                        'label'       => 'Averia Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asset *',
                        'label'       => 'Asset *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Open Sans *',
                        'label'       => 'Open Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jura *',
                        'label'       => 'Jura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merienda One *',
                        'label'       => 'Merienda One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dr Sugiyama *',
                        'label'       => 'Dr Sugiyama *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miss Fajardose *',
                        'label'       => 'Miss Fajardose *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Noticia Text *',
                        'label'       => 'Noticia Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Me Again Down Here *',
                        'label'       => 'Just Me Again Down Here *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jim Nightshade *',
                        'label'       => 'Jim Nightshade *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alice *',
                        'label'       => 'Alice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Serif Libre *',
                        'label'       => 'Averia Serif Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Reenie Beanie *',
                        'label'       => 'Reenie Beanie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Molengo *',
                        'label'       => 'Molengo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Enriqueta *',
                        'label'       => 'Enriqueta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vibur *',
                        'label'       => 'Vibur *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miniver *',
                        'label'       => 'Miniver *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bubblegum Sans *',
                        'label'       => 'Bubblegum Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Basic *',
                        'label'       => 'Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Copse *',
                        'label'       => 'Copse *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lustria *',
                        'label'       => 'Lustria *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fresca *',
                        'label'       => 'Fresca *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chau Philomene One *',
                        'label'       => 'Chau Philomene One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lemon *',
                        'label'       => 'Lemon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Sketch *',
                        'label'       => 'Cabin Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika Negative *',
                        'label'       => 'Signika Negative *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inder *',
                        'label'       => 'Inder *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sevillana *',
                        'label'       => 'Sevillana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tenor Sans *',
                        'label'       => 'Tenor Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maven Pro *',
                        'label'       => 'Maven Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Julee *',
                        'label'       => 'Julee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate *',
                        'label'       => 'Mate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sarina *',
                        'label'       => 'Sarina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tangerine *',
                        'label'       => 'Tangerine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Henny Penny *',
                        'label'       => 'Henny Penny *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Muli *',
                        'label'       => 'Muli *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Flat *',
                        'label'       => 'Nova Flat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica SC *',
                        'label'       => 'IM Fell DW Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fugaz One *',
                        'label'       => 'Fugaz One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trade Winds *',
                        'label'       => 'Trade Winds *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ewert *',
                        'label'       => 'Ewert *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Brawler *',
                        'label'       => 'Brawler *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Annie Use Your Telescope *',
                        'label'       => 'Annie Use Your Telescope *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sirin Stencil *',
                        'label'       => 'Sirin Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Waiting for the Sunrise *',
                        'label'       => 'Waiting for the Sunrise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Modern Antiqua *',
                        'label'       => 'Modern Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'VT323 *',
                        'label'       => 'VT323 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Devonshire *',
                        'label'       => 'Devonshire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Piedra *',
                        'label'       => 'Piedra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chivo *',
                        'label'       => 'Chivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flavors *',
                        'label'       => 'Flavors *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buenard *',
                        'label'       => 'Buenard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Engagement *',
                        'label'       => 'Engagement *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shanti *',
                        'label'       => 'Shanti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monofett *',
                        'label'       => 'Monofett *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butcherman *',
                        'label'       => 'Butcherman *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ovo *',
                        'label'       => 'Ovo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate Inline *',
                        'label'       => 'Fascinate Inline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marmelad *',
                        'label'       => 'Marmelad *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English SC *',
                        'label'       => 'IM Fell English SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sonsie One *',
                        'label'       => 'Sonsie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr De Haviland *',
                        'label'       => 'Mr De Haviland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sniglet *',
                        'label'       => 'Sniglet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poiret One *',
                        'label'       => 'Poiret One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trykker *',
                        'label'       => 'Trykker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra SC *',
                        'label'       => 'Almendra SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Germania One *',
                        'label'       => 'Germania One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Plaster *',
                        'label'       => 'Plaster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Titan One *',
                        'label'       => 'Titan One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crushed *',
                        'label'       => 'Crushed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rammetto One *',
                        'label'       => 'Rammetto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Solid *',
                        'label'       => 'Londrina Solid *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar Fill *',
                        'label'       => 'Geostar Fill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Unicase *',
                        'label'       => 'Delius Unicase *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye Marrow *',
                        'label'       => 'Ribeye Marrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marck Script *',
                        'label'       => 'Marck Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gochi Hand *',
                        'label'       => 'Gochi Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Salsa *',
                        'label'       => 'Salsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sunshiney *',
                        'label'       => 'Sunshiney *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arapey *',
                        'label'       => 'Arapey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gorditas *',
                        'label'       => 'Gorditas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emilys Candy *',
                        'label'       => 'Emilys Candy *',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_logo_text_color',
                'label'       => 'Color of Text Logo',
                'desc'        => 'Specify any customized color you want for the logo text.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_logo_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_heading_font',
                'label'       => 'Heading Font',
                'desc'        => 'Choose the font of your choice for heading elements(h1,h2,h3,h4).',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Rokkitt *',
                        'label'       => 'Rokkitt * (default)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arial',
                        'label'       => 'Arial',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Helvetica',
                        'label'       => 'Helvetica',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Courier New',
                        'label'       => 'Courier New',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Georgia',
                        'label'       => 'Georgia',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lucida Sans Unicode',
                        'label'       => 'Lucida Sans Unicode',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tahoma',
                        'label'       => 'Tahoma',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geneva',
                        'label'       => 'Geneva',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Times New Roman',
                        'label'       => 'Times New Roman',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trebuchet MS',
                        'label'       => 'Trebuchet MS',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Verdana',
                        'label'       => 'Verdana',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arial Black',
                        'label'       => 'Arial Black',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Eater *',
                        'label'       => 'Eater *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chango *',
                        'label'       => 'Chango *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Press Start 2P *',
                        'label'       => 'Press Start 2P *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Source Sans Pro *',
                        'label'       => 'Source Sans Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Basic *',
                        'label'       => 'Gentium Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon SC *',
                        'label'       => 'IM Fell French Canon SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Sketch *',
                        'label'       => 'Londrina Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Concert One *',
                        'label'       => 'Concert One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cagliostro *',
                        'label'       => 'Cagliostro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredoka One *',
                        'label'       => 'Fredoka One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Loved by the King *',
                        'label'       => 'Loved by the King *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belgrano *',
                        'label'       => 'Belgrano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quantico *',
                        'label'       => 'Quantico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantata One *',
                        'label'       => 'Cantata One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Berkshire Swash *',
                        'label'       => 'Berkshire Swash *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spirax *',
                        'label'       => 'Spirax *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Graduate *',
                        'label'       => 'Graduate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Swanky and Moo Moo *',
                        'label'       => 'Swanky and Moo Moo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock *',
                        'label'       => 'Overlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bangers *',
                        'label'       => 'Bangers *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pontano Sans *',
                        'label'       => 'Pontano Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light *',
                        'label'       => 'Shadows Into Light *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ceviche One *',
                        'label'       => 'Ceviche One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andada *',
                        'label'       => 'Andada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Boogaloo *',
                        'label'       => 'Boogaloo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fanwood Text *',
                        'label'       => 'Fanwood Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Codystar *',
                        'label'       => 'Codystar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voltaire *',
                        'label'       => 'Voltaire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'EB Garamond *',
                        'label'       => 'EB Garamond *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belleza *',
                        'label'       => 'Belleza *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cutive *',
                        'label'       => 'Cutive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo *',
                        'label'       => 'Bilbo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emblema One *',
                        'label'       => 'Emblema One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Felipa *',
                        'label'       => 'Felipa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lusitana *',
                        'label'       => 'Lusitana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Sans *',
                        'label'       => 'Port Lligat Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lovers Quarrel *',
                        'label'       => 'Lovers Quarrel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crafty Girls *',
                        'label'       => 'Crafty Girls *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alex Brush *',
                        'label'       => 'Alex Brush *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Play *',
                        'label'       => 'Play *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Doppio One *',
                        'label'       => 'Doppio One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Sheppards *',
                        'label'       => 'Mrs Sheppards *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spicy Rice *',
                        'label'       => 'Spicy Rice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Limelight *',
                        'label'       => 'Limelight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Give You Glory *',
                        'label'       => 'Give You Glory *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredericka the Great *',
                        'label'       => 'Fredericka the Great *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oleo Script *',
                        'label'       => 'Oleo Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cardo *',
                        'label'       => 'Cardo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ledger *',
                        'label'       => 'Ledger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italiana *',
                        'label'       => 'Italiana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One *',
                        'label'       => 'Bowlby One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homenaje *',
                        'label'       => 'Homenaje *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Karla *',
                        'label'       => 'Karla *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italianno *',
                        'label'       => 'Italianno *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Condensed *',
                        'label'       => 'Cabin Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dosis *',
                        'label'       => 'Dosis *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Krona One *',
                        'label'       => 'Krona One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Advent Pro *',
                        'label'       => 'Advent Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Podkova *',
                        'label'       => 'Podkova *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chicle *',
                        'label'       => 'Chicle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Didone *',
                        'label'       => 'Antic Didone *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Exo *',
                        'label'       => 'Exo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Syncopate *',
                        'label'       => 'Syncopate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sancreek *',
                        'label'       => 'Sancreek *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marko One *',
                        'label'       => 'Marko One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metamorphous *',
                        'label'       => 'Metamorphous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Outline *',
                        'label'       => 'Londrina Outline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patua One *',
                        'label'       => 'Patua One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Bedfort *',
                        'label'       => 'Mr Bedfort *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nunito *',
                        'label'       => 'Nunito *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inika *',
                        'label'       => 'Inika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta *',
                        'label'       => 'Allerta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosarivo *',
                        'label'       => 'Rosarivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Viga *',
                        'label'       => 'Viga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shojumaru *',
                        'label'       => 'Shojumaru *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Francois One *',
                        'label'       => 'Francois One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'The Girl Next Door *',
                        'label'       => 'The Girl Next Door *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passion One *',
                        'label'       => 'Passion One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Days One *',
                        'label'       => 'Days One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Sans *',
                        'label'       => 'Josefin Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Acme *',
                        'label'       => 'Acme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asul *',
                        'label'       => 'Asul *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Happy Monkey *',
                        'label'       => 'Happy Monkey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Sans Libre *',
                        'label'       => 'Averia Sans Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rancho *',
                        'label'       => 'Rancho *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anonymous Pro *',
                        'label'       => 'Anonymous Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glegoo *',
                        'label'       => 'Glegoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Galdeano *',
                        'label'       => 'Galdeano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Saint Delafield *',
                        'label'       => 'Mrs Saint Delafield *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kranky *',
                        'label'       => 'Kranky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Share *',
                        'label'       => 'Share *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Baumans *',
                        'label'       => 'Baumans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quicksand *',
                        'label'       => 'Quicksand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Habibi *',
                        'label'       => 'Habibi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Seaweed Script *',
                        'label'       => 'Seaweed Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yesteryear *',
                        'label'       => 'Yesteryear *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Economica *',
                        'label'       => 'Economica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Niconne *',
                        'label'       => 'Niconne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sofia *',
                        'label'       => 'Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bonbon *',
                        'label'       => 'Bonbon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Herr Von Muellerhoff *',
                        'label'       => 'Herr Von Muellerhoff *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Duru Sans *',
                        'label'       => 'Duru Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Creepster *',
                        'label'       => 'Creepster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Great Vibes *',
                        'label'       => 'Great Vibes *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Junge *',
                        'label'       => 'Junge *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Condensed *',
                        'label'       => 'Stint Ultra Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya SC *',
                        'label'       => 'Alegreya SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Russo One *',
                        'label'       => 'Russo One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amatic SC *',
                        'label'       => 'Amatic SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Condiment *',
                        'label'       => 'Condiment *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Medula One *',
                        'label'       => 'Medula One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Judson *',
                        'label'       => 'Judson *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Orbitron *',
                        'label'       => 'Orbitron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster Two *',
                        'label'       => 'Lobster Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pinyon Script *',
                        'label'       => 'Pinyon Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unkempt *',
                        'label'       => 'Unkempt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nixie One *',
                        'label'       => 'Nixie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trocchi *',
                        'label'       => 'Trocchi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Comfortaa *',
                        'label'       => 'Comfortaa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Erica One *',
                        'label'       => 'Erica One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Mono *',
                        'label'       => 'Ubuntu Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montaga *',
                        'label'       => 'Montaga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lilita One *',
                        'label'       => 'Lilita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Expanded *',
                        'label'       => 'Stint Ultra Expanded *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Love Ya Like A Sister *',
                        'label'       => 'Love Ya Like A Sister *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oxygen *',
                        'label'       => 'Oxygen *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Frijole *',
                        'label'       => 'Frijole *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arvo *',
                        'label'       => 'Arvo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prata *',
                        'label'       => 'Prata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Magra *',
                        'label'       => 'Magra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oswald *',
                        'label'       => 'Oswald *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flamenco *',
                        'label'       => 'Flamenco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dorsa *',
                        'label'       => 'Dorsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bad Script *',
                        'label'       => 'Bad Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika *',
                        'label'       => 'Signika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Serif *',
                        'label'       => 'Droid Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wellfleet *',
                        'label'       => 'Wellfleet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata SC *',
                        'label'       => 'Diplomata SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Average *',
                        'label'       => 'Average *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Righteous *',
                        'label'       => 'Righteous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento Sans *',
                        'label'       => 'Quattrocento Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rouge Script *',
                        'label'       => 'Rouge Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruluko *',
                        'label'       => 'Ruluko *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aclonica *',
                        'label'       => 'Aclonica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruda *',
                        'label'       => 'Ruda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kotta One *',
                        'label'       => 'Kotta One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Questrial *',
                        'label'       => 'Questrial *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jolly Lodger *',
                        'label'       => 'Jolly Lodger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cookie *',
                        'label'       => 'Cookie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Changa One *',
                        'label'       => 'Changa One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alfa Slab One *',
                        'label'       => 'Alfa Slab One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Slab *',
                        'label'       => 'Josefin Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantarell *',
                        'label'       => 'Cantarell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fjord One *',
                        'label'       => 'Fjord One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merriweather *',
                        'label'       => 'Merriweather *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Philosopher *',
                        'label'       => 'Philosopher *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela Round *',
                        'label'       => 'Varela Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amethysta *',
                        'label'       => 'Amethysta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dawning of a New Day *',
                        'label'       => 'Dawning of a New Day *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coming Soon *',
                        'label'       => 'Coming Soon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light Two *',
                        'label'       => 'Shadows Into Light Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mako *',
                        'label'       => 'Mako *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Black Ops One *',
                        'label'       => 'Black Ops One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rationale *',
                        'label'       => 'Rationale *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lato *',
                        'label'       => 'Lato *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trochut *',
                        'label'       => 'Trochut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gloria Hallelujah *',
                        'label'       => 'Gloria Hallelujah *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Parisienne *',
                        'label'       => 'Parisienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Short Stack *',
                        'label'       => 'Short Stack *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goudy Bookletter 1911 *',
                        'label'       => 'Goudy Bookletter 1911 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Didact Gothic *',
                        'label'       => 'Didact Gothic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo Swash Caps *',
                        'label'       => 'Bilbo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vollkorn *',
                        'label'       => 'Vollkorn *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu *',
                        'label'       => 'Ubuntu *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Slab *',
                        'label'       => 'Port Lligat Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Princess Sofia *',
                        'label'       => 'Princess Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mountains of Christmas *',
                        'label'       => 'Mountains of Christmas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Qwigley *',
                        'label'       => 'Qwigley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Candal *',
                        'label'       => 'Candal *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Old Standard TT *',
                        'label'       => 'Old Standard TT *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Istok Web *',
                        'label'       => 'Istok Web *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Puritan *',
                        'label'       => 'Puritan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montez *',
                        'label'       => 'Montez *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lekton *',
                        'label'       => 'Lekton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cambo *',
                        'label'       => 'Cambo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela *',
                        'label'       => 'Varela *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glass Antiqua *',
                        'label'       => 'Glass Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bigshot One *',
                        'label'       => 'Bigshot One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arimo *',
                        'label'       => 'Arimo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosario *',
                        'label'       => 'Rosario *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rokkitt *',
                        'label'       => 'Rokkitt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Architects Daughter *',
                        'label'       => 'Architects Daughter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans *',
                        'label'       => 'Droid Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cuprum *',
                        'label'       => 'Cuprum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Slim *',
                        'label'       => 'Nova Slim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Book Basic *',
                        'label'       => 'Gentium Book Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aladin *',
                        'label'       => 'Aladin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Corben *',
                        'label'       => 'Corben *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Six Caps *',
                        'label'       => 'Six Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voces *',
                        'label'       => 'Voces *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Original Surfer *',
                        'label'       => 'Original Surfer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kelly Slab *',
                        'label'       => 'Kelly Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda Caption *',
                        'label'       => 'Coda Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cedarville Cursive *',
                        'label'       => 'Cedarville Cursive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playfair Display *',
                        'label'       => 'Playfair Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Actor *',
                        'label'       => 'Actor *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pacifico *',
                        'label'       => 'Pacifico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Caption *',
                        'label'       => 'PT Sans Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Covered By Your Grace *',
                        'label'       => 'Covered By Your Grace *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Convergence *',
                        'label'       => 'Convergence *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Handlee *',
                        'label'       => 'Handlee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Raleway *',
                        'label'       => 'Raleway *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wallpoet *',
                        'label'       => 'Wallpoet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Script *',
                        'label'       => 'Nova Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceland *',
                        'label'       => 'Iceland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Meddon *',
                        'label'       => 'Meddon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Shadow *',
                        'label'       => 'Londrina Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nobile *',
                        'label'       => 'Nobile *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carter One *',
                        'label'       => 'Carter One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Walter Turncoat *',
                        'label'       => 'Walter Turncoat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amaranth *',
                        'label'       => 'Amaranth *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Indie Flower *',
                        'label'       => 'Indie Flower *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Telex *',
                        'label'       => 'Telex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ropa Sans *',
                        'label'       => 'Ropa Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abel *',
                        'label'       => 'Abel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock SC *',
                        'label'       => 'Overlock SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sigmar One *',
                        'label'       => 'Sigmar One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate *',
                        'label'       => 'Fascinate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Hammersmith One *',
                        'label'       => 'Hammersmith One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vast Shadow *',
                        'label'       => 'Vast Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruge Boogie *',
                        'label'       => 'Ruge Boogie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Balthazar *',
                        'label'       => 'Balthazar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Simonetta *',
                        'label'       => 'Simonetta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Schoolbell *',
                        'label'       => 'Schoolbell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pompiere *',
                        'label'       => 'Pompiere *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kenia *',
                        'label'       => 'Kenia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Slab *',
                        'label'       => 'Antic Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caesar Dressing *',
                        'label'       => 'Caesar Dressing *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Gruesa Libre *',
                        'label'       => 'Averia Gruesa Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bevan *',
                        'label'       => 'Bevan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Forum *',
                        'label'       => 'Forum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Narrow *',
                        'label'       => 'PT Sans Narrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturMaguntia *',
                        'label'       => 'UnifrakturMaguntia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vidaloka *',
                        'label'       => 'Vidaloka *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sansita One *',
                        'label'       => 'Sansita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kristi *',
                        'label'       => 'Kristi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Imprima *',
                        'label'       => 'Imprima *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sail *',
                        'label'       => 'Sail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tulpen One *',
                        'label'       => 'Tulpen One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dynalight *',
                        'label'       => 'Dynalight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chewy *',
                        'label'       => 'Chewy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Norican *',
                        'label'       => 'Norican *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Redressed *',
                        'label'       => 'Redressed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One SC *',
                        'label'       => 'Bowlby One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Holtwood One SC *',
                        'label'       => 'Holtwood One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Dafoe *',
                        'label'       => 'Mr Dafoe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bentham *',
                        'label'       => 'Bentham *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento *',
                        'label'       => 'Quattrocento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer SC *',
                        'label'       => 'IM Fell Great Primer SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tinos *',
                        'label'       => 'Tinos *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kreon *',
                        'label'       => 'Kreon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buda *',
                        'label'       => 'Buda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lora *',
                        'label'       => 'Lora *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans *',
                        'label'       => 'PT Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Open Sans Condensed *',
                        'label'       => 'Open Sans Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Esteban *',
                        'label'       => 'Esteban *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Armata *',
                        'label'       => 'Armata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Contrail One *',
                        'label'       => 'Contrail One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'La Belle Aurore *',
                        'label'       => 'La Belle Aurore *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Astloch *',
                        'label'       => 'Astloch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Satisfy *',
                        'label'       => 'Satisfy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oldenburg *',
                        'label'       => 'Oldenburg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allura *',
                        'label'       => 'Allura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homemade Apple *',
                        'label'       => 'Homemade Apple *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Audiowide *',
                        'label'       => 'Audiowide *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo Swash Caps *',
                        'label'       => 'Macondo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans Mono *',
                        'label'       => 'Droid Sans Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Petrona *',
                        'label'       => 'Petrona *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Electrolize *',
                        'label'       => 'Electrolize *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inconsolata *',
                        'label'       => 'Inconsolata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aguafina Script *',
                        'label'       => 'Aguafina Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda *',
                        'label'       => 'Coda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fontdiner Swanky *',
                        'label'       => 'Fontdiner Swanky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Slackey *',
                        'label'       => 'Slackey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'News Cycle *',
                        'label'       => 'News Cycle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Calligraffitti *',
                        'label'       => 'Calligraffitti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic *',
                        'label'       => 'Antic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif Caption *',
                        'label'       => 'PT Serif Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Cut *',
                        'label'       => 'Nova Cut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bree Serif *',
                        'label'       => 'Bree Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike *',
                        'label'       => 'Alike *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montserrat *',
                        'label'       => 'Montserrat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mystery Quest *',
                        'label'       => 'Mystery Quest *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Another Hand *',
                        'label'       => 'Just Another Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fondamento *',
                        'label'       => 'Fondamento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Knewave *',
                        'label'       => 'Knewave *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer *',
                        'label'       => 'IM Fell Great Primer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gruppo *',
                        'label'       => 'Gruppo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif *',
                        'label'       => 'PT Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Adamina *',
                        'label'       => 'Adamina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stardos Stencil *',
                        'label'       => 'Stardos Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Radley *',
                        'label'       => 'Radley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cherry Cream Soda *',
                        'label'       => 'Cherry Cream Soda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patrick Hand *',
                        'label'       => 'Patrick Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anton *',
                        'label'       => 'Anton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jockey One *',
                        'label'       => 'Jockey One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unlock *',
                        'label'       => 'Unlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Round *',
                        'label'       => 'Nova Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Over the Rainbow *',
                        'label'       => 'Over the Rainbow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Megrim *',
                        'label'       => 'Megrim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chelsea Market *',
                        'label'       => 'Chelsea Market *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arbutus *',
                        'label'       => 'Arbutus *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata *',
                        'label'       => 'Diplomata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caudex *',
                        'label'       => 'Caudex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nosifer *',
                        'label'       => 'Nosifer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Leckerli One *',
                        'label'       => 'Leckerli One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica SC *',
                        'label'       => 'IM Fell Double Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rochester *',
                        'label'       => 'Rochester *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passero One *',
                        'label'       => 'Passero One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar *',
                        'label'       => 'Geostar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prosto One *',
                        'label'       => 'Prosto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poller One *',
                        'label'       => 'Poller One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Condensed *',
                        'label'       => 'Ubuntu Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andika *',
                        'label'       => 'Andika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gravitas One *',
                        'label'       => 'Gravitas One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rock Salt *',
                        'label'       => 'Rock Salt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruslan Display *',
                        'label'       => 'Ruslan Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius *',
                        'label'       => 'Delius *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aubrey *',
                        'label'       => 'Aubrey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin *',
                        'label'       => 'Cabin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Swash Caps *',
                        'label'       => 'Delius Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wire One *',
                        'label'       => 'Wire One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'MedievalSharp *',
                        'label'       => 'MedievalSharp *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Mono *',
                        'label'       => 'PT Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Oval *',
                        'label'       => 'Nova Oval *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sue Ellen Francisco *',
                        'label'       => 'Sue Ellen Francisco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian *',
                        'label'       => 'Miltonian *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica *',
                        'label'       => 'IM Fell Double Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yellowtail *',
                        'label'       => 'Yellowtail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sorts Mill Goudy *',
                        'label'       => 'Sorts Mill Goudy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Permanent Marker *',
                        'label'       => 'Permanent Marker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poly *',
                        'label'       => 'Poly *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dancing Script *',
                        'label'       => 'Dancing Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maiden Orange *',
                        'label'       => 'Maiden Orange *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coustard *',
                        'label'       => 'Coustard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Supermercado One *',
                        'label'       => 'Supermercado One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Atomic Age *',
                        'label'       => 'Atomic Age *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cousine *',
                        'label'       => 'Cousine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federant *',
                        'label'       => 'Federant *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Irish Grover *',
                        'label'       => 'Irish Grover *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stoke *',
                        'label'       => 'Stoke *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Snippet *',
                        'label'       => 'Snippet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monsieur La Doulaise *',
                        'label'       => 'Monsieur La Doulaise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crimson Text *',
                        'label'       => 'Crimson Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Della Respira *',
                        'label'       => 'Della Respira *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smokum *',
                        'label'       => 'Smokum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smythe *',
                        'label'       => 'Smythe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya *',
                        'label'       => 'Alegreya *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturCook *',
                        'label'       => 'UnifrakturCook *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neuton *',
                        'label'       => 'Neuton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Zeyada *',
                        'label'       => 'Zeyada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goblin One *',
                        'label'       => 'Goblin One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arizonia *',
                        'label'       => 'Arizonia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Special Elite *',
                        'label'       => 'Special Elite *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Euphoria Script *',
                        'label'       => 'Euphoria Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gudea *',
                        'label'       => 'Gudea *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica *',
                        'label'       => 'IM Fell DW Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Linden Hill *',
                        'label'       => 'Linden Hill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate SC *',
                        'label'       => 'Mate SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kameron *',
                        'label'       => 'Kameron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unna *',
                        'label'       => 'Unna *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceberg *',
                        'label'       => 'Iceberg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federo *',
                        'label'       => 'Federo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carme *',
                        'label'       => 'Carme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'League Script *',
                        'label'       => 'League Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yanone Kaffeesatz *',
                        'label'       => 'Yanone Kaffeesatz *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allan *',
                        'label'       => 'Allan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Squada One *',
                        'label'       => 'Squada One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aldrich *',
                        'label'       => 'Aldrich *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike Angular *',
                        'label'       => 'Alike Angular *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian Tattoo *',
                        'label'       => 'Miltonian Tattoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metrophobic *',
                        'label'       => 'Metrophobic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Michroma *',
                        'label'       => 'Michroma *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kaushan Script *',
                        'label'       => 'Kaushan Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spinnaker *',
                        'label'       => 'Spinnaker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ultra *',
                        'label'       => 'Ultra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crete Round *',
                        'label'       => 'Crete Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye *',
                        'label'       => 'Ribeye *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Mono *',
                        'label'       => 'Nova Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English *',
                        'label'       => 'IM Fell English *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marvel *',
                        'label'       => 'Marvel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playball *',
                        'label'       => 'Playball *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butterfly Kids *',
                        'label'       => 'Butterfly Kids *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Numans *',
                        'label'       => 'Numans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neucha *',
                        'label'       => 'Neucha *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prociono *',
                        'label'       => 'Prociono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Uncial Antiqua *',
                        'label'       => 'Uncial Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abril Fatface *',
                        'label'       => 'Abril Fatface *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monoton *',
                        'label'       => 'Monoton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nothing You Could Do *',
                        'label'       => 'Nothing You Could Do *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yeseva One *',
                        'label'       => 'Yeseva One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruthie *',
                        'label'       => 'Ruthie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lancelot *',
                        'label'       => 'Lancelot *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Artifika *',
                        'label'       => 'Artifika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo *',
                        'label'       => 'Macondo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Paytone One *',
                        'label'       => 'Paytone One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bitter *',
                        'label'       => 'Bitter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Revalia *',
                        'label'       => 'Revalia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Luckiest Guy *',
                        'label'       => 'Luckiest Guy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra *',
                        'label'       => 'Almendra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geo *',
                        'label'       => 'Geo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asap *',
                        'label'       => 'Asap *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Square *',
                        'label'       => 'Nova Square *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Expletus Sans *',
                        'label'       => 'Expletus Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon *',
                        'label'       => 'IM Fell French Canon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Volkhov *',
                        'label'       => 'Volkhov *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster *',
                        'label'       => 'Lobster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Damion *',
                        'label'       => 'Damion *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta Stencil *',
                        'label'       => 'Allerta Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tienne *',
                        'label'       => 'Tienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Libre *',
                        'label'       => 'Averia Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asset *',
                        'label'       => 'Asset *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Open Sans *',
                        'label'       => 'Open Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jura *',
                        'label'       => 'Jura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merienda One *',
                        'label'       => 'Merienda One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dr Sugiyama *',
                        'label'       => 'Dr Sugiyama *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miss Fajardose *',
                        'label'       => 'Miss Fajardose *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Noticia Text *',
                        'label'       => 'Noticia Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Me Again Down Here *',
                        'label'       => 'Just Me Again Down Here *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jim Nightshade *',
                        'label'       => 'Jim Nightshade *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alice *',
                        'label'       => 'Alice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Serif Libre *',
                        'label'       => 'Averia Serif Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Reenie Beanie *',
                        'label'       => 'Reenie Beanie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Molengo *',
                        'label'       => 'Molengo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Enriqueta *',
                        'label'       => 'Enriqueta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vibur *',
                        'label'       => 'Vibur *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miniver *',
                        'label'       => 'Miniver *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bubblegum Sans *',
                        'label'       => 'Bubblegum Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Basic *',
                        'label'       => 'Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Copse *',
                        'label'       => 'Copse *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lustria *',
                        'label'       => 'Lustria *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fresca *',
                        'label'       => 'Fresca *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chau Philomene One *',
                        'label'       => 'Chau Philomene One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lemon *',
                        'label'       => 'Lemon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Sketch *',
                        'label'       => 'Cabin Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika Negative *',
                        'label'       => 'Signika Negative *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inder *',
                        'label'       => 'Inder *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sevillana *',
                        'label'       => 'Sevillana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tenor Sans *',
                        'label'       => 'Tenor Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maven Pro *',
                        'label'       => 'Maven Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Julee *',
                        'label'       => 'Julee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate *',
                        'label'       => 'Mate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sarina *',
                        'label'       => 'Sarina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tangerine *',
                        'label'       => 'Tangerine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Henny Penny *',
                        'label'       => 'Henny Penny *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Muli *',
                        'label'       => 'Muli *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Flat *',
                        'label'       => 'Nova Flat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica SC *',
                        'label'       => 'IM Fell DW Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fugaz One *',
                        'label'       => 'Fugaz One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trade Winds *',
                        'label'       => 'Trade Winds *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ewert *',
                        'label'       => 'Ewert *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Brawler *',
                        'label'       => 'Brawler *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Annie Use Your Telescope *',
                        'label'       => 'Annie Use Your Telescope *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sirin Stencil *',
                        'label'       => 'Sirin Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Waiting for the Sunrise *',
                        'label'       => 'Waiting for the Sunrise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Modern Antiqua *',
                        'label'       => 'Modern Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'VT323 *',
                        'label'       => 'VT323 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Devonshire *',
                        'label'       => 'Devonshire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Piedra *',
                        'label'       => 'Piedra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chivo *',
                        'label'       => 'Chivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flavors *',
                        'label'       => 'Flavors *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buenard *',
                        'label'       => 'Buenard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Engagement *',
                        'label'       => 'Engagement *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shanti *',
                        'label'       => 'Shanti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monofett *',
                        'label'       => 'Monofett *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butcherman *',
                        'label'       => 'Butcherman *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ovo *',
                        'label'       => 'Ovo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate Inline *',
                        'label'       => 'Fascinate Inline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marmelad *',
                        'label'       => 'Marmelad *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English SC *',
                        'label'       => 'IM Fell English SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sonsie One *',
                        'label'       => 'Sonsie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr De Haviland *',
                        'label'       => 'Mr De Haviland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sniglet *',
                        'label'       => 'Sniglet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poiret One *',
                        'label'       => 'Poiret One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trykker *',
                        'label'       => 'Trykker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra SC *',
                        'label'       => 'Almendra SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Germania One *',
                        'label'       => 'Germania One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Plaster *',
                        'label'       => 'Plaster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Titan One *',
                        'label'       => 'Titan One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crushed *',
                        'label'       => 'Crushed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rammetto One *',
                        'label'       => 'Rammetto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Solid *',
                        'label'       => 'Londrina Solid *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar Fill *',
                        'label'       => 'Geostar Fill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Unicase *',
                        'label'       => 'Delius Unicase *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye Marrow *',
                        'label'       => 'Ribeye Marrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marck Script *',
                        'label'       => 'Marck Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gochi Hand *',
                        'label'       => 'Gochi Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Salsa *',
                        'label'       => 'Salsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sunshiney *',
                        'label'       => 'Sunshiney *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arapey *',
                        'label'       => 'Arapey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gorditas *',
                        'label'       => 'Gorditas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emilys Candy *',
                        'label'       => 'Emilys Candy *',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_custom_heading_font',
                'label'       => 'Custom Heading Font',
                'desc'        => 'Enter the font name for the custom font for Headings (H1, H2, H3, H4, H5 , H6 Html tags). You can utilize the Custom CSS tab (or edit custom.css file) to specify required CSS for font import.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_heading_font_spacing',
                'label'       => 'Heading Font Spacing',
                'desc'        => 'Letter Spacing for Heading Font in pixels(px).',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => '0',
                        'label'       => '0 (default)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '-1',
                        'label'       => '-1',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '-0.8',
                        'label'       => '-0.8',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '-0.6',
                        'label'       => '-0.6',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '-0.4',
                        'label'       => '-0.4',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '-0.2',
                        'label'       => '-0.2',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '0.2',
                        'label'       => '0.2',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '0.4',
                        'label'       => '0.4',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '0.6',
                        'label'       => '0.6',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '0.8',
                        'label'       => '0.8',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1',
                        'label'       => '1',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1.2',
                        'label'       => '1.2',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1.4',
                        'label'       => '1.4',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_body_font',
                'label'       => 'Body Font',
                'desc'        => 'Select the font of the content.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Open Sans *',
                        'label'       => 'Open Sans * (default)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arial',
                        'label'       => 'Arial',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Helvetica',
                        'label'       => 'Helvetica',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Courier New',
                        'label'       => 'Courier New',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Georgia',
                        'label'       => 'Georgia',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lucida Sans Unicode',
                        'label'       => 'Lucida Sans Unicode',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tahoma',
                        'label'       => 'Tahoma',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geneva',
                        'label'       => 'Geneva',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Times New Roman',
                        'label'       => 'Times New Roman',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trebuchet MS',
                        'label'       => 'Trebuchet MS',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Verdana',
                        'label'       => 'Verdana',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Eater *',
                        'label'       => 'Eater *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chango *',
                        'label'       => 'Chango *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Press Start 2P *',
                        'label'       => 'Press Start 2P *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Basic *',
                        'label'       => 'Gentium Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon SC *',
                        'label'       => 'IM Fell French Canon SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Sketch *',
                        'label'       => 'Londrina Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Concert One *',
                        'label'       => 'Concert One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cagliostro *',
                        'label'       => 'Cagliostro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredoka One *',
                        'label'       => 'Fredoka One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Loved by the King *',
                        'label'       => 'Loved by the King *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belgrano *',
                        'label'       => 'Belgrano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quantico *',
                        'label'       => 'Quantico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantata One *',
                        'label'       => 'Cantata One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Berkshire Swash *',
                        'label'       => 'Berkshire Swash *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spirax *',
                        'label'       => 'Spirax *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Graduate *',
                        'label'       => 'Graduate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Swanky and Moo Moo *',
                        'label'       => 'Swanky and Moo Moo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock *',
                        'label'       => 'Overlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bangers *',
                        'label'       => 'Bangers *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pontano Sans *',
                        'label'       => 'Pontano Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light *',
                        'label'       => 'Shadows Into Light *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ceviche One *',
                        'label'       => 'Ceviche One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andada *',
                        'label'       => 'Andada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Boogaloo *',
                        'label'       => 'Boogaloo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fanwood Text *',
                        'label'       => 'Fanwood Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Codystar *',
                        'label'       => 'Codystar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voltaire *',
                        'label'       => 'Voltaire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'EB Garamond *',
                        'label'       => 'EB Garamond *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Belleza *',
                        'label'       => 'Belleza *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cutive *',
                        'label'       => 'Cutive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo *',
                        'label'       => 'Bilbo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emblema One *',
                        'label'       => 'Emblema One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Felipa *',
                        'label'       => 'Felipa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lusitana *',
                        'label'       => 'Lusitana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Sans *',
                        'label'       => 'Port Lligat Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lovers Quarrel *',
                        'label'       => 'Lovers Quarrel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crafty Girls *',
                        'label'       => 'Crafty Girls *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alex Brush *',
                        'label'       => 'Alex Brush *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Play *',
                        'label'       => 'Play *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Doppio One *',
                        'label'       => 'Doppio One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Sheppards *',
                        'label'       => 'Mrs Sheppards *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spicy Rice *',
                        'label'       => 'Spicy Rice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Limelight *',
                        'label'       => 'Limelight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Give You Glory *',
                        'label'       => 'Give You Glory *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fredericka the Great *',
                        'label'       => 'Fredericka the Great *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oleo Script *',
                        'label'       => 'Oleo Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cardo *',
                        'label'       => 'Cardo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ledger *',
                        'label'       => 'Ledger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italiana *',
                        'label'       => 'Italiana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One *',
                        'label'       => 'Bowlby One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homenaje *',
                        'label'       => 'Homenaje *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Karla *',
                        'label'       => 'Karla *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Italianno *',
                        'label'       => 'Italianno *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Condensed *',
                        'label'       => 'Cabin Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dosis *',
                        'label'       => 'Dosis *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Krona One *',
                        'label'       => 'Krona One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Advent Pro *',
                        'label'       => 'Advent Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Podkova *',
                        'label'       => 'Podkova *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chicle *',
                        'label'       => 'Chicle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Didone *',
                        'label'       => 'Antic Didone *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Exo *',
                        'label'       => 'Exo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Syncopate *',
                        'label'       => 'Syncopate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sancreek *',
                        'label'       => 'Sancreek *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marko One *',
                        'label'       => 'Marko One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metamorphous *',
                        'label'       => 'Metamorphous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Outline *',
                        'label'       => 'Londrina Outline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patua One *',
                        'label'       => 'Patua One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Bedfort *',
                        'label'       => 'Mr Bedfort *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nunito *',
                        'label'       => 'Nunito *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inika *',
                        'label'       => 'Inika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta *',
                        'label'       => 'Allerta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosarivo *',
                        'label'       => 'Rosarivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Viga *',
                        'label'       => 'Viga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shojumaru *',
                        'label'       => 'Shojumaru *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Francois One *',
                        'label'       => 'Francois One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'The Girl Next Door *',
                        'label'       => 'The Girl Next Door *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passion One *',
                        'label'       => 'Passion One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Days One *',
                        'label'       => 'Days One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Sans *',
                        'label'       => 'Josefin Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Acme *',
                        'label'       => 'Acme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asul *',
                        'label'       => 'Asul *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Happy Monkey *',
                        'label'       => 'Happy Monkey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Sans Libre *',
                        'label'       => 'Averia Sans Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rancho *',
                        'label'       => 'Rancho *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anonymous Pro *',
                        'label'       => 'Anonymous Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glegoo *',
                        'label'       => 'Glegoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Galdeano *',
                        'label'       => 'Galdeano *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mrs Saint Delafield *',
                        'label'       => 'Mrs Saint Delafield *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kranky *',
                        'label'       => 'Kranky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Share *',
                        'label'       => 'Share *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Baumans *',
                        'label'       => 'Baumans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quicksand *',
                        'label'       => 'Quicksand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Habibi *',
                        'label'       => 'Habibi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Seaweed Script *',
                        'label'       => 'Seaweed Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yesteryear *',
                        'label'       => 'Yesteryear *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Economica *',
                        'label'       => 'Economica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Niconne *',
                        'label'       => 'Niconne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sofia *',
                        'label'       => 'Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bonbon *',
                        'label'       => 'Bonbon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Herr Von Muellerhoff *',
                        'label'       => 'Herr Von Muellerhoff *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Duru Sans *',
                        'label'       => 'Duru Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Creepster *',
                        'label'       => 'Creepster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Great Vibes *',
                        'label'       => 'Great Vibes *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Junge *',
                        'label'       => 'Junge *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Condensed *',
                        'label'       => 'Stint Ultra Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya SC *',
                        'label'       => 'Alegreya SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Russo One *',
                        'label'       => 'Russo One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amatic SC *',
                        'label'       => 'Amatic SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Condiment *',
                        'label'       => 'Condiment *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Medula One *',
                        'label'       => 'Medula One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Judson *',
                        'label'       => 'Judson *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Orbitron *',
                        'label'       => 'Orbitron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster Two *',
                        'label'       => 'Lobster Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pinyon Script *',
                        'label'       => 'Pinyon Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unkempt *',
                        'label'       => 'Unkempt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nixie One *',
                        'label'       => 'Nixie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trocchi *',
                        'label'       => 'Trocchi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Comfortaa *',
                        'label'       => 'Comfortaa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Erica One *',
                        'label'       => 'Erica One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Mono *',
                        'label'       => 'Ubuntu Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montaga *',
                        'label'       => 'Montaga *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lilita One *',
                        'label'       => 'Lilita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stint Ultra Expanded *',
                        'label'       => 'Stint Ultra Expanded *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Love Ya Like A Sister *',
                        'label'       => 'Love Ya Like A Sister *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oxygen *',
                        'label'       => 'Oxygen *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Frijole *',
                        'label'       => 'Frijole *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arvo *',
                        'label'       => 'Arvo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prata *',
                        'label'       => 'Prata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Magra *',
                        'label'       => 'Magra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oswald *',
                        'label'       => 'Oswald *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flamenco *',
                        'label'       => 'Flamenco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dorsa *',
                        'label'       => 'Dorsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bad Script *',
                        'label'       => 'Bad Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika *',
                        'label'       => 'Signika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Serif *',
                        'label'       => 'Droid Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wellfleet *',
                        'label'       => 'Wellfleet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata SC *',
                        'label'       => 'Diplomata SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Average *',
                        'label'       => 'Average *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Righteous *',
                        'label'       => 'Righteous *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento Sans *',
                        'label'       => 'Quattrocento Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rouge Script *',
                        'label'       => 'Rouge Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruluko *',
                        'label'       => 'Ruluko *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aclonica *',
                        'label'       => 'Aclonica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruda *',
                        'label'       => 'Ruda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kotta One *',
                        'label'       => 'Kotta One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Questrial *',
                        'label'       => 'Questrial *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jolly Lodger *',
                        'label'       => 'Jolly Lodger *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cookie *',
                        'label'       => 'Cookie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Changa One *',
                        'label'       => 'Changa One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alfa Slab One *',
                        'label'       => 'Alfa Slab One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Josefin Slab *',
                        'label'       => 'Josefin Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cantarell *',
                        'label'       => 'Cantarell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fjord One *',
                        'label'       => 'Fjord One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merriweather *',
                        'label'       => 'Merriweather *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Philosopher *',
                        'label'       => 'Philosopher *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela Round *',
                        'label'       => 'Varela Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amethysta *',
                        'label'       => 'Amethysta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dawning of a New Day *',
                        'label'       => 'Dawning of a New Day *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coming Soon *',
                        'label'       => 'Coming Soon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shadows Into Light Two *',
                        'label'       => 'Shadows Into Light Two *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mako *',
                        'label'       => 'Mako *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Black Ops One *',
                        'label'       => 'Black Ops One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rationale *',
                        'label'       => 'Rationale *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lato *',
                        'label'       => 'Lato *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trochut *',
                        'label'       => 'Trochut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gloria Hallelujah *',
                        'label'       => 'Gloria Hallelujah *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Parisienne *',
                        'label'       => 'Parisienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Short Stack *',
                        'label'       => 'Short Stack *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goudy Bookletter 1911 *',
                        'label'       => 'Goudy Bookletter 1911 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Didact Gothic *',
                        'label'       => 'Didact Gothic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bilbo Swash Caps *',
                        'label'       => 'Bilbo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vollkorn *',
                        'label'       => 'Vollkorn *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu *',
                        'label'       => 'Ubuntu *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Port Lligat Slab *',
                        'label'       => 'Port Lligat Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Princess Sofia *',
                        'label'       => 'Princess Sofia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mountains of Christmas *',
                        'label'       => 'Mountains of Christmas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Qwigley *',
                        'label'       => 'Qwigley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Candal *',
                        'label'       => 'Candal *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Old Standard TT *',
                        'label'       => 'Old Standard TT *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Istok Web *',
                        'label'       => 'Istok Web *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Puritan *',
                        'label'       => 'Puritan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montez *',
                        'label'       => 'Montez *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lekton *',
                        'label'       => 'Lekton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cambo *',
                        'label'       => 'Cambo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Varela *',
                        'label'       => 'Varela *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Glass Antiqua *',
                        'label'       => 'Glass Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bigshot One *',
                        'label'       => 'Bigshot One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arimo *',
                        'label'       => 'Arimo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rosario *',
                        'label'       => 'Rosario *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rokkitt *',
                        'label'       => 'Rokkitt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Architects Daughter *',
                        'label'       => 'Architects Daughter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans *',
                        'label'       => 'Droid Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cuprum *',
                        'label'       => 'Cuprum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Slim *',
                        'label'       => 'Nova Slim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gentium Book Basic *',
                        'label'       => 'Gentium Book Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aladin *',
                        'label'       => 'Aladin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Corben *',
                        'label'       => 'Corben *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Six Caps *',
                        'label'       => 'Six Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Voces *',
                        'label'       => 'Voces *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Original Surfer *',
                        'label'       => 'Original Surfer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kelly Slab *',
                        'label'       => 'Kelly Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda Caption *',
                        'label'       => 'Coda Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cedarville Cursive *',
                        'label'       => 'Cedarville Cursive *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playfair Display *',
                        'label'       => 'Playfair Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Actor *',
                        'label'       => 'Actor *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pacifico *',
                        'label'       => 'Pacifico *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Caption *',
                        'label'       => 'PT Sans Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Covered By Your Grace *',
                        'label'       => 'Covered By Your Grace *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Convergence *',
                        'label'       => 'Convergence *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Handlee *',
                        'label'       => 'Handlee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Raleway *',
                        'label'       => 'Raleway *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wallpoet *',
                        'label'       => 'Wallpoet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Script *',
                        'label'       => 'Nova Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceland *',
                        'label'       => 'Iceland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Meddon *',
                        'label'       => 'Meddon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Shadow *',
                        'label'       => 'Londrina Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nobile *',
                        'label'       => 'Nobile *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carter One *',
                        'label'       => 'Carter One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Walter Turncoat *',
                        'label'       => 'Walter Turncoat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Amaranth *',
                        'label'       => 'Amaranth *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Indie Flower *',
                        'label'       => 'Indie Flower *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Telex *',
                        'label'       => 'Telex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ropa Sans *',
                        'label'       => 'Ropa Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abel *',
                        'label'       => 'Abel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Overlock SC *',
                        'label'       => 'Overlock SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sigmar One *',
                        'label'       => 'Sigmar One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate *',
                        'label'       => 'Fascinate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Hammersmith One *',
                        'label'       => 'Hammersmith One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vast Shadow *',
                        'label'       => 'Vast Shadow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruge Boogie *',
                        'label'       => 'Ruge Boogie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Balthazar *',
                        'label'       => 'Balthazar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Simonetta *',
                        'label'       => 'Simonetta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Schoolbell *',
                        'label'       => 'Schoolbell *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Pompiere *',
                        'label'       => 'Pompiere *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kenia *',
                        'label'       => 'Kenia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic Slab *',
                        'label'       => 'Antic Slab *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caesar Dressing *',
                        'label'       => 'Caesar Dressing *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Gruesa Libre *',
                        'label'       => 'Averia Gruesa Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bevan *',
                        'label'       => 'Bevan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Forum *',
                        'label'       => 'Forum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans Narrow *',
                        'label'       => 'PT Sans Narrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturMaguntia *',
                        'label'       => 'UnifrakturMaguntia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vidaloka *',
                        'label'       => 'Vidaloka *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sansita One *',
                        'label'       => 'Sansita One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kristi *',
                        'label'       => 'Kristi *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Imprima *',
                        'label'       => 'Imprima *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sail *',
                        'label'       => 'Sail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tulpen One *',
                        'label'       => 'Tulpen One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dynalight *',
                        'label'       => 'Dynalight *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chewy *',
                        'label'       => 'Chewy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Norican *',
                        'label'       => 'Norican *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Redressed *',
                        'label'       => 'Redressed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bowlby One SC *',
                        'label'       => 'Bowlby One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Holtwood One SC *',
                        'label'       => 'Holtwood One SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr Dafoe *',
                        'label'       => 'Mr Dafoe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bentham *',
                        'label'       => 'Bentham *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Quattrocento *',
                        'label'       => 'Quattrocento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer SC *',
                        'label'       => 'IM Fell Great Primer SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tinos *',
                        'label'       => 'Tinos *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kreon *',
                        'label'       => 'Kreon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buda *',
                        'label'       => 'Buda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lora *',
                        'label'       => 'Lora *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Sans *',
                        'label'       => 'PT Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Open Sans Condensed *',
                        'label'       => 'Open Sans Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Esteban *',
                        'label'       => 'Esteban *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Armata *',
                        'label'       => 'Armata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Contrail One *',
                        'label'       => 'Contrail One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'La Belle Aurore *',
                        'label'       => 'La Belle Aurore *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Astloch *',
                        'label'       => 'Astloch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Satisfy *',
                        'label'       => 'Satisfy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Oldenburg *',
                        'label'       => 'Oldenburg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allura *',
                        'label'       => 'Allura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Homemade Apple *',
                        'label'       => 'Homemade Apple *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Audiowide *',
                        'label'       => 'Audiowide *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo Swash Caps *',
                        'label'       => 'Macondo Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Droid Sans Mono *',
                        'label'       => 'Droid Sans Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Petrona *',
                        'label'       => 'Petrona *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Electrolize *',
                        'label'       => 'Electrolize *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inconsolata *',
                        'label'       => 'Inconsolata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aguafina Script *',
                        'label'       => 'Aguafina Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coda *',
                        'label'       => 'Coda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fontdiner Swanky *',
                        'label'       => 'Fontdiner Swanky *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Slackey *',
                        'label'       => 'Slackey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'News Cycle *',
                        'label'       => 'News Cycle *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Calligraffitti *',
                        'label'       => 'Calligraffitti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Antic *',
                        'label'       => 'Antic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif Caption *',
                        'label'       => 'PT Serif Caption *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Cut *',
                        'label'       => 'Nova Cut *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bree Serif *',
                        'label'       => 'Bree Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike *',
                        'label'       => 'Alike *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Montserrat *',
                        'label'       => 'Montserrat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mystery Quest *',
                        'label'       => 'Mystery Quest *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Another Hand *',
                        'label'       => 'Just Another Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fondamento *',
                        'label'       => 'Fondamento *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Knewave *',
                        'label'       => 'Knewave *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Great Primer *',
                        'label'       => 'IM Fell Great Primer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gruppo *',
                        'label'       => 'Gruppo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Serif *',
                        'label'       => 'PT Serif *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Adamina *',
                        'label'       => 'Adamina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stardos Stencil *',
                        'label'       => 'Stardos Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Radley *',
                        'label'       => 'Radley *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cherry Cream Soda *',
                        'label'       => 'Cherry Cream Soda *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Patrick Hand *',
                        'label'       => 'Patrick Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Anton *',
                        'label'       => 'Anton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jockey One *',
                        'label'       => 'Jockey One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unlock *',
                        'label'       => 'Unlock *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Round *',
                        'label'       => 'Nova Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Over the Rainbow *',
                        'label'       => 'Over the Rainbow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Megrim *',
                        'label'       => 'Megrim *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chelsea Market *',
                        'label'       => 'Chelsea Market *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arbutus *',
                        'label'       => 'Arbutus *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Diplomata *',
                        'label'       => 'Diplomata *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Caudex *',
                        'label'       => 'Caudex *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nosifer *',
                        'label'       => 'Nosifer *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Leckerli One *',
                        'label'       => 'Leckerli One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica SC *',
                        'label'       => 'IM Fell Double Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rochester *',
                        'label'       => 'Rochester *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Passero One *',
                        'label'       => 'Passero One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar *',
                        'label'       => 'Geostar *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prosto One *',
                        'label'       => 'Prosto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poller One *',
                        'label'       => 'Poller One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ubuntu Condensed *',
                        'label'       => 'Ubuntu Condensed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Andika *',
                        'label'       => 'Andika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gravitas One *',
                        'label'       => 'Gravitas One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rock Salt *',
                        'label'       => 'Rock Salt *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruslan Display *',
                        'label'       => 'Ruslan Display *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius *',
                        'label'       => 'Delius *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aubrey *',
                        'label'       => 'Aubrey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin *',
                        'label'       => 'Cabin *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Swash Caps *',
                        'label'       => 'Delius Swash Caps *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Wire One *',
                        'label'       => 'Wire One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'MedievalSharp *',
                        'label'       => 'MedievalSharp *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'PT Mono *',
                        'label'       => 'PT Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Oval *',
                        'label'       => 'Nova Oval *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sue Ellen Francisco *',
                        'label'       => 'Sue Ellen Francisco *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian *',
                        'label'       => 'Miltonian *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell Double Pica *',
                        'label'       => 'IM Fell Double Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yellowtail *',
                        'label'       => 'Yellowtail *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sorts Mill Goudy *',
                        'label'       => 'Sorts Mill Goudy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Permanent Marker *',
                        'label'       => 'Permanent Marker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poly *',
                        'label'       => 'Poly *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dancing Script *',
                        'label'       => 'Dancing Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maiden Orange *',
                        'label'       => 'Maiden Orange *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Coustard *',
                        'label'       => 'Coustard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Supermercado One *',
                        'label'       => 'Supermercado One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Atomic Age *',
                        'label'       => 'Atomic Age *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cousine *',
                        'label'       => 'Cousine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federant *',
                        'label'       => 'Federant *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Irish Grover *',
                        'label'       => 'Irish Grover *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Stoke *',
                        'label'       => 'Stoke *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Snippet *',
                        'label'       => 'Snippet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monsieur La Doulaise *',
                        'label'       => 'Monsieur La Doulaise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crimson Text *',
                        'label'       => 'Crimson Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Della Respira *',
                        'label'       => 'Della Respira *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smokum *',
                        'label'       => 'Smokum *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Smythe *',
                        'label'       => 'Smythe *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alegreya *',
                        'label'       => 'Alegreya *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'UnifrakturCook *',
                        'label'       => 'UnifrakturCook *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neuton *',
                        'label'       => 'Neuton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Zeyada *',
                        'label'       => 'Zeyada *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Goblin One *',
                        'label'       => 'Goblin One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arizonia *',
                        'label'       => 'Arizonia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Special Elite *',
                        'label'       => 'Special Elite *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Euphoria Script *',
                        'label'       => 'Euphoria Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gudea *',
                        'label'       => 'Gudea *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica *',
                        'label'       => 'IM Fell DW Pica *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Linden Hill *',
                        'label'       => 'Linden Hill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate SC *',
                        'label'       => 'Mate SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kameron *',
                        'label'       => 'Kameron *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Unna *',
                        'label'       => 'Unna *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Iceberg *',
                        'label'       => 'Iceberg *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Federo *',
                        'label'       => 'Federo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Carme *',
                        'label'       => 'Carme *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'League Script *',
                        'label'       => 'League Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yanone Kaffeesatz *',
                        'label'       => 'Yanone Kaffeesatz *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allan *',
                        'label'       => 'Allan *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Squada One *',
                        'label'       => 'Squada One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Aldrich *',
                        'label'       => 'Aldrich *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alike Angular *',
                        'label'       => 'Alike Angular *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miltonian Tattoo *',
                        'label'       => 'Miltonian Tattoo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Metrophobic *',
                        'label'       => 'Metrophobic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Michroma *',
                        'label'       => 'Michroma *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Kaushan Script *',
                        'label'       => 'Kaushan Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Spinnaker *',
                        'label'       => 'Spinnaker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ultra *',
                        'label'       => 'Ultra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crete Round *',
                        'label'       => 'Crete Round *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye *',
                        'label'       => 'Ribeye *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Mono *',
                        'label'       => 'Nova Mono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English *',
                        'label'       => 'IM Fell English *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marvel *',
                        'label'       => 'Marvel *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Playball *',
                        'label'       => 'Playball *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butterfly Kids *',
                        'label'       => 'Butterfly Kids *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Numans *',
                        'label'       => 'Numans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Neucha *',
                        'label'       => 'Neucha *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Prociono *',
                        'label'       => 'Prociono *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Uncial Antiqua *',
                        'label'       => 'Uncial Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Abril Fatface *',
                        'label'       => 'Abril Fatface *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monoton *',
                        'label'       => 'Monoton *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nothing You Could Do *',
                        'label'       => 'Nothing You Could Do *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Yeseva One *',
                        'label'       => 'Yeseva One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ruthie *',
                        'label'       => 'Ruthie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lancelot *',
                        'label'       => 'Lancelot *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Artifika *',
                        'label'       => 'Artifika *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Macondo *',
                        'label'       => 'Macondo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Paytone One *',
                        'label'       => 'Paytone One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bitter *',
                        'label'       => 'Bitter *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Revalia *',
                        'label'       => 'Revalia *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Luckiest Guy *',
                        'label'       => 'Luckiest Guy *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra *',
                        'label'       => 'Almendra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geo *',
                        'label'       => 'Geo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asap *',
                        'label'       => 'Asap *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Square *',
                        'label'       => 'Nova Square *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Expletus Sans *',
                        'label'       => 'Expletus Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell French Canon *',
                        'label'       => 'IM Fell French Canon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Volkhov *',
                        'label'       => 'Volkhov *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lobster *',
                        'label'       => 'Lobster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Damion *',
                        'label'       => 'Damion *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Allerta Stencil *',
                        'label'       => 'Allerta Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tienne *',
                        'label'       => 'Tienne *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Libre *',
                        'label'       => 'Averia Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Asset *',
                        'label'       => 'Asset *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jura *',
                        'label'       => 'Jura *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Merienda One *',
                        'label'       => 'Merienda One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Dr Sugiyama *',
                        'label'       => 'Dr Sugiyama *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miss Fajardose *',
                        'label'       => 'Miss Fajardose *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Noticia Text *',
                        'label'       => 'Noticia Text *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Just Me Again Down Here *',
                        'label'       => 'Just Me Again Down Here *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Jim Nightshade *',
                        'label'       => 'Jim Nightshade *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Alice *',
                        'label'       => 'Alice *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Averia Serif Libre *',
                        'label'       => 'Averia Serif Libre *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Reenie Beanie *',
                        'label'       => 'Reenie Beanie *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Molengo *',
                        'label'       => 'Molengo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Enriqueta *',
                        'label'       => 'Enriqueta *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Vibur *',
                        'label'       => 'Vibur *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Miniver *',
                        'label'       => 'Miniver *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Bubblegum Sans *',
                        'label'       => 'Bubblegum Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Basic *',
                        'label'       => 'Basic *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Copse *',
                        'label'       => 'Copse *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lustria *',
                        'label'       => 'Lustria *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fresca *',
                        'label'       => 'Fresca *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chau Philomene One *',
                        'label'       => 'Chau Philomene One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Lemon *',
                        'label'       => 'Lemon *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Cabin Sketch *',
                        'label'       => 'Cabin Sketch *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Signika Negative *',
                        'label'       => 'Signika Negative *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Inder *',
                        'label'       => 'Inder *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sevillana *',
                        'label'       => 'Sevillana *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tenor Sans *',
                        'label'       => 'Tenor Sans *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Maven Pro *',
                        'label'       => 'Maven Pro *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Julee *',
                        'label'       => 'Julee *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mate *',
                        'label'       => 'Mate *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sarina *',
                        'label'       => 'Sarina *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Tangerine *',
                        'label'       => 'Tangerine *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Henny Penny *',
                        'label'       => 'Henny Penny *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Muli *',
                        'label'       => 'Muli *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Nova Flat *',
                        'label'       => 'Nova Flat *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell DW Pica SC *',
                        'label'       => 'IM Fell DW Pica SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fugaz One *',
                        'label'       => 'Fugaz One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trade Winds *',
                        'label'       => 'Trade Winds *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ewert *',
                        'label'       => 'Ewert *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Brawler *',
                        'label'       => 'Brawler *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Annie Use Your Telescope *',
                        'label'       => 'Annie Use Your Telescope *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sirin Stencil *',
                        'label'       => 'Sirin Stencil *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Waiting for the Sunrise *',
                        'label'       => 'Waiting for the Sunrise *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Modern Antiqua *',
                        'label'       => 'Modern Antiqua *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'VT323 *',
                        'label'       => 'VT323 *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Devonshire *',
                        'label'       => 'Devonshire *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Piedra *',
                        'label'       => 'Piedra *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Chivo *',
                        'label'       => 'Chivo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Flavors *',
                        'label'       => 'Flavors *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Buenard *',
                        'label'       => 'Buenard *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Engagement *',
                        'label'       => 'Engagement *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Shanti *',
                        'label'       => 'Shanti *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Monofett *',
                        'label'       => 'Monofett *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Butcherman *',
                        'label'       => 'Butcherman *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ovo *',
                        'label'       => 'Ovo *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Fascinate Inline *',
                        'label'       => 'Fascinate Inline *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marmelad *',
                        'label'       => 'Marmelad *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'IM Fell English SC *',
                        'label'       => 'IM Fell English SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sonsie One *',
                        'label'       => 'Sonsie One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Mr De Haviland *',
                        'label'       => 'Mr De Haviland *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sniglet *',
                        'label'       => 'Sniglet *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Poiret One *',
                        'label'       => 'Poiret One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Trykker *',
                        'label'       => 'Trykker *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Almendra SC *',
                        'label'       => 'Almendra SC *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Germania One *',
                        'label'       => 'Germania One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Plaster *',
                        'label'       => 'Plaster *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Titan One *',
                        'label'       => 'Titan One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Crushed *',
                        'label'       => 'Crushed *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Rammetto One *',
                        'label'       => 'Rammetto One *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Londrina Solid *',
                        'label'       => 'Londrina Solid *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Geostar Fill *',
                        'label'       => 'Geostar Fill *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Delius Unicase *',
                        'label'       => 'Delius Unicase *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Ribeye Marrow *',
                        'label'       => 'Ribeye Marrow *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Marck Script *',
                        'label'       => 'Marck Script *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gochi Hand *',
                        'label'       => 'Gochi Hand *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Salsa *',
                        'label'       => 'Salsa *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Sunshiney *',
                        'label'       => 'Sunshiney *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Arapey *',
                        'label'       => 'Arapey *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Gorditas *',
                        'label'       => 'Gorditas *',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Emilys Candy *',
                        'label'       => 'Emilys Candy *',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_custom_body_font',
                'label'       => 'Custom Body Font',
                'desc'        => 'Enter the font name for the custom font for body to be used across the site. You can utilize the Custom CSS tab (or edit custom.css file) to specify required CSS for font import.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_body_font_size',
                'label'       => 'Body Font Size',
                'desc'        => '',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => '12',
                        'label'       => '12 (default)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '8',
                        'label'       => '8',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '9',
                        'label'       => '9',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '10',
                        'label'       => '10',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '11',
                        'label'       => '11',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '13',
                        'label'       => '13',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '14',
                        'label'       => '14',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '15',
                        'label'       => '15',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '16',
                        'label'       => '16',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '17',
                        'label'       => '17',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '18',
                        'label'       => '18',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_h1_font_size',
                'label'       => 'Heading 1 H1 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the Heading 1 (H1) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_h2_font_size',
                'label'       => 'Heading 2 H2 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the Heading 2 (H2) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_h3_font_size',
                'label'       => 'Heading 3 H3 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the Heading 3 (H3) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_h4_font_size',
                'label'       => 'Heading 4 H4 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the Heading 4 (H4) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_post_title_h1_font_size',
                'label'       => 'Post Title Heading 1 H1 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the single post title Heading 1 (H1) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_page_title_h1_font_size',
                'label'       => 'Page Title Heading 1 H1 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the single page title Heading 1 (H1) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_portfolio_page_h2_font_size',
                'label'       => 'Portfolio Page Heading 2 Font Size',
                'desc'        => 'Specify in Pixel units the font size for the Heading 2 shown for titles of posts listed in the portfolio pages.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_archive_pages_h2_font_size',
                'label'       => 'Archive Pages Heading 2 Font Size',
                'desc'        => 'Specify in Pixel units font size for the post titles shown in archive pages like the category pages, author pages, search results etc.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_post_tagline_h2_font_size',
                'label'       => 'Post Tagline Heading 2 H2 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the Heading 2 (H2) HTML tagline displayed on the top of a single blog post.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_services_area_h3_font_size',
                'label'       => 'Services Area Heading 3 Font Size',
                'desc'        => 'Specify in Pixel units font size for the Heading 3 of titles shown in the services area of home pages mostly.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_widget_title_h3_font_size',
                'label'       => 'Sidebar Widget Title Heading 3 H3 Font Size',
                'desc'        => 'Enter the preferred font size in Pixel units for the sidebar widget title Heading 3 (H3) HTML tag.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_entry_meta_font_size',
                'label'       => 'Blog Post Meta Font Size',
                'desc'        => 'Specify an font size of the meta information (including text and anchors) displayed for the blog posts listings in archive pages, widgets in sidebars and in single post page.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_fonts_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_skin_font_color',
                'label'       => 'Skin Font Color',
                'desc'        => 'Enter the preferred font color for the elements that change color together when skin is chosen. Examples include links, titles for services, dividers, headers etc.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_heading_font_color',
                'label'       => 'Heading Font Color',
                'desc'        => 'Specify an default font color of the headings (H1, H2, H3, H4, H5, H6). Will be overriden by other styles specified for headings like post title, page title etc. if specified.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_body_font_color',
                'label'       => 'Body Font Color',
                'desc'        => 'Select the font color of the content.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_post_title_h1_font_color',
                'label'       => 'Post Title Heading 1 H1 Font Color',
                'desc'        => 'Enter the preferred font color for the single post title Heading 1 (H1) HTML tag.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_page_title_h1_font_color',
                'label'       => 'Page Title Heading 1 H1 Font Color',
                'desc'        => 'Enter the preferred font color for the single page title Heading 1 (H1) HTML tag.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_archive_pages_h2_font_color',
                'label'       => 'Archive Pages Heading 2 Font Color',
                'desc'        => 'Specify font color for the post titles shown in archive pages like the category pages, author pages, search results etc.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_post_tagline_h2_font_color',
                'label'       => 'Post Tagline Heading 2 H2 Font Color',
                'desc'        => 'Enter the preferred font color for the Heading 2 (H2) HTML tagline displayed on the top of a single blog post.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_portfolio_page_h2_font_color',
                'label'       => 'Portfolio Page Heading 2 Font Color',
                'desc'        => 'Specify the font color for the Heading 2 shown for titles of posts listed in the portfolio pages.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_services_area_h3_font_color',
                'label'       => 'Services Area Heading 3 Font Color',
                'desc'        => 'Specify the font color for the Heading 3 of titles shown in the services area of home pages mostly. The default font color for service area titles is derived from skin color option.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_widget_title_h3_font_color',
                'label'       => 'Sidebar Widget Title Heading 3 H3 Font Color',
                'desc'        => 'Enter the preferred font color for the sidebar widget title Heading 3 (H3) HTML tag.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_widget_title_h3_font_color',
                'label'       => 'Bottom Area Widget Title Heading 3 H3 Font Color',
                'desc'        => 'Enter the preferred font color for the widget title Heading 3 (H3) HTML tag for widgets in the bottom area near footer.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_sidebar_text_font_color',
                'label'       => 'Sidebar Text Font Color',
                'desc'        => 'Specify an font color of the text displayed in the right or left sidebar(s) in a post or a page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_sidebar_link_font_color',
                'label'       => 'Sidebar Link Anchor Font Color',
                'desc'        => 'Specify an font color of the link/anchor displayed in the right or left sidebar(s) in a post or a page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_sidebar_link_hover_font_color',
                'label'       => 'Sidebar Link Anchor Hover Font Color',
                'desc'        => 'Specify an font color of the link/anchor displayed in the right or left sidebar(s) in a post or a page, upon on hovering over the link.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_text_font_color',
                'label'       => 'Font Color for Text in Bottom Widget Area',
                'desc'        => 'Specify the font color for the text displayed in the widget area at the bottom of the page (above footer).',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_link_font_color',
                'label'       => 'Font Color for Link in Bottom Widget Area',
                'desc'        => 'Specify the font color for the link anchor displayed in the widget area at the bottom of the page (above footer).',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_link_hover_font_color',
                'label'       => 'Font Color for Link Hover in Bottom Widget Area',
                'desc'        => 'Specify the font color for the link anchor displayed in the widget area at the bottom of the page (above footer), upon hovering over the link.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_footer_text_font_color',
                'label'       => 'Font Color for Text in Footer Area',
                'desc'        => 'Specify the font color for the text displayed in the footer area at the bottom of the page/post.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_footer_link_font_color',
                'label'       => 'Font Color for Link Anchor in Footer Area',
                'desc'        => 'Specify the font color for the anchor text/link displayed in the footer area at the bottom of the page/post.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_footer_link_font_hover_color',
                'label'       => 'Font Color for Hover of Link Anchor in Footer Area',
                'desc'        => 'Specify the font color for the anchor text/link displayed in the footer area at the bottom of the page/post, upon hovering over the link.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_entry_meta_border_color',
                'label'       => 'Blog Post Meta Border Color',
                'desc'        => 'Specify an color for the border of the meta information displayed for the blog posts listings in archive pages and at the top of content in single post page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_entry_meta_font_color',
                'label'       => 'Blog Post Meta Font Color',
                'desc'        => 'Specify an color for the text of the meta information displayed for the blog posts listings in archive pages, widgets in sidebars and in single post page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_entry_meta_font_color',
                'label'       => 'Blog Post Meta Font Color in Bottom Widget Area',
                'desc'        => 'Specify an color for the text of the meta information displayed for the blog posts listings in bottom widget area.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_color_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_boxed_layout_background',
                'label'       => 'Background for Boxed Layout',
                'desc'        => 'Background for the theme when using boxed layout.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_stretch_boxed_background',
                'label'       => 'Stretch Background Image for Boxed Layout',
                'desc'        => 'Scale the background image specified for boxed layout to fill the area outside the boxed content.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Enable',
                        'label'       => 'Enable',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_theme_background',
                'label'       => 'Background for the Theme',
                'desc'        => 'Background for the theme covering the entire entire page/post unless overridden by individual elements.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_header_background',
                'label'       => 'Background for Header Area',
                'desc'        => 'Background for the header area of the theme which displays logo and menu. You may specify a banner or a cool background image for the header.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_header_height',
                'label'       => 'Height of Header',
                'desc'        => 'Specify in Pixels the desired height of the header.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_slider_area_background',
                'label'       => 'Background for Slider Stage Area',
                'desc'        => 'Background for the slider stage area in the home pages.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_slider_area_divider_color',
                'label'       => 'Slider Stage Area Bottom Divider Color',
                'desc'        => 'Specify the color of the divider/border at the bottom of the slider stage area in the home pages.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_before_content_sidebar_background',
                'label'       => 'Background of Before Content Widget Area',
                'desc'        => 'Specify the background for the widget area displayed before content in the home pages.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_content_area_background',
                'label'       => 'Background for Content Area',
                'desc'        => 'Background for the content area of the page/post. This is also the area below the slider stage area in the home page.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_tagline_background',
                'label'       => 'Background for Title Header Area',
                'desc'        => 'Specify a background for the title header area displayed on top of a page or a post, just below the header.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_tagline_height',
                'label'       => 'Height of Top Title Header Area',
                'desc'        => 'Specify the height of the title header area in pixel units displayed on top of each page and post, just below the header.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_tagline_heading_margin_top',
                'label'       => 'Top Margin of Title Heading in Title Header Area',
                'desc'        => 'Specify the top margin in pixel units of the page title displayed in the title header area displayed on top of each page and post, just below the header.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_bottom_area_background',
                'label'       => 'Background for Bottom Widget Area',
                'desc'        => 'Background for the widget area at the bottom of the page (above footer).',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_footer_background',
                'label'       => 'Background for Footer',
                'desc'        => 'Background for the footer which displays footer menu and company or copyright information.',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_remove_comments_background',
                'label'       => 'Remove Comments Background',
                'desc'        => 'Make the comments area transparent without any background color of its own. Useful when you have set a background color or image for the content area in a post/page.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_comment_background',
                'label'       => 'Comment Background Color',
                'desc'        => 'Enter the preferred background color for comment list shown in blog posts and static pages where comments are enabled.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_alternate_comment_background',
                'label'       => 'Alternate Comment Background Color',
                'desc'        => 'Enter the preferred alternate background color for nested comment list shown in blog posts and static pages where comments are enabled.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_backgrounds_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_left_sidebar_background',
                'label'       => 'Left Sidebar Background',
                'desc'        => 'Specify an background for the left sidebar(s)',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_sidebar_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_right_sidebar_background',
                'label'       => 'Right Sidebar Background',
                'desc'        => 'Specify an background for the right sidebar(s)',
                'std'         => '',
                'type'        => 'background',
                'section'     => 'mo_sidebar_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_margin_top',
                'label'       => 'Top Margin of Primary Menu in Header',
                'desc'        => 'Specify the top margin of the primary menu displayed in the header displayed on top of each page and page.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_margin_right',
                'label'       => 'Right Margin of Primary Menu in Header',
                'desc'        => 'Specify the right margin of the primary menu displayed in the header displayed on top of each page/post. Helps to position the menu precisely in the header area.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_font_size',
                'label'       => 'Font Size of Primary Menu Item in Header',
                'desc'        => 'Specify the font size in pixel units for the primary menu items displayed in the header displayed on top of each page and page.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_font_color',
                'label'       => 'Font Color of Primary Menu Item',
                'desc'        => 'Specify the font color of a primary menu item displayed in the header displayed on top of each page and page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_hover_font_color',
                'label'       => 'Font Color on Hovering Primary Menu Item',
                'desc'        => 'Specify the font color on hovering on a primary menu item displayed in the header displayed on top of each page and page.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_primary_menu_hover_background_color',
                'label'       => 'Primary Menu Hover Background Color',
                'desc'        => 'Specify the color of the background when the user hovers over the top menu items.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dropdown_menu_background_color',
                'label'       => 'Primary Menu Dropdown Background Color',
                'desc'        => 'Enter the preferred background color for dropdown menu. The arrow pointer of the dropdown menu is automatically disabled when this option is chosen.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dropdown_menu_font_size',
                'label'       => 'Primary Menu Dropdown Font Size',
                'desc'        => 'Enter the preferred font size for dropdown menu.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dropdown_menu_font_color',
                'label'       => 'Primary Menu Dropdown Font Color',
                'desc'        => 'Enter the preferred font color for dropdown menu.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dropdown_menu_hover_background_color',
                'label'       => 'Primary Menu Dropdown Menu Item Hover Background',
                'desc'        => 'Enter the preferred color for the dropdown menu item upon mouse hover.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dropdown_menu_hover_font_color',
                'label'       => 'Primary Dropdown Menu Item Hover Font Color',
                'desc'        => 'Enter the preferred font color for dropdown menu item when the user hovers over the item.',
                'std'         => '',
                'type'        => 'colorpicker',
                'section'     => 'mo_primary_menu_tab',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_remove_homepage_tagline',
                'label'       => 'Remove tagline from Home Page',
                'desc'        => 'Remove tagline displayed on top of the home page (the default WordPress blog view shown when no static page is specified as home page).',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_header',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_blog_tagline',
                'label'       => 'Tagline for Blog Posts',
                'desc'        => 'Specify the tag line to be used for blog post. Tag line is displayed on the top of the post below the header.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_header',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_homepage_tagline',
                'label'       => 'Title for Home Page',
                'desc'        => 'Specify the title to display for the default home page that display blog posts in WordPress. Useful when you have not specified a static page as home page.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_header',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_hide_breadcrumbs',
                'label'       => 'Hide Breadcrumbs',
                'desc'        => 'Hide Breadcrumbs in Pages, Posts and Archives',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_header',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_footer_columns',
                'label'       => 'Footer Columns',
                'desc'        => 'Specify the number of columns required for display of widgets in the footer area and/or how you want to them to be laid out.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'mo_footer',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => '3',
                        'label'       => '3',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '4',
                        'label'       => '4',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '5',
                        'label'       => '5',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '6',
                        'label'       => '6',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '2',
                        'label'       => '2',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1 + 2(3c)',
                        'label'       => '1 + 2(3c)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '2(3c) + 1',
                        'label'       => '2(3c) + 1',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1 + 2(4c)',
                        'label'       => '1 + 2(4c)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '2(4c) + 1',
                        'label'       => '2(4c) + 1',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1 + 1(2c)',
                        'label'       => '1 + 1(2c)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1 + 1(3c)',
                        'label'       => '1 + 1(3c)',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1(2c) + 1',
                        'label'       => '1(2c) + 1',
                        'src'         => ''
                    ),
                    array(
                        'value'       => '1(3c) + 1',
                        'label'       => '1(3c) + 1',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_footer_insert',
                'label'       => 'Footer Insert',
                'desc'        => 'Enter the text to be inserted into the footer of a page/post. This can include copyright information or attributions.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_footer',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_google_analytics_code',
                'label'       => 'Google Analytics Code',
                'desc'        => 'Signup for <a href="http://www.google.com/analytics/">Google Analytics</a> and paste your analytics code below to enable tracking on each post.The code will be placed in the footer of each page/post.',
                'std'         => '',
                'type'        => 'textarea-simple',
                'section'     => 'mo_footer',
                'rows'        => '10',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_theme_layout',
                'label'       => 'Theme Layout',
                'desc'        => 'Specify if you want to switch to boxed layout instead of the default stretched layout',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Stretched',
                        'label'       => 'Stretched',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Boxed',
                        'label'       => 'Boxed',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_add_boxed_margins',
                'label'       => 'Add Margins on Boxed Layout',
                'desc'        => 'Add top and bottom margins on the boxed layout.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_blog_styling',
                'label'       => 'Blog Styling',
                'desc'        => 'Choose the styling for page using blog template. Also sets the styling for default blog based home page created by WordPress.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'List',
                        'label'       => 'List',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Thumbnail',
                        'label'       => 'Thumbnail',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Grid',
                        'label'       => 'Grid',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_archive_styling',
                'label'       => 'Archive Pages Styling',
                'desc'        => 'Enter the styling of the archive pages. This styling will be reflected in archive and taxonomy pages like category, tag or author pages.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'List',
                        'label'       => 'List',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Thumbnail',
                        'label'       => 'Thumbnail',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Grid',
                        'label'       => 'Grid',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_blog_layout',
                'label'       => 'Blog Layout',
                'desc'        => 'Choose the layout for page using Blog template. Also sets the layout for default blog based home page created by WordPress.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Default',
                        'label'       => 'Default',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Right Sidebar',
                        'label'       => 'Two Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Left Sidebar',
                        'label'       => 'Two Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Full Width',
                        'label'       => 'Full Width',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Left Sidebar',
                        'label'       => 'Three Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Right Sidebar',
                        'label'       => 'Three Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Sidebar Both Sides',
                        'label'       => 'Three Columns - Sidebar Both Sides',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_archive_layout',
                'label'       => 'Archive Layout',
                'desc'        => 'Choose the layout for archive pages.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Default',
                        'label'       => 'Default',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Right Sidebar',
                        'label'       => 'Two Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Left Sidebar',
                        'label'       => 'Two Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Full Width',
                        'label'       => 'Full Width',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Left Sidebar',
                        'label'       => 'Three Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Right Sidebar',
                        'label'       => 'Three Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Sidebar Both Sides',
                        'label'       => 'Three Columns - Sidebar Both Sides',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_page_layout',
                'label'       => 'Default Page Layout',
                'desc'        => 'Default Layout for Pages',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Default',
                        'label'       => 'Default',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Right Sidebar',
                        'label'       => 'Two Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Left Sidebar',
                        'label'       => 'Two Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Full Width',
                        'label'       => 'Full Width',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Left Sidebar',
                        'label'       => 'Three Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Right Sidebar',
                        'label'       => 'Three Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Sidebar Both Sides',
                        'label'       => 'Three Columns - Sidebar Both Sides',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_post_layout',
                'label'       => 'Default Post Layout',
                'desc'        => 'Specify the default layout for the posts.',
                'std'         => '',
                'type'        => 'select',
                'section'     => 'layout_management',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Default',
                        'label'       => 'Default',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Right Sidebar',
                        'label'       => 'Two Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Two Columns - Left Sidebar',
                        'label'       => 'Two Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Full Width',
                        'label'       => 'Full Width',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Left Sidebar',
                        'label'       => 'Three Columns - Left Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Right Sidebar',
                        'label'       => 'Three Columns - Right Sidebar',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'Three Columns - Sidebar Both Sides',
                        'label'       => 'Three Columns - Sidebar Both Sides',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_ajax_portfolio',
                'label'       => 'Enable Ajax on Portfolio',
                'desc'        => 'Enable Ajax on Portfolio',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_portfolio_page',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_hide_text_in_portfolio',
                'label'       => 'Hide Text in Portfolio Page',
                'desc'        => 'Show excerpt or post content in Portfolio Pages.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_portfolio_page',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_show_content_summary_in_portfolio',
                'label'       => 'Show Content Summary in Portfolio Page',
                'desc'        => 'Specify if content summary needs to be shown in portfolio pages instead of manually specified excerpts. This takes effect if hide text in portfolio option is not checked. 

It is not recommended to enable this option unless you have <i>more</i> tags in place in all portfolio entries.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_portfolio_page',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_flex_slider_effect',
                'label'       => 'Flex Slider Effect',
                'desc'        => 'Choose the custom effect to use on the responsive Flex slider.',
                'std'         => '',
                'type'        => 'radio',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'slide',
                        'label'       => 'slide',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'fade',
                        'label'       => 'fade',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_flex_slider_height',
                'label'       => 'Flex Slider Height',
                'desc'        => 'Enter the height in pixel units the desired height of the responsive Flex Slider. Leave blank to default to height of 420 pixels.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_flex_slider_animation_speed',
                'label'       => 'Flex Slider Animation Speed',
                'desc'        => 'Specify the duration of slide animations in milliseconds. Defaults to 500 milliseconds.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_flex_slider_pause_time',
                'label'       => 'Flex Slider Pause Duration',
                'desc'        => 'Specify the duration of the waiting period between slide animations in milliseconds. Defaults to 3000 milliseconds.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_flex_slider_disable_pause_on_hover',
                'label'       => 'Disable Pause on Hover',
                'desc'        => 'If checked, the animation will disable pause on mouse hover function for the sliders.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Enable',
                        'label'       => 'Enable',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_flex_slider_display_random_slide',
                'label'       => 'Display Random Slide',
                'desc'        => 'Specify that the Flex Slider slideshow start on a randomly picked slide instead of displaying slides in sequence.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Enable',
                        'label'       => 'Enable',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_disable_flex_slider_caption',
                'label'       => 'Disable Flex Slider Caption',
                'desc'        => 'Disable caption for Flex Slider displayed in the top slider area of home page. This helps to have slider images with embedded captions.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_flex_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_slider_post_count',
                'label'       => 'Nivo Slider Post Count',
                'desc'        => 'Enter the number of posts to display in the Nivo slider',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_effect',
                'label'       => 'Nivo Effect',
                'desc'        => 'Specify which custom effect to use on the Nivo slider.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'random',
                        'label'       => 'random',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceDown',
                        'label'       => 'sliceDown',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceDownLeft',
                        'label'       => 'sliceDownLeft',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceUp',
                        'label'       => 'sliceUp',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceUpLeft',
                        'label'       => 'sliceUpLeft',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceUpDown',
                        'label'       => 'sliceUpDown',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceUpDownLeft',
                        'label'       => 'sliceUpDownLeft',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'fold',
                        'label'       => 'fold',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'fade',
                        'label'       => 'fade',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'sliceUpDownLeft',
                        'label'       => 'sliceUpDownLeft',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'slideInRight',
                        'label'       => 'slideInRight',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'slideInLeft',
                        'label'       => 'slideInLeft',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'boxRandom',
                        'label'       => 'boxRandom',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'boxRain',
                        'label'       => 'boxRain',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'boxRainReverse',
                        'label'       => 'boxRainReverse',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'boxRainGrow',
                        'label'       => 'boxRainGrow',
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'boxRainGrowReverse',
                        'label'       => 'boxRainGrowReverse',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_slider_height',
                'label'       => 'Nivo Slider Height',
                'desc'        => 'Enter the height in pixel units the desired height of the Nivo Slider. Leave blank to default to height of 420 pixels.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_slices',
                'label'       => 'Number of Slices',
                'desc'        => 'Specify the number of image slices for slice animations.Defaults to 15.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_animation_speed',
                'label'       => 'Animation Speed',
                'desc'        => 'Specify the duration of slide animations in milliseconds. Defaults to 500 milliseconds.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_pause_time',
                'label'       => 'Nivo Pause Duration',
                'desc'        => 'Specify the duration of the waiting period between slide animations in milliseconds. Defaults to 3000 milliseconds.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_hide_dir_navigation',
                'label'       => 'Hide Next and Prev Buttons',
                'desc'        => 'Specify if you need to hide the Next and Previous buttons on the slide show.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_dir_show_navigation',
                'label'       => 'Always Show Next and Prev Buttons',
                'desc'        => 'Specify if you need to display Next and Previous arrow buttons on the slide show at all times. The default is to display the navigation buttons only upon hovering over the slideshow.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_hide_controls',
                'label'       => 'Hide Navigation Controls',
                'desc'        => 'Hides the navigation controls on the Nivo slider.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_disable_pause_on_hover',
                'label'       => 'Disable Pause on Hover',
                'desc'        => 'If checked, the animation will disable pause on mouse hover function on Nivo slider.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_nivo_caption_opacity',
                'label'       => 'Opacity of Caption',
                'desc'        => 'Specify a value between 0 and 1 as opacity of the caption on the image. Defaults to 0.6.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_nivo_start_random_slider',
                'label'       => 'Start Random Slide',
                'desc'        => 'Specify that the Nivo slideshow start on a randomly picked slide instead of starting from first slide.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_disable_nivo_slider_caption',
                'label'       => 'Disable Nivo Slider Caption',
                'desc'        => 'Disable caption displayed on the Nivo slider. This helps to have slider images with embedded caption.',
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'mo_nivo_slider',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'       => 'Yes',
                        'label'       => 'Yes',
                        'src'         => ''
                    )
                ),
            ),
            array(
                'id'          => 'mo_youtube_height',
                'label'       => 'YouTube Video Height (px)',
                'desc'        => 'Choose the height of the YouTube video frame',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_youtube_width',
                'label'       => 'YouTube Video Width (px)',
                'desc'        => '',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_vimeo_height',
                'label'       => 'Vimeo Video Height (px)',
                'desc'        => '',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_vimeo_width',
                'label'       => 'Vimeo Video Width (px)',
                'desc'        => '',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dailymotion_height',
                'label'       => 'Dailymotion Video Height (px)',
                'desc'        => '',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_dailymotion_width',
                'label'       => 'Dailymotion Video Width (px)',
                'desc'        => '',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_flash_height',
                'label'       => 'Flash Video Height (px)',
                'desc'        => 'Specify the height of the Flash Videos',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_flash_width',
                'label'       => 'Flash Video Width (px)',
                'desc'        => 'Specify the width for Flash videos.',
                'std'         => '',
                'type'        => 'text',
                'section'     => 'video_options',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_custom_css',
                'label'       => 'Custom CSS',
                'desc'        => 'Specify any custom CSS code you will like to see applied here. Will override everything else, including the styles defined by skins.<br />

Handy when you can\'t find the option you want or just in a mood to write CSS code instead of choosing an option.',
                'std'         => '',
                'type'        => 'textarea-simple',
                'section'     => 'mo_custom_css_tab',
                'rows'        => '50',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            ),
            array(
                'id'          => 'mo_adsense_code',
                'label'       => 'Adsense Code',
                'desc'        => 'Enter the Adsense code for use with the Adsense shortcode.',
                'std'         => '',
                'type'        => 'textarea-simple',
                'section'     => 'mo_miscellaneous_options',
                'rows'        => '12',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => ''
            )
        )
    );

    /* allow settings to be filtered before saving */
    $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );

    /* settings are not the same update the DB */
    if ( $saved_settings !== $custom_settings ) {
        update_option( 'option_tree_settings', $custom_settings );
    }

}