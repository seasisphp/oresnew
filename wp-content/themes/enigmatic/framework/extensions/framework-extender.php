<?php

/**
 * Framework Extender - Extends theme functions, handles customizations
 *
 *
 * @package Livemesh_Framework
 */
class MO_Framework_Extender {

    private static $instance;

    /**
     * Construct method for the MO_Framework_Extender class.
     */
    private function __construct() {

    }

    /**
     * Constructor method for the MO_Framework_Extender class.
     *
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    /**
     * Prevent cloning of this singleton
     */
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    /**
     * Init method for the MO_Framework_Extender class.
     */
    function initialize() {

        /* Get action/filter hook prefix. */
        $prefix = mo_get_prefix();

        /* Add the breadcrumb trail just after the container is open. */
        $show_breadcrumbs = mo_get_theme_option('mo_hide_breadcrumbs') ? false : true;
        if ($show_breadcrumbs) {
            add_action("{$prefix}_start_content", array(&$this, 'display_breadcrumbs'), 15); // Display after the start content
        }

        /* Embed width/height defaults. Sets dynamically the width and height for videos based on current column width */
        add_filter('embed_defaults', array(&$this, 'set_defaults_for_embeds'));

        /* Filter the comment form defaults. */
        add_filter('comment_form_defaults', array(&$this, 'set_comment_form_args'), 11);

        add_action('wp_footer', array(&$this, 'enable_google_analytics'));

        add_filter('the_content_more_link', array(&$this, 'remove_more_link'));


        add_filter('body_class', array(&$this, 'mo_browser_body_class'));

        $this->handle_social_fields_for_users();
    }

    function mo_browser_body_class($classes) {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
        if ($is_lynx)
            $classes[] = 'lynx';
        elseif ($is_gecko)
            $classes[] = 'gecko'; elseif ($is_opera)
            $classes[] = 'opera'; elseif ($is_NS4)
            $classes[] = 'ns4'; elseif ($is_safari)
            $classes[] = 'safari'; elseif ($is_chrome)
            $classes[] = 'chrome'; elseif ($is_IE) {
            $classes[] = 'ie';
            if (preg_match('/MSIE ( [0-9]+ )( [a-zA-Z0-9.]+ )/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
                $classes[] = 'ie' . $browser_version[1];
        } else $classes[] = 'unknown';
        if ($is_iphone)
            $classes[] = 'iphone';
        return $classes;
    }

    function display_breadcrumbs() {
        $disable_breadcrumbs_for_entry = get_post_meta(get_queried_object_id(), 'mo_disable_breadcrumbs_for_entry', true);
        if (empty($disable_breadcrumbs_for_entry)) {
            mo_breadcrumb();
        }
    }

    function remove_more_jump_link($link) {
        $offset = strpos($link, '#more-');
        if ($offset) {
            $end = strpos($link, '"', $offset);
        }
        if ($end) {
            $link = substr_replace($link, '', $offset, $end - $offset);
        }
        return $link;
    }

    function remove_more_link() {
        $html = "&nbsp;[&middot;&middot;&middot;]";
        return $html;
    }

    /**
     * Extend the user profile page to handle social network information for individual authors
     * Credit - http://wpsplash.com/how-to-create-a-wordpress-authors-page/
     * @param
     * @return
     */
    function handle_social_fields_for_users() {
        add_action('show_user_profile', array(&$this, 'insert_extra_profile_fields'));
        add_action('edit_user_profile', array(&$this, 'insert_extra_profile_fields'));
        add_action('personal_options_update', array(&$this, 'save_extra_profile_fields'));
        add_action('edit_user_profile_update', array(&$this, 'save_extra_profile_fields'));
    }

    function save_extra_profile_fields($userID) {

        if (!current_user_can('edit_user', $userID)) {
            return false;
        }

        update_user_meta($userID, 'twitter', $_POST['twitter']);
        update_user_meta($userID, 'facebook', $_POST['facebook']);
        update_user_meta($userID, 'linkedin', $_POST['linkedin']);
        update_user_meta($userID, 'googleplus', $_POST['googleplus']);
        update_user_meta($userID, 'flickr', $_POST['flickr']);
    }

    function insert_extra_profile_fields($user) {
        ?>
        <h3>Connect Information</h3>

        <table class='form-table'>
            <tr>
                <th><label for='twitter'>Twitter</label></th>
                <td>
                    <input type='text' name='twitter' id='twitter'
                           value='<?php echo esc_attr(get_the_author_meta('twitter', $user->ID)); ?>'
                           class='input-social regular-text'/>
                    <span class='description'>Please enter your Twitter username. http://www.twitter.com/<strong>username</strong></span>
                </td>
            </tr>
            <tr>
                <th><label for='facebook'>Facebook</label></th>
                <td>
                    <input type='text' name='facebook' id='facebook'
                           value='<?php echo esc_attr(get_the_author_meta('facebook', $user->ID)); ?>'
                           class='input-social regular-text'/>
                    <span
                        class='description'>Please enter your Facebook username/alias. http://www.facebook.com/<strong>username</strong></span>
                </td>
            </tr>
            <tr>
                <th><label for='linkedin'>LinkedIn</label></th>
                <td>
                    <input type='text' name='linkedin' id='linkedin'
                           value='<?php echo esc_attr(get_the_author_meta('linkedin', $user->ID)); ?>'
                           class='input-social regular-text'/>
                    <span class='description'>Please enter your LinkedIn username. http://www.linkedin.com/in/<strong>username</strong></span>
                </td>
            </tr>
            <tr>
                <th><label for='googleplus'>Google Plus</label></th>
                <td>
                    <input type='text' name='googleplus' id='googleplus'
                           value='<?php echo esc_attr(get_the_author_meta('googleplus', $user->ID)); ?>'
                           class='input-social regular-text'/>
                    <span class='description'>Please enter your Google Plus username. http://plus.google.com/<strong>username</strong></span>
                </td>
            </tr>
            <tr>
                <th><label for='flickr'>Flickr</label></th>
                <td>
                    <input type='text' name='flickr' id='flickr'
                           value='<?php echo esc_attr(get_the_author_meta('flickr', $user->ID)); ?>'
                           class='input-social regular-text'/>
                    <span class='description'>Please enter your flickr username. http://www.flickr.com/photos/<strong>username</strong>/</span>
                </td>
            </tr>
        </table>

    <?php
    }


    /**
     * Creates custom settings for the WordPress comment form.
     */
    function set_comment_form_args($args) {
        $args['label_submit'] = __('Post Comment', 'mo_theme');
        return $args;
    }

    /**
     * Overwrites the default widths for embeds.This is especially useful for making sure videos properly
     * expand the full width on video pages.This function overwrites what the $content_width variable handles
     * with context-based widths.
     *
     */
    function set_defaults_for_embeds($args) {

        $layout_manager = MO_LayoutManager::getInstance();

        if ($layout_manager->is_three_column_layout())
            $args['width'] = 640;
        elseif ($layout_manager->is_full_width_layout())
            $args['width'] = 1140; else
            $args['width'] = 820;

        return $args;
    }

    /* Enable Google Analytics for every post/page */

    function enable_google_analytics() {

        $analytics_code = mo_get_theme_option('mo_google_analytics_code');

        if (isset($analytics_code))
            echo '<div class="hidden">' . $analytics_code . '</div>';
    }

}