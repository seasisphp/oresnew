<?php
/**
 * Custom Meta Boxes using Option Tree framework
 * @package Livemesh_Framework
 */

/**
 * Initialize the meta boxes.
 */
add_action('admin_init', 'custom_meta_boxes');

function custom_meta_boxes() {

    $my_meta_box = array(
        'id' => 'mo_general_options',
        'title' => 'General',
        'desc' => '',
        'pages' => array('post', 'page', 'portfolio'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'id' => 'mo_description',
                'label' => 'Description',
                'desc' => '',
                'std' => '',
                'type' => 'textarea',
                'desc' => 'Enter the description of the page/post. Shown under the entry title.',
                'rows' => '2'
            ),
            array(
                'id' => 'mo_custom_heading_content',
                'label' => 'Custom Heading Content',
                'desc' => '',
                'std' => '',
                'type' => 'textarea',
                'desc' => 'Enter custom heading content HTML that replaces the regular page/post title area. This can be any of these - image, a slider, a slogan, purchase/request quote button, an invitation to signup or any plain marketing material.<br><br>Shown under the logo area. Be aware of SEO implications and use tags appropriately',
                'rows' => '6'
            ),
            array(
                'id' => 'mo_custom_heading_layout',
                'label' => 'Layout of the Custom Heading Content',
                'desc' => 'Specify if you need the header content to span the entire width of the browser window instead of being boxed to 1140px grid.',
                'std' => '',
                'std' => '',
                'type' => 'select',
                'section' => 'general_default',
                'rows' => '',
                'post_type' => '',
                'taxonomy' => '',
                'class' => '',
                'choices' => array(
                    array(
                        'value' => 'boxed',
                        'label' => 'Boxed (Default)',
                        'src' => ''
                    ),
                    array(
                        'value' => 'wide',
                        'label' => 'Wide',
                        'src' => ''
                    )
                )
            ),
            array(
                'id' => 'mo_custom_heading_background',
                'label' => 'Custom Heading Background',
                'desc' => '',
                'std' => '',
                'type' => 'background',
                'desc' => 'Specify a background for custom heading content that replaces the regular page/post title area.'
            ),
            array(
                'id' => 'mo_disable_breadcrumbs_for_entry',
                'label' => 'Disable Breadcrumbs on this Post/Page',
                'desc' => '',
                'std' => '',
                'type' => 'checkbox',
                'desc' => 'Disable Breadcrumbs on this Post/Page. Breadcrumbs can be a hindrance in many pages that showcase marketing content.',
                'choices' => array(
                    array(
                        'value' => 'Yes',
                        'label' => 'Yes',
                        'src' => ''
                    )
                )
            )
        )
    );

    ot_register_meta_box($my_meta_box);

    /*$page_meta_box = array(
        'id' => 'mo_page_options',
        'title' => 'Page Options',
        'desc' => '',
        'pages' => array('page'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(

            array(
                'id'          => 'mo_featured_portfolio',
                'label'       => 'Select the featured portfolio',
                'desc'        => 'Select the featured portfolio item to be displayed on top of the page.',
                'std'         => '',
                'type'        => 'custom-post-type-select',
                'rows'        => '',
                'post_type'   => 'portfolio',
                'taxonomy'    => '',
                'class'       => ''
            )

        )
    );

    ot_register_meta_box($page_meta_box);*/

    $post_meta_box = array(
        'id' => 'mo_post_detail',
        'title' => 'Post Options',
        'desc' => '',
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(

            array(
                'label' => 'Use Video as Thumbnail',
                'id' => 'mo_use_video_thumbnail',
                'type' => 'checkbox',
                'desc' => 'Specify if video will be used as a thumbnail instead of a featured image.',
                'choices' => array(
                    array(
                        'label' => 'Yes',
                        'value' => 'Yes'
                    )
                ),
                'std' => '',
                'rows' => '',
                'class' => ''
            ),

            array(
                'label' => 'Video URL',
                'id' => 'mo_video_thumbnail_url',
                'type' => 'text',
                'desc' => 'Specify the URL of the video (Youtube or Vimeo only).',
                'std' => '',
                'rows' => '',
                'class' => ''
            ),

            array(
                'label' => 'Use Slider as Thumbnail',
                'id' => 'mo_use_slider_thumbnail',
                'type' => 'checkbox',
                'desc' => 'Specify if slider will be used as a thumbnail instead of a featured image or a video.',
                'choices' => array(
                    array(
                        'label' => 'Yes',
                        'value' => 'Yes'
                    )
                ),
                'std' => '',
                'rows' => '',
                'class' => ''
            ),

            array(
                'label' => 'Images for thumbnail slider',
                'id' => 'post_slider',
                'desc' => 'Specify the images to be used a slider thumbnails for the post',
                'type' => 'list-item',
                'class' => '',
                'settings' => array(
                    array(
                        'id' => 'slider_image',
                        'label' => 'Image',
                        'desc' => '',
                        'std' => '',
                        'type' => 'upload',
                        'class' => '',
                        'choices' => array()
                    )
                )
            )

        )
    );

    ot_register_meta_box($post_meta_box);

}