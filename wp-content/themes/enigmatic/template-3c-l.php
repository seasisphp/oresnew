<?php
/**
 * Template Name: Three Column Right Navigation 
 *
 * A custom page template for displaying content with two sidebars on the right
 *
 * @package Enigmatic
 * @subpackage Template
 */
get_header();
?>

<div id="right_nav_3c_template" class="layout-3c-l">

    <?php get_template_part('page-content'); // Loads the reusable page-content.php template. ?>

    <?php mo_display_sidebars(); ?>

</div> <!-- #right_nav_3c_template -->

<?php get_footer(); ?>